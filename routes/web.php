<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\ListStrategy;
use App\Http\Livewire\Dashboard;
use App\Http\Livewire\Partnership;
use App\Http\Livewire\Withdraw;
use App\Http\Livewire\Admin\CommissionTransfer;
use App\Http\Livewire\Admin\ConfirmWithdraw;
use App\Http\Livewire\Admin\TraderValidation;
use App\Http\Livewire\Admin\UploadCommission;
use App\Http\Livewire\Auth\EditUserProfile;
use App\Http\Livewire\Auth\ChangeUserPassword;
use App\Http\Controllers\PasswordResetLinkController;
use App\Http\Livewire\Admin\ClientIdList;
use App\Http\Livewire\SupportTicket;
use App\Http\Livewire\CreateTicket;
use App\Http\Livewire\DetailTicket;
use App\Http\Livewire\Admin\SupportTicketAdmin;
use App\Http\Livewire\Admin\DetailTicketAdmin;
use App\Http\Livewire\Admin\ClientValidation;
use App\Http\Livewire\Admin\EditUserData;
use App\Http\Livewire\Admin\RankApproval;
use App\Http\Livewire\CommissionHistory;
use Illuminate\Support\Facades\Redirect;
use App\Http\Livewire\Admin\CertificateList;
use App\Http\Livewire\Admin\FaqDetail;
use App\Http\Livewire\Admin\FaqList;
use App\Http\Livewire\Admin\SeminarInvoice;
use App\Http\Livewire\Admin\SeminarList;
use App\Http\Livewire\Admin\UploadTaxSlip;
use App\Http\Livewire\ViewStatusCertificate;
use App\Http\Livewire\WithholdingTaxSlip;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::get('/ref={reference}', function ($reference) {
    return Redirect::route('register', ['ref' => $reference]);
});

Route::get('/cert={code}', ViewStatusCertificate::class);

Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
            ->middleware(['guest:'.config('fortify.guard')])
            ->name('password.email');

Route::get('/doc={filename}', function ($filename) {
    if (Storage::exists('PDF/faq-document/' . $filename)) {
        return response()->download(storage_path('app/PDF/faq-document/' . $filename));
    } else {
        abort(404);
    }
});

// TODO create user access roles
Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {
    Route::get('/dashboard', Dashboard::class)->name('dashboard');
    Route::get('/user/edit-profile', EditUserProfile::class)->name('user.editprofile');
    Route::get('/user/change-password', ChangeUserPassword::class)->name('user.changepassword');
    Route::get('/user/withdraw', Withdraw::class)->name('user.withdraw');
    Route::get('/user/list-strategy', ListStrategy::class)->name('user.liststrategy');
    Route::get('/user/support-ticket', SupportTicket::class)->name('user.support.ticket');
    Route::get('/user/create-tiket', CreateTicket::class)->name('user.create.ticket');
    Route::get('/user/detail-ticket/{id}/{page}', DetailTicket::class)->name('user.detail.ticket');
    Route::get('/user/partnership', Partnership::class)->name('user.partnership');
    Route::get('/user/commission-history', CommissionHistory::class)->name('user.commission.history');
    Route::get('/user/withholding-tax-slip', WithholdingTaxSlip::class)->name('user.withholdingtaxslip');

    Route::group(['middleware' => ['admin']], function () {
        Route::get('/admin/dashboard', Dashboard::class)->name('admin.dashboard');
        Route::get('/admin/trader-validation', TraderValidation::class)->name('admin.tradervalidation');
        Route::get('/admin/confirm-withdraw', ConfirmWithdraw::class)->name('admin.confirmwithdraw');
        Route::get('/admin/support-ticket/{table}', SupportTicketAdmin::class)->name('admin.support.ticket');
        Route::get('/admin/detail-ticket/{id}/{page}/{table}', DetailTicketAdmin::class)->name('admin.detail.ticket');
        Route::get('/admin/upload-commission', UploadCommission::class)->name('admin.uploadcommission');
        Route::get('/admin/commission-transfer', CommissionTransfer::class)->name('admin.commissiontransfer');
        Route::get('/admin/client-validation', ClientValidation::class)->name('admin.clientvalidation');
        Route::get('/admin/rank-approval', RankApproval::class)->name('admin.rankapproval');
        Route::get('/admin/client-id-list', ClientIdList::class)->name('admin.clientidlist');
        Route::get('/admin/edit-user-data', EditUserData::class)->name('admin.edituserdata');
        Route::get('/admin/certificate-data', CertificateList::class)->name('admin.certificatedata');
        Route::get('/admin/seminar-list', SeminarList::class)->name('admin.seminarlist');
        Route::get('/admin/seminar-invoice', SeminarInvoice::class)->name('admin.seminarinvoice');
        Route::get('/admin/upload-tax-slip', UploadTaxSlip::class)->name('admin.uploadtaxslip');
        Route::get('/admin/faq-list', FaqList::class)->name('admin.faqlist');
        Route::get('/admin/faq-detail/{id}/{page}', FaqDetail::class)->name('admin.faqdetail');
    });
});
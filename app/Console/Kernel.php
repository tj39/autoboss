<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Models\GeneralSetting;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\TicketClose::class,
        Commands\InsertRebateData::class,
        Commands\InsertSharingProfitData::class,
        Commands\ExportRebate::class,
        Commands\ExportSprofit::class,
        Commands\ExportRebatePDF::class,
        Commands\ExportSprofitPDF::class,
        Commands\TicketNotActiveClose::class,
        Commands\AutomaticDowngrade::class,
        Commands\SeminarReminder::class,
        Commands\SeminarReminderAdminManagement::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $ticked_closed_date = GeneralSetting::where('code', 'sch_hk_ticket')->first()->value;
        $gensetSchTransRebate = GeneralSetting::where('code', 'sch_trans_rebate')->where('is_active', '1')->first->value;
        $gensetSchTransSProfit = GeneralSetting::where('code', 'sch_trans_sprofit')->where('is_active', '1')->first->value;
        $gensetSchExportRebate = GeneralSetting::where('code', 'sch_export_rebate')->where('is_active', '1')->first->value;
        $gensetSchExportSprofit = GeneralSetting::where('code', 'sch_export_sprofit')->where('is_active', '1')->first->value;
        $gensetSchExportRebatePdf = GeneralSetting::where('code', 'sch_exp_rebate_pdf')->where('is_active', '1')->first->value;
        $gensetSchExportSprofitPdf = GeneralSetting::where('code', 'sch_exp_sprofit_pdf')->where('is_active', '1')->first->value;

        $schedule->command('command:ticket')->monthlyOn($ticked_closed_date)->timezone('Asia/Jakarta');
        $schedule->command('command:ticketcommentclose')->daily();
        $schedule->command('command:reminderseminar')->dailyAt('12:00')->timezone('Asia/Jakarta');
        $schedule->command('command:reminderseminaradmin')->dailyAt('12:00')->timezone('Asia/Jakarta');
        $schedule->command('insert:rebate')->cron("0/$gensetSchTransRebate 0 0 ? * * *");
        $schedule->command('insert:sharingprofit')->cron("0/$gensetSchTransSProfit 0 0 ? * * *");
        $schedule->command('export:rebate')->cron($gensetSchExportRebate)->timezone('Asia/Jakarta');
        $schedule->command('export:sharingprofit')->cron($gensetSchExportSprofit)->timezone('Asia/Jakarta');
        $schedule->command('export:rebatepdf')->cron($gensetSchExportRebatePdf)->timezone('Asia/Jakarta');
        $schedule->command('export:sprofitpdf')->cron($gensetSchExportSprofitPdf)->timezone('Asia/Jakarta');
        $schedule->command('validate:clientid')->everyFiveMinutes()->timezone('Asia/Jakarta');
        $schedule->command('insert:rebate')->cron("0/$gensetSchTransRebate 0 0 ? * * *");
        $schedule->command('command:downgrade')->dailyAt('23:30')->timezone('Asia/Jakarta');

        if (!$this->osProcessIsRunning('queue:work')) {
            $schedule->command('queue:work')->everyMinute();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    protected function osProcessIsRunning($needle)
    {
        // get process status. the "-ww"-option is important to get the full output!
        exec('ps aux -ww', $process_status);

        // search $needle in process status
        $result = array_filter($process_status, function($var) use ($needle) {
            return strpos($var, $needle);
        });

        // if the result is not empty, the needle exists in running processes
        if (!empty($result)) {
            return true;
        }
        return false;
    }
}

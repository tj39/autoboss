<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ActiveTicket;
use App\Models\ActiveComment;
use Illuminate\Support\Carbon;

class TicketNotActiveClose extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ticketcommentclose';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $active_ticket_open = ActiveTicket::where('status_id', 1)->get();

        foreach($active_ticket_open as $tickets){

            $comments = ActiveComment::where('ticket_id', $tickets->id)->orderBy('created_date', 'desc');
                    
            if($comments->count() != 0){
                $dateComments = Carbon::createFromFormat('Y-m-d H:s:i', $comments->first()->created_date);
                $today = Carbon::now()->format('Y-m-d H:s:i');

                $diff_in_days = $dateComments->diffInDays($today);
                                                
                if($diff_in_days > 5){
                    if($comments->first()->user->is_admin == 1){
                        ActiveTicket::where('id', $tickets->id)->update([
                            'created_date' => Date(now()),
                            'status_id' => 3
                        ]);
                    }

                    $active_comment = ActiveComment::where('ticket_id' , $tickets->id)->get();
                    if($active_comment->count() != 0){
                        ActiveComment::where('ticket_id', $tickets->id)->update([
                            'status_id' => 3
                        ]);
                    }
                }
            }
        }

        $active_ticket_solved = ActiveTicket::where('status_id', 2)->get();
        
        foreach($active_ticket_solved as $tickets){
            $comments = \App\Models\ActiveComment::where('ticket_id', $tickets->id)->orderBy('created_date', 'desc');

            $dateTickets = Carbon::createFromFormat('Y-m-d H:s:i', $tickets->created_date);
            $today = Carbon::now()->format('Y-m-d H:s:i');

            $diff_in_days = $dateTickets->diffInDays($today);
                                                    
            if($diff_in_days > 5){
                if ($comments->count() == 0) {
                    ActiveTicket::where('id', $tickets->id)->update([
                        'created_date' => Date(now()),
                        'status_id' => 3
                    ]);
                } else{
                    $isAdmin = false;
                    foreach ($comments->get() as $key => $value) {
                        if($value->user->is_admin == 1){
                            $isAdmin = true;
                        }
                    }
                                
                    if($isAdmin){
                        ActiveTicket::where('id', $tickets->id)->update([
                            'created_date' => Date(now()),
                            'status_id' => 3
                        ]);
                        
                        ActiveComment::where('ticket_id', $tickets->id)->update([
                            'status_id' => 3
                        ]);
                    }
                }
            }
        }          
    }
}

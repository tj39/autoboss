<?php

namespace App\Console\Commands;

use App\Models\RebateDetail;
use App\Models\RebatePayroll;
use App\Models\RebateSummary;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use PDF;

class ExportRebatePDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:rebatepdf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for exporting rebate_summary and rebate_detail into PDF file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // GENERATING PDF FILE
        $startTime = microtime(true);
        echo "1. BEGIN: generating PDF file";

        $dateStr = Carbon::now()->toDateString();
        $todayDate = Date('d M Y');

        $rebateSummaries = RebateSummary::where('is_exported', '3')
            ->whereRaw("(transfer_proof is not null and transfer_proof != '')")
            ->get();

        $summaryIds = [];
        foreach ($rebateSummaries as $value) {
            if ($value->leaderId->is_admin == '1') {
                continue;
            }
            array_push($summaryIds, $value->id);
        }

        $details = RebateDetail::whereIn('summary_id', $summaryIds)
            ->orderBy('reward_date', 'desc')->get();
            
        $data = [
            'commissionType' => 'REBATE',
            'summaries' => $rebateSummaries,
            'details' => $details,
            'todayDate' => $todayDate
        ];
        
        $pdfFile = PDF::loadView('commission-report-pdf', $data)->setPaper('A4', 'landscape');

        Storage::makeDirectory('PDF/rebate-report/' . $dateStr . '/');
        $fileName = 'REBATE_REPORT-' . $dateStr;
        $filePath = 'PDF/rebate-report/' . $dateStr . '/' . $fileName . '.pdf';
        Storage::put($filePath, $pdfFile->output());

        foreach ($rebateSummaries as $rebateSummary) {
            $rebateSummary->is_exported = '1';
            $rebateSummary->save();

            $rebatePayroll = RebatePayroll::find($rebateSummary->payroll_id);
            $rebatePayroll->report_path = $filePath;
            $rebatePayroll->report_name = $fileName;
            $rebatePayroll->is_final = '1';
            $rebatePayroll->save();
        }

        $endTime = microtime(true);
        $execTime = $endTime-$startTime;
        echo "\n1. END: generating PDF file ($execTime)";

        return Command::SUCCESS;
    }
}

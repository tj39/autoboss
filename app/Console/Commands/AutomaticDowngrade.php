<?php

namespace App\Console\Commands;

use App\Traits\GlobalValues;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AutomaticDowngrade extends Command
{
    use GlobalValues;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:downgrade';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scheduler for automatic downgrade user who did not upload certificate during overdue date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

            Log::info("BEGIN: execute auto downgrade user who did not upload certificate silver");
            DB::statement('CALL AUTOMATIC_DOWNGRADE_CERTIFICATE()');
            Log::info("END: execute auto downgrade user who did not upload certificate silver");

            Log::info("BEGIN: automatic register seminar");
            DB::statement('CALL AUTOMATIC_REGISTER_SEMINAR()');
            Log::info("END: automatic register seminar");

        return Command::SUCCESS;
    }
}

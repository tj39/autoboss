<?php

namespace App\Console\Commands;

use App\Models\RebateFile;
use App\Models\RebatePayroll;
use App\Models\RebateSummary;
use App\Models\TransferGateway;
use App\Models\User;
use Carbon\Carbon;
use App\Traits\GlobalValues;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet; 
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExportRebate extends Command
{
    use GlobalValues;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:rebate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for exporting rebate_summary to Excel file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // CALCULATE REBATE NET + PPH
        $startTime = microtime(true);
        echo "1. BEGIN: calculating rebate nett and PPH";
        DB::statement('CALL CALCULATE_REBATE_NET()');
        $endTime = microtime(true);
        $execTime = $endTime-$startTime;
        echo "\n1. END: calculating rebate nett and PPH ($execTime)";

        // UPDATE COMMSSION BALANCE
        $startTime = microtime(true);
        echo "\n2. BEGIN: updating commission balance";
        DB::statement('CALL UPDATE_COMMISSION_BALANCE()');
        $endTime = microtime(true);
        $execTime = $endTime-$startTime;
        echo "\n2. END: updating commission balance ($execTime)";

        // FILTER MIN PAYROLL TRANSFER
        $startTime = microtime(true);
        echo "\n3. BEGIN: min payroll transfer";
        $minTransfer = $this->getGeneralSettingValue('min_payroll');
        $queryUpdate1 = "UPDATE rebate_summary
                        SET is_exported = 4
                        WHERE (commission_net = 0 OR (commission_net-admin_fee) < $minTransfer)
                        AND is_exported = 2;";
        DB::statement($queryUpdate1);

        $queryUpdate2 = "UPDATE rebate_summary
                        SET is_exported = 2
                        WHERE ((commission_net-admin_fee) >= $minTransfer OR NIK = '' OR NIK = NULL)
                        AND is_exported = 4;";
        DB::statement($queryUpdate2);

        $endTime = microtime(true);
        $execTime = $endTime-$startTime;
        echo "\n3. END: min payroll transfer ($execTime)";

        // GENERATING EXCEl FILE
        $dateStr = Carbon::now()->toDateString();
        $startTime = microtime(true);
        echo "\n4. BEGIN: generating Excel file";

        // Column headers
        $columns = array('Trx ID', 'Transfer Type', 'Beneficiary ID', 'Credited Account', 'Receiver Name', 
            'Amount', 'NIP', 'Remark', 'Beneficiary email', 'Swift Code', 'Cust Type', 'Cust Residence', 'No.Identity', 'NPWP', 'Gross', 'PPH', 'Account Name', 'Address');
        $usdtColumns = array('Wallet', 'Nominal');

        $path = 'Excel/rebate-payroll/' . $dateStr . '/';
        $fileName = 'rebate-payroll_' . $dateStr;

        $fileExists = Storage::exists($path . $fileName . '.xlsx');

        if ($fileExists) {
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
            $reader->setReadDataOnly(FALSE);
            $spreadsheet = $reader->load(storage_path('app/' . $path . $fileName . '.xlsx'));
            $sheet = $spreadsheet->getActiveSheet();
            $spreadsheet->setActiveSheetIndex(1);
            $sheet2 = $spreadsheet->getActiveSheet();
            $spreadsheet->setActiveSheetIndex(2);
            $sheet3 = $spreadsheet->getActiveSheet();
            $spreadsheet->setActiveSheetIndex(3);
            $sheet4 = $spreadsheet->getActiveSheet();
        } else {
            Storage::makeDirectory($path);
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            // Set header values
            $colIdx = 1;
            foreach($columns as $column) {
                $sheet->setCellValueByColumnAndRow($colIdx, 1, $column);
                $colIdx++;
            }
            $spreadsheet->getActiveSheet()->setTitle('BCA');

            $colIdx = 1;
            $sheet2 = $spreadsheet->createSheet();
            $spreadsheet->setActiveSheetIndex(1);
            foreach($columns as $column) {
                $sheet2->setCellValueByColumnAndRow($colIdx, 1, $column);
                $colIdx++;
            }
            $spreadsheet->getActiveSheet()->setTitle('LLG');

            $colIdx = 1;
            $sheet3 = $spreadsheet->createSheet();
            $spreadsheet->setActiveSheetIndex(2);
            foreach($columns as $column) {
                $sheet3->setCellValueByColumnAndRow($colIdx, 1, $column);
                $colIdx++;
            }
            $spreadsheet->getActiveSheet()->setTitle('RTG');

            $colIdx = 1;
            $sheet4 = $spreadsheet->createSheet();
            $spreadsheet->setActiveSheetIndex(3);
            foreach($usdtColumns as $column) {
                $sheet4->setCellValueByColumnAndRow($colIdx, 1, $column);
                $colIdx++;
            }
            $spreadsheet->getActiveSheet()->setTitle('USDT');
        }

        $highestRow = $sheet->getHighestRow();
        $highestRow2 = $sheet2->getHighestRow();
        $highestRow3 = $sheet3->getHighestRow();
        $highestRow4 = $sheet4->getHighestRow();

        // Processing rows
        $rowIdx = $highestRow + 1;
        $rowIdx2 = $highestRow2 + 1;
        $rowIdx3 = $highestRow3 + 1;
        $usdtRowIdx = $highestRow4 + 1;
        $rebateSummaries = RebateSummary::where('is_exported', '2')->get();
        foreach ($rebateSummaries as $rebateSummary) {
            if ($rebateSummary->leaderId->is_admin == '1') {
                continue;
            }
            $rebateSummary->is_exported = '3';
            $rebateSummary->save();

            $user = User::find($rebateSummary->leader_id);
            $transferGateway = TransferGateway::find($user->gateways_id);
            if ($transferGateway->method == 'BANK') {
                $gateway = 'BCA';
                if ($transferGateway->code != 'BCA' && $rebateSummary->commission_net <= 500000000) {
                    $gateway = 'LLG';
                } elseif ($transferGateway->code != 'BCA' && $rebateSummary->commission_net > 500000000) {
                    $gateway = 'RTG';
                }

                $rebateSummary->transfer_type = $gateway;
                $rebateSummary->save();

                $swiftCode = '';
                $custType = '';
                $custResidence = '';
                if ($gateway != 'BCA') {
                    $swiftCode = $transferGateway->swift_code;
                    $custType = '1';
                    $custResidence = $user->country == 'Indonesia' ? '1' : '2';
                }

                $row[$columns[0]] = $rebateSummary->code;
                $row[$columns[1]] = $gateway;
                $row[$columns[2]] = '';
                $row[$columns[3]] = "'".$user->no_account;
                $row[$columns[4]] = $user->account_holder;
                $row[$columns[5]] = $rebateSummary->commission_net - $rebateSummary->admin_fee;
                $row[$columns[6]] = '';
                $row[$columns[7]] = '';
                $row[$columns[8]] = '';
                $row[$columns[9]] = $swiftCode;
                $row[$columns[10]] = $custType;
                $row[$columns[11]] = $custResidence;
                $row[$columns[12]] = "'".$user->nik;
                $row[$columns[13]] = "'".$user->no_npwp;
                $row[$columns[14]] = $rebateSummary->commission_gross;
                $row[$columns[15]] = $rebateSummary->ppn;
                $row[$columns[16]] = $user->firstname.' '.$user->lastname;
                $row[$columns[17]] = $user->street_adress.' '.$user->city.' '.$user->state;

                $data = [];
                foreach ($columns as $value) {
                    array_push($data, $row[$value]);
                }

                if($gateway == "BCA"){
                    $colIdx = 1;
                    $spreadsheet->setActiveSheetIndex(0);
                    foreach ($data as $value) {
                        $sheet->setCellValueByColumnAndRow($colIdx, $rowIdx, $value);
                        $colIdx++;
                    }

                    $rowIdx++;
                }
                else if($gateway == "LLG"){
                    $colIdx = 1;
                    $spreadsheet->setActiveSheetIndex(1);
                    foreach ($data as $value) {
                        $sheet2->setCellValueByColumnAndRow($colIdx, $rowIdx2, $value);
                        $colIdx++;
                    }

                    $rowIdx2++;
                }
                else if($gateway == "RTG"){
                    // Set row values
                    $colIdx = 1;
                    $spreadsheet->setActiveSheetIndex(2);
                    foreach ($data as $value) {
                        $sheet3->setCellValueByColumnAndRow($colIdx, $rowIdx3, $value);
                        $colIdx++;
                    }

                    $rowIdx3++;
                }
            } elseif ($transferGateway->method == 'OTHER' && $transferGateway->code == 'USDT') {
                $gateway = 'USDT';
                $rebateSummary->transfer_type = $gateway;
                $rebateSummary->save();

                $row[$usdtColumns[0]] = $user->no_account;
                $row[$usdtColumns[1]] = $rebateSummary->commission_usd;

                $data = [];
                foreach ($usdtColumns as $value) {
                    array_push($data, $row[$value]);
                }

                $colIdx = 1;
                $spreadsheet->setActiveSheetIndex(3);
                foreach ($data as $value) {
                    $sheet4->setCellValueByColumnAndRow($colIdx, $usdtRowIdx, $value);
                    $colIdx++;
                }

                $usdtRowIdx++;
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(storage_path('app/' . $path . $fileName . '.xlsx'));

        $code = $this->generatePayrollCode($dateStr);
            
        // Insert rebate_payroll
        $rebatePayroll = null;
        if ($fileExists) {
            $rebatePayroll = RebatePayroll::where('payroll_name', $fileName)->first();
        } else {
            $rebatePayroll = new RebatePayroll();
            $rebatePayroll->created_at = Carbon::now();
            $rebatePayroll->code = strtoupper($code);
            $rebatePayroll->payroll_name = $fileName;
            $rebatePayroll->payroll_path = $path . $fileName . '.xlsx';
            $rebatePayroll->is_final = '0';
            $rebatePayroll->save();
        }

        // Update payroll_id of rebate_summary
        foreach ($rebateSummaries as $rebateSummary) {
            $rebateSummary->payroll_id = $rebatePayroll->id;
            $rebateSummary->save();
        }

        RebateFile::where('is_settled', '2')->update([
            'payroll_code' => $rebatePayroll->code
        ]);

        $countLots = "SELECT SUM(a.lots) total FROM (
            SELECT DISTINCT rda.`reward_order`, rda.volume_lots lots
                FROM rebate_payroll AS rp
                JOIN rebate_file AS rf
                ON rf.payroll_code = rp.code
                JOIN rebate_data AS rda
                ON rf.id = rda.file_id
                WHERE rp.id = " . $rebatePayroll->id ."
            ) a;";

        $result = DB::select($countLots, []);
        $rebatePayroll->lots_all = $result[0]->total;
        $rebatePayroll->save();

        $endTime = microtime(true);
        $execTime = $endTime-$startTime;
        echo "\n4. END: generating excel file ($execTime)";

        return Command::SUCCESS;
    }

    public function generatePayrollCode($dateStr)
    {
        $payrollCode = substr(str_shuffle('1234567890'), 0, 5) . $dateStr;

        return 'PRR' . substr(md5($payrollCode), 0, 5);
    }
}

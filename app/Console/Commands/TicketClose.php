<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ActiveTicket;
use App\Models\ActiveComment;
use App\Models\ClosedTicket;
use App\Models\ClosedComment;


class TicketClose extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ticket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $active_ticket = ActiveTicket::where('status_id', 3)->get();

        foreach($active_ticket as $tickets){
            $closed_ticket = ClosedTicket::where('no_ticket', $tickets->no_ticket)->count();

            if ($closed_ticket == 0){
                ActiveTicket::where('id', $tickets->id)->update([
                    'completed_date' => Date(now())
                ]);

                ClosedTicket::create([
                    'no_ticket' => $tickets->no_ticket,
                    'created_date' => $tickets->created_date,
                    'completed_date' => Date(now()),
                    'status_id' => $tickets->status_id,
                    'category_id' => $tickets->category_id,
                    'user_id' => $tickets->user_id,
                    'title' => $tickets->title,
                    'message' => $tickets->message,
                    'url_image' => $tickets->url_image
                ]);
    
                $active_comment = ActiveComment::where('ticket_id' , $tickets->id)->get();
                foreach ($active_comment as $comment){
                    ClosedComment::create([
                        'ticket_id' => $comment->ticket_id,
                        'user_id' => $comment->user_id,
                        'created_at' => $comment->created_date,
                        'message' => $comment->message, 
                        'url_image' => $comment->url_image
                    ]);
                }
            }
        }
    }
}

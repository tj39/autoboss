<?php

namespace App\Console\Commands;

use App\Models\SprofitDetail;
use App\Models\SprofitPayroll;
use App\Models\SprofitSummary;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use PDF;

class ExportSprofitPDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:sprofitpdf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // GENERATING PDF FILE
        $startTime = microtime(true);
        echo "1. BEGIN: generating PDF file";

        $dateStr = Carbon::now()->toDateString();
        $todayDate = Date('d M Y');

        $sprofitSummaries = SprofitSummary::where('is_exported', '3')
            ->whereRaw("(transfer_proof is not null and transfer_proof != '')")
            ->get();

        $summaryIds = [];
        foreach ($sprofitSummaries as $value) {
            if ($value->leaderId->is_admin == '1') {
                continue;
            }
            array_push($summaryIds, $value->id);
        }

        $details = SprofitDetail::whereIn('summary_id', $summaryIds)
            ->orderBy('leader_generation', 'asc')->get();
            
        $data = [
            'commissionType' => 'SHARING PROFIT',
            'summaries' => $sprofitSummaries,
            'details' => $details,
            'todayDate' => $todayDate
        ];
        
        $pdfFile = PDF::loadView('commission-report-pdf', $data)->setPaper('A4', 'landscape');

        Storage::makeDirectory('PDF/sprofit-report/' . $dateStr . '/');
        $fileName = 'SHARING_PROFIT_REPORT-' . $dateStr;
        $filePath = 'PDF/sprofit-report/' . $dateStr . '/' . $fileName . '.pdf';
        Storage::put($filePath, $pdfFile->output());

        foreach ($sprofitSummaries as $sprofitSummary) {
            $sprofitSummary->is_exported = '1';
            $sprofitSummary->save();

            $sprofitPayroll = SprofitPayroll::find($sprofitSummary->payroll_id);
            $sprofitPayroll->report_path = $filePath;
            $sprofitPayroll->report_name = $fileName;
            $sprofitPayroll->is_final = '1';
            $sprofitPayroll->save();
        }

        $endTime = microtime(true);
        $execTime = $endTime-$startTime;
        echo "\n1. END: generating PDF file ($execTime)";

        return Command::SUCCESS;
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\SeminarEvent;
use App\Models\User;
use App\Models\GeneralSetting;
use Illuminate\Support\Facades\Mail;
use App\Mail\SeminarReminderMail;
use App\Traits\GlobalValues;

class SeminarReminder extends Command
{
    use GlobalValues;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:reminderseminar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date_genset = GeneralSetting::where('code', 'reminder_seminar')->first();
        $seminars = SeminarEvent::where('status', 'NEW')->get();

        foreach($seminars as $item){
            $seminarSubstract = $item->event_date->subDays($date_genset->value);
            
            if($seminarSubstract->isToday()){
                
                $users = User::where('seminar_status', 'ATTENDING')->where('seminar_id', $item->id)->get();

                foreach($users as $user){
                    if ($user->otp_verified_at == null){
                        $details = [
                            'event_name' => $item->event_name,
                            'event_date' => $item->event_date,
                            'event_time' => $item->event_time,
                            'url' => $item->url,
                            'passcode' => $item->passcode
                        ];
                        
                        Mail::to($user->email)->send(new SeminarReminderMail($details));
                    }
                    else{
                        $this->send_whatsapp($user->id, $item->event_name, $item->event_date,$item->event_time,$item->url, $item->meeting_id, $item->passcode); 
                    }
                }
            }
        }
    }

    public function send_whatsapp($id, $event_name, $event_date, $event_time, $url, $meeting_id, $passcode){
        $user = User::where('id', $id)->first();

        $whatsoApi = $this->getGeneralSettingValue('whatso_api');
        $whatsoUsername = $this->getGeneralSettingValue('whatso_username');
        $whatsoPassword = $this->getGeneralSettingValue('whatso_password');
        $fromNumber = $this->getGeneralSettingValue('tjl_phone_number');

        $data = [
            'Username' => $whatsoUsername, // Whatso username
            'Password' => $whatsoPassword, // Whatso password
            'MessageText' => $this->encodeURIComponent('*Hi TPP 99 Member!*

We invite you to attend our zoom training.  The "*'. $event_name. '*" training will be held on:

    *Date :* '.date_format(date_create($event_date), 'D, d M Y').'
    *Time :* '.$event_time.'
    *Meeting ID :* '.$meeting_id.'
    *Passcode* : '.$passcode.'

Save the date and dont miss it!
Lets success with TP99!

*NOTE:*** Unfortunately, if you cannot attend the training, your rank will be downgraded automatically a few days after the training day.

Regards,
*TP 99 Management*'),
            'MobileNumbers' => $user->phone, // Receiver number
            'ScheduleDate' => '',
            'FromNumber' => $fromNumber,
            'Channel' => '1'
        ];
        $json = json_encode($data);
        $url = $whatsoApi;
        $options = stream_context_create(['http' => [
                'method'  => 'POST',
                'header'  => ['Content-type: application/json', 'Accept: application/json'],
                'content' => $json
            ]
        ]);

        // Send a request
        $result = file_get_contents($url, false, $options);
    }

    function encodeURIComponent($str) {
        $revert = array('%0D%0A'=>'
        ','%20'=>' ','%21'=>'!', '%22'=>'"', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')', '%2C'=>',', '%3A'=>':');
        return strtr(rawurlencode($str), $revert);
    }
}

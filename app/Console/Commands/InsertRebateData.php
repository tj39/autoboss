<?php

namespace App\Console\Commands;

use App\Models\GeneralSetting;
use App\Models\RebateFile;
use App\Models\RebateTemp;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class InsertRebateData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:rebate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for inserting rebate data from uploaded CSV files in rebate_file table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // INSERT - START INSERT DATA FROM CSV
        $sTime = microtime(true);
        // echo "1. BEGIN: Insert rebate_data from rebate_file";
        // $rebateFiles = RebateFile::where('is_inserted', '0')
        //     ->orderBy('created_at', 'asc')->get();

        // foreach ($rebateFiles as $rebateFile) {
        //     RebateFile::where('id', $rebateFile->id)->update([
        //         'is_inserted' => 2
        //     ]);

        //     $filePath = str_replace('\\', '/', base_path()) . '/storage/app' . '/' . $rebateFile->file_path;

        //     $queryTruncate = "TRUNCATE TABLE rebate_temp";
        //     DB::statement($queryTruncate);

        //     $queryLoad = sprintf(
        //         "LOAD DATA LOCAL INFILE '%s' REPLACE
        //         INTO TABLE rebate_temp 
        //         FIELDS TERMINATED BY ',' 
        //         ENCLOSED BY ''
        //         LINES TERMINATED BY '\n'
        //         IGNORE 1 ROWS
        //         (@reward_date,@ignore,@ignore,@ignore,client_id,@ignore,@volume_lots,@ignore,@ignore,@ignore,@ignore,reward_order,investment_id,@ignore,@file_id)
        //         SET reward_date = STR_TO_DATE(@reward_date, '%%Y-%%m-%%d'), volume_lots = CAST(REPLACE(@volume_lots, ',', '.') AS DECIMAL(10,7)), file_id = '%s';",
        //         $filePath, $rebateFile->id, 
        //     );
        //     DB::statement($queryLoad);
            
            // $countData = "SELECT count(1) total
            //     FROM rebate_temp rt
            //     LEFT JOIN rebate_data rd
            //     ON rt.reward_order = rd.reward_order
            //     WHERE rd.reward_order IS NULL;";
            // $result = DB::select($countData, []);

            // if ($result[0]->total!= 0) {
            //     $queryInsert = "INSERT INTO rebate_data (reward_date, client_id, volume_lots, reward_order, investment_id, created_at, updated_at, is_calculated, is_checked, file_id)
            //             SELECT DISTINCT rt.reward_date, rt.client_id, rt.volume_lots, rt.reward_order, rt.investment_id, CURRENT_TIMESTAMP, NULL, 0, 0, rt.file_id
            //             FROM rebate_temp rt
            //             LEFT JOIN rebate_data rd
            //             ON rt.reward_order = rd.reward_order
            //             WHERE rd.reward_order IS NULL;";
            //     DB::statement($queryInsert);
            // } else {
            //     RebateFile::where('id', $rebateFile->id)->update([
            //         'is_inserted' => 1,
            //         'is_calculated' => 1
            //     ]);
            // }
        // }
        
        $authUrl = GeneralSetting::where('code', 'exness_auth_api')->first()->value;
        $exnessEmail = GeneralSetting::where('code', 'exness_affiliate_email')->first()->value;
        $exnessPassword = GeneralSetting::where('code', 'exness_affiliate_password')->first()->value;
        $encryptionCiphering = GeneralSetting::where('code', 'encryption_ciphering')->first()->value;
        $encryptionKey = GeneralSetting::where('code', 'decryption_key')->first()->value;
        $encryptionIv = GeneralSetting::where('code', 'decryption_iv')->first()->value;
        
        Log::info("1. BEGIN: authenticate to Exness Affiliates API to get token");
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $authUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                'Accept: application/json',
                'Content-Type: application/json'
            ),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode(
                array(
                    "login" => openssl_decrypt($exnessEmail, $encryptionCiphering, $encryptionKey, 0, $encryptionIv),
                    "password" => openssl_decrypt($exnessPassword, $encryptionCiphering, $encryptionKey, 0, $encryptionIv)
                ),
                JSON_FORCE_OBJECT
            )
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        Log::info("1. END: authenticate to Exness Affiliates API to get token");

        if (empty($err)) {
            Log::info("2. BEGIN: retrieving data count from Exness Rewards Report API");
            $rewardReportUrl = GeneralSetting::where('code', 'exness_reward_report_api')->first()->value;
            
            $json = json_decode($response);
            $exnessToken = $json->token;

            $authorization = 'Authorization: JWT ' . $exnessToken;
            curl_setopt($curl, CURLOPT_URL, $rewardReportUrl . '?partner_account=9307325&reward_date=' . date('Y-m-d', strtotime("-1 day")));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Accept: application/json',
                $authorization
            ));
            curl_setopt($curl, CURLOPT_POST, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, []);

            $response = curl_exec($curl);
            $err = curl_error($curl);
            Log::info("2. END: retrieving data count from Exness Rewards Report API");

            if (empty($err)) {
                Log::info("3. BEGIN: processing json data");
                $jsonData = json_decode($response);
                $count = $jsonData->totals->count;
                $loops = (int) ceil($count / 1000);
                for ($i = 0; $i < $loops; $i++) {
                    $offset = $i * 1000;
                    Log::info("3a. BEGIN: sending request to Exness Rewards Report API", ['offset' => $offset, 'limit' => 1000]);
                    curl_setopt($curl, CURLOPT_URL, $rewardReportUrl . '?partner_account=9307325&reward_date=' . date('Y-m-d', strtotime("-1 day")) . '&limit=1000&offset=' . $offset);
                    $response = curl_exec($curl);
                    $err = curl_error($curl);

                    if (empty($err)) {
                        $jsonData = json_decode($response);
                        $rebates = $jsonData->data;
                        foreach ($rebates as $rebate) {
                            $sTime = microtime(true);
                            Log::info("3b. BEGIN: inserting into table rebate_temp.", ['client_id' => $rebate->client_uid, 'investment_id' => $rebate->client_account]);
                            RebateTemp::firstOrCreate([
                                    'reward_order' => $rebate->reward_order
                                ], [
                                    'reward_date' => $rebate->reward_date,
                                    'client_id' => $rebate->client_uid,
                                    'volume_lots' => $rebate->volume_lots,
                                    'investment_id' => $rebate->client_account,
                                    'file_id' => 0
                                ]
                            );
                            $endTime = microtime(true);
                            $execTime = ($endTime - $sTime);
                            Log::info("3b. END: inserting into table rebate_temp. (".$execTime." sec)");
                        }
                    } else {
                        Log::error("ERROR: sending request to Exness Rewards Reports API.", ["error" => $err]);
                    }
                    Log::info("3a. END: sending request to Exness Rewards Report API");
                }
                Log::info("3. END: processing json data");

                Log::info("4. BEGIN: inserting into rebate_data table");
                $getSettled = RebateFile::where('is_settled', '2')->first();
                if ($getSettled == null) {
                    $rewardGroup = "SELECT reward_date date FROM rebate_temp
                                    GROUP BY reward_date";
                    $data = DB::select($rewardGroup, []);

                    for ($i = 0; $i < count($data); $i++) {
                        $rebateFile = new RebateFile();
                        Log::info("BEGIN: inserting into rebate_file");
                        $fileName = 'RewardsReport ' . date_format(date_create($data[$i]->date), 'd M y');
                        $rebateFile->file_name = $fileName;
                        $rebateFile->file_path = null;
                        $rebateFile->is_inserted = '2';
                        $rebateFile->is_calculated = '0';
                        $rebateFile->is_settled = '0';
                        $rebateFile->save();
                        Log::info("END: inserting into rebate_file");

                        RebateTemp::where('reward_date',$data[$i]->date)->update([
                            'file_id' => $rebateFile->id
                        ]);
                    }

                    $countData = "SELECT count(1) total
                        FROM rebate_temp rt
                        LEFT JOIN rebate_data rd
                        ON rt.reward_order = rd.reward_order
                        WHERE rd.reward_order IS NULL;";
                    $result = DB::select($countData, []);

                    if ($result[0]->total!= 0) {
                        $queryInsert = "INSERT INTO rebate_data (reward_date, client_id, volume_lots, reward_order, investment_id, created_at, updated_at, is_calculated, is_checked, file_id)
                                SELECT DISTINCT rt.reward_date, rt.client_id, rt.volume_lots, rt.reward_order, rt.investment_id, CURRENT_TIMESTAMP, NULL, 0, 0, rt.file_id
                                FROM rebate_temp rt
                                LEFT JOIN rebate_data rd
                                ON rt.reward_order = rd.reward_order
                                WHERE rd.reward_order IS NULL;";
                        DB::statement($queryInsert);
                    }
                }
                Log::info("4. END: inserting into rebate_data table");
            } else {
                Log::error("ERROR: retrieving count from Exness Rewards Report API.", ["error" => $err]);
            }

            $endTime = microtime(true);
            $execTime = ($endTime - $sTime);

            if ($getSettled == null) {
                // INSERT - START INSERT UPDATE INVESTMENT ID
                $startTime = microtime(true);
                Log::info("5. BEGIN: Insert update investment ID to sprofit_data");

                $callSP = "CALL INSERT_UPDATE_INVESTMENT_ID()";
                DB::statement($callSP);

                $endTime = microtime(true);
                $execTime = ($endTime - $startTime);
                Log::info("5. END: Insert update investment ID to sprofit_data (".$execTime." sec)");

                // CALCULATE - REBATE CALCULATE COMMISSION GROSS
                $startTime = microtime(true);
                Log::info("6. BEGIN: Calculate commission rebate on table rebate_data");

                $callSP = "CALL CALCULATE_REBATE_GROSS()";
                DB::statement($callSP);

                $endTime = microtime(true);
                $execTime = ($endTime - $startTime);
                Log::info("6. END: Calculate commission rebate on table rebate_data (".$execTime." sec)");

                // UPDATE - UPDATE COMMISSION BALANCE 
                $startTime = microtime(true);
                Log::info("7. BEGIN: Update commission balance on table user_data");

                $callSP = "CALL UPDATE_COMMISSION_BALANCE()";
                DB::statement($callSP);

                $endTime = microtime(true);
                $execTime = ($endTime - $startTime);
                Log::info("7. END: Update commission balance on table user_data (".$execTime." sec)");

                // UPDATE - DEPOSIT DAN TURNOVER USER
                $startTime = microtime(true);
                Log::info("8. BEGIN: Update deposit and turnover balance on table user_data");

                $callSP = "CALL UPDATE_DEPOSIT_BALANCE()";
                DB::statement($callSP);

                $callSP = "CALL UPDATE_TURNOVER()";
                DB::statement($callSP);

                $endTime = microtime(true);
                $execTime = ($endTime - $startTime);
                Log::info("8. END: Update deposit and turnover balance on table user_data (".$execTime." sec)");

                $queryTruncate = "TRUNCATE TABLE rebate_temp";
                DB::statement($queryTruncate);
            }
        }
        curl_close($curl);

        return Command::SUCCESS;
    }
}

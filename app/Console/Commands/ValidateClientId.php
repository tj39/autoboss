<?php

namespace App\Console\Commands;

use App\Models\ClientidTemp;
use App\Models\GeneralSetting;
use App\Models\User;
use App\Traits\GlobalValues;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ValidateClientId extends Command
{
    use GlobalValues;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'validate:clientid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scheduler for validating Client ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $authUrl = GeneralSetting::where('code', 'exness_auth_api')->first()->value;
        $exnessEmail = GeneralSetting::where('code', 'exness_affiliate_email')->first()->value;
        $exnessPassword = GeneralSetting::where('code', 'exness_affiliate_password')->first()->value;
        $encryptionCiphering = GeneralSetting::where('code', 'encryption_ciphering')->first()->value;
        $encryptionKey = GeneralSetting::where('code', 'decryption_key')->first()->value;
        $encryptionIv = GeneralSetting::where('code', 'decryption_iv')->first()->value;
        

        $countData = "SELECT COUNT(1) total FROM user_data
                        WHERE ((is_clientid_valid = 0 AND client_id IS NOT NULL)
                            OR (is_clientid_valid = 2 AND updated_clientid_at >= DATE_SUB(NOW(), INTERVAL 10 MINUTE)))
                        AND is_admin = 0;";
        $result = DB::select($countData, []);

        if ($result[0]->total > 0) {
            Log::info("1. BEGIN: authenticate to Exness Affiliates API to get token");
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $authUrl,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => array(
                    'Accept: application/json',
                    'Content-Type: application/json'
                ),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => json_encode(
                    array(
                        "login" => openssl_decrypt($exnessEmail, $encryptionCiphering, $encryptionKey, 0, $encryptionIv),
                        "password" => openssl_decrypt($exnessPassword, $encryptionCiphering, $encryptionKey, 0, $encryptionIv)
                    ),
                    JSON_FORCE_OBJECT
                )
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            Log::info("1. END: authenticate to Exness Affiliates API to get token");

            if (empty($err)) {
                Log::info("2. BEGIN: processing Client ID");
                $clientReportUrl = GeneralSetting::where('code', 'exness_client_report_api')->first()->value;

                $json = json_decode($response);
                $exnessToken = $json->token;

                Log::info("3. BEGIN: retrieving data count from Exness Clients Reports API");
                $authorization = 'Authorization: JWT ' . $exnessToken;
                curl_setopt($curl, CURLOPT_URL, $clientReportUrl . '?reg_date=' . date('Y-m-d'));
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Accept: application/json',
                    $authorization
                ));
                curl_setopt($curl, CURLOPT_POST, false);
                curl_setopt($curl, CURLOPT_POSTFIELDS, []);
                $response = curl_exec($curl);
                $err = curl_error($curl);
                Log::info("3. END: retrieving data count from Exness Clients Reports API");

                if (empty($err)) {
                    Log::info("4. BEGIN: processing json data");
                    $jsonData = json_decode($response);
                    $count = $jsonData->totals->count;
                    $loops = (int) ceil($count / 1000);
                    $userClientIds = [];
                    for ($i = 0; $i < $loops; $i++) {
                        $offset = $i * 1000;
                        Log::info("4a. BEGIN: sending request to Exness Clients Reports API", ['offset' => $offset, 'limit' => 1000]);
                        curl_setopt($curl, CURLOPT_URL, $clientReportUrl . '?reg_date=' . date('Y-m-d') . '&limit=1000&offset=' . $offset);
                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        if (empty($err)) {
                            $jsonData = json_decode($response);
                            $clients = $jsonData->data;
                            foreach ($clients as $client) {
                                Log::info("4b. BEGIN: inserting into table client_id_temp.", ['client_uid' => $client->client_uid]);
                                array_push($userClientIds, $client->client_uid);
                                ClientidTemp::firstOrCreate( //insert if not exist
                                    ['client_uid' => $client->client_uid],
                                    ['reg_date' => $client->reg_date]
                                );
                                Log::info("4b. END: inserting into table client_id_temp.");
                            }
                        }
                        Log::info("4a. END: sending request to Exness Clients Reports API");
                    }
                    Log::info("4. END: processing json data");

                    $queryUpdate1 = "UPDATE user_data ud
                        LEFT JOIN clientid_temp ct
                        ON ud.client_id = ct.client_uid
                        SET is_clientid_valid = 2
                        WHERE ct.client_uid IS NULL
                        AND ud.is_clientid_valid != 1
                        AND ud.client_id IS NOT NULL
                        AND ud.is_admin = 0;";

                    $queryUpdate2 = "UPDATE user_data ud 
                        JOIN clientid_temp ct 
                        ON ud.client_id = ct.client_uid 
                        SET is_clientid_valid = '1';";

                    Log::info("5. BEGIN: updating invalid Client ID in user_data table");
                    DB::statement($queryUpdate1);
                    Log::info("5. END: updating invalid Client ID in user_data table");
                    Log::info("6. BEGIN: updating valid Client ID in user_data table");
                    DB::statement($queryUpdate2);
                    Log::info("6. END: updating valid Client ID in user_data table");

                    foreach ($userClientIds as $clientId) {
                        $user = User::where('client_id', $clientId)->first();
                        if ($user) {
                            Log::info("7. BEGIN: updating total direct downline.", ['id' => $user->direct_upline]);
                            $callF = "CALL GET_DIRECT_DOWNLINE_COUNT(".$user->direct_upline.")";
                            DB::statement($callF);
                            Log::info("7. END: updating total direct downline.");
                        }
                    }
                } else {
                    Log::error("ERROR: sending request to Exness Clients Reports API.", ["error" => $err]);
                }
                Log::info("1. END: processing Client ID");
            }
            curl_close($curl);
        }

        return Command::SUCCESS;
    }
}

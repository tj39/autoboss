<?php

namespace App\Console\Commands;

use App\Models\RebateFile;
use App\Models\SprofitFile;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class InsertSharingProfitData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:sharingprofit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for inserting sharing profit data from uploaded CSV files in sprofit_file table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $rebateProcess = RebateFile::where('is_inserted', '!=', '1')->count();

        if ($rebateProcess == 0) {
            // INSERT - START INSERT DATA FROM CSV
            $sTime = microtime(true);
            echo "1. BEGIN: Insert sprofit_temp from sprofit_file";
            $sProfitFiles = SprofitFile::where('is_inserted', '0')
                ->orderBy('created_at', 'asc')->get();

            foreach ($sProfitFiles as $sProfitFile) {
                SprofitFile::where('id', $sProfitFile->id)->update([
                    'is_inserted' => 2
                ]);

                $filePath = str_replace('\\', '/', base_path()) . '/storage/app' . '/' . $sProfitFile->file_path;

                $queryTruncate = "TRUNCATE TABLE sprofit_temp";
                DB::statement($queryTruncate);

                if ($sProfitFile->is_paid == '1') {
                    $queryLoad = sprintf(
                        "LOAD DATA LOCAL INFILE '%s' REPLACE
                        INTO TABLE sprofit_temp
                        FIELDS TERMINATED BY ',' 
                        ENCLOSED BY ''
                        LINES TERMINATED BY '\n'
                        IGNORE 1 ROWS
                        (investment_id, @ignore, @ignore, @amount, @file_id)
                        SET amount = CAST(REPLACE(@amount, ',', '.') AS DECIMAL(15,2)), file_id = '%s';",
                        $filePath, $sProfitFile->id 
                    );
                    DB::statement($queryLoad);

                } else if ($sProfitFile->is_paid == '0') {
                    $queryLoad = sprintf(
                        "LOAD DATA LOCAL INFILE '%s' REPLACE
                        INTO TABLE sprofit_temp 
                        FIELDS TERMINATED BY ',' 
                        ENCLOSED BY ''
                        LINES TERMINATED BY '\n'
                        IGNORE 1 ROWS
                        (investment_id, @ignore, @ignore, @is_active, @ignore, @initial_investment, @amount, @file_id, @ignore, @ignore, @ignore, @ignore, @ignore, @ignore, @ignore, @ignore, @ignore, @ignore, @ignore, @ignore, @ignore, @ignore)
                        SET initial_investment = CAST(REPLACE(@initial_investment, ',', '.') AS DECIMAL(15,2)), is_active = UPPER(@is_active) ,amount = NULL, file_id = '%s';",
                        $filePath, $sProfitFile->id 
                    );
                    DB::statement($queryLoad);
                }
            }

            $endTime = microtime(true);
            $execTime = ($endTime - $sTime);
            echo "\n1. END: Insert sprofit_temp from sprofit_file (".$execTime." sec)";

            // UPDATE - TABLE SPROFIT_DATA BASED ON SPROFIT_TEMP
            $startTime = microtime(true);
            echo "\n2. BEGIN: Update tabel sprofit_data dan sprofit_file berdasarkan CSV";

            $queryUpdate = "UPDATE sprofit_data sd
                            JOIN rebate_data rd
                            ON sd.investment_id = rd.investment_id
                            SET sd.client_id = rd.client_id;";
            DB::statement($queryUpdate);

            $callSP = "CALL UPDATE_INITIAL_PAID_COMMISSION()";
            DB::statement($callSP);

            $endTime = microtime(true);
            $execTime = ($endTime - $startTime);
            echo "\n2. END: Update tabel sprofit_data dan sprofit_file berdasarkan CSV (".$execTime." sec)";


            // CALCULATE - SHARING PROFIT CALCULATE COMMISSION GROSS
            $startTime = microtime(true);
            echo "\n3. BEGIN: Calculate commission sharing profit on table sprofit_data";

            $callSP = "CALL CALCULATE_SPROFIT_GROSS()";
            DB::statement($callSP);

            $endTime = microtime(true);
            $execTime = ($endTime - $startTime);
            echo "\n3. END: Calculate commission sharing profit on table sprofit_data (".$execTime." sec)";

            // UPDATE - UPDATE COMMISSION BALANCE 
            $startTime = microtime(true);
            echo "\n4. BEGIN: Update commission balance on table user_data";

            $callSP = "CALL UPDATE_COMMISSION_BALANCE()";
            DB::statement($callSP);

            $endTime = microtime(true);
            $execTime = ($endTime - $startTime);
            echo "\n4. END: Update commission balance on table user_data (".$execTime." sec)";
        }

        return Command::SUCCESS;
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\GeneralSetting;
use App\Models\User;
use App\Models\SeminarEvent;
use Illuminate\Support\Facades\Mail;
use App\Traits\GlobalValues;
use App\Mail\SeminarReminderMailAdmin;

class SeminarReminderAdminManagement extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:reminderseminaradmin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date_genset = GeneralSetting::where('code', 'reminder_seminar')->first();
        $seminars = SeminarEvent::where('status', 'NEW')->get();

        foreach($seminars as $item){
            $seminarSubstractDate = $item->event_date->subDays($date_genset->value);
            $seminarSubstract = $item->event_date->subDays($date_genset->value + 3);
            
            if($seminarSubstract->isToday()){
                
                $users = User::where('is_admin', 1)->whereHas('adminRole', function($query) {
                    $query->where('code', 'MGT');
                })->get();

                foreach($users as $user){
                    $details = [
                        'event_name' => $item->event_name,
                        'event_date' => $seminarSubstractDate,
                        'event_time' => $item->event_date,
                    ];
                    
                    Mail::to($user->email)->send(new SeminarReminderMailAdmin($details));

                    $this->send_whatsapp($user->id, $item->event_name, $seminarSubstractDate, $item->event_date);
                }
            }
        }
    }

    public function send_whatsapp($id, $event_name, $event_date, $event_time){
        $user = User::where('id', $id)->first();

        $whatsoApi = $this->getGeneralSettingValue('whatso_api');
        $whatsoUsername = $this->getGeneralSettingValue('whatso_username');
        $whatsoPassword = $this->getGeneralSettingValue('whatso_password');
        $fromNumber = $this->getGeneralSettingValue('tjl_phone_number');

        $data = [
            'Username' => $whatsoUsername, // Whatso username
            'Password' => $whatsoPassword, // Whatso password
            'MessageText' => $this->encodeURIComponent('
            *Halo management admin!* 
            you have to ensure the seminar "*'. $event_name. '*" schedule, 
            because it will be held on '.date_format(date_create($event_time), 'D, d M Y').'. 
            Please reschedule before the reminder message for traders on '.date_format(date_create($event_date), 'D, d M Y').' if you want to cancel.
            '),
            'MobileNumbers' => $user->phone, // Receiver number
            'ScheduleDate' => '',
            'FromNumber' => $fromNumber,
            'Channel' => '1'
        ];
        $json = json_encode($data);
        $url = $whatsoApi;
        $options = stream_context_create(['http' => [
                'method'  => 'POST',
                'header'  => ['Content-type: application/json', 'Accept: application/json'],
                'content' => $json
            ]
        ]);

        // Send a request
        $result = file_get_contents($url, false, $options);
    }

    function encodeURIComponent($str) {
        $revert = array('%0D%0A'=>'
        ','%20'=>' ','%21'=>'!', '%22'=>'"', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')', '%2C'=>',', '%3A'=>':');
        return strtr(rawurlencode($str), $revert);
    }
}

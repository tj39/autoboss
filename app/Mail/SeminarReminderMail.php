<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\GeneralSetting;
use App\Models\SeminarEvent;

class SeminarReminderMail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //$genset = GeneralSetting::where('code', 'ticket_message')->first()->value;

        $seminar = SeminarEvent::latest()->first();

        return $this->subject('Invitation - '.$this->details['event_name'])
                    ->view('emails.reminder-seminar');
    }
}

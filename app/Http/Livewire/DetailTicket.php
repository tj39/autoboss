<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\ActiveTicket;
use App\Models\ActiveComment;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class DetailTicket extends Component
{
    public $id_ticket;
    public $message;
    public $imageurl;
    public $page;

    public function mount($id, $page){
        $this->id_ticket = $id;
        $this->page = $page;

        ActiveTicket::where('id', $id)->update([
            'is_read_user' => 1
        ]);

        ActiveComment::where('ticket_id', $id)->update([
            'is_read_user' => 1
        ]);
    }

    public function render()
    {
        $comment = ActiveComment::where('ticket_id', $this->id_ticket)->get();
        $ticket = ActiveTicket::find($this->id_ticket);

        return view('livewire.detail-ticket', [
            'comment' => $comment,
            'ticket' => $ticket
        ]);
    }

    public function showModalConfirmSolved(){
        $this->emit('showModalConfirmSolved');
    }

    public function showModalConfirmSend(){
        $this->validate([
            'message' => 'required'
        ]);

        $this->emit('showModalConfirmSend');
    }

    public function sendMessage(){
        $ticket = ActiveTicket::find($this->id_ticket);

        ActiveComment::create([
            'ticket_id' => $this->id_ticket,
            'user_id' => Auth::user()->id,            
            'status_id' => $ticket->status_id,
            'created_date' => Date(now()),
            'message' => $this->message,
            'url_image' => $this->imageurl,
            'is_read_user' => 1
        ]);

        ActiveTicket::where('id', $this->id_ticket)->update([
            'created_date' => Date(now()),
            'is_read_admin' => 0
        ]);

        
        $this->hideModal();
        return redirect()->to('/user/detail-ticket/'.$this->id_ticket.'/'.$this->page);
    }

    public function hideModal(){
        $this->message = null;
        $this->imageurl = null;

        $this->emit('hideModal');
        $this->emit('showAlert', ['msg' => 'Message Sent']);
    }

    public function storeStatus(){
        ActiveTicket::where('id', $this->id_ticket)->update([
            'status_id' => 2
        ]);

        $this->hideModal();
        $this->emit('showAlert', ['msg' => 'Ticket Solved']);
        return redirect()->to('/user/detail-ticket/'.$this->id_ticket.'/'.$this->page);
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\AdminRole;
use App\Models\MasterStrategy;
use App\Models\RankRequest;
use App\Models\TraderRank;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\GeneralSetting;

class Dashboard extends Component
{
    protected $listeners = ['refreshData' => '$refresh'];

    public $clientId;
    public $step;
    public $strategies;
    public $choosenStrategy;
    public $strategy;
    public $investmentId;
    public $totalDeposited;
    public $downline;
    public $turnover;
    // public $rankRequest;
    public $canRequestUpgrade;
    public $percentDeposit;
    public $percentDownline;
    public $percentTurnover;
    public $targetDeposit;
    public $targetDownline;
    public $targetTurnover;

    public function render()
    {
        if (Auth::user()->is_admin == 1) {
            return view('livewire.dashboard', [
                'role' => AdminRole::find(auth()->user()->role_id)->code,
                'unverifiedTraderCount' => $this->getUnverifiedTraderCount()
            ]);
        }

        $this->step = auth()->user()->step;

        $rankRequest = null;
        if ($this->step == 'OK') {
            $user = User::find(auth()->user()->id);

            $currentTraderRank = TraderRank::find(auth()->user()->role_id);

            if ($currentTraderRank->code != 'D') {
                $nextTraderRank = TraderRank::where('order', $currentTraderRank->order - 1)->first();
                $this->totalDeposited = $user->deposit_balance;
                $this->downline = $user->tot_direct_downline;
                $this->turnover = $user->turnover;

                $this->percentDeposit = ($user->deposit_balance / $nextTraderRank->deposit) * 100;
                $this->percentDownline = ($user->tot_direct_downline / $nextTraderRank->direct_downline) * 100;
                $this->percentTurnover = ($user->turnover / $nextTraderRank->turnover) * 100;

                $this->targetDeposit = $nextTraderRank->deposit;
                $this->targetDownline = $nextTraderRank->direct_downline;
                $this->targetTurnover = $nextTraderRank->turnover;
                
                if ($this->percentDeposit >= 100 && $this->percentDownline >= 100 && $this->percentTurnover >= 100) {
                    $this->canRequestUpgrade = true;
                }

                $rankRequest = RankRequest::where('user_id', $user->id)
                    ->orderBy('updated_at', 'desc')->first();
                
                if ($rankRequest && strpos($rankRequest->status, 'certificate') !== false) {
                    $whereCondition = $rankRequest->status == 'ACCEPTED' ? 'to_rank' : 'from_rank';
                    $rankRequest = DB::table('rank_request')
                        ->selectRaw('id, updated_at, status, message, is_dismissed')
                        ->where('user_id', '=', $user->id)
                        ->where($whereCondition, '=', $currentTraderRank->code)
                        ->orderByRaw('updated_at DESC')->first();
                }                
            } else {
                $this->totalDeposited = 100;
                $this->downline = 100;
                $this->turnover = 100;
                
                $this->targetDeposit = $currentTraderRank->deposit;
                $this->targetDownline = $currentTraderRank->direct_downline;
                $this->targetTurnover = $currentTraderRank->turnover;

                $this->percentDeposit = 100;
                $this->percentDownline = 100;
                $this->percentTurnover = 100;
            }
        }

        $this->strategies = MasterStrategy::where('is_active', 1)->get();

        $ref_exness = GeneralSetting::where('code', 'ref_exness')->where('is_active', 1)->first()->value;

        return view('livewire.dashboard', [
            'step' => auth()->user()->step,
            'rankRequest' => $rankRequest,
            'ref_exness' => $ref_exness,
            'benefitHistory' => $this->getBenefitHistory()
        ]);
    }

    public function submit($step)
    {
        $user = User::find(auth()->user()->id);

        if ($step == '2') {
            $user->client_id = $this->clientId;
            $user->updated_at = Carbon::now();
            $user->updated_clientid_at = Carbon::now();
            $user->step = '3';
            $user->save();

            $this->emit('hideModalConfirmation');
            $this->emit('showAlert', ['msg' => 'Your Client ID has been updated.']);
        }

        if ($step == '3') {
            $user->updated_at = Carbon::now();
            $user->step = 'OK';
            $user->save();

            $this->emit('hideModalConfirmation');
            $this->emit('showAlert', ['msg' => 'Strategy has been successfully selected. Enjoy your auto trading experience!']);
        }

        $this->choosenStrategy = null;
        $this->investmentId = null;

        $this->emit('refreshData');
    }

    public function showModalStrategyDetail($id)
    {
        $this->strategy = MasterStrategy::find($id);
        $this->emit('showModalStrategyDetail');
    }

    public function selectStrategy($id)
    {
        $this->choosenStrategy = $id;
        $this->emit('hideModalStrategyDetail');
    }

    public function showModalConfirmation($step)
    {
        if ($step == '2') {
            $this->validate([
                'clientId' => 'required|max:8|unique:App\Models\User,client_id'
            ], [
                'clientId.unique' => 'Client ID already exists. Please check your Client ID again.'
            ]);
        } else if ($step == '3') {
            $this->validate([
                'choosenStrategy' => 'required'
            ], [
                'choosenStrategy.required' => 'You must choose trading strategy.'
            ]);
        }

        $this->emit('showModalConfirmation');
    }

    public function hideModalConfirmation()
    {
        $this->emit('hideModalConfirmation');
    }

    private function getUnverifiedTraderCount()
    {
        return User::where('account_verify_msg', '=', 'PROCESS')
            ->orWhere('kyc_verify_msg', '=', 'PROCESS')
            ->count();
    }

    public function dismissMessage($id)
    {
        $rankRequest = RankRequest::find($id);
        $rankRequest->is_dismissed = '1';
        $rankRequest->save();
    }

    public function getBenefitHistory()
    {
        $rebateSummary = DB::table('rebate_summary')
            ->selectRaw('id, DATE_FORMAT(IFNULL(updated_at, created_at), "%d %b %Y") as created_at, IFNULL(updated_at, created_at) date, "Rebate" as category, commission_usd as amount, (case when (transfer_proof is null or transfer_proof = "") then "PENDING" else "SETTLED" end) as status')
            ->where('leader_id', '=', auth()->user()->id)
            ->orderByRaw('created_at desc');

        return DB::table('sprofit_summary')
            ->selectRaw('id, DATE_FORMAT(IFNULL(updated_at, created_at), "%d %b %Y") as created_at, created_at date, "Sharing Profit" as category, commission_usd as amount, (case when (transfer_proof is null or transfer_proof = "") then "PENDING" else "SETTLED" end) as status')
            ->where('leader_id', '=', auth()->user()->id)
            ->orderByRaw('created_at desc')
            ->union($rebateSummary)
            ->orderByRaw('date desc')
            ->limit(5)
            ->get();
    }
}

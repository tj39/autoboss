<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\GeneralSetting;
use App\Models\User;
use App\Models\TransferGateway;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class TraderValidation extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['refreshList' => '$refresh'];

    public $userId;
    public $user;
    public $bankAccount;
    public $accountHolder;
    public $note;
    public $search;

    public function render()
    {

        $pageRow = GeneralSetting::where('code', 'pagination_row')->get()->first()->value;
        return view('livewire.admin.trader-validation', [
            'data' => $this->getUserList()->paginate($pageRow)
        ]);
    }

    public function getUserList()
    {
        $users = DB::table('user_data')
            ->selectRaw('id, IFNULL(updated_at, created_at) AS date, CONCAT(firstname, " ", lastname) AS name, kyc_verify_msg, account_verify_msg, check_npwp')
            ->whereRaw("(account_verify_msg = 'PROCESS' OR kyc_verify_msg = 'PROCESS' OR (check_npwp = 2 AND (kyc_verify_msg = 'VALID' OR kyc_verify_msg = 'PROCESS')))")
            ->whereRaw("(firstname LIKE '%".$this->search."%' OR lastname LIKE '%".$this->search."%')")
            ->orderByRaw('date ASC');
                    
        return $users;
    }

    public function showKycModal($id)
    {
        $this->userId = $id;
        $this->emit('getKycData');

        $this->user = User::find($id);
    }

    public function hideKYCmodal()
    {
        $this->userId = null;
    }

    public function showBankAccountModal($id)
    {
        $this->userId = $id;
        $this->emit('getBankAccountData');

        $this->user = User::find($id);
        $bank = TransferGateway::where('id', $this->user->gateways_id)->get()->first()->description;
        $this->bankAccount = $bank . ' - ' . $this->user->no_account;
    }

    public function hideBankAccountModal()
    {
        $this->userId = null;
    }

    public function showNpwpModal($id)
    {
        $this->userId = $id;
        $this->emit('getNpwpData');

        $this->user = User::find($id);
    }

    public function hideNPWPmodal()
    {
        $this->userId = null;
    }

    public function verify($type)
    {
        $this->validate([
            'note' => 'required'
        ]);

        $this->user->timestamps = false;
        $this->user->save();

        if ($type == 'kyc') {
            $this->user->update([
                'kyc_verify_msg' => $this->note
            ]);
        } else {
            $this->user->update([
                'account_verify_msg' => $this->note
            ]);
        }

        if ($this->user->kyc_verify_msg != 'PROCESS' && $this->user->account_verify_msg != 'PROCESS' ) {
            $this->user->timestamps = true;
            $this->user->updated_at = Carbon::now();
            $this->user->save();
        }

        if ($this->user->kyc_verify_msg == 'VALID' && $this->user->account_verify_msg == 'VALID' && $this->user->step != 'OK') {
            $this->user->update([
                'step' => 2
            ]);

            if ($this->user->client_id != null || $this->user->client_id != '' ) {
                $this->user->update([
                    'step' => 3
                ]); 
            }
        }

        if ($this->user->kyc_verify_msg == 'VALID'  && ($this->user->no_npwp != null && ($this->user->npwp != '' || $this->user->npwp != null))) {
            $this->user->update([
                'check_npwp' => 1
            ]);
        }

        $this->note = null;
        $this->user = null;
        $this->userId = null;
        $this->bankAccount = null;
        $this->accountHolder = null;
        $this->emit('hideModal');
        $this->emit('showAlert', ['msg' => 'Data has been updated.']);
        $this->emit('refreshList');
    }

    public function verifynpwp($type)
    {
        if ($type == '1') {
            $this->user->update([
                'check_npwp' => 1
            ]);
        } else if ($type == '3') {
            $this->user->update([
                'check_npwp' => 3,
            ]);
        }

        if ($this->user->kyc_verify_msg == 'VALID' && $this->user->account_verify_msg == 'VALID' && $this->user->step != 'OK') {
            $this->user->update([
                'step' => 2
            ]);

            if ($this->user->client_id != null || $this->user->client_id != '' ) {
                $this->user->update([
                    'step' => 3
                ]); 
            }
        }

        $this->emit('hideModal');
        $this->emit('showAlert', ['msg' => 'Data has been updated.']);
        $this->emit('refreshList');
    }
}

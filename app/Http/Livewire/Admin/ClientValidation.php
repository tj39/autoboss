<?php

namespace App\Http\Livewire\Admin;

use App\Models\GeneralSetting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class ClientValidation extends Component
{
    use WithFileUploads;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $excelFile;
    public $clientprocess;
    public $clientinvalid;
    public $clientprocesscount;
    public $clientinvalidcount;

    public function render()
    {
        $pageRow = GeneralSetting::where('code', 'pagination_row')->where('is_active', '1')->first()->value;

        $this->clientprocesscount = User::where('is_admin', '!=', '1')->whereRaw('(is_clientid_valid = 0)')->count();
        $this->clientinvalidcount = User::where('is_admin', '!=', '1')->whereRaw('(is_clientid_valid = 2)')->count();
        return view('livewire.admin.client-validation', [
            'users' => $this->retrieveUserList()->paginate($pageRow)
        ]);
    }

    public function retrieveUserList()
    {

        $user = User::where('is_admin', '!=', '1')
                ->whereRaw('(is_clientid_valid = 2 OR is_clientid_valid = 0)')
                ->orderBy('firstname', 'asc');

        if($this->clientprocess == true && $this->clientinvalid == false){
            $user = User::where('is_admin', '!=', '1')
                    ->whereRaw('(is_clientid_valid = 0)')
                    ->orderBy('firstname', 'asc');
        }
        elseif($this->clientprocess == false && $this->clientinvalid == true){
            $user = User::where('is_admin', '!=', '1')
                ->whereRaw('(is_clientid_valid = 2)')
                ->orderBy('firstname', 'asc');
        }

        return $user;
    }

    public function queryClient($table){
        if($table == "process"){
            $this->clientprocess = true;
            $this->clientinvalid = false;
        }
        else if($table == "invalid"){
            $this->clientprocess = false;
            $this->clientinvalid = true;
        }
    }

    public function submit()
    {
        $this->validate([
            'excelFile' => 'required|mimes:csv,txt'
        ]);

        $dateStr = Carbon::now()->toDateString();
        $path = 'Excel/client-id-validation/' . $dateStr;
        Storage::makeDirectory($path);
        $fileName = $this->excelFile->getClientOriginalName();
        $fileExists = Storage::exists($path . '/' . $fileName);
        
        if ($fileExists) {
            $fileName = str_replace('.csv', '', $this->excelFile->getClientOriginalName()) . '-' . substr(md5(str_shuffle('12345')), 0, 5) . '.csv';
        }
        
        $file = $this->excelFile->storeAs($path, $fileName);
        $filePath = str_replace('\\', '/', base_path()) . '/storage/app' . '/' . $file;

        $queryLoadData = "LOAD DATA LOCAL INFILE '$filePath' REPLACE
            INTO TABLE clientid_temp
            FIELDS TERMINATED BY ','
            ENCLOSED BY ''
            LINES TERMINATED BY '\n'
            IGNORE 1 ROWS
            (@reg_date, client_uid, @ignore)
            SET reg_date = STR_TO_DATE(@reg_date, '%Y-%m-%d');";
            
        $queryUpdate1 = "UPDATE user_data ud
            LEFT JOIN clientid_temp ct
            ON ud.client_id = ct.client_uid
            SET is_clientid_valid = 2
            WHERE ct.client_uid IS NULL
            AND ud.is_clientid_valid != 1
            AND ud.client_id IS NOT NULL
            AND ud.is_admin = 0;";

        $queryUpdate2 = "UPDATE user_data ud 
            JOIN clientid_temp ct 
            ON ud.client_id = ct.client_uid 
            SET is_clientid_valid = '1';";

        DB::statement($queryLoadData);
        DB::statement($queryUpdate1);
        DB::statement($queryUpdate2);

        $users = User::all();
        foreach ($users as $user) {
            $callF = "CALL GET_DIRECT_DOWNLINE_COUNT($user->id)";
            DB::statement($callF);
        }

        $this->resetFields();

        $this->emit('showAlert', ['msg' => 'File has been processed.']);
        $this->emit('hideModalUploadFile');
    }

    public function resetFields()
    {
        $this->excelFile = null;
    }
}

<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\GeneralSetting;
use App\Models\User;
use App\Models\WithdrawData;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\WithPagination;

class ConfirmWithdraw extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['refreshList' => '$refresh'];

    public $wdId;
    public $searchWdCode;
    public $wdCode;
    public $amount;
    public $account;

    public function render()
    {
        $pageRow = GeneralSetting::where('code', 'pagination_row')->get()->first()->value;
        return view('livewire.admin.confirm-withdraw', [
            'data' => $this->getWithdrawList()->paginate($pageRow)
        ]);
    }

    public function getWithdrawList()
    {
        return DB::table('withdraw_data AS wd')
            ->selectRaw("wd.id, wd.created_at AS date, wd.wd_code, FORMAT(wd.amount, 2, 'id_ID') AS amount, wd.status, IFNULL(wd.message, '') AS message, wd.confirmed_at")
            ->where('wd.wd_code', 'LIKE', '%'.$this->searchWdCode.'%')
            ->orderByRaw("FIELD(wd.status, 'P', 'F', 'S')")
            ->orderBy('date', 'DESC');
    }

    public function showModal($id)
    {
        $this->wdId = $id;
        $this->emit('showModal');

        $wdData = WithdrawData::where('id', $this->wdId)->get()->first();
        $this->wdCode = $wdData->wd_code;
        $this->amount = $wdData->amount;
        $this->account = User::find($wdData->user_request)->no_account;
    }

    public function confirm()
    {
        $wdData = WithdrawData::where('id', $this->wdId)->get()->first();
        $user = User::find($wdData->user_request);

        $wdData->update([
            'confirmed_at' => Carbon::now(),
            'user_confirm' => auth()->id(),
            'status' => 'S'
        ]);

        $user->update([
            'commission_balance' => $user->commission_balance - $this->amount,
            'withdraw_balance' => $user->withdraw_balance + $this->amount,
            'updated_at' => Carbon::now()
        ]);

        $this->emit('hideModal');
        $this->emit('showAlert', ['msg' => 'Withdrawal data has been confirmed.']);
        $this->emit('refreshList');
    }

    public function showModalConfirmation()
    {
        $this->emit('showModalConfirmation');
    }

    public function hideModalConfirmation()
    {
        $this->emit('hideModalConfirmation');
    }

    public function getRate()
    {
        // TEMPORARY API usage limited to 1000 times
        // set API Endpoint and API key
        $endpoint = 'latest';
        $access_key = '1b1f627a61173f7b60b7c40485aa772a';

        $from = 'USD';
        $to = 'IDR';
        $amount = 1;

        // initialize CURL:
        $ch = curl_init('http://api.exchangeratesapi.io/v1/'.$endpoint.'?access_key='.$access_key.'&from='.$from.'&to='.$to.'&amount='.$amount.'');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // get the JSON data:
        $json = curl_exec($ch);
        curl_close($ch);

        // Decode JSON response:
        $conversionResult = json_decode($json, true);

        // access the conversion result
        return $conversionResult['rates'][$to];
    }
}

<?php

namespace App\Http\Livewire\Admin;

use App\Models\TransferGateway;
use App\Models\User;
use App\Traits\GlobalValues;
use Livewire\Component;
use Livewire\WithPagination;

class EditUserData extends Component
{
    use GlobalValues;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $userId;
    public $search1, $search2;
    public $searchClicked = false;
    public $editClicked = false;
    public $phone, $email, $selectedBank, $accountNo, $accountHolder;
    public $selectedMethod;
    public TransferGateway $transferGateway;

    public function updatingSelectedMethod()
    {
        $this->accountNo = null;
    }

    public function render()
    {
        $users = null;
        if ($this->searchClicked) {
            $pageRow = $this->getGeneralSettingValue('pagination_row');
            $users = $this->retrieveUserListBySearch()->paginate($pageRow);
        }

        $user = null;
        $banks = [];
        if ($this->editClicked) {
            $user = User::find($this->userId);
            $transferGateway = TransferGateway::find($user->gateways_id);
            $this->selectedMethod = $this->selectedMethod != null ? $this->selectedMethod : $transferGateway->method;
            $this->selectedBank = $this->selectedBank != null ? $this->selectedBank : $transferGateway->id;

            if ($this->selectedMethod == 'BANK') {
                $banks = TransferGateway::where('is_active', 1)
                    ->where('method', 'BANK')
                    ->get();
            } else {
                $banks = TransferGateway::where('is_active', 1)
                    ->where('method', 'OTHER')
                    ->get();
                if ($this->selectedBank != null) {
                    $this->transferGateway = $transferGateway;
                }
            }
        }

        return view('livewire.admin.edit-user-data', [
            'users' => $users,
            'banks' => $banks,
            'user' => $user
        ]);
    }

    public function retrieveUserListBySearch()
    {
        return User::whereRaw("(client_id like '%".$this->search1."%'")
            ->orWhereRaw("firstname like '%".$this->search1."%'")
            ->orWhereRaw("lastname like '%".$this->search1."%'")
            ->orWhereRaw("username like '%".$this->search1."%')")
            ->whereRaw("(email like '%".$this->search2."%'")
            ->orWhereRaw("phone like '%".$this->search2."%'")
            ->orwhereRaw("nik like '%".$this->search2."%')");
    }

    public function searchUserData()
    {
        $this->searchClicked = true;
        $this->resetPage();
    }

    public function showEditModal($id)
    {
        $this->userId = $id;
        $user = User::find($id);
        $this->selectedBank = $user->gateways_id;        
        $this->phone = $user->phone;
        $this->email = $user->email;
        $this->accountNo = $user->no_account;
        $this->accountHolder = $user->account_holder;
        $this->editClicked = true;
        $this->emit('showModalEdit');
    }

    public function showModalConfirmation()
    {
        $user = User::find($this->userId);

        if ($this->email != $user->email) {
            $this->validate([
                'email' => 'required|email|unique:user_data|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix|max:255'
            ]);
        }

        if ($this->phone != $user->phone) {
            $this->validate([
                'phone' => 'required|unique:user_data|regex:/([+])\d{1,3}/|min:10|max:15'
            ]);
        }

        if ($this->selectedMethod == 'OTHER') {
            $this->accountHolder = $user->firstname . ' ' . $user->lastname;
            $accountNoValidator = 'required|max:255';
        } elseif ($this->selectedMethod == 'BANK') {
            $accountNoValidator = 'required|digits_between:1,32|numeric';
        }

        $this->validate([
            'selectedBank' => 'required',
            'accountNo' => $accountNoValidator,
            'accountHolder' => 'required|max:50'
        ]);

        $this->emit('showModalConfirmation');
    }

    public function submit()
    {
        User::find($this->userId)->update([
            'phone' => $this->phone,
            'email' => $this->email,
            'gateways_id' => $this->selectedBank,
            'no_account' => $this->accountNo,
            'account_holder' => $this->accountHolder
        ]);

        $this->emit('showAlert', ['msg' => 'Data has been updated.']);
        $this->emit('hideModalConfirmation');
        $this->emit('hideModalEdit');
        $this->resetFields();
    }

    public function resetFields()
    {
        $this->phone = null;
        $this->email = null;
        $this->selectedBank = null;
        $this->accountNo = null;
        $this->accountHolder = null;
    }
}

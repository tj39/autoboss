<?php

namespace App\Http\Livewire\Admin;

use App\Models\FaqCategory;
use App\Models\FaqDocument;
use App\Traits\GlobalValues;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class FaqList extends Component
{
    use GlobalValues;
    use WithFileUploads;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['refreshList' => '$refresh'];

    public $categoryId, $documentId, $categoryName, $isActive, $documentName, $pdfFile, $mode, $type;
    public $editClicked = false;
    public $selectedTab = 'category';
    public FaqCategory $faqCategory;
    public FaqDocument $faqDocument;

    public function render()
    {
        $pageRow = $this->getGeneralSettingValue('pagination_row');

        return view('livewire.admin.faq-list', [
            'categories' => $this->retrieveCategoryList()->paginate($pageRow, ['*'], 'category'),
            'documents' => $this->retrieveDocumentList()->paginate($pageRow, ['*'], 'document')
        ]);
    }

    public function retrieveCategoryList()
    {
        return FaqCategory::query()->orderBy('name', 'asc')->orderBy('is_active', 'desc');
    }

    public function retrieveDocumentList()
    {
        return FaqDocument::query()->orderBy('name', 'asc');
    }

    public function showEditModal($mode, $type, $id)
    {
        $this->editClicked = true;
        $this->mode = $mode;
        $this->type = $type;
        if ($mode == 'edit') {
            if ($type == 'category') {
                $this->categoryId = $id;
                $this->faqCategory = FaqCategory::findOrFail($id);
                $this->categoryName = $this->faqCategory->name;
                $this->isActive = (boolean) $this->faqCategory->is_active;
            } elseif ($type == 'document') {
                $this->documentId = $id;
                $this->faqDocument = FaqDocument::findOrFail($id);
                $this->documentName = $this->faqDocument->name;
            }
        }
        $this->emit('showModalEdit');
    }

    public function showModalConfirmation()
    {
        if ($this->type == 'category') {
            $this->validate([
                'categoryName' => 'required|max:32'
            ]);

            if ($this->mode == 'add') {
                $this->validate([
                    'categoryName' => 'unique:faq_category,name'
                ], [
                    'categoryName.unique' => 'The category name already exists.'
                ]);
            } elseif ($this->mode == 'edit' && $this->faqCategory->name != $this->categoryName) {
                $this->validate([
                    'categoryName' => 'unique:faq_category,name'
                ], [
                    'categoryName.unique' => 'The category name already exists.'
                ]);
            }
        } else {
            $this->validate([
                'documentName' => 'required|max:32'
            ]);

            if ($this->mode == 'add') {
                $this->validate([
                    'documentName' => 'unique:faq_document,name',
                    'pdfFile' => 'required|mimes:pdf'
                ], [
                    'documentName.unique' => 'The document name already exists.'
                ]);
            } elseif ($this->mode == 'edit' && $this->faqDocument->name != $this->documentName) {
                $this->validate([
                    'documentName' => 'unique:faq_document,name'
                ], [
                    'documentName.unique' => 'The document name already exists.'
                ]);
            }
        }
        $this->emit('showModalConfirmation');
    }

    public function submit()
    {
        $msg = '';
        if ($this->mode == 'add') {
            if ($this->type == 'category') {
                FaqCategory::create([
                    'created_at' => Carbon::now(),
                    'name' => $this->categoryName,
                    'is_active' => $this->isActive ? '1' : '0'
                ]);

                $msg = 'Category has been added.';
            } else {
                $this->pdfFile->storeAs('PDF', 'faq-document/' . $this->pdfFile->getClientOriginalName());
                $url = request()->getHost() . '/doc=' . str_replace(' ', '_', $this->pdfFile->getClientOriginalName());

                FaqDocument::create([
                    'created_at' => Carbon::now(),
                    'name' => $this->documentName,
                    'url' => $url
                ]);

                $msg = 'Document has been added.';
            }
        } else {
            if ($this->type == 'category') {
                $this->faqCategory->update([
                    'name' => $this->categoryName,
                    'is_active' => $this->isActive ? '1' : '0'
                ]);

                $msg = 'Category has been updated.';
            } else {
                if ($this->pdfFile != null) {
                    $path = 'PDF/faq-document/' . $this->pdfFile->getClientOriginalName();
                    if (Storage::exists($path)) {
                        Storage::delete($path);
                    }
                    $this->pdfFile->storeAs('PDF', 'faq-document/' . $this->pdfFile->getClientOriginalName());
                }

                $this->faqDocument->update([
                    'created_at' => Carbon::now(),
                    'name' => $this->documentName,
                    'url' => $this->pdfFile != null ? request()->getHost() . '/doc=' . str_replace(' ', '_', $this->pdfFile->getClientOriginalName()) : $this->faqDocument->url
                ]);

                $msg = 'Document has been updated.';
            }
        }

        $this->resetFields();
        $this->emit('showAlert', ['msg' => $msg]);
        $this->emit('hideModalConfirmation');
        $this->emit('hideModalEdit');
    }

    public function viewQuestionList($id, $page)
    {
        return redirect()->route('admin.faqdetail', ['id' => $id, 'page' => $page]);
    }

    public function resetFields()
    {
        $this->categoryId = null;
        $this->documentId = null;
        $this->categoryName = null;
        $this->documentName = null;
        $this->isActive = null;
        $this->pdfFile = null;
    }

    public function refreshTab($type)
    {
        $this->selectedTab = $type;
        $this->emit('refreshList');
    }
}

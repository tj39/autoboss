<?php

namespace App\Http\Livewire\Admin;

use App\Models\SeminarEvent;
use App\Models\TraderRank;
use App\Models\User;
use App\Traits\GlobalValues;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Mail;
use App\Mail\SeminarReminderMail;

class SeminarList extends Component
{
    use GlobalValues;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public SeminarEvent $seminarEvent;
    public $editClicked = false;
    public $userModalClicked = false;
    public $mode, $eventName, $eventDate, $eventTime, $url, $meetingID, $passcode, $rank, $status, $selectedId, $userSearch;

    public function render()
    {
        if (auth()->user()->is_admin != '1') {
            abort(403, 'Access forbidden.');
        }

        $ranks = null;
        if ($this->editClicked) {
            $ranks = TraderRank::all();
        }

        $pageRow = $this->getGeneralSettingValue('pagination_row');
        $pageRowModal = $this->getGeneralSettingValue('pagination_modal');

        $users = null;
        if ($this->userModalClicked) {
            $users = $this->retrieveUserList()->paginate($pageRowModal, ['*'], 'userPage');
        }

        return view('livewire.admin.seminar-list', [
            'events' => $this->retriveEventList()->paginate($pageRow, ['*'], 'eventPage'),
            'ranks' => $ranks,
            'users' => $users
        ]);
    }

    public function retriveEventList()
    {
        return DB::table('seminar_events as se')
            ->selectRaw('se.id, se.created_at, se.event_name, se.event_date, se.event_time, se.rank, count(ud.id) as user_count')
            ->leftJoin('user_data as ud', 'se.id', '=', 'ud.seminar_id')
            ->groupByRaw('se.id, se.created_at, se.event_name, se.event_date, se.event_time, se.rank')
            ->orderByRaw('se.created_at desc');
    }

    public function showEditModal($id, $mode)
    {
        $this->editClicked = true;
        $this->mode = $mode;
        if ($id != null) {
            $this->seminarEvent = SeminarEvent::find($id);
            $this->eventName = $this->seminarEvent->event_name;
            $this->eventDate = $this->seminarEvent->event_date;
            $this->eventTime = $this->seminarEvent->event_time;
            $this->url = $this->seminarEvent->url;
            $this->rank = $this->seminarEvent->rank;
            $this->status = $this->seminarEvent->status;
            $this->meetingID = $this->seminarEvent->meeting_id;
            $this->passcode = $this->seminarEvent->passcode;
        }
        $this->emit('showModalEdit');
    }

    public function showModalConfirmation()
    {
        $this->validate([
            'eventName' => 'required',
            'eventDate' => 'required',
            'eventTime' => 'required',
            'url' => 'required',
            'meetingID' => 'required',
            'passcode' => 'required',
            'rank' => 'required'
        ]);

        $this->emit('showModalConfirmation');
    }

    public function submit()
    {
        if ($this->mode == 'add') {
            SeminarEvent::create([
                'event_name' => $this->eventName,
                'event_date' => date_create($this->eventDate),
                'event_time' => $this->eventTime,
                'url' => $this->url,
                'meeting_id' => $this->meetingID,
                'passcode' => $this->passcode,
                'rank' => $this->rank,
                'status' => 'NEW'
            ]);
            $this->emit('showAlert', ['msg' => 'Data has been inserted.']);
        } else {
            $this->seminarEvent->update([
                'event_name' => $this->eventName,
                'event_date' => date_create($this->eventDate),
                'event_time' => $this->eventTime,
                'url' => $this->url,
                'meeting_id' => $this->meetingID,
                'passcode' => $this->passcode,
                'rank' => $this->rank,
                'status' => $this->status
            ]);
            $this->emit('showAlert', ['msg' => 'Data has been updated.']);
        }

        $this->emit('resetPikaday');
        $this->emit('hideModalConfirmation');
        $this->emit('hideModalEdit');
        $this->emit('destroyBackdrop'); //don't know why this happened
        $this->resetFields();
    }

    public function showUserModal($id)
    {
        $this->userModalClicked = true;
        $this->selectedId = $id;
        $this->emit('showUserModal');
    }

    public function retrieveUserList()
    {
        return User::where('seminar_id', $this->selectedId)
            ->where('seminar_status', 'ATTENDING')
            ->whereRaw("(client_id like '%".$this->userSearch."%'")
            ->orWhereRaw("firstname like '%".$this->userSearch."%'")
            ->orWhereRaw("lastname like '%".$this->userSearch."%')");
    }

    public function resetFields()
    {
        $this->eventName = null;
        $this->eventDate = null;
        $this->eventTime = null;
        $this->meetingID = null;
        $this->url = null;
        $this->passcode = null;
        $this->rank = null;
        $this->status = null;
        $this->editClicked = false;
        $this->userModalClicked = false;
        $this->userSearch = null;
    }

    public function sendEmailReminder($id){
        $seminar_event = SeminarEvent::find($id);

        $users = User::where('seminar_status', 'ATTENDING')->where('seminar_id', $seminar_event->id)->get();

        foreach($users as $user){
            if ($user->otp_verified_at == null){
                $details = [
                'event_name' => $seminar_event->event_name,
                'event_date' => $seminar_event->event_date,
                'event_time' => $seminar_event->event_time,
                'url' => $seminar_event->url,
                'passcode' => $seminar_event->passcode
                ];
                        
                Mail::to($user->email)->send(new SeminarReminderMail($details));
            }
            else{
                $this->send_whatsapp($user->id, $seminar_event->event_name, $seminar_event->event_date,$seminar_event->event_time,$seminar_event->url, $seminar_event->meeting_id, $seminar_event->passcode); 
            }
        }
        
        $this->emit('showAlert', ['msg' => 'Email reminder has been sent']);
    }

    public function send_whatsapp($id, $event_name, $event_date, $event_time, $url, $meeting_id, $passcode){
        $user = User::where('id', $id)->first();

        $whatsoApi = $this->getGeneralSettingValue('whatso_api');
        $whatsoUsername = $this->getGeneralSettingValue('whatso_username');
        $whatsoPassword = $this->getGeneralSettingValue('whatso_password');
        $fromNumber = $this->getGeneralSettingValue('tjl_phone_number');

        $data = [
            'Username' => $whatsoUsername, // Whatso username
            'Password' => $whatsoPassword, // Whatso password
            'MessageText' => $this->encodeURIComponent('*Hi TPP 99 Member!*

We invite you to attend our zoom training.  The "*'. $event_name. '*" training will be held on:

    *Date :* '.date_format(date_create($event_date), 'D, d M Y').'
    *Time :* '.$event_time.'
    *Meeting ID :* '.$meeting_id.'
    *Passcode* : '.$passcode.'

Save the date and dont miss it!
Lets success with TP99!

*NOTE:*** Unfortunately, if you cannot attend the training, your rank will be downgraded automatically a few days after the training day.

Regards,
*TP 99 Management*'),
            'MobileNumbers' => $user->phone, // Receiver number
            'ScheduleDate' => '',
            'FromNumber' => $fromNumber,
            'Channel' => '1'
        ];
        $json = json_encode($data);
        $url = $whatsoApi;
        $options = stream_context_create(['http' => [
                'method'  => 'POST',
                'header'  => ['Content-type: application/json', 'Accept: application/json'],
                'content' => $json
            ]
        ]);

        // Send a request
        $result = file_get_contents($url, false, $options);
    }

    function encodeURIComponent($str) {
        $revert = array('%0D%0A'=>'
        ','%20'=>' ','%21'=>'!', '%22'=>'"', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')', '%2C'=>',', '%3A'=>':');
        return strtr(rawurlencode($str), $revert);
    }
}

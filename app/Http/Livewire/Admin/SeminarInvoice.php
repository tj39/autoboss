<?php

namespace App\Http\Livewire\Admin;

use App\Models\RankRequest;
use App\Models\TraderRank;
use App\Models\User;
use App\Traits\GlobalValues;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class SeminarInvoice extends Component
{
    use GlobalValues;
    use WithPagination;
    use WithFileUploads;

    public $search1, $search2;
    public $pdfFile, $transferProofPath;
    public $viewClicked = false;
    public $uploadClicked = false;
    public User $user;

    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        if (auth()->user()->is_admin != '1') {
            abort(403);
        }

        $pageRow = $this->getGeneralSettingValue('pagination_row');

        return view('livewire.admin.seminar-invoice', [
            'invoices' => $this->retrieveInvoiceList()->paginate($pageRow),
            'ranks' => TraderRank::all()
        ]);
    }

    public function retrieveInvoiceList()
    {
        return DB::table('rank_request as rr')
            ->selectRaw("rr.id, ud.firstname, ud.lastname, ud.client_id, ud.nik, ud.no_npwp, concat(ud.street_adress, ' ', ud.city, ' ', ud.state) address")
            ->join('user_data as ud', 'rr.user_id', '=', 'ud.id')
            ->whereRaw("(ud.firstname like '%".$this->search1."%'")
            ->orWhereRaw("ud.lastname like '%".$this->search1."%')")
            ->where('rr.to_rank', 'like', '%'.$this->search2.'%')
            ->where('rr.status', 'like', 'PAID')
            ->orderBy('rr.created_at', 'asc');
    }

    public function showViewModal($id)
    {
        $this->viewClicked = true;
        $this->user = User::where('request_id', $id)->first();
        $this->transferProofPath = $this->user->requestId->transfer_proof;
        $this->emit('showViewModal');
    }

    public function resetViewModal()
    {
        $this->viewClicked = false;
        $this->user = new User();
        $this->transferProofPath = null;
    }

    public function showUploadModal($id)
    {
        $this->uploadClicked = true;
        $this->user = User::where('request_id', $id)->first();
        $this->emit('showUploadModal');
    }

    public function showModalConfirmation()
    {
        $maxUploadSize = $this->getGeneralSettingValue('max_pdf_size');
        $this->validate([
            'pdfFile' => "required|mimes:pdf|max:$maxUploadSize"
        ]);

        $this->emit('showModalConfirmation');
    }

    public function submit()
    {
        $nextRank = TraderRank::where('order', $this->user->role_id - 1)->first();
        $rankRequest = RankRequest::find($this->user->request_id);
        $rankRequest->invoice = $this->pdfFile->storeAs('training_invoice', $this->user->username . '/Invoice_' . $rankRequest->no_invoice . '.pdf');
        $rankRequest->status = 'INVOICE';
        $rankRequest->message = 'Your Rank has been upgraded to '.$nextRank->description;
        $rankRequest->save();

        $this->user->update([
            'role_id' => $nextRank->id
        ]);

        DB::statement("CALL GET_DIRECT_DOWNLINE_COUNT(" . $this->user->id . ")");
        DB::statement("CALL GET_DIRECT_DOWNLINE_COUNT(" . $this->user->direct_upline . ")");

        $this->emit('showAlert', ['msg' => 'Invoice has been uploaded.']);
        $this->emit('hideModalConfirmation');
        $this->emit('hideUploadModal');
    }

    public function resetFields()
    {
        $this->pdfFile = null;
    }
}

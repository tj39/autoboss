<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\GeneralSetting;
use App\Models\ActiveTicket;
use App\Models\ActiveComment;
use App\Models\ClosedTicket;
use App\Models\ClosedComment;
use Livewire\WithPagination;


class SupportTicketAdmin extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $ticketopencount;
    public $ticketsolvecount;
    public $ticketclosecount;
    public $ticketunreadcount;
    public $table;
    public $searchName;
    public $searchNameLast;
    public $searchTitle;

    public function mount($table){
        if($table == "open"){
            $this->table = "open";
        }
        else if($table == "close"){
            $this->table = "close";
        }
        else if($table == "solve"){
            $this->table = "solve";
        }
        else if($table == "unread"){
            $this->table = "unread";
        }
        else if($table == "all"){
            $this->table = "all";
        }
    }

    public function render()
    {
        $general_setting_pagination = GeneralSetting::where('code', 'pagination_row')->first()->value;

        if($this->table == "open"){
            $tickets = ActiveTicket::where('status_id', '1')
                                ->orderBy('is_read_admin', 'asc')
                                ->orderBy('created_date', 'desc');
        }
        elseif($this->table == "close"){
            $tickets = ActiveTicket::where('status_id', '2')
                                ->orderBy('is_read_admin', 'asc')
                                ->orderBy('created_date', 'desc');
        }
        elseif($this->table == "solve"){
            $tickets = ActiveTicket::where('status_id', '3')
                                ->orderBy('is_read_admin', 'asc')
                                ->orderBy('created_date', 'desc');
        }
        elseif($this->table == "unread"){
            $tickets = ActiveTicket::where('is_read_admin', 0)
                                ->orderBy('is_read_admin', 'asc')
                                ->orderBy('created_date', 'desc');
        }
        else if($this->table == "all"){
            $tickets = ActiveTicket::where('status_id', '3')
                                ->orWhere('status_id', '2')
                                ->orWhere('status_id', '1')
                                ->orderBy('is_read_admin', 'asc')
                                ->orderBy('created_date', 'desc');
        }
        else{
            $tickets = ActiveTicket::where('status_id', '3')
                                ->orWhere('status_id', '2')
                                ->orWhere('status_id', '1')
                                ->orderBy('is_read_admin', 'asc')
                                ->orderBy('created_date', 'desc');
        }

        if($this->searchName != null){
            $tickets = $tickets->whereRelation('user', 'firstname', 'LIKE', '%'.$this->searchName.'%')->orWhereRelation('user', 'lastname', 'LIKE', '%'.$this->searchName.'%');
        }

        if($this->searchTitle != null){
            $tickets = $tickets->where('title', 'LIKE' , '%'.$this->searchTitle.'%');
        }

        $this->ticketopencount = ActiveTicket::where('status_id', '1')->count();
        $this->ticketsolvecount = ActiveTicket::where('status_id', '2')->count();
        $this->ticketclosecount = ActiveTicket::where('status_id', '3')->count();
        $this->ticketunreadcount = ActiveTicket::where('is_read_admin', 0)->count();

        return view('livewire.admin.support-ticket-admin',[
            'tickets' => $tickets->paginate($general_setting_pagination),
            'ticketclosed' => $this->ticketclosecount
        ]);
    }

    public function queryTicket($table){
        if($table == "open"){
            $this->table = "open";
        }
        else if($table == "close"){
            $this->table = "close";
        }
        else if($table == "solve"){
            $this->table = "solve";
        }

        else if($table == "unread"){
            $this->table = "unread";
        }
    }

    public function archiveTicket(){
        $ticket_close = ClosedTicket::pluck('no_ticket')->all();
        $tiket = ActiveTicket::whereNotIn('no_ticket', $ticket_close)->where('status_id', 3)->get();
        $tiketCount = ActiveTicket::whereNotIn('no_ticket', $ticket_close)->where('status_id', 3)->count();

        foreach ($tiket as $tikets){
            ActiveTicket::find($tikets->id)->update([
                'completed_date' => Date(now())
            ]);

            ClosedTicket::create([
                'no_ticket' => $tikets->no_ticket,
                'created_date' => $tikets->created_date,
                'completed_date' => Date(now()),
                'status_id' => 3,
                'category_id' => $tikets->category_id,
                'user_id' => $tikets->user_id,
                'title' => $tikets->title,
                'message' => $tikets->message,
                'url_image' => $tikets->url_image
            ]);

            $comment = ActiveComment::where('ticket_id', $tikets->id)->where('status_id', 1)->get();

            if($comment != null){
                foreach($comment as $comments){
                    ActiveComment::where('ticket_id', $tikets->id)->update([
                        'status_id' => 3
                    ]);
        
                    ClosedComment::create([
                        'ticket_id' => $comments->ticket_id,
                        'user_id' => $comments->user_id,
                        'created_at' => $comments->created_date,
                        'message' => $comments->message,
                        'url_image' => $comments->url_image
                    ]);

                    $activeComment = ActiveComment::find($comments->id);
                    $activeComment->delete();
                }    
            }

            $activeTicket = ActiveTicket::find($tikets->id);
            $activeTicket->delete();
            
        }

        $this->emit('showAlert', ['msg' => $tiketCount.' archived']);
    }

    public function getCommentRead($id){
        $count = ActiveComment::where('ticket_id', $id)->where('is_read_admin', 0)->count();

        return $count;
    }
}

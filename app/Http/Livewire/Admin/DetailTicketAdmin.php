<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\ActiveTicket;
use App\Models\ActiveComment;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Mail\NotifTicket;
use Illuminate\Support\Facades\Mail;
use App\Models\GeneralSetting;
use App\Traits\GlobalValues;

class DetailTicketAdmin extends Component
{
    use GlobalValues;
    
    public $id_ticket;
    public $message;
    public $imageurl;
    public $page;
    public $table;

    public function mount($id, $page, $table){
        $this->id_ticket = $id;
        $this->page = $page;
        $this->table= $table;

        ActiveTicket::where('id', $id)->update([
            'is_read_admin' => 1
        ]);

        ActiveComment::where('ticket_id', $id)->update([
            'is_read_admin' => 1
        ]);
    }

    public function render()
    {
        $comment = ActiveComment::where('ticket_id', $this->id_ticket)->get();
        $ticket = ActiveTicket::find($this->id_ticket);

        return view('livewire.admin.detail-ticket-admin', [
            'comment' => $comment,
            'ticket' => $ticket
        ]);
    }

    public function showModalConfirmSolved(){
        $this->emit('showModalConfirmSolved');
    }

    public function showModalConfirmSend(){
        $this->validate([
            'message' => 'required'
        ]);

        $this->emit('showModalConfirmSend');
    }

    public function sendMessage(){
        $ticket = ActiveTicket::find($this->id_ticket);

        ActiveComment::create([
            'ticket_id' => $this->id_ticket,
            'user_id' => Auth::user()->id,
            'status_id' => $ticket->status_id,
            'created_date' => Date(now()),
            'message' => $this->message,
            'url_image' => $this->imageurl,
            'is_read_admin' => 1
        ]);

        ActiveTicket::where('id', $this->id_ticket)->update([
            'created_date' => Date(now()),
            'is_read_user' => 0
        ]);
        
        $user = User::where('id', $ticket->user_id)->first();

        if ($user->otp_verified_at == null){
            $genset = GeneralSetting::where('code', 'ticket_message')->first()->value;
            $details = [
                'title' => $genset,
            ];
           
            Mail::to($user->email)->send(new NotifTicket($details));
        }
        else{
            $this->send_whatsapp(); 
        }
        $this->hideModal();
        return redirect()->to('/admin/detail-ticket/'.$this->id_ticket.'/'.$this->page.'/'.$this->table);
    }

    public function hideModal(){
        $this->message = null;
        $this->imageurl = null;

        $this->emit('hideModal');
        $this->emit('showAlert', ['msg' => 'Message Sent']);
    }

    public function storeStatus(){
        ActiveTicket::where('id', $this->id_ticket)->update([
            'status_id' => 3
        ]);

        $this->hideModal();
        $this->emit('showAlert', ['msg' => 'Ticket Closed']);
        return redirect()->to('/admin/detail-ticket/'.$this->id_ticket.'/'.$this->page.'/'.$this->table);
    }

    public function send_whatsapp(){
        $ticket = ActiveTicket::where('id', $this->id_ticket)->first();
        $user = User::where('id', $ticket->user_id)->first();
        $genset = GeneralSetting::where('code', 'ticket_message')->first()->value;

        $whatsoApi = $this->getGeneralSettingValue('whatso_api');
        $whatsoUsername = $this->getGeneralSettingValue('whatso_username');
        $whatsoPassword = $this->getGeneralSettingValue('whatso_password');
        $fromNumber = $this->getGeneralSettingValue('tjl_phone_number');

        $data = [
            'Username' => $whatsoUsername, // Whatso username
            'Password' => $whatsoPassword, // Whatso password
            'MessageText' => $genset,
            'MobileNumbers' => $user->phone, // Receiver number
            'ScheduleDate' => '',
            'FromNumber' => $fromNumber,
            'Channel' => '1'
        ];
        $json = json_encode($data);
        $url = $whatsoApi;
        $options = stream_context_create(['http' => [
                'method'  => 'POST',
                'header'  => ['Content-type: application/json', 'Accept: application/json'],
                'content' => $json
            ]
        ]);

        // Send a request
        $result = file_get_contents($url, false, $options);
    }
}

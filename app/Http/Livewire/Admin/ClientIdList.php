<?php

namespace App\Http\Livewire\Admin;

use App\Traits\GlobalValues;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\TraderRank;

class ClientIdList extends Component
{
    use GlobalValues;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search1, $search2, $search3, $searchRank;

    public function updatingSearch1()
    {
        $this->resetPage();
    }

    public function updatingSearch2()
    {
        $this->resetPage();
    }

    public function updatingSearch3()
    {
        $this->resetPage();
    }

    public function render()
    {
        $pageRow = $this->getGeneralSettingValue('pagination_row');
        $rank = TraderRank::all();

        return view('livewire.admin.client-id-list', [
            'users' => $this->retrieveUserList()->paginate($pageRow),
            'ranks' => $rank
        ]);
    }

    public function retrieveUserList()
    {
        return DB::table('clientid_temp as ct')
            ->selectRaw('distinct ud.id, ud.created_at, concat(ud.firstname, " ", ud.lastname) as name, ud.client_id, concat(up.firstname, " ", up.lastname) as upline, GET_ROOT_UPLINE(ud.id) as root_upline, ud.step, tr.description, ud.role_id, ud.firstname')
            ->join('user_data as ud', 'ct.client_uid', '=', 'ud.client_id')
            ->join('user_data as up', 'ud.direct_upline', '=', 'up.id')
            ->join('trader_ranks as tr', 'ud.role_id', '=', 'tr.id')
            ->whereRaw("(ud.client_id like '%".$this->search1."%'")
            ->orWhereRaw("ud.firstname like '%".$this->search1."%'")
            ->orWhereRaw("ud.lastname like '%".$this->search1."%')")
            ->whereRaw("(up.client_id like '%".$this->search2."%'")
            ->orWhereRaw("up.firstname like '%".$this->search2."%'")
            ->orWhereRaw("up.lastname like '%".$this->search2."%')")
            ->where('ud.role_id', 'like', '%' . $this->searchRank . '%')
            ->groupByRaw('ud.id, ud.firstname, ud.lastname, ud.created_at, name, ud.client_id, up.firstname, up.lastname, upline, root_upline, ud.step, tr.description, ud.role_id')
            // ->whereRaw('GET_ROOT_UPLINE(ud.id) like ' . '%' . $this->search3 . '%')
            ->having('root_upline', 'like', '%' . $this->search3 . '%')
            ->orderBy('ud.firstname', 'asc');
    }
}

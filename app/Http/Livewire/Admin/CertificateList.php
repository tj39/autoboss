<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\CertificateData;
use Livewire\WithPagination;
use App\Traits\GlobalValues;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Livewire\WithFileUploads;

class CertificateList extends Component
{
    use WithPagination;
    use GlobalValues;
    use WithFileUploads;

    protected $paginationTheme = 'bootstrap';

    public $searchCode;
    public $excelFile;

    public function render()
    {
        $certificate = CertificateData::orderBy('created_at');

        $pageRow = $this->getGeneralSettingValue('pagination_row');

        if($this->searchCode != null){
            $certificate = $certificate->where('code', 'LIKE' , '%'.$this->searchCode.'%')->orWhere('name', 'LIKE' , '%'.$this->searchCode.'%');
        }
        
        return view('livewire.admin.certificate-list',[
            'certificate' => $certificate->paginate($pageRow)
        ]);
    }

    public function submit()
    {
        $this->validate([
            'excelFile' => 'required|mimes:csv,txt'
        ]);

        $dateStr = Carbon::now()->toDateString();
        $path = 'Excel/certificate-data/' . $dateStr;
        Storage::makeDirectory($path);
        $fileName = $this->excelFile->getClientOriginalName();
        $fileExists = Storage::exists($path . '/' . $fileName);
        
        if ($fileExists) {
            $fileName = str_replace('.csv', '', $this->excelFile->getClientOriginalName()) . '-' . substr(md5(str_shuffle('12345')), 0, 5) . '.csv';
        }
        
        $file = $this->excelFile->storeAs($path, $fileName);
        $filePath = str_replace('\\', '/', base_path()) . '/storage/app' . '/' . $file;

        $queryLoadData = "LOAD DATA LOCAL INFILE '$filePath' REPLACE
                    INTO TABLE certificate_data 
                    FIELDS TERMINATED BY ','
                    ENCLOSED BY ''
                    LINES TERMINATED BY '\n'
                    IGNORE 1 ROWS
                    (@created_at,title,@ignore,NAME,@achievement_date,TYPE,CODE,@ignore,@ignore,url_certificate,@ignore,@ignore,@ignore,@ignore)
                    SET created_at = CURRENT_TIMESTAMP, achievement_date = STR_TO_DATE(@achievement_date, '%d/%m/%Y');";

        DB::statement($queryLoadData);

        $this->resetFields();

        $this->emit('showAlert', ['msg' => 'File has been processed.']);
        $this->emit('hideModalUploadFile');
    }

    public function resetFields()
    {
        $this->excelFile = null;
    }
}

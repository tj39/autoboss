<?php

namespace App\Http\Livewire\Admin;

use App\Mail\CommissionPaid;
use App\Models\RebateFile;
use App\Models\RebatePayroll;
use App\Models\RebateSummary;
use App\Models\SprofitFile;
use App\Models\SprofitPayroll;
use App\Models\SprofitSummary;
use App\Models\User;
use App\Traits\GlobalValues;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use App\Models\TransferGateway;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet; 
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class CommissionTransfer extends Component
{
    use GlobalValues;
    use WithFileUploads;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['refreshData' => '$refresh'];

    public $commissionType, $code, $proofNumber, $codes, $transferType, $startDate, $endDate, $month;

    public function render()
    {
        if (auth()->user()->is_admin != 1) {
            abort(403);
        }

        $this->codes = $this->retrieveCodeCombo();

        $rebateProcess = RebateFile::where('is_calculated', '1')->where('is_settled', '0')->count();
        $isRebateEnable = 0;
        if ($rebateProcess > 0) {
            $isRebateEnable = 1;
        }

        $sProfitProcess = SprofitFile::where('is_calculated', '1')->where('is_settled', '0')->count();
        $isSProfitEnable = 0;
        if ($sProfitProcess > 0) {
            $isSProfitEnable = 1;
        }

        $pageRow = $this->getGeneralSettingValue('pagination_row');
        return view('livewire.admin.commission-transfer', [
            'sprofitPayrollList' => $this->retrieveSprofitPayrollList()->paginate($pageRow, ['*'], 'sprofitPage'),
            'rebatePayrollList' => $this->retrieveRebatePayrollList()->paginate($pageRow, ['*'], 'rebatePage'),
            'isRebateEnable' => $isRebateEnable,
            'isSProfitEnable' => $isSProfitEnable
        ]);
    }

    public function retrieveSprofitPayrollList()
    {
        return DB::table('sprofit_payroll')
            ->selectRaw('id, DATE_FORMAT(month, "%b %Y") as date, code, payroll_path, report_path, proof_number, is_final')
            ->orderByRaw('created_at desc');
    }

    public function retrieveRebatePayrollList()
    {
        return DB::table('rebate_payroll')
            ->selectRaw('id, DATE_FORMAT(start_date, "%d %b %Y") as start_date, DATE_FORMAT(end_date, "%d %b %Y") as end_date, code, payroll_path, report_path, proof_number, is_final, lots_all')
            ->orderByRaw('created_at desc');
    }

    public function retrieveCodeCombo()
    {
        $this->codes = [];

        if ($this->commissionType == '1') {
            $this->codes = DB::select("select code from rebate_payroll where is_final = '0'");
        } elseif ($this->commissionType == '2') {
            $this->codes = DB::select("select code from sprofit_payroll where is_final = '0'");
        }
        return $this->codes;
    }

    public function showModalConfirmation()
    {
        $this->validate([
            'commissionType' => 'required',
            'code' => 'required',
            'proofNumber' => 'required|max:50',
            'transferType' => 'required|max:10'
        ]);
        
        if ($this->commissionType == '1') {
            $this->validate([
                'startDate' => 'required',
                'endDate' => 'required'
            ]);
        }

        if ($this->commissionType == '2') {
            $this->validate([
                'month' => 'required'
            ]);
        }

        $this->emit('showModalConfirmation');
    }

    public function submit()
    {
        if ($this->commissionType == '1') {
            $rebatePayroll = RebatePayroll::where('code', $this->code)->first();
            
            if ('BCA' == $this->transferType) {
                $rebatePayroll->bca = $this->proofNumber;
            } else if ('LLG' == $this->transferType) {
                $rebatePayroll->llg = $this->proofNumber;
            } else if ('RTG' == $this->transferType) {
                $rebatePayroll->rtg = $this->proofNumber;
            } else if ('USDT' == $this->transferType) {
                $rebatePayroll->usdt = $this->proofNumber;
            }

            $rebatePayroll->start_date = date_create($this->startDate);
            $rebatePayroll->end_date = date_create($this->endDate);
            $rebatePayroll->save();

            $rebateSummaries = RebateSummary::where('payroll_id', $rebatePayroll->id)
                ->where('transfer_type', $this->transferType)
                ->get();
            foreach ($rebateSummaries as $rebateSummary) {
                if ($rebateSummary->transfer_proof == null || $rebateSummary->transfer_proof == '') {
                    $rebateSummary->transfer_proof = $this->proofNumber;
                    $rebateSummary->save();
    
                    $this->sendMessageToUser($rebateSummary->leader_id, $this->commissionType, $rebatePayroll);
                }
            }

            $countData = "SELECT COUNT(1) total FROM (
                            SELECT 	payroll_id,
                                transfer_type,
                                CASE 
                                    WHEN transfer_type = 'BCA' THEN sp.bca
                                    WHEN transfer_type = 'LLG' THEN sp.llg
                                    WHEN transfer_type = 'RTG' THEN sp.rtg
                                    WHEN transfer_type = 'USDT' THEN sp.usdt
                                    ELSE ''
                                END transfer_proof
                            FROM rebate_summary ss
                            JOIN rebate_payroll sp
                            ON ss.payroll_id = sp.id
                            WHERE is_exported = 3
                            GROUP BY payroll_id, transfer_type, sp.bca, sp.llg, sp.rtg, sp.usdt ) a
                        WHERE a.transfer_proof = '' OR a.transfer_proof IS NULL;";
            $result = DB::select($countData, []);

            if ($result[0]->total == 0) { // sudah ada proof number semua
                $rebatePayroll->is_final = '2';
                $rebatePayroll->save();

                $rebateFiles = RebateFile::where('payroll_code', $rebatePayroll->code)
                ->where('is_settled', '2')->get();
                foreach ($rebateFiles as $rebateFile) {
                    $rebateFile->is_settled = '1';
                    $rebateFile->save();
                }
            }
        } elseif ($this->commissionType == '2') {
            $sprofitPayroll = SprofitPayroll::where('code', $this->code)->first();
                 
            if ('BCA' == $this->transferType) {
                $sprofitPayroll->bca = $this->proofNumber;
            } else if ('LLG' == $this->transferType) {
                $sprofitPayroll->llg = $this->proofNumber;
            } else if ('RTG' == $this->transferType) {
                $sprofitPayroll->rtg = $this->proofNumber;
            } else if ('USDT' == $this->transferType) {
                $sprofitPayroll->usdt = $this->proofNumber;
            }
            
            $sprofitPayroll->month = date_create($this->month);
            $sprofitPayroll->save();
            
            $sprofitSummaries = SprofitSummary::where('payroll_id', $sprofitPayroll->id)
                ->where('transfer_type', $this->transferType)->get();
            foreach ($sprofitSummaries as $sprofitSummary) {
                if ($sprofitSummary->transfer_proof == null || $sprofitSummary->transfer_proof == '') {
                    $sprofitSummary->transfer_proof = $this->proofNumber;
                    $sprofitSummary->save();
    
                    $this->sendMessageToUser($sprofitSummary->leader_id, $this->commissionType, $sprofitPayroll);
                }
            }

            $countData = "SELECT COUNT(1) total FROM (
                            SELECT 	payroll_id,
                                transfer_type,
                                CASE 
                                    WHEN transfer_type = 'BCA' THEN sp.bca
                                    WHEN transfer_type = 'LLG' THEN sp.llg
                                    WHEN transfer_type = 'RTG' THEN sp.rtg
                                    WHEN transfer_type = 'USDT' THEN sp.usdt
                                    ELSE ''
                                END transfer_proof
                            FROM sprofit_summary ss
                            JOIN sprofit_payroll sp
                            ON ss.payroll_id = sp.id
                            WHERE is_exported = 3
                            GROUP BY payroll_id, transfer_type, sp.bca, sp.llg, sp.rtg, sp.usdt ) a
                        WHERE a.transfer_proof = '' OR a.transfer_proof IS NULL;";
            $result = DB::select($countData, []);

            if ($result[0]->total == 0) { // sudah ada proof number semua
                $sprofitPayroll->is_final = '2';
                $sprofitPayroll->save();

                $sprofitFiles = SprofitFile::where('payroll_code', $sprofitPayroll->code)
                ->where('is_settled', '2')->get();
                foreach ($sprofitFiles as $sprofitFile) {
                    $sprofitFile->is_settled = '1';
                    $sprofitFile->save();
                }
            }
        }

        // UPDATE SETTLED BALANCE
        Log::info("BEGIN: updating settled balance");
        $startTime = microtime(true);
        DB::statement('CALL UPDATE_SETTLED_BALANCE()');
        $endTime = microtime(true);
        $execTime = $endTime-$startTime;
        Log::info("END: updating settled balance ($execTime)");

        $this->resetFields();

        $this->emit('showAlert', ['msg' => 'Proof number updated successfully.']);
        $this->emit('resetPikaday');
        $this->emit('hideModalConfirmation');
        $this->emit('hideModalUploadFile');
        $this->emit('refreshData');
    }

    public function downloadFile($id, $type, $name)
    {
        if ($type == '1') {
            $rebatePayroll = RebatePayroll::find($id);
            if ($name == 'payroll') {
                $filePath = $rebatePayroll->payroll_path;
            } else {
                $filePath = $rebatePayroll->report_path;
            }
            return response()->download(storage_path('app/'.$filePath));
        } else {
            $sprofitPayroll = SprofitPayroll::find($id);
            if ($name == 'payroll') {
                $filePath = $sprofitPayroll->payroll_path;
            } else {
                $filePath = $sprofitPayroll->report_path;
            }
            return response()->download(storage_path('app/'.$filePath));
        }
    }

    public function sendMessageToUser($userId, $type, $payroll)
    {
        if ($type == '1') {
            $startDate = date_format($payroll->start_date, 'd/m/y');
            $endDate = date_format($payroll->end_date, 'd/m/y');
        } else {
            $mothYear = date_format($payroll->month, 'M Y');
        }
        
        $user = User::find($userId);
        if ($user->otp_verified_at != null) {
            Log::info("BEGIN: sending message to Whatsapp $user->phone");
            $whatsoApi = $this->getGeneralSettingValue('whatso_api');
            $whatsoUsername = $this->getGeneralSettingValue('whatso_username');
            $whatsoPassword = $this->getGeneralSettingValue('whatso_password');
            $fromNumber = $this->getGeneralSettingValue('tjl_phone_number');

            $destinationMsg = $this->transferType == 'USDT' ? 'USDT wallet' : 'bank account';

            if ($type == '1') {
                $msg = 'Hello, ' . $user->firstname . '. Your Rebate for period ' . $startDate . ' - ' . $endDate . ' has been transferred to your ' . $destinationMsg . '. —TP99';
            } else {
                $msg = 'Hello, ' . $user->firstname . '. Your Sharing Profit for ' . $mothYear . ' has been transferred to your ' . $destinationMsg . '. —TP99';
            }

            $data = [
                'Username' => $whatsoUsername, // Whatso username
                'Password' => $whatsoPassword, // Whatso password
                'MessageText' => $msg,
                'MobileNumbers' => $user->phone, // Receiver number
                'ScheduleDate' => '',
                'FromNumber' => $fromNumber,
                'Channel' => '1'
            ];
            $json = json_encode($data);
            $url = $whatsoApi;
            $options = stream_context_create(['http' => [
                    'method'  => 'POST',
                    'header'  => ['Content-type: application/json', 'Accept: application/json'],
                    'content' => $json
                ]
            ]);

            // Send a request
            $result = file_get_contents($url, false, $options);
            Log::info("END: sending message to Whatsapp $user->phone");
        } else {
            Log::info("BEGIN: adding email to queue $user->email");
            $destinationMsg = $this->transferType == 'USDT' ? 'USDT wallet' : 'bank account';
            if ($type == '1') {
                $msg = 'Your Rebate for period ' . $startDate . ' - ' . $endDate . ' has been transferred to your ' . $destinationMsg . '.';
                $subject = 'Rebate Transferred';
            } else {
                $msg = 'Your Sharing Profit for ' . $mothYear . ' has been transferred to your ' . $destinationMsg . '.';
                $subject = 'Sharing Profit Transferred';
            }

            $data = [
                'fullname' => $user->firstname . ' ' . $user->lastname,
                'message' => $msg,
            ];

            Mail::to($user->email)->queue(new CommissionPaid($data, $subject));
            Log::info("END: adding email to queue $user->email");
        }
    }

    public function resetFields()
    {
        $this->commissionType = null;
        $this->code = null;
        $this->proofNumber = null;
        $this->transferType = null;
    }

    public function refreshTab()
    {
        $this->emit('refreshData');
    }

    public function generateRebate() {
        // CALCULATE REBATE NET + PPH
        DB::statement('CALL CALCULATE_REBATE_NET()');
        
        // UPDATE COMMSSION BALANCE
        DB::statement('CALL UPDATE_COMMISSION_BALANCE()');
        
        // FILTER MIN PAYROLL TRANSFER
        $minTransfer = $this->getGeneralSettingValue('min_payroll');
        $queryUpdate1 = "UPDATE rebate_summary
                        SET is_exported = 4
                        WHERE (commission_net = 0 OR (commission_net-admin_fee) < $minTransfer)
                        AND is_exported = 2;";
        DB::statement($queryUpdate1);

        $queryUpdate2 = "UPDATE rebate_summary rs
                        JOIN user_data ud
                        ON rs.leader_id = ud.id
                        SET is_exported = 2
                        WHERE ((commission_net-admin_fee) >= $minTransfer AND NIK != '' AND NIK IS NOT NULL)
                        AND is_exported = 4;";
        DB::statement($queryUpdate2);

        // GENERATING EXCEl FILE
        $dateStr = Carbon::now()->toDateString();
        
        // Column headers
        $columns = array('Trx ID', 'Transfer Type', 'Beneficiary ID', 'Credited Account', 'Receiver Name', 
            'Amount', 'NIP', 'Remark', 'Beneficiary email', 'Swift Code', 'Cust Type', 'Cust Residence', 'No.Identity', 'NPWP', 'Gross', 'PPH', 'Account Name', 'Address');
        $usdtColumns = array('Wallet', 'Nominal');

        $path = 'Excel/rebate-payroll/' . $dateStr . '/';
        $fileName = 'rebate-payroll_' . $dateStr;

        $fileExists = Storage::exists($path . $fileName . '.xlsx');

        if ($fileExists) {
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
            $reader->setReadDataOnly(FALSE);

            $spreadsheet = $reader->load(storage_path('app/' . $path . $fileName . '.xlsx'));
            $sheet = $spreadsheet->getActiveSheet();
            $spreadsheet->setActiveSheetIndex(1);
            $sheet2 = $spreadsheet->getActiveSheet();
            $spreadsheet->setActiveSheetIndex(2);
            $sheet3 = $spreadsheet->getActiveSheet();
            $spreadsheet->setActiveSheetIndex(3);
            $sheet4 = $spreadsheet->getActiveSheet();
        } else {
            Storage::makeDirectory($path);
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            // Set header values
            $colIdx = 1;
            foreach($columns as $column) {
                $sheet->setCellValueByColumnAndRow($colIdx, 1, $column);
                $colIdx++;
            }
            $spreadsheet->getActiveSheet()->setTitle('BCA');

            $colIdx = 1;
            $sheet2 = $spreadsheet->createSheet();
            $spreadsheet->setActiveSheetIndex(1);
            foreach($columns as $column) {
                $sheet2->setCellValueByColumnAndRow($colIdx, 1, $column);
                $colIdx++;
            }
            $spreadsheet->getActiveSheet()->setTitle('LLG');

            $colIdx = 1;
            $sheet3 = $spreadsheet->createSheet();
            $spreadsheet->setActiveSheetIndex(2);
            foreach($columns as $column) {
                $sheet3->setCellValueByColumnAndRow($colIdx, 1, $column);
                $colIdx++;
            }
            $spreadsheet->getActiveSheet()->setTitle('RTG');

            $colIdx = 1;
            $sheet4 = $spreadsheet->createSheet();
            $spreadsheet->setActiveSheetIndex(3);
            foreach($usdtColumns as $column) {
                $sheet4->setCellValueByColumnAndRow($colIdx, 1, $column);
                $colIdx++;
            }
            $spreadsheet->getActiveSheet()->setTitle('USDT');
        }

        $highestRow = $sheet->getHighestRow();
        $highestRow2 = $sheet2->getHighestRow();
        $highestRow3 = $sheet3->getHighestRow();
        $highestRow4 = $sheet4->getHighestRow();

        // Processing rows
        $rowIdx = $highestRow + 1;
        $rowIdx2 = $highestRow2 + 1;
        $rowIdx3 = $highestRow3 + 1;
        $usdtRowIdx = $highestRow4 + 1;
        $rebateSummaries = RebateSummary::where('is_exported', '2')->get();
        foreach ($rebateSummaries as $rebateSummary) {
            if ($rebateSummary->leaderId->is_admin == '1') {
                continue;
            }
            $rebateSummary->is_exported = '3';
            $rebateSummary->save();

            $user = User::find($rebateSummary->leader_id);
            $transferGateway = TransferGateway::find($user->gateways_id);
            if ($transferGateway->method == 'BANK') {
                $gateway = 'BCA';
                if ($transferGateway->code != 'BCA' && $rebateSummary->commission_net <= 500000000) {
                    $gateway = 'LLG';
                } elseif ($transferGateway->code != 'BCA' && $rebateSummary->commission_net > 500000000) {
                    $gateway = 'RTG';
                }

                $rebateSummary->transfer_type = $gateway;
                $rebateSummary->save();

                $swiftCode = '';
                $custType = '';
                $custResidence = '';
                if ($gateway != 'BCA') {
                    $swiftCode = $transferGateway->swift_code;
                    $custType = '1';
                    $custResidence = $user->country == 'Indonesia' ? '1' : '2';
                }

                $row[$columns[0]] = $rebateSummary->code;
                $row[$columns[1]] = $gateway;
                $row[$columns[2]] = '';
                $row[$columns[3]] = "'".$user->no_account;
                $row[$columns[4]] = $user->account_holder;
                $row[$columns[5]] = $rebateSummary->commission_net - $rebateSummary->admin_fee;
                $row[$columns[6]] = '';
                $row[$columns[7]] = '';
                $row[$columns[8]] = '';
                $row[$columns[9]] = $swiftCode;
                $row[$columns[10]] = $custType;
                $row[$columns[11]] = $custResidence;
                $row[$columns[12]] = "'".$user->nik;
                $row[$columns[13]] = "'".$user->no_npwp;
                $row[$columns[14]] = $rebateSummary->commission_gross;
                $row[$columns[15]] = $rebateSummary->ppn;
                $row[$columns[16]] = $user->firstname.' '.$user->lastname;
                $row[$columns[17]] = $user->street_adress.' '.$user->city.' '.$user->state;

                $data = [];
                foreach ($columns as $value) {
                    array_push($data, $row[$value]);
                }
                
                if($gateway == "BCA"){
                    $colIdx = 1;
                    $spreadsheet->setActiveSheetIndex(0);
                    foreach ($data as $value) {
                        $sheet->setCellValueByColumnAndRow($colIdx, $rowIdx, $value);
                        $colIdx++;
                    }

                    $rowIdx++;
                }
                else if($gateway == "LLG"){
                    $colIdx = 1;
                    $spreadsheet->setActiveSheetIndex(1);
                    foreach ($data as $value) {
                        $sheet2->setCellValueByColumnAndRow($colIdx, $rowIdx2, $value);
                        $colIdx++;
                    }

                    $rowIdx2++;
                }
                else if($gateway == "RTG"){
                    // Set row values
                    $colIdx = 1;
                    $spreadsheet->setActiveSheetIndex(2);
                    foreach ($data as $value) {
                        $sheet3->setCellValueByColumnAndRow($colIdx, $rowIdx3, $value);
                        $colIdx++;
                    }

                    $rowIdx3++;
                }
            } elseif ($transferGateway->method == 'OTHER' && $transferGateway->code == 'USDT') {
                $gateway = 'USDT';
                $rebateSummary->transfer_type = $gateway;
                $rebateSummary->save();

                $row[$usdtColumns[0]] = "'".$user->no_account;
                $row[$usdtColumns[1]] = $rebateSummary->commission_usd;

                $data = [];
                foreach ($usdtColumns as $value) {
                    array_push($data, $row[$value]);
                }

                $colIdx = 1;
                $spreadsheet->setActiveSheetIndex(3);
                foreach ($data as $value) {
                    $sheet4->setCellValueByColumnAndRow($colIdx, $usdtRowIdx, $value);
                    $colIdx++;
                }

                $usdtRowIdx++;
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save(storage_path('app/' . $path . $fileName . '.xlsx'));

        $code = $this->generatePayrollCodeRebate($dateStr);
            
        // Insert rebate_payroll
        $rebatePayroll = null;
        if ($fileExists) {
            $rebatePayroll = RebatePayroll::where('payroll_name', $fileName)->first();
        } else {
            $rebatePayroll = new RebatePayroll();
            $rebatePayroll->created_at = Carbon::now();
            $rebatePayroll->code = strtoupper($code);
            $rebatePayroll->payroll_name = $fileName;
            $rebatePayroll->payroll_path = $path . $fileName . '.xlsx';
            $rebatePayroll->is_final = '0';
            $rebatePayroll->save();
        }

        // Update payroll_id of rebate_summary
        foreach ($rebateSummaries as $rebateSummary) {
            $rebateSummary->payroll_id = $rebatePayroll->id;
            $rebateSummary->save();
        }

        RebateFile::where('is_settled', '2')->update([
            'payroll_code' => $rebatePayroll->code
        ]);

        $countLots = "SELECT SUM(a.lots) total FROM (
            SELECT DISTINCT rda.`reward_order`, rda.volume_lots lots
                FROM rebate_payroll AS rp
                JOIN rebate_file AS rf
                ON rf.payroll_code = rp.code
                JOIN rebate_data AS rda
                ON rf.id = rda.file_id
                WHERE rp.id = " . $rebatePayroll->id ."
            ) a;";

        $result = DB::select($countLots, []);
        $rebatePayroll->lots_all = $result[0]->total;
        $rebatePayroll->save();

        $this->emit('showAlert', ['msg' => 'Generate Payroll File successfully.']);
        $this->emit('refreshData');
    }

    public function generatePayrollCodeRebate($dateStr)
    {
        $payrollCode = substr(str_shuffle('1234567890'), 0, 5) . $dateStr;

        return 'PRR' . substr(md5($payrollCode), 0, 5);
    }

    public function generateSProfit() {
        // CALCULATE SHARING PROFIT NET + PPH
        DB::statement('CALL CALCULATE_SPROFIT_NET()');
        
        // UPDATE COMMSSION BALANCE
        DB::statement('CALL UPDATE_COMMISSION_BALANCE()');
        
        // FILTER MIN PAYROLL TRANSFER
        $minTransfer = $this->getGeneralSettingValue('min_payroll');
        $queryUpdate1 = "UPDATE sprofit_summary ss
                        JOIN user_data ud
                        ON ss.leader_id = ud.id
                        SET is_exported = 4
                        WHERE ((commission_net-admin_fee) < $minTransfer AND is_exported = 2);";
        DB::statement($queryUpdate1);

        $queryUpdate2 = "UPDATE sprofit_summary ss
                        JOIN user_data ud
                        ON ss.leader_id = ud.id
                        SET is_exported = 2
                        WHERE ((commission_net-admin_fee) >= $minTransfer AND deposit_balance > 10 AND NIK != '' AND NIK IS NOT NULL) 
                        AND is_exported = 4;";
        DB::statement($queryUpdate2);
        
        // GENERATING EXCEl FILE
        $dateStr = Carbon::now()->toDateString();
        
        // Column headers
        $columns = array('Trx ID', 'Transfer Type', 'Beneficiary ID', 'Credited Account', 'Receiver Name', 
            'Amount', 'NIP', 'Remark', 'Beneficiary email', 'Swift Code', 'Cust Type', 'Cust Residence', 'No.Identity', 'NPWP', 'Gross', 'PPH', 'Account Name', 'Address');
        $usdtColumns = array('Wallet', 'Nominal');

        $path = 'Excel/sprofit-payroll/' . $dateStr . '/';
        $fileName = 'sprofit-payroll_' . $dateStr;
        
        $fileExists = Storage::exists($path . $fileName . '.xlsx');
        
        if ($fileExists) {
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
            $reader->setReadDataOnly(FALSE);
            
            $spreadsheet = $reader->load(storage_path('app/' . $path . $fileName . '.xlsx'));
            $sheet = $spreadsheet->getActiveSheet();
            $spreadsheet->setActiveSheetIndex(1);
            $sheet2 = $spreadsheet->getActiveSheet();
            $spreadsheet->setActiveSheetIndex(2);
            $sheet3 = $spreadsheet->getActiveSheet();
            $spreadsheet->setActiveSheetIndex(3);
            $sheet4 = $spreadsheet->getActiveSheet();
        } else {
            Storage::makeDirectory($path);
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            // Set header values
            $colIdx = 1;
            foreach($columns as $column) {
                $sheet->setCellValueByColumnAndRow($colIdx, 1, $column);
                $colIdx++;
            }
            $spreadsheet->getActiveSheet()->setTitle('BCA');

            $colIdx = 1;
            $sheet2 = $spreadsheet->createSheet();
            $spreadsheet->setActiveSheetIndex(1);
            foreach($columns as $column) {
                $sheet2->setCellValueByColumnAndRow($colIdx, 1, $column);
                $colIdx++;
            }
            $spreadsheet->getActiveSheet()->setTitle('LLG');

            $colIdx = 1;
            $sheet3 = $spreadsheet->createSheet();
            $spreadsheet->setActiveSheetIndex(2);
            foreach($columns as $column) {
                $sheet3->setCellValueByColumnAndRow($colIdx, 1, $column);
                $colIdx++;
            }
            $spreadsheet->getActiveSheet()->setTitle('RTG');

            $colIdx = 1;
            $sheet4 = $spreadsheet->createSheet();
            $spreadsheet->setActiveSheetIndex(3);
            foreach($usdtColumns as $column) {
                $sheet4->setCellValueByColumnAndRow($colIdx, 1, $column);
                $colIdx++;
            }
            $spreadsheet->getActiveSheet()->setTitle('USDT');
        }

        $highestRow = $sheet->getHighestRow();
        $highestRow2 = $sheet2->getHighestRow();
        $highestRow3 = $sheet3->getHighestRow();
        $highestRow4 = $sheet4->getHighestRow();

        // Processing rows
        $rowIdx = $highestRow + 1;
        $rowIdx2 = $highestRow2 + 1;
        $rowIdx3 = $highestRow3 + 1;
        $usdtRowIdx = $highestRow4 + 1;
        $sprofitSummaries = SprofitSummary::where('is_exported', '2')->get();
        foreach ($sprofitSummaries as $sprofitSummary) {
            if ($sprofitSummary->leaderId->is_admin == '1') {
                continue;
            }
            $sprofitSummary->is_exported = '3';
            $sprofitSummary->save();

            $user = User::find($sprofitSummary->leader_id);
            $transferGateway = TransferGateway::find($user->gateways_id);
            if ($transferGateway->method == 'BANK') {
                $gateway = 'BCA';
                if ($transferGateway->code != 'BCA' && $sprofitSummary->commission_net <= 500000000) {
                    $gateway = 'LLG';
                } elseif ($transferGateway->code != 'BCA' && $sprofitSummary->commission_net > 500000000) {
                    $gateway = 'RTG';
                }

                $sprofitSummary->transfer_type = $gateway;
                $sprofitSummary->save();

                $swiftCode = '';
                $custType = '';
                $custResidence = '';
                if ($gateway != 'BCA') {
                    $swiftCode = $transferGateway->swift_code;
                    $custType = '1';
                    $custResidence = $user->country == 'Indonesia' ? '1' : '2';
                }

                $row[$columns[0]] = $sprofitSummary->code;
                $row[$columns[1]] = $gateway;
                $row[$columns[2]] = '';
                $row[$columns[3]] = "'".$user->no_account;
                $row[$columns[4]] = $user->account_holder;
                $row[$columns[5]] = $sprofitSummary->commission_net - $sprofitSummary->admin_fee;
                $row[$columns[6]] = '';
                $row[$columns[7]] = '';
                $row[$columns[8]] = '';
                $row[$columns[9]] = $swiftCode;
                $row[$columns[10]] = $custType;
                $row[$columns[11]] = $custResidence;
                $row[$columns[12]] = "'".$user->nik;
                $row[$columns[13]] = "'".$user->no_npwp;
                $row[$columns[14]] = $sprofitSummary->commission_gross;
                $row[$columns[15]] = $sprofitSummary->ppn;
                $row[$columns[16]] = $user->firstname.' '.$user->lastname;
                $row[$columns[17]] = $user->street_adress.' '.$user->city.' '.$user->state;

                $data = [];
                foreach ($columns as $value) {
                    array_push($data, $row[$value]);
                }

                if($gateway == "BCA"){
                    $colIdx = 1;
                    $spreadsheet->setActiveSheetIndex(0);
                    foreach ($data as $value) {
                        $sheet->setCellValueByColumnAndRow($colIdx, $rowIdx, $value);
                        $colIdx++;
                    }

                    $rowIdx++;
                }
                else if($gateway == "LLG"){
                    $colIdx = 1;
                    $spreadsheet->setActiveSheetIndex(1);
                    foreach ($data as $value) {
                        $sheet2->setCellValueByColumnAndRow($colIdx, $rowIdx2, $value);
                        $colIdx++;
                    }

                    $rowIdx2++;
                }
                else if($gateway == "RTG"){
                    // Set row values
                    $colIdx = 1;
                    $spreadsheet->setActiveSheetIndex(2);
                    foreach ($data as $value) {
                        $sheet3->setCellValueByColumnAndRow($colIdx, $rowIdx3, $value);
                        $colIdx++;
                    }

                    $rowIdx3++;
                }
            } elseif ($transferGateway->method == 'OTHER' && $transferGateway->code == 'USDT') {
                $gateway = 'USDT';
                $sprofitSummary->transfer_type = $gateway;
                $sprofitSummary->save();

                $row[$usdtColumns[0]] = "'".$user->no_account;
                $row[$usdtColumns[1]] = $sprofitSummary->commission_usd;

                $data = [];
                foreach ($usdtColumns as $value) {
                    array_push($data, $row[$value]);
                }

                $colIdx = 1;
                $spreadsheet->setActiveSheetIndex(3);
                foreach ($data as $value) {
                    $sheet4->setCellValueByColumnAndRow($colIdx, $usdtRowIdx, $value);
                    $colIdx++;
                }

                $usdtRowIdx++;
            }
        } 

        $writer = new Xlsx($spreadsheet);
        $writer->save(storage_path('app/' . $path . $fileName . '.xlsx'));

        $code = $this->generatePayrollCodeSProfit($dateStr);

        // Insert sprofit_payroll
        $sprofitPayroll = null;
        if ($fileExists) {
            $sprofitPayroll = SprofitPayroll::where('payroll_name', $fileName)->first();
        } else {
            $sprofitPayroll = new SprofitPayroll();
            $sprofitPayroll->created_at = Carbon::now();
            $sprofitPayroll->code = strtoupper($code);
            $sprofitPayroll->payroll_name = $fileName;
            $sprofitPayroll->payroll_path = $path . $fileName . '.xlsx';
            $sprofitPayroll->is_final = '0';
            $sprofitPayroll->save();
        }
        
        // Update payroll_id
        foreach ($sprofitSummaries as $sprofitSummary) {
            $sprofitSummary->payroll_id = $sprofitPayroll->id;
            $sprofitSummary->save();
        }

        SprofitFile::where('is_settled', '2')->update([
            'payroll_code' => $sprofitPayroll->code
        ]);
 
        $this->emit('showAlert', ['msg' => 'Generate Payroll File successfully.']);
        $this->emit('refreshData');
    }

    public function generatePayrollCodeSProfit($dateStr)
    {
        $payrollCode = substr(str_shuffle('1234567890'), 0, 5) . $dateStr;

        return 'PRS' . substr(md5($payrollCode), 0, 5);
    }
}

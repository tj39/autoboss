<?php

namespace App\Http\Livewire\Admin;

use App\Models\PortofolioFile;
use App\Models\RankRequest;
use App\Models\TraderRank;
use App\Models\User;
use App\Traits\GlobalValues;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class RankApproval extends Component
{
    use GlobalValues;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $search = '';
    public $portofolioModalClicked = false;
    public $rankUpgradeModalClicked = false;
    public $viewPortofolio = false;
    public $name, $userId, $rankRequestId, $message, $filePath, $result;
    public $file = [];

    public function render()
    {
        $pageRow = $this->getGeneralSettingValue('pagination_row');
        $pageRowModal = $this->getGeneralSettingValue('pagination_modal');

        $portofolios = null;
        if ($this->portofolioModalClicked) {
            $portofolios = $this->retrievePortofolioList()->paginate($pageRowModal, ['*'], 'portofolio');
        }

        $files = null;
        if ($this->rankUpgradeModalClicked) {
            $files = PortofolioFile::where('user_id', $this->userId)
                ->where('request_id', $this->rankRequestId)
                ->where('is_accept', '2')->get();
        }

        return view('livewire.admin.rank-approval', [
            'rankApprovals' => $this->retrieveRankApprovalList()->paginate($pageRow, ['*'], 'rankApproval'),
            'portofolios' => $portofolios,
            'files' => $files
        ]);
    }

    public function retrieveRankApprovalList()
    {
        return DB::table('rank_request as rr')
            ->selectRaw('rr.id, rr.updated_at, ud.client_id, ud.firstname, ud.lastname, rr.from_rank, rr.to_rank, rr.user_id')
            ->join('user_data as ud', 'rr.user_id', '=', 'ud.id')
            ->where('rr.status', 'PENDING')
            ->whereRaw("(ud.firstname like '%$this->search%'"
                . "or ud.lastname like '%$this->search%'"
                . "or ud.client_id like '%$this->search%')")
            ->orderBy('updated_at', 'desc');
    }

    public function retrievePortofolioList()
    {
        $user = User::find($this->userId);
        $this->name = $user->firstname . ' ' . $user->lastname;
        return PortofolioFile::where('request_id', $this->rankRequestId)
            ->where('user_id', $this->userId);
    }

    public function submit()
    {
        if ($this->result == '1') {
            $user = User::find($this->userId);
            $rankRequest = RankRequest::find($this->rankRequestId);
            
            if($user->request_id == null || $user->seminar_status == 'EXPIRED') {
                $nextRank = TraderRank::find($user->role_id - 1);
                $user->role_id = $nextRank->id;
                $rankRequest->message = 'Your Rank has been upgraded to ' . $nextRank->description . '.';
            } else {
                if ($user->seminar_status == 'REJECTED' && $user->request_id != null) {
                    $rank = TraderRank::find($user->role_id-1);
                } else {
                    $rank = TraderRank::find($user->role_id);
                }
                $rankRequest->message = 'Congratulation! now you are officially as a ' . $rank->description . ' achiever.';
                $user->role_id = $rank->id;
            }
            
            $rankRequest->status = 'ACCEPTED';
            $rankRequest->is_dismissed = '0';
            $rankRequest->updated_at = Carbon::now();
            $rankRequest->save();

            foreach ($this->file as $item) {
                $portofolioFile = PortofolioFile::find($item);
                $portofolioFile->is_accept = '1';
                $portofolioFile->save();
            }
            
            $user->seminar_id = null;
            $user->seminar_status = null;
            $user->request_id = null;
            $user->save();

            DB::statement('CALL GET_DIRECT_DOWNLINE_COUNT(?)', [$user->id]);
            DB::statement('CALL GET_DIRECT_DOWNLINE_COUNT(?)', [$user->direct_upline]);

            $msg = 'Rank request has been accepted.';
        } else {
            foreach ($this->file as $item) { 
                $portofolioFile = PortofolioFile::find($item);
                $portofolioFile->is_accept = '0';
                $portofolioFile->save();
            }

            $rankRequest = RankRequest::find($this->rankRequestId);
            $rankRequest->status = 'REJECTED';
            $rankRequest->message = $this->message;
            $rankRequest->updated_at = Carbon::now();
            $rankRequest->save();

            $rankBefore = TraderRank::where('code', $rankRequest->from_rank)->first();

            $user = User::find($this->userId);
            $user->role_id = $rankBefore->id;

            if ($user->request_id != null) {
                $user->seminar_status = "REJECTED";
            }
            $user->save();

            DB::statement('CALL GET_DIRECT_DOWNLINE_COUNT(?)', [$user->id]);
            DB::statement('CALL GET_DIRECT_DOWNLINE_COUNT(?)', [$user->direct_upline]);

            $msg = 'Rank request has been rejected.';
        }

        $this->emit('showAlert', ['msg' => $msg]);
        $this->emit('hideModalConfirmation');
        $this->emit('hideRankUpgradeModal');
    }

    public function showPortofolioModal($userId, $rankRequestId)
    {
        $this->portofolioModalClicked = true;
        $this->userId = $userId;
        $this->rankRequestId = $rankRequestId;

        $this->emit('showPortofolioModal');
    }

    public function showRankUpgradeModal($userId, $rankRequestId)
    {
        $this->rankUpgradeModalClicked = true;
        $this->userId = $userId;
        $this->rankRequestId = $rankRequestId;
        $user = User::find($userId);
        $this->name = $user->firstname . ' ' . $user->lastname;

        $this->emit('showRankUpgradeModal');
    }

    public function showPdfModal($id)
    {
        $this->viewPortofolio = true;
        $this->filePath = PortofolioFile::find($id)->file_path;

        $this->emit('showPdfModal');
    }

    public function showModalConfirmation($result)
    {
        $this->validate([
            'file' => 'required'
        ]);

        if ($result == '0') {
            $this->validate([
                'message' => 'required'
            ]);
        }

        $this->result = $result;

        $this->emit('showModalConfirmation');
    }

    public function resetFields()
    {
        $this->file = null;
        $this->message = null;
        $this->rankUpgradeModalClicked = false;
        $this->emit('resetFields');
    }
}

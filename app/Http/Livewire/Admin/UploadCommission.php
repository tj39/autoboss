<?php

namespace App\Http\Livewire\Admin;

use App\Models\GeneralSetting;
use App\Models\RebateFile;
use App\Models\SprofitData;
use App\Models\SprofitFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class UploadCommission extends Component
{
    use WithFileUploads;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['refreshData' => '$refresh'];
    
    protected $columnsQuery = 'id, DATE_FORMAT(created_at, "%d %b %Y") as date, file_name, is_inserted, is_calculated, is_settled, payroll_code';

    public $commissionType;
    public $excelFile;
    public $isPaid;
    public $isSprofitUploadDisabled = false;
    public $isRebateUploadDisabled = false;

    public function render()
    {
        $this->isSprofitUploadDisabled = SprofitFile::where('is_settled', '2')->first() != null;
        $this->isRebateUploadDisabled = RebateFile::where('is_settled', '2')->first() != null;
        $pageRow = GeneralSetting::where('code', 'pagination_row')->get()->first()->value;
        return view('livewire.admin.upload-commission', [
            'sprofitFileList' => $this->retrieveSprofitFileList()->paginate($pageRow, ['*'], 'sprofitPage'),
            'rebateFileList' => $this->retrieveRebateFileList()->paginate($pageRow, ['*'], 'rebatePage')
        ]);
    }

    public function retrieveSprofitFileList()
    {
        return DB::table('sprofit_file')
            ->selectRaw($this->columnsQuery)
            ->orderByRaw('created_at desc');
    }

    public function retrieveRebateFileList()
    {
        return DB::table('rebate_file')
            ->selectRaw($this->columnsQuery)
            ->orderByRaw('created_at desc');
    }

    public function showModalUploadFile()
    {
        $this->isSprofitUploadDisabled = SprofitFile::where('is_settled', '2')->first() != null;
        $this->isRebateUploadDisabled = RebateFile::where('is_settled', '2')->first() != null;
    }

    public function submit()
    {
        $this->isSprofitUploadDisabled = SprofitFile::where('is_settled', '2')->first() != null;
        $this->isRebateUploadDisabled = RebateFile::where('is_settled', '2')->first() != null;

        if ($this->commissionType == '1') {
            if ($this->isRebateUploadDisabled) {
                $this->emit('showAlertInfo', ['Upload can not be processed. There might be file being calculated.']);
                $this->emit('hideModalUploadFile');
                $this->emit('refreshData');
                return;
            }

            $rebateFile = new RebateFile();
            $rebateFile->file_name = $this->excelFile->getClientOriginalName();

            $excelFile = $this->excelFile->storeAs('CSV', 'rebate/' . Carbon::now()->toDateString() . '/' . $this->excelFile->getClientOriginalName());
            $rebateFile->file_path = $excelFile;
            $rebateFile->is_inserted = '0';
            $rebateFile->is_calculated = '0';
            $rebateFile->is_settled = '0';

            $rebateFile->save();
        } elseif ($this->commissionType == '2') {
            if ($this->isSprofitUploadDisabled) {
                $this->emit('showAlertInfo', ['Upload can not be processed. There might be file being calculated.']);
                $this->emit('hideModalUploadFile');
                $this->emit('refreshData');
                return;
            }

            $sprofitFile = new SprofitFile();
            $sprofitFile->file_name = $this->excelFile->getClientOriginalName();

            $excelFile = $this->excelFile->storeAs('CSV', 'sprofit/' . Carbon::now()->toDateString() . '/' . $this->excelFile->getClientOriginalName());
            $sprofitFile->file_path = $excelFile;
            $sprofitFile->is_inserted = '0';
            $sprofitFile->is_calculated = '0';
            $sprofitFile->is_settled = '0';
            $sprofitFile->is_paid = $this->isPaid == '1' ? $this->isPaid : '0';
            
            $sprofitFile->save();
        }

        $this->resetFields();
        $this->emit('showAlert', ['File uploaded successfully.']);
        $this->emit('hideModalUploadFile');
        $this->emit('refreshData');
    }

    public function downloadCsv($id, $type)
    {
        if ($type == '1') {
            $rebateFile = RebateFile::find($id);
            return response()->download(storage_path('app/'.$rebateFile->file_path));
        } else {
            $sprofitFile = SprofitFile::find($id);
            return response()->download(storage_path('app/'.$sprofitFile->file_path));
        }
    }

    public function resetFields()
    {
        $this->commissionType = null;
        $this->isPaid = null;
        $this->excelFile = null;
    }

    public function refreshTab()
    {
        $this->emit('refreshData');
    }
}

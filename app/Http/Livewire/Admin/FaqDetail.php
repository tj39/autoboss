<?php

namespace App\Http\Livewire\Admin;

use App\Models\FaqCategory;
use App\Models\FaqList;
use Carbon\Carbon;
use Livewire\Component;

class FaqDetail extends Component
{
    public $categoryId, $page, $search, $mode, $questionId, $question, $answer, $isActive;
    public $editClicked = false;
    public $submit = false;

    public function mount($id, $page)
    {
        $this->categoryId = $id;
        $this->page = $page;
    }

    public function render()
    {
        return view('livewire.admin.faq-detail', [
            'questions' => $this->retrieveQuestionList(),
            'categoryName' => FaqCategory::find($this->categoryId)->name
        ]);
    }

    public function retrieveQuestionList()
    {
        return FaqList::query()
            ->where('category_id', $this->categoryId)
            ->whereRaw("(question like '%".$this->search."%'")
            ->orWhereRaw("answer like '%".$this->search."%')")
            ->orderBy('is_active', 'desc')
            ->orderBy('question', 'asc')
            ->get();
    }

    public function showEditModal($mode, $id)
    {
        $this->editClicked = true;
        $this->mode = $mode;
        if ($mode == 'edit') {
            $faqList = FaqList::findOrFail($id);
            $this->questionId = $id;
            $this->question = $faqList->question;
            $this->answer = $faqList->answer;
            $this->isActive = $faqList->is_active;
        }

        $this->emit('showModalEdit');
        $this->emit('initEditor');
    }

    public function showModalConfirmation()
    {
        $this->submit = true;
        $this->validate([
            'question' => 'required',
            'answer' => 'required'
        ]);

        $this->emit('showModalConfirmation');
    }

    public function showModalConfirmationDelete($id)
    {
        $this->mode = 'delete';
        $this->questionId = $id;
        $this->emit('showModalConfirmation');
    }

    public function submit()
    {
        $msg = '';
        if ($this->mode == 'add') {
            FaqList::create([
                'created_at' => Carbon::now(),
                'question' => $this->question,
                'answer' => $this->answer,
                'is_active' => $this->isActive ? '1' : '0',
                'category_id' => $this->categoryId
            ]);
            $msg = 'Question has been added.';
        } elseif ($this->mode == 'edit') {
            FaqList::find($this->questionId)->update([
                'question' => $this->question,
                'answer' => $this->answer,
                'is_active' => $this->isActive ? '1' : '0'
            ]);
            $msg = 'Question has been updated.';
        } elseif ($this->mode == 'delete') {
            FaqList::find($this->questionId)->delete();
            $msg = 'Question has been deleted.';
        }

        $this->resetFields();
        $this->emit('showAlert', ['msg' => $msg]);
        $this->emit('hideModalConfirmation');
        $this->emit('hideModalEdit');
        $this->emit('destroyBackdrop');
    }

    public function resetFields()
    {
        $this->question = null;
        $this->isActive = null;
        $this->answer = null;
        $this->editClicked = false;
        $this->submit = false;
        $this->emit('destroyEditor');
    }
}

<?php

namespace App\Http\Livewire\Admin;

use App\Models\TaxFile;
use App\Traits\GlobalValues;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class UploadTaxSlip extends Component
{
    use GlobalValues;
    use WithFileUploads;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $folderName, $category, $csvFile, $mode;
    public TaxFile $taxFile;

    public function render()
    {
        if (auth()->user()->is_admin != 1) {
            abort(403);
        }

        $pageRow = $this->getGeneralSettingValue('pagination_row');
        return view('livewire.admin.upload-tax-slip', [
            'taxFileList' => TaxFile::query()->orderBy('created_at', 'desc')->paginate($pageRow)
        ]);
    }

    public function showUploadModal($id, $mode)
    {
        $this->mode = $mode;
        if ($mode == 'edit') {
            $this->taxFile = TaxFile::find($id);
            $this->folderName = $this->taxFile->folder_name;
            $this->category = $this->taxFile->category;
        }
        $this->emit('showModalUploadFile');
    }

    public function showModalConfirmation()
    {
        $this->validate([
            'folderName' => 'required|max:36'
        ]);
        if ($this->mode == 'add') {
            $this->validate([
                'csvFile' => 'required|mimes:txt,csv'
            ]);
        }

        $this->emit('showModalConfirmation');
    }

    public function submit()
    {
        if ($this->mode == 'add') {
            $csvFile = $this->csvFile->storeAs('CSV', 'tax_file/' . Carbon::now()->toDateString() . '/' . $this->csvFile->getClientOriginalName());
            $taxFile = TaxFile::create([
                'folder_name' => $this->folderName,
                'category' => empty($this->category) ? null : $this->category,
                'created_at' => Carbon::now()
            ]);

            $filePath = str_replace('\\', '/', base_path()) . '/storage/app' . '/' . $csvFile;

            $query = "LOAD DATA LOCAL INFILE '$filePath'
                INTO TABLE tax_slip
                FIELDS TERMINATED BY ','
                ENCLOSED BY ''
                LINES TERMINATED BY '\n'
                IGNORE 1 ROWS
                (file_code, @ignore, @ignore, @no_identity, @npwp, commission_gross, pph, @file_id)
                SET no_identity = REPLACE(@no_identity,'\'',''), npwp = REPLACE(@npwp,'\'',''), file_id = $taxFile->id;";

            DB::statement($query);
        } else {
            $this->taxFile->update([
                'folder_name' => $this->folderName,
                'category' => empty($this->category) ? null : $this->category,
                'created_at' => Carbon::now()
            ]);
        }

        $this->resetFields();
        $this->emit('showAlert', ['msg' => 'File has been uploaded']);
        $this->emit('hideModalConfirmation');
        $this->emit('hideModalUploadFile');
    }

    public function resetFields()
    {
        $this->folderName = null;
        $this->category = null;
        $this->csvFile = null;
    }
}

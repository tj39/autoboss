<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\ActiveTicket;
use Illuminate\Support\Facades\Auth;
use App\Models\GeneralSetting;
use Livewire\WithPagination;
use App\Models\ActiveComment;

class SupportTicket extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        $general_setting_pagination = GeneralSetting::where('code', 'pagination_row')->first()->value;

        $tickets = ActiveTicket::where('user_id', Auth::user()->id)
                                    ->whereIn('status_id', [1,2,3])
                                    ->orderBy('created_date', 'desc')
                                    ->paginate($general_setting_pagination);

        return view('livewire.support-ticket', [
            'tickets' => $tickets
        ]);
    }

    public function getCommentRead($id){
        $count = ActiveComment::where('ticket_id', $id)->where('is_read_user', 0)->count();

        return $count;
    }
}

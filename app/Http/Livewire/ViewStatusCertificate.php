<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\CertificateData;

class ViewStatusCertificate extends Component
{
    public $certificate;
    public $certificateCount;

    public function mount($code){
        $certificate = CertificateData::where('code', $code);

        $this->certificate = $certificate->first();
        $this->certificateCount = $certificate->count();
    }

    public function render()
    {
        return view('livewire.view-status-certificate')->layout('layouts.base');
    }
}

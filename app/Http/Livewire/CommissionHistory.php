<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\GeneralSetting;
use App\Models\SprofitDetail;
use App\Models\RebateDetail;
use App\Models\SprofitSummary;
use App\Models\RebateSummary;
use Illuminate\Support\Facades\Auth;
use PDF;
use Illuminate\Support\Facades\Storage;
use Livewire\WithPagination;

class CommissionHistory extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';


    public $table = "profit";
    public $detail_click = false;
    public $summary_id;
    public $summary_code;

    public function render()
    {
        $pagination = GeneralSetting::where('code', 'pagination_row')->first()->value;
        $rebate_summary = RebateSummary::where('leader_id', Auth::user()->id)->orderBy('created_at', 'DESC')->paginate($pagination, ['*'], 'rebate');
        $sprofit_summary = SprofitSummary::where('leader_id', Auth::user()->id)->orderBy('created_at', 'DESC')->paginate($pagination, ['*'], 'sprofit');
        $sharing_profit = SprofitSummary::where('leader_id', Auth::user()->id)->where('transfer_proof', null)->sum('commission_usd');
        $rebate_profit = RebateSummary::where('leader_id', Auth::user()->id)->where('transfer_proof', null)->sum('commission_usd');

        $paginationModal = GeneralSetting::where('code', 'pagination_modal')->first()->value;
        if ($this->table == "profit"){
            $details = SprofitDetail::where('summary_id', $this->summary_id)->orderBy('leader_generation', 'ASC')->paginate($paginationModal, ['*'], 'commentsPageDetails');
        }
        else{
            $details = RebateDetail::where('summary_id', $this->summary_id)->paginate($paginationModal, ['*'], 'commentsPageRebate');
        }

        return view('livewire.commission-history', [
            'rebate_summary' => $rebate_summary,
            'sprofit_summary' => $sprofit_summary,
            'details' => $details,
            'sharing_profit' => $sharing_profit,
            'rebate_profit' => $rebate_profit
        ]);
    }

    public function changeTable($table){
        if ($table == "profit"){
            $this->table = "profit";
        }
        else{
            $this->table = "rebate";
        }
    }

    public function showModalDetail($id){
        $this->detail_click = true;
        $this->summary_id = $id;

        if ($this->table == "profit"){
            $summary = SprofitSummary::where('id', $id)->first();
        }
        else{
            $summary = RebateSummary::where('id', $id)->first();
        }
        

        $this->summary_code = $summary->code;
        $this->emit('showModalDetail');
    }

    public function exportPdf($id)
    {
        // dd($this->pdfData);
        if ($this->table == "profit"){
            $summary = SprofitSummary::where('id', $id)->first();
            $details = SprofitDetail::where('summary_id', $id)->orderBy('leader_generation', 'ASC')->get();
            $file_name = 'SHARINGPROFIT_'.$summary->code.'_'.Date('M Y').'.pdf';
        }
        else{
            $summary = RebateSummary::where('id', $id)->first();
            $details = RebateDetail::where('summary_id', $id)->get();
            $file_name = 'REBATE_'.$summary->code.'_'.Date('d M Y').'.pdf';
        }

        if($summary->file_path == null){
            $data = [
                'summary' => $summary,
                'details' => $details,
                'table' => $this->table 
            ];
    
            $pdf = PDF::loadview('commission-history-pdf', $data)->setPaper('A4', 'landscape');
    
            Storage::put('COMMISSIONHISTORY/'.$file_name,$pdf->output());

            if ($this->table == "profit"){
                SprofitSummary::where('id', $id)->update([
                    'file_path' => $file_name
                ]);
            }

            else{
                RebateSummary::where('id', $id)->update([
                    'file_path' => $file_name
                ]);
            }
            
            return response()->streamDownload(
                fn() => print($pdf->output()),
                $file_name
            );

        }

        else{
            
            $destination = storage_path('app/COMMISSIONHISTORY/'); 
            $filename = $summary->file_path;
            $pathToFile = $destination.$filename;
            return response()->download($pathToFile, $file_name); 
        }
        
    }
}

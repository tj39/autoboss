<?php

namespace App\Http\Livewire;

use App\Models\GeneralSetting;
use App\Models\TransferGateway;
use App\Models\User;
use App\Models\WithdrawData;
use PDF;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithPagination;

class Withdraw extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['refreshData' => '$refresh'];

    public $user;
    public $balance;
    public $gateway;
    public $amount;
    public $source;
    public $account;
    public $agreement;
    public $password;
    public $minWithdraw;
    public $rate;
    public $pdfData;
    public $currPage;

    public function render()
    {
        $this->user = User::find(auth()->user()->id);
        $this->minWithdraw = GeneralSetting::where('code', 'min_withdraw')->get()->first()->value;
        if ($this->source == null || $this->source == 1) {
            $this->balance = $this->user->commission_balance;
        } else {
            // TODO MT4 balance
            $this->balance = 0;
        }

        $pageRow = GeneralSetting::where('code', 'pagination_row')->get()->first()->value;
        $this->rate = 0;
        $historyData = $this->getHistory()->paginate($pageRow);
        $this->pdfData = $historyData->items();
        $this->currPage = $historyData->currentPage();
        // dd($historyData);

        return view('livewire.withdraw', [
            'gateways' => $this->getTransferGateways(),
            'historyData' => $historyData
        ]);
    }

    public function getTransferGateways()
    {
        return TransferGateway::where('is_active', 1)->get();
    }

    public function updated($propertyName)
    {
        $minWithdraw = GeneralSetting::where('code', 'min_withdraw')->get()->first()->value;
        $maxWithdraw = GeneralSetting::where('code', 'max_withdraw')->get()->first()->value;
        $this->validateOnly($propertyName, [
            'gateway' => 'required',
            'amount' => "required|numeric|max:$maxWithdraw|min:$minWithdraw"
        ]);
    }

    public function messages()
    {
        return [
            'amount.min' => 'The minimum wihtdrawal allowed is $:min.',
            'amount.max' => 'The maximum wihtdrawal allowed is $:max.',
        ];
    }

    public function request()
    {
        $minWithdraw = GeneralSetting::where('code', 'min_withdraw')->get()->first()->value;
        $maxWithdraw = GeneralSetting::where('code', 'max_withdraw')->get()->first()->value;

        $this->validate([
            'gateway' => 'required',
            'amount' => "required|numeric|max:$maxWithdraw|min:$minWithdraw"
        ]);

        if ($this->agreement == false) {
            $this->emit('showAlertInfo', ['msg' => 'You must agree to the Terms and Conditions.']);
            return;
        }

        if ($this->password == '' || $this->password == null) {
            $this->emit('showAlertInfo', ['msg' => 'Please enter your password to continue.']);
            return;
        }

        if (Hash::check($this->password, $this->user->password)) {
            if ($this->amount > $this->user->commission_balance) {
                $this->emit('showAlertInfo', ['msg' => 'Your balance is not enough.']);
                return;
            }

            $wd_code = strtoupper("DP".$this->generateInitial($this->user->firstname))
                .$this->randomAlpha()
                .Carbon::now()->format('ymd')
                .$this->randomNumber();
            
            DB::table('withdraw_data')->insert([
                'wd_code' => $wd_code,
                'created_at' => Carbon::now(),
                'user_request' => $this->user->id,
                'amount' => floatval($this->amount),
                'status' => 'P'
            ]);

            $this->agreement = null;
            $this->password = null;
            $this->emit('showAlert', ['msg' => 'Your withdraw request has been submitted.']);
            $this->emit('refreshData');
        } else {
            $this->emit('showAlertError', ['msg' => 'Your password does not match.']);
        }
    }

    public function generateInitial(string $name) : string{
        $words = explode(' ', $name);
        return strtoupper(substr($words[0], 0, 1) . substr(end($words), 0, 1));
    }

    public function randomAlpha() {
        $permitted_chars = 'abcdefghijklmnopqrstuvwxyz';
        return strtoupper(substr(str_shuffle($permitted_chars), 0, 3));
    }

    public function randomNumber() {
        $permitted_chars = '1234567890';
        return substr(str_shuffle($permitted_chars), 0, 4);
    }

    public function getHistory()
    {
        return DB::table('withdraw_data')
            ->selectRaw("id, created_at AS date, wd_code, FORMAT(amount, 2, 'id_ID') AS amount, 0 AS rate, status, IFNULL(message, '') AS message, confirmed_at")
            ->where('user_request', '=', $this->user->id)
            ->orderBy('date', 'DESC');
    }

    public function exportPdf()
    {
        // dd($this->pdfData);
        $data = [
            'name' => $this->user->firstname . ' ' . $this->user->lastname,
            'historyData' => $this->pdfData,
            'currentPage' => $this->currPage
        ];

        $pdf = PDF::loadview('withdraw-history-pdf', $data)->setPaper('A4', 'portrait')->output();

        return response()->streamDownload(
            fn() => print($pdf),
            'WITHDRAW-'.$this->user->username.'-'.$this->currPage.'.pdf'
        );
    }
}

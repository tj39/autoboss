<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\GeneralSetting;
use App\Models\MasterStrategy;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class ListStrategy extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['refreshList' => '$refresh'];

    public $search;
    public $strategies;
    public $strategy;
    public $choosenStrategy;
    public $investmentId;

    public function render()
    {
        $this->strategies = MasterStrategy::where('is_active', 1)->get();

        $pageRow = GeneralSetting::where('code', 'pagination_row')->get()->first()->value;

        return view('livewire.list-strategy', [
            'data' => $this->getInvestmentList()->paginate($pageRow)
        ]);
    }

    public function getInvestmentList()
    {
        return DB::table('sprofit_data AS mi')
            ->selectRaw('mi.id, IFNULL(mi.updated_at, mi.created_at) AS date, mi.investment_id, mi.initial_investment, mi.status')
            ->where("mi.client_id", '=', auth()->user()->client_id)
            ->whereRaw("mi.investment_id LIKE '%".$this->search."%'")
            ->orderByRaw('date DESC');
    }

    public function showModalStrategyDetail($id)
    {
        $this->strategy = MasterStrategy::find($id);
        $this->emit('showModalStrategyDetail');
    }

    public function selectStrategy($id)
    {
        $this->choosenStrategy = $id;
        $this->emit('hideModalStrategyDetail');
        $this->emit('refeshList');
    }

    public function showModalConfirmation()
    {
        $this->validate([
            'choosenStrategy' => 'required'
        ], [
            'choosenStrategy.required' => 'You must choose trading strategy.'
        ]);

        $this->emit('showModalConfirmation');
    }

    public function hideModalConfirmation()
    {
        $this->emit('hideModalConfirmation');
    }
}

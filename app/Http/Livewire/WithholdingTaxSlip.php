<?php

namespace App\Http\Livewire;

use App\Traits\GlobalValues;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class WithholdingTaxSlip extends Component
{
    use GlobalValues;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $search;

    public function render()
    {
        $pageRow = $this->getGeneralSettingValue('pagination_row');
        return view('livewire.withholding-tax-slip', [
            'data' => $this->retrieveTaxSlipList()->paginate($pageRow)
        ]);
    }

    public function retrieveTaxSlipList()
    {
        return DB::table('tax_slip as ts')
            ->selectRaw("tf.folder_name, tf.category, ts.npwp, ts.commission_gross, ts.pph,
                CASE WHEN category IS NOT NULL THEN CONCAT(tf.folder_name,'/',ts.file_code,'/',tf.category,'.pdf') 
                ELSE CONCAT(tf.folder_name,'/',ts.file_code,'.pdf') 
                END AS location")
            ->join('user_data as ud', 'ts.no_identity', '=', 'ud.nik')
            ->join('tax_file as tf', 'ts.file_id', '=', 'tf.id')
            ->where('ud.nik', '=', auth()->user()->nik)
            ->where('folder_name', 'like', '%'.$this->search.'%')
            ->orderBy('tf.created_at', 'desc');
    }

    public function downloadFile($location)
    {
        return response()->download(storage_path('app/bukti_potong/'.$location));
    }
}

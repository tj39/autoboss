<?php

namespace App\Http\Livewire;

use App\Models\GeneralSetting;
use App\Models\MasterGeneration;
use App\Models\PortofolioFile;
use App\Models\RankRequest;
use App\Models\SeminarEvent;
use App\Models\SprofitData;
use App\Models\TraderRank;
use App\Models\User;
use App\Traits\GlobalValues;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Partnership extends Component
{
    use GlobalValues;
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['refreshData' => '$refresh'];

    public $currentRankCode;
    public $nextRankDesc;
    public $referenceUrl;
    public $totalDeposited;
    public $downline;
    public $turnover;
    public $percentDeposit;
    public $percentDownline;
    public $percentTurnover;
    public $targetDeposit;
    public $targetDownline;
    public $targetTurnover;
    public $canRequestUpgrade = false;
    public $depositTaskStatus;
    public $downlineTaskStatus;
    public $turnoverTaskStatus;
    public $certificatePdf;
    public $fileName;
    public $selectedIdToDelete;
    public $directUplineName;
    public $directUplinePhone;
    public $search = '';
    public $filterColumn;
    public $filterValue;
    public $totalRecordDownline;
    public $currentPage = 1;
    public $hasPages;
    public $totalPages;
    public $customPages;
    public $investmentPopupClicked = false;
    public $investmentPopupName;
    public $investmentPopupTotal;
    public $uplineId;
    public $userUpline;
    public $uplineRank;
    public $clientId;
    public $traderName;
    public $seminarCheck = false;
    public $agreeToAttend;
    public $type;
    public $modalUpload2;
    public $nextRankCode;
    public $amountToBePaid, $accountNumber, $destinationAccount, $transferProofImg, $invoiceNumber;

    protected $uploadedPortofolios;
    
    public function updatingSearch()
    {
        $this->resetCurrentPage();
    }

    public function updatingFilterValue()
    {
        $this->resetCurrentPage();
    }

    public function render()
    {
        $user = User::find(auth()->user()->id);
        $rankTarget = TraderRank::where('id', '=', auth()->user()->role_id-1)->first();
        if (auth()->user()->step == 'OK') {
            $currentRank = TraderRank::where('id', '=', auth()->user()->role_id)->first();

            $this->currentRankCode = $currentRank->code;
            $this->referenceUrl = $user->username;

            $this->totalDeposited = $user->deposit_balance;
            $this->downline = $user->tot_direct_downline;
            $this->turnover = $user->turnover;

            if ($currentRank->code != 'D') {
                $nextRank = TraderRank::where('order', $currentRank->order - 1)->first();
                $this->nextRankCode = $nextRank->code;
                
                $this->nextRankDesc = $nextRank->description;

                $this->validateTaskStatus($nextRank);
                $this->checkExistingRankRequest($this->canRequestUpgrade);

                if (($nextRank->code == 'G' || $nextRank->code == 'P') && $this->canRequestUpgrade) {
                    do {
                        $this->invoiceNumber = strtoupper(substr(uniqid('TP-' . $nextRank->code), 0, 10));
                    } while (DB::table('rank_request')->where('no_invoice', $this->invoiceNumber)->exists());
                    
                    if ($nextRank->code == 'G') {
                        $this->amountToBePaid = $this->getGeneralSettingValue('seminar_fee_gold');
                    } elseif ($nextRank->code == 'P') {
                        $this->amountToBePaid = $this->getGeneralSettingValue('seminar_fee_platinum');
                    }
                    
                    $this->accountNumber = $this->getGeneralSettingValue('tjl_account_no');
                    $this->destinationAccount = $this->getGeneralSettingValue('tjl_account');
                }
            } else {
                $this->nextRankDesc = $currentRank->description;

                $this->percentDeposit = 100;
                $this->percentDownline = 100;
                $this->percentTurnover = 100;

                $this->targetDeposit = $currentRank->deposit;
                $this->targetDownline = $currentRank->direct_downline;
                $this->targetTurnover = $currentRank->turnover;

                $this->depositTaskStatus = 1;
                $this->downlineTaskStatus = 1;
                $this->turnoverTaskStatus = 1;
            }

            $directUpline = User::find($user->direct_upline);
            $this->directUplineName = $directUpline->firstname . ' ' . $directUpline->lastname;
            $this->directUplinePhone = $directUpline->phone;
        }

        $pageRowModal = $this->getGeneralSettingValue('pagination_modal');
        $investmentList = null;
        if ($this->investmentPopupClicked) {
            $investmentList = $this->getInvestmentList()->paginate($pageRowModal, ['*'], 'investment');
        }
        
        $seminarEvent = $this->getSeminarEvent($rankTarget->description);

        $pageRow = $this->getGeneralSettingValue('pagination_row');
        return view('livewire.partnership', [
            'portofolioList' => $this->retrievePortofolioFileList()->paginate($pageRow, ['*'], 'portofolios'),
            'filterValues' => $this->getFilterValueCombo(),
            'downlineList' => $this->retrieveDownlineList(),
            'rowPerPage' => $pageRow,
            'investmentList' => $investmentList,
            'seminarEvent' => $seminarEvent
        ]);
    }

    public function validateTaskStatus($nextRank)
    {
        $user = User::find(auth()->user()->id);

        $this->targetDeposit = $nextRank->deposit;
        $this->targetDownline = $nextRank->direct_downline;
        $this->targetTurnover = $nextRank->turnover;

        $this->percentDeposit = ($user->deposit_balance / $nextRank->deposit) * 100;
        $this->percentDownline = ($user->tot_direct_downline / $nextRank->direct_downline) * 100;
        $this->percentTurnover = ($user->turnover / $nextRank->turnover) * 100;

        $this->depositTaskStatus = $this->percentDeposit >= 100 ? 1 : 0;
        $this->downlineTaskStatus = $this->percentDownline >= 100 ? 1 : 0;
        $this->turnoverTaskStatus = $this->percentTurnover >= 100 ? 1 : 0;

        if ($this->percentDeposit >= 100 && $this->percentDownline >= 100 && $this->percentTurnover >= 100) {
            $this->canRequestUpgrade = true;
        } else {
            $this->canRequestUpgrade = false;
        }
    }

    public function checkExistingRankRequest($canRequestUpgrade)
    {
        $rankRequest = RankRequest::where('user_id', auth()->user()->id)
            ->where('from_rank', $this->currentRankCode)
            ->where('status', 'PENDING')->first();

        if ($rankRequest != null) {
            $canRequestUpgrade = false;
        }

        $this->canRequestUpgrade = $canRequestUpgrade;
    }

    public function requestRankUpgrade($type)
    {
        $currentRank = TraderRank::where('id', '=', auth()->user()->role_id)->first();
        $nextRank = TraderRank::where('order', $currentRank->order - 1)->first();
        $this->validateTaskStatus($nextRank);

        if (!$this->canRequestUpgrade && $type != '1') {
            $this->emit('showAlertWarning', ['msg' => 'Unable to request Rank Upgrade. There might be update on your data, please refresh the page.']);
            return;
        }

        $this->type = $type;
        if ($this->currentRankCode != 'P') {
            if ($type == 1 || auth()->user()->seminar_status == 'EXPIRED') {
                $this->modalUpload2 = true;
                $this->emit('showModalUploadFile2');
                return;
            }
            $this->emit('showModalUploadFile');
        } else {
            $this->uploadedPortofolios = PortofolioFile::where('user_id', '=', auth()->user()->id)
                ->where('request_id', '=', null)->get();

            // dd($this->uploadedPortofolios->get());
            if (count($this->uploadedPortofolios) != 0) {
                $this->submit();
            } else {
                $this->emit('showAlertInfo', ['msg' => "You need to upload Portofolio(s) first in the <strong>Portofolio File</strong> tab before requesting Rank Upgrade to $nextRank->description."]);
            }
        }
    }

    public function submit()
    {
        $nextRank = TraderRank::where('id', '=', auth()->user()->role_id - 1)->first();
        $rankRequest = RankRequest::where('user_id', '=', auth()->user()->id)->first();

        $this->validateTaskStatus($nextRank);

        if (!$this->canRequestUpgrade && $this->type != '1') {
            $this->emit('hideModalUploadFile');
            $this->emit('hideModalUploadFile2');
            $this->emit('showAlertWarning', ['msg' => 'Unable to request Rank Upgrade. There might be update on your data, please refresh the page.']);
            return;
        }

        $msg = '';

        // To rank Silver, Gold, and Platinum
        if ($this->currentRankCode != 'P' && $nextRank->code != 'D') {
            $user = User::find(auth()->user()->id);
            $maxPdfSize = $this->getGeneralSettingValue('max_pdf_size');
            $rankTarget = TraderRank::where('id', '=', $user->role_id-1)->first();

            $seminarEvent = $this->getSeminarEvent($rankTarget->description);
            if ($this->type == 1) {
                // pernah setuju ikut seminar dan belum expired.
                $this->validate([
                    'certificatePdf' => "required|mimes:pdf|max:$maxPdfSize",
                    'fileName' => 'required|max:255'
                ], [
                    'certificatePdf.mimes' => 'Only PDF file is accepted.'
                ]);

                if (null != $user->request_id) {
                    $this->storePortofolioFile($user->request_id);

                    if ($user->seminar_status != 'REJECTED') {
                        $user->seminar_status = 'ATTENDED';
                    }
                    $user->save();
                    
                    $rankRequest = RankRequest::find($user->request_id);
                    $rankRequest->status = 'PENDING';
                    $rankRequest->message = 'Your certificate is being verified.';
                    $rankRequest->updated_at = Carbon::now();
                    $rankRequest->is_dismissed = '0';
                    $rankRequest->save();                    
                }
                
                $msg = 'Your certificate has been submitted.';

                $this->emit('hideModalUploadFile');                
                $this->emit('hideModalUploadFile2');
                $this->emit('showAlertSuccess', ['msg' => $msg]);
                $this->resetFields();
                $this->emit('refreshData');
            } else {
                if ($this->seminarCheck) {
                    $this->validate([
                        'certificatePdf' => "required|mimes:pdf|max:$maxPdfSize",
                        'fileName' => 'required|max:255'
                    ], [
                        'certificatePdf.mimes' => 'Only PDF file is accepted.'
                    ]);
        
                    if ($rankRequest && $rankRequest->to_rank == $nextRank->code) {
                        $this->updateRankRequest($rankRequest, null, $user);
                        $this->storePortofolioFile($rankRequest->id);
                    } else {
                        $rankRequest = $this->insertRankRequest($nextRank, null, $user);
                        $this->storePortofolioFile($rankRequest->id);
                    }
    
                    if ($user->seminar_status != 'REJECTED') {
                        $user->seminar_status = 'ATTENDED';
                    }
                    $user->save();
                    
                    $msg = 'Your certificate has been submitted.';
    
                    $this->emit('hideModalUploadFile');
                    $this->emit('hideModalUploadFile2');
                    $this->emit('showAlertSuccess', ['msg' => $msg]);
                    $this->resetFields();
                } else {
                    if ($nextRank->code == 'G' || $nextRank->code == 'P') {
                        $maxImgUploadSize = $this->getGeneralSettingValue('max_image_size');
                        $this->validate([
                            'transferProofImg' => 'required|mimes:png,jpg,jpeg|max:'.$maxImgUploadSize,
                            'agreeToAttend' => 'required'
                        ], [
                            'transferProofImg.required' => 'Please upload your transfer proof before proceeding.',
                            'agreeToAttend.required' => 'You must agree to attend the webinar before proceeding.'
                        ]);
                    } else {
                        $this->validate([
                            'agreeToAttend' => 'required'
                        ], [
                            'agreeToAttend.required' => 'You must agree to attend the webinar before proceeding.'
                        ]);
                    }
    
                    if ($rankRequest && $rankRequest->to_rank == $nextRank->code) {
                        $this->updateRankRequest($rankRequest, $seminarEvent, $user);
                    } else {
                        $rankRequest = $this->insertRankRequest($nextRank, $seminarEvent, $user);
                    }

                    if ($nextRank->code == 'S') {
                        $callSP = "CALL GET_DIRECT_DOWNLINE_COUNT(" . $user->id . ")";
                        DB::statement($callSP);
                        $callSPParent = "CALL GET_DIRECT_DOWNLINE_COUNT(" . $user->direct_upline . ")";
                        DB::statement($callSPParent);
                    }

                    $user->seminar_status = 'ATTENDING';
                    $user->seminar_id = $seminarEvent->id;
                    $user->request_id = $rankRequest->id;
                    $user->save();

                    $traderRank = TraderRank::where('code', $this->currentRankCode)->first()->description;

                    $day = GeneralSetting::where('code', 'overdue_certificate')->first()->value;
                    $date = date('Y-m-d', strtotime($seminarEvent->event_date . ' + ' . $day . ' days'));
                    $dateDowngrade = date_format(date_create($date), 'd M y');
                    $msg = 'You have agreed to attend the webinar. However, remember to upload the certificate not later than the webinar day + '. $day .' (Max '. $dateDowngrade .'). Otherwise, your rank will be downgraded to '. $traderRank . '.';
                    $this->emit('hideModalUploadFile');
                    $this->emit('hideModalUploadFile2');
                    $this->emit('showAlertWarningPartnership', ['msg' => $msg]);
                    $this->resetFields();
                }
            }            
        } else {
            // To rank Diamond
            if ($rankRequest && $rankRequest->to_rank == $nextRank->code) {
                $this->updateRankRequest($rankRequest, null, null);
            } else {
                $rankRequest = $this->insertRankRequest($nextRank, null, null);
            }

            foreach ($this->uploadedPortofolios as $portofolioFile) {
                $portofolioFile->updated_at = Carbon::now();
                $portofolioFile->request_id = $rankRequest->id;
                $portofolioFile->save();
            }
        }
        $this->emit('refreshData');
    }

    public function getSeminarEvent($rank)
    {
        return SeminarEvent::where('status', 'NEW')
            ->whereRaw('event_date > CURRENT_TIMESTAMP')
            ->where('rank',$rank)
            ->orderBy('event_date', 'asc')
            ->first();
    }

    public function insertRankRequest($nextRank, $seminarEvent, $user)
    {
        $rankRequest = new RankRequest();
        $rankRequest->created_at = Carbon::now();
        $rankRequest->user_id = auth()->user()->id;
        if ($nextRank->code == 'S' && $seminarEvent != null) {
            $rankRequest->status = 'ACCEPTED';
            $rankRequest->message = 'Webinar reminder: ' . date_format(date_create($seminarEvent->event_date), 'D, d M Y') . ', ' . $seminarEvent->event_time . '. URL: <a href="' . $seminarEvent->url .'"><strong>' . $seminarEvent->url . '</strong></a>. Passcode: ' . $seminarEvent->passcode;
            $user->role_id = $nextRank->id;
            $user->save();
        } else {
            if ($nextRank->code == 'G' || $nextRank->code == 'P') {
                if ($this->transferProofImg == null) {
                    $rankRequest->status = 'PENDING';
                } else {
                    $transferProof = $this->transferProofImg->storeAs('training_transfer_proof', auth()->user()->username . '/'.$this->invoiceNumber.'.' . $this->transferProofImg->getClientOriginalExtension(), 'public');
                    $rankRequest->transfer_proof = $transferProof;
                    $rankRequest->status = 'PAID';
                    $rankRequest->no_invoice = $this->invoiceNumber;
                }
            } elseif ($nextRank->code == 'S') {
                $rankRequest->status = 'PENDING';
            }
            $rankRequest->message = 'Your Rank Upgrade request is being processed.';
        }
        
        $rankRequest->from_rank = $this->currentRankCode;
        $rankRequest->to_rank = $nextRank->code;
        $rankRequest->save();

        return $rankRequest;
    }

    public function updateRankRequest($rankRequest, $seminarEvent, $user)
    {
        if ($rankRequest->from_rank == 'B' && $rankRequest->status != 'REJECTED') {
            $rankRequest->status = 'ACCEPTED';
            if ($seminarEvent == null) {
                $rankRequest->message = 'Your Rank has been upgraded to Silver.';
            } else {
                $rankRequest->message = 'Webinar reminder: ' . date_format(date_create($seminarEvent->event_date), 'D, d M Y') . ', ' . $seminarEvent->event_time . '. URL: <a href="' . $seminarEvent->url .'"><strong>' . $seminarEvent->url . '</strong></a>. Passcode: ' . $seminarEvent->passcode;
                $user->role_id = $user->role_id - 1;
                $user->save();
            }
        } else {
            if ($rankRequest->from_rank == 'S' || $rankRequest->from_rank == 'G') {
                if ($this->transferProofImg != null) {
                    $transferProof = $this->transferProofImg->storeAs('training_transfer_proof', auth()->user()->username . '/proof_number.' . $this->transferProofImg->getClientOriginalExtension(), 'public');
                    $rankRequest->transfer_proof = $transferProof;
                    $rankRequest->no_invoice = $this->invoiceNumber;
                }
                $rankRequest->status = 'PAID';
            } elseif ($rankRequest->from_rank == 'B') {
                $rankRequest->status = 'PENDING';
            }

            $rankRequest->message = 'Your Rank Upgrade request is being processed.';
        }
        $rankRequest->updated_at = Carbon::now();
        $rankRequest->is_dismissed = '0';
        $rankRequest->save();
    }

    public function storePortofolioFile($requestId)
    {
        $rankRequest = RankRequest::find($requestId);
        if ($rankRequest->to_rank != 'D') {
            $portofolio = PortofolioFile::where('request_id', $requestId)->get()->first();

            if (!$portofolio) {
                $portofolioFile = new PortofolioFile();
                $portofolioFile->user_id = auth()->user()->id;
                $portofolioFile->file_name = $this->fileName;
        
                $pdfFile = $this->certificatePdf->storeAs('portofolio_files', auth()->user()->username . '/' . $this->fileName . '.pdf', 'public');
                $portofolioFile->file_path = $pdfFile;
                $portofolioFile->request_id = $requestId;
                $portofolioFile->is_accept = '2';
                $portofolioFile->save();
            } else {
                $portofolio->file_name = $this->fileName;        
                $portofolio->file_path = $this->certificatePdf->storeAs('portofolio_files', auth()->user()->username . '/' . $this->fileName . '.pdf', 'public');
                $portofolio->updated_at = Carbon::now();
                $portofolio->is_accept = '2';
                $portofolio->save();
            }
            
        } else {
            $portofolioFile = new PortofolioFile();
            $portofolioFile->user_id = auth()->user()->id;
            $portofolioFile->file_name = $this->fileName;
    
            $pdfFile = $this->certificatePdf->storeAs('portofolio_files', auth()->user()->username . '/' . $this->fileName . '.pdf', 'public');
            $portofolioFile->file_path = $pdfFile;
            $portofolioFile->request_id = $requestId;
            $portofolioFile->is_accept = '2';
            $portofolioFile->save();
        }
    }

    public function uploadPdfFile()
    {
        $maxPdfSize = GeneralSetting::where('code', 'max_pdf_size')->where('is_active', 1)->first()->value;
        $this->validate([
            'certificatePdf' => "required|mimes:pdf|max:$maxPdfSize",
            'fileName' => 'required|max:255'
        ], [
            'certificatePdf.mimes' => 'Only PDF file is accepted.'
        ]);

        $this->storePortofolioFile(null);

        $this->resetFields();
        $this->emit('showAlert', ['msg' => 'Portofolio uploaded successfully.']);
        // $this->emit('refreshData');
    }

    public function downloadInvoice()
    {
        $rankRequest = RankRequest::find(auth()->user()->request_id);
        $rankRequest->status = 'ACCEPTED';
        $rankRequest->save();
        $this->emit('refreshData');
        return response()->download(storage_path('app/'.auth()->user()->requestId->invoice));
    }

    public function retrievePortofolioFileList()
    {
        return DB::table('portofolio_file AS pf')
            ->selectRaw('pf.id, DATE_FORMAT(IFNULL(pf.updated_at, pf.created_at), "%d %b %Y %k:%i") AS date, pf.file_name, pf.is_accept, pf.file_path')
            ->where("pf.user_id", '=', auth()->user()->id)
            ->orderBy('date', 'desc');
    }

    public function downloadPortofolio($id)
    {
        $portofolioFile = PortofolioFile::find($id);
        return response()->download('storage/'.$portofolioFile->file_path);
    }

    public function showModalConfirmation($id)
    {
        $this->selectedIdToDelete = $id;
        $this->emit('showModalConfirmation');
    }

    public function deletePortofolio($id)
    {
        $portofolioFile = PortofolioFile::find($id);
        $portofolioFile->delete();

        $this->emit('showAlert', ['msg' => 'Portofolio deleted successfully.']);
        $this->emit('hideModalConfirmation');
        // $this->emit('refreshData');
    }

    public function resetFields()
    {
        $this->certificatePdf = null;
        $this->fileName = null;
        $this->seminarCheck = null;
    }

    public function getFilterValueCombo()
    {
        $filterValues = [];

        if ($this->filterColumn == 'rank') {
            $ranks = TraderRank::all();
            foreach ($ranks as $rank) { 
                array_push($filterValues, $rank->description);
            }
        } elseif ($this->filterColumn == 'generation') {
            $generations = MasterGeneration::all();
            foreach ($generations as $generation) {
                array_push($filterValues, $generation->code);
            }
        } elseif ($this->filterColumn == 'status') {
            array_push($filterValues, 'On Process', 'Valid', 'Invalid');
        }

        // $this->emit('refreshData');
        return $filterValues;
    }

    public function retrieveDownlineList()
    {
        $perPage = GeneralSetting::where('code', 'pagination_row')->get()->first()->value;
        $start = ($this->currentPage * $perPage) - $perPage + 1;
        $end = ($this->currentPage * $perPage);
        
        if ($this->filterColumn == 'rank') {
            $rank = $this->filterValue == null ? '' : $this->filterValue;
            $generation = '%';
            $status = '%';
        } elseif ($this->filterColumn == 'generation') {
            $rank = '%';
            $generation = $this->filterValue == null ? '' : $this->filterValue;
            $status = '%';
        } elseif ($this->filterColumn == 'status') {
            
            if ($this->filterValue == 'On Process') {
                $this->filterValue = '0';
            } else if ($this->filterValue == 'Valid') {
                $this->filterValue = '1';
            } else if ($this->filterValue == 'Invalid') {
                $this->filterValue = '2';
            }

            $rank = '%';
            $generation = '%';
            $status = $this->filterValue == null ? '' : $this->filterValue;
        } else {
            $rank = '%';
            $generation = '%';
            $status = '%';
        }

        $this->search = $this->search == null ? '' : $this->search;

        $dataList = DB::select('CALL GET_DOWNLINE(?, ?, ?, ?, ?, ?, ?)', [
            auth()->user()->id, $this->search, $generation, $rank, $status, $start, $end
        ]);

        $dataCount = DB::select('CALL GET_DOWNLINE_COUNT(?, ?, ?, ?, ?)', [
            auth()->user()->id, $this->search, $generation, $rank, $status
        ]);

        $this->customPages = [];
        if (null != $dataCount && 0 != $dataCount['0']->count) {
            $this->totalRecordDownline = $dataCount['0']->count;
            $this->hasPages = $this->totalRecordDownline > $perPage;
            $this->totalPages = ceil($this->totalRecordDownline / $perPage);
           
            for ($i=0; $i < $this->totalPages; $i++) { 
                array_push($this->customPages, $i+1);
            }
        } else {
            $this->totalRecordDownline = 0;
            $this->hasPages = false;
            $this->totalPages = 0;
        }

        return $dataList;
    }

    public function showInvestmentPopup($clientId, $name) 
    {
        $this->investmentPopupClicked = true;
        $this->clientId = $clientId;
        $this->traderName = $name;
        $this->emit('showInvestmentPopup');
    }

    public function getInvestmentList()
    {
        $investmentList = SprofitData::where('client_id', $this->clientId)
            ->whereRaw("(status = 'ACTIVE' OR status = 'INACTIVE')")
            ->orderBy('created_at', 'desc');

        $InvestmentTotal = SprofitData::where('client_id', $this->clientId)
            ->whereRaw("(status = 'ACTIVE')")
            ->orderBy('created_at', 'desc');
        
        $totalInvestment = 0;
        foreach ($InvestmentTotal->get() as $item) {
            $totalInvestment += $item->initial_investment;
        }
        $this->investmentPopupTotal = $totalInvestment;
        $this->investmentPopupName = $this->traderName;
        return $investmentList;
    }

    public function hideInvestmentPopup()
    {
        $this->investmentPopupClicked = false;
        $this->emit('hideInvestmentPopup');
    }

    public function showDirectUplinePopup($id)
    {
        $this->uplineId = $id;
        $this->userUpline = User::find($id);
        $this->uplineRank = TraderRank::where('id', $this->userUpline->role_id)->first()->description;
        $this->emit('showDirectUplinePopup');
    }

    public function hideDirectUplinePopup()
    {
        $this->emit('hideDirectUplinePopup');
    }

    // Custom pagination
    public function resetCurrentPage()
    {
        $this->currentPage = 1;
    }

    public function goToSpecificPage($page)
    {
        $this->currentPage = $page;
    }

    public function goToNextPage()
    {
        $this->currentPage = $this->currentPage + 1;
    }

    public function goToPreviousPage()
    {
        $this->currentPage = $this->currentPage - 1;
    }

    public function refreshTab()
    {
        $this->resetPage('portofolios');
        $this->resetPage('investment');
    }
}

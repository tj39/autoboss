<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\TicketCategory;
use App\Models\ActiveTicket;
use Illuminate\Support\Facades\Auth;

class CreateTicket extends Component
{
    public $title;
    public $category;
    public $message;
    public $image_url;


    public function render()
    {
        $ticket_categories = TicketCategory::all();
        return view('livewire.create-ticket', [
            'ticket_category' => $ticket_categories
        ]);
    }

    public function store(){
        $date = date('dmYHis');
        $ticketcategory = TicketCategory::find($this->category);
        $selectedcategory = $ticketcategory->code;
        $noticket = 'ST'.$selectedcategory.$date;
        $noticket = strtoupper(substr(md5($noticket), 0, 8));

        ActiveTicket::create([
            'no_ticket' => $noticket,
            'created_date' => Date(now()),
            'status_id' => 1,
            'category_id' => $this->category,
            'user_id' => Auth::user()->id,
            'title' => $this->title,
            'message' => $this->message,
            'url_image' => $this->image_url
        ]); 

        $this->hideModal();
        $this->emit('showAlert', ['msg' => 'Ticket submitted']);
        
        return redirect()->route('user.support.ticket');
    }

    public function showModalConfirmation(){
        $this->validate([
            'title' => 'required|max:20',
            'category' => 'required',
            'message' => 'required',
        ]);
        $this->emit('showModalConfirmation');
    }

    public function hideModal()
    {
        $this->title = null;
        $this->message = null;
        $this->image_url = null;
        $this->category = null;
        $this->emit('hideModal');
    }
    
}

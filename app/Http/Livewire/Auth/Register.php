<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\User;
use Monarobase\CountryList\CountryListFacade;
use App\Models\GeneralSetting;
use App\Models\TransferGateway;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use App\Models\TraderRank;
use Illuminate\Auth\Events\Registered;
use Illuminate\Validation\Rules\Password;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Register extends Component
{
    use WithFileUploads;

    public $referenceLeader;
    public $username;
    public $firstname;
    public $lastname;
    public $avatar;
    public $nik;
    public $card_identity;
    public $npwp;
    public $address;
    public $city;
    public $province;
    public $zip_code;
    public $country;
    public $address_checkbox;
    public $selectedBank;
    public $no_account;
    public $account_holder;
    public $email;
    public $phone;
    public $password;
    public $confirm_password;
    public $step;
    public $checkedReferal;
    public $user_referral;
    public $checkedUsername;
    public $savedReferal;
    public $savedUsername;
    public $captcha = 0;
    public $userAgreed = false;
    public $no_npwp;
    public $searchCountry = '';
    public $selectedMethod;
    public TransferGateway $transferGateway;
    
    private $stepActions = [
        'submit1',
        'submit2',
        'submit3',
        'submit4',
        'submit5'
    ];

    public function updatingSelectedMethod()
    {
        $this->emit('reloadform');
    }

    public function updatingNoAccount()
    {
        $this->emit('reloadform');
    }

    public function mount(Request $request)
    {
        if ($request->has('ref')) {
            $this->referenceLeader = $request->query('ref');
        }
        $this->step = 0;
    }

    public function render()
    {
        $countries = CountryListFacade::getList('en');
        $listBank = null;
        if ($this->selectedMethod == 'BANK') {
            $listBank = TransferGateway::where('is_active', 1)
                ->where('method', 'BANK')
                ->get();
        } else {
            $listBank = TransferGateway::where('is_active', 1)
                ->where('method', 'OTHER')
                ->get();
            if ($this->selectedBank != null) {
                $this->transferGateway = TransferGateway::find($this->selectedBank);
            }
        }
        
        return view('livewire.auth.register',[ 
            'countries' => $countries,
            'listBank' => $listBank
        ]);
    }
    
    public function decreaseStep()
    {
        if ($this->step == 2 ) {
            $this->avatar = null;
            $this->card_identity = null;
            $this->npwp = null;
        }

        $this->emit('reloadform');
        $this->step--;
    }

    public function increaseStep()
    {
        $action = $this->stepActions[$this->step];

        $this->emit('reloadform');
        $this->$action();
    }

    public function submit1(){
        if($this->checkedReferal == 2){
            if($this->savedReferal != $this->referenceLeader){
                $this->emit('showAlertError', [
                    'msg' => 'CHECK REFERENCE FIRST!',
                    'redirect' => false,
                    'path' => '/'
                ]);
            }
            else{
                $this->step++;
            }
        }
        else if($this->checkedReferal == 1){
            $this->emit('showAlertError', [
                'msg' => 'INVALID REFERENCE!',
                'redirect' => false,
                'path' => '/'
            ]);
        }
        else if($this->checkedReferal == null){
            $this->emit('showAlertError', [
                'msg' => 'CHECK REFERENCE FIRST!',
                'redirect' => false,
                'path' => '/'
            ]);
        }

        if($this->referenceLeader == null){
            $this->emit('showAlertError', [
                'msg' => 'You must check the Leader reference first before proceeding to the next step',
                'redirect' => false,
                'path' => '/'
            ]);
        }
    }

    public function submit2(){
        $max_size_upload = GeneralSetting::where('code', 'max_image_size')->where('is_active', 1)->first()->value;
        $this->validate([
            'username' => 'required|min:5|max:15|alpha_dash',
            'firstname' => 'required|max:25',
            'lastname' => 'required|max:25',
            'avatar' => "required|image|mimes:jpg|max:$max_size_upload",
            'nik' => 'required|max:25|unique:user_data',
            'card_identity' => "required|image|mimes:jpg|max:$max_size_upload"
        ]);

        if ($this->npwp != null) {
            $this->validate([
                'npwp' => "image|mimes:jpg|max:$max_size_upload"
            ]);
        }

        if($this->checkedUsername == 1){
            if($this->savedUsername != $this->username){
                $this->emit('showAlertError', [
                    'msg' => 'CHECK USERNAME FIRST',
                    'redirect' => false,
                    'path' => '/'
                ]);
            }
            else{
                $this->step++;
            }
        }
        else{
            $this->emit('showAlertError', [
                'msg' => 'CHECK USERNAME FIRST',
                'redirect' => false,
                'path' => '/'
            ]);
        }

    }

    public function submit3(){
        $this->validate([
            'address' => 'required|max:255',
            'city' => 'required|max:50',
            'province' => 'required|max:32',
            'zip_code' => 'required|digits_between:1,16|numeric',
            'country' => 'required'
        ]);

        $this->step++;
    }

    public function submit4(){
        if ($this->selectedMethod == 'OTHER') {
            $this->account_holder = $this->firstname . ' ' . $this->lastname;
        }

        $this->validate([
            'selectedMethod' => 'required',
            'selectedBank' => 'required',
            'account_holder' => 'required|max:50',
        ]);

        if ($this->selectedMethod == 'BANK') {
            $this->validate([
                'no_account' => 'required|digits_between:1,32|numeric',
            ]);
        } elseif ($this->selectedMethod == 'OTHER') {
            $this->validate([
                'no_account' => 'required|max:255',
            ]);
        }

        $this->step++;
    }

    public function checkUsername(){
        $this->validate([
            'username' => 'required|min:5|max:20|alpha_dash'
        ]);

        $user_username = User::where('username', $this->username)->count();

        if($user_username == 0){
            $this->checkedUsername = 1;
            $this->savedUsername = $this->username;
            $this->emit('showAlert', [
                'msg' => 'USERNAME AVAILABLE',
                'redirect' => false,
                'path' => '/'
            ]);
        }
        else{
            $this->checkedUsername = 2;
            $this->emit('showAlertError', [
                'msg' => 'USERNAME HAS BEEN TAKEN! PLEASE ENTER ANOTHER USERNAME',
                'redirect' => false,
                'path' => '/'
            ]);
        }
    }

    public function checkReferral(){
        $user_referral = User::where('username', $this->referenceLeader);

        if($user_referral->count() == 0){
            $this->checkedReferal = 1;
        }
        else{
            $this->checkedReferal = 2;
            $this->user_referral = $user_referral->first();
            $this->savedReferal = $this->referenceLeader;
        }
        
    }

    public function showAgreementModal()
    {
        $this->validate([
            'email' => 'required|email|unique:user_data|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix|max:255',
            'phone' => 'required|unique:user_data|regex:/([+])\d{1,3}/|min:10|max:15',
            'password' => ['required', Password::min(8)
            ->letters()
            ->mixedCase()
            ->numbers()
            ->symbols()
            ->uncompromised()
            ],
            'confirm_password' => 'required|min:8|same:password',
        ],[
            'phone.regex' => 'Please add country code to your phone number'
        ]);


        $this->emit('showAgreementModal');
    }

    public function showSignupModalConfirmation(){
        if (!$this->userAgreed) {
            $this->emit('showAlertInfo', ['msg' => 'You must agree to the Terms and Conditions before proceeding.']);
            return;
        }

        $this->emit('showSignupModalConfirmation');
    }

    public function updatedCaptcha($token)
    {
        $response = Http::post('https://www.google.com/recaptcha/api/siteverify?secret=' . env('CAPTCHA_SECRET_KEY') . '&response=' . $token);
        $this->captcha = $response->json()['score'];

        if (!$this->captcha > .3) {
            $this->store();
        } else {
            return session()->flash('success', 'Google thinks you are a bot, please refresh and try again');
        }

    }

    public function store(){
        $traderOrder = TraderRank::max('order');
        $traderRank = TraderRank::where('order', $traderOrder)->first();

        $avatar = $this->avatar->store('avatar', 'public');
        $card_identity = $this->card_identity->store('card_identity', 'public');
        
        $npwp = null;
        if (null != $this->npwp) {
            $npwp = $this->npwp->store('npwp', 'public');
        }
        

        $user_parent = User::where('username', $this->referenceLeader)->first();

        $user = User::create([
            'username' => $this->username,
            'email' => $this->email,
            'password' => Hash::make($this->password),
            'firstname' => $this->firstname,
            "lastname" => $this->lastname,
            'phone' => $this->phone,
            'is_admin' => 0,
            'role_id' => $traderRank->id,
            'avatar' => $avatar,
            'nik' => $this->nik,
            'identity_card' => $card_identity,
            'npwp' => $npwp,
            'street_adress' => $this->address,
            'city' => $this->city, 
            'state' => $this->province,
            'zipcode' => $this->zip_code,
            'country' => $this->country,
            'commission_balance' => 0,
            'deposit_balance' => 0,
            'withdraw_balance' => 0,            
            'no_account' => $this->no_account,
            'account_holder' => $this->account_holder,
            'kyc_verify_msg' => "PROCESS",
            'account_verify_msg' => "PROCESS",
            'gateways_id' => $this->selectedBank,
            'direct_upline' => $user_parent->id,
            'no_npwp' => $this->no_npwp,
            'check_npwp' => 0
        ]);

        $this->resetForm();
        event(new Registered($user));
        $this->hideModal();
        $this->emit('showAlert', [
            'msg' => 'Account has been created, Check Your Email For Verify The Account',
            'redirect' => true,
            'path' => 'login'
        ]);
        $this->emitSelf('postAdded');
    }

    public function hideModal(){
        $this->emit('hideModal');
    }

    public function resetForm(){
        $this->username = null;
        $this->email = null;
        $this->password = null;
        $this->confirm_password = null;
        $this->firstname = null;
        $this->lastname =null;
        $this->phone = null;
        $this->avatar = null;
        $this->card_identity = null;
        $this->npwp = null;
        $this->address = null;
        $this->city = null;
        $this->province = null;
        $this->zip_code = null;
        $this->country = null;
        $this->no_account = null;
        $this->account_holder = null;
        $this->userAgreed = false;
    }

    public function hideAgreementModal()
    {
        $this->userAgreed = false;
    }
}

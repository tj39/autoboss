<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\TraderHierarchy;
use App\Models\TransferGateway;
use Monarobase\CountryList\CountryListFacade;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Models\GeneralSetting;

class EditUserProfile extends Component
{
    use WithFileUploads;

    public $user;
    public $avatar;
    public $currentAvatar;
    public $nik;
    public $identity;
    public $currentIdentity;
    public $leaderUsername;
    public $leaderPhone;
    public $reference;
    public $username;
    public $firstname;
    public $lastname;
    public $streetAddress;
    public $city;
    public $province;
    public $zipcode;
    public $country;
    public $currentCountry;
    public $bank;
    public $currentBank;
    public $accountNo;
    public $accountHolder;
    public $email;
    public $phone;
    public $password;
    public $clientId;
    public $npwp;
    public $currentNpwp;
    public $no_npwp;
    public $currentNoNpwp;
    public $check_npwp;
    public $currentFirstName;
    public $currentLastName;
    public $currentNik;
    public $selectedMethod;
    public TransferGateway $transferGateway;

    public function updatingSelectedMethod()
    {
        $this->emit('reloadform');
        $this->accountNo = null;
    }

    public function mount()
    {
        $user = User::find(auth()->user()->id);
        $this->user = $user;
        $this->avatar = $user->avatar;
        $this->currentAvatar = $user->avatar;
        $this->identity = $user->identity_card;
        $this->nik = $user->nik;
        $this->currentNik = $user->nik;
        $this->currentIdentity = $user->identity_card;
        $this->clientId = $user->client_id;
        $this->currentClientId = $user->client_id;
        $this->npwp = $user->npwp;        
        $this->currentNpwp = $user->npwp;
        $this->no_npwp = $user->no_npwp;
        $this->currentNoNpwp = $user->no_npwp;
        $this->currentFirstName = $user->firstname;
        $this->currentLastName = $user->lastname;

        $parent = User::where('id', $user->direct_upline)->first();
        if ($parent != null) {
            $this->leaderUsername = $parent->username;
            $this->leaderPhone = $parent->phone;
        } else {
            $this->leaderUsername = '';
            $this->leaderPhone = '';
        }

        $this->username = $user->username;
        $this->firstname = $user->firstname;
        $this->lastname = $user->lastname;
        $this->streetAddress = $user->street_adress;
        $this->city = $user->city;
        $this->province = $user->state;
        $this->zipcode = $user->zipcode;
        $this->currentCountry = $user->country;
        $this->country = $user->country;
        
        if ($user->gateways_id != null && $user->gateways_id != 0) {
            $this->currentBank = $user->gateways_id;
            $this->bank = $user->gateways_id;
            $transferGateway = TransferGateway::find($user->gateways_id);
            $this->selectedMethod = $transferGateway->method;
         } else {
             $this->currentBank = 0;
         }
 
        $this->accountNo = $user->no_account;
        $this->accountHolder = $user->account_holder;
        $this->email = $user->email;
        $this->phone = $user->phone;
        $this->check_npwp = $user->check_npwp;
    }

    public function render()
    {
        $countries = CountryListFacade::getList('en');
        $banks = null;
        if ($this->selectedMethod == 'BANK') {
            $banks = TransferGateway::where('is_active', 1)
                ->where('method', 'BANK')
                ->get();
        } else {
            $banks = TransferGateway::where('is_active', 1)
                ->where('method', 'OTHER')
                ->get();
            if ($this->bank != null) {
                $this->transferGateway = TransferGateway::find($this->bank);
            }
        }
        return view('livewire.auth.edit-user-profile', [
            'countries' => $countries,
            'banks' => $banks
        ]);
    }
    
    public function saveProfile() 
    {        
        $max_size_upload = GeneralSetting::where('code', 'max_image_size')->where('is_active', 1)->first()->value;
        $user = User::find(auth()->user()->id);
        $kyc_msg = $user->kyc_verify_msg;

        if ($this->avatar == $this->currentAvatar) {
            $avatar = $this->currentAvatar;
            $kyc_msg = $user->kyc_verify_msg;
        }
        else {
            $this->validate([
                'avatar' => "required|image|mimes:jpg|max:$max_size_upload"
            ]);
            
            $avatar = $this->avatar->store('avatar', 'public');
            if($user->kyc_verify_msg != 'VALID') {
                $kyc_msg = 'PROCESS';
            }
        }

        $this->validate([
            'firstname' => 'required|max:25',
            'lastname' => 'required|max:25'
        ]);

        $clientUpdate = null;
        $isClientIdValid = $user->is_clientid_valid;
        if('' == $this->clientId) {
            $this->clientId = NULL;
        } else if ($this->clientId != $this->currentClientId && ('' != $this->clientId || null != $this->clientId) ) {
            $clientUpdate = Carbon::now();
            $isClientIdValid = '0';
        }
        
        if($this->firstname != $this->currentFirstName && $user->kyc_verify_msg != 'VALID') {
            $kyc_msg = 'PROCESS';
        }
        
        if($this->lastname != $this->currentLastName && $user->kyc_verify_msg != 'VALID') {
            $kyc_msg = 'PROCESS';
        }

        $user->update([
            'avatar' => $avatar,
            'updated_at' => Carbon::now(),
            'kyc_verify_msg' => $kyc_msg,            
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'client_id' => $this->clientId,
            'updated_clientid_at' => $clientUpdate,
            'is_clientid_valid'=> $isClientIdValid
        ]);

        $this->emit('showAlert', ['msg' => 'Profile has been updated.']);
        return redirect()->route('user.editprofile');
    }

    public function saveUserInformation()
    {
        $max_size_upload = GeneralSetting::where('code', 'max_image_size')->where('is_active', 1)->first()->value;
        $user = User::find(auth()->user()->id);
        $kyc_msg = $user->kyc_verify_msg;
        if ($this->identity == $this->currentIdentity) {
            $identity = $this->currentIdentity;
            $kyc_msg = $user->kyc_verify_msg;
            
            if ($this->nik != $this->currentNik ) {
                $this->validate([
                    'nik' => 'required|unique:user_data'
                ]);
                $kyc_msg = 'PROCESS';
            }
        }
        else {
            $this->validate([
                'identity' => "required|image|mimes:jpg|max:$max_size_upload",
                'nik' => 'required'
            ]);
            
            if ($this->nik != $this->currentNik) {
                $this->validate([
                    'nik' => 'unique:user_data'
                ]);
            }

            $identity = $this->identity->store('card_identity', 'public');
            $kyc_msg = 'PROCESS';
        }      

        $check_npwp = $this->check_npwp;
        if ($this->npwp == $this->currentNpwp) {
            $npwp = $this->currentNpwp;

            if ($this->no_npwp != $this->currentNoNpwp) {
                $check_npwp = 2;
                if ($this->user->kyc_verify_msg != 'VALID'){
                    $kyc_msg = 'PROCESS';
                }
            }
        }
        else {
            $this->validate([
                'npwp' => "image|mimes:jpg|max:$max_size_upload",
                'no_npwp' => 'required'
            ]);
            $npwp = $this->npwp->store('npwp', 'public');
            
            if ($this->user->kyc_verify_msg == 'VALID') {
                $check_npwp = 2;
            } else {
                $kyc_msg = 'PROCESS';
            }
        }

        $this->validate([
            'streetAddress' => 'required|max:255',
            'city' => 'required|max:32',
            'province' => 'required|max:32',
            'zipcode' => 'required|max:16',
            'country' => 'required',
            'password' => 'required'
        ]);

        if ($this->selectedMethod == 'OTHER') {
            $this->accountHolder = $this->firstname . ' ' . $this->lastname;
        }

        $account_msg = $user->account_verify_msg;
        if ($this->bank != $this->currentBank) {
            $this->validate([
                'bank' => 'required'
            ]);
            $newBank = $this->bank;
            $account_msg = 'PROCESS';
        } else {
            $newBank = $user->gateways_id;
        }

        if ($this->accountNo != $user->no_account) {
            if ($this->selectedMethod == 'BANK') {
                $this->validate([
                    'accountNo' => 'required|digits_between:1,32|numeric',
                ]);
            } elseif ($this->selectedMethod == 'OTHER') {
                $this->validate([
                    'accountNo' => 'required|max:255',
                ]);
            }
            $newAccountNo = $this->accountNo;
            $account_msg = 'PROCESS';
        } else {
            $newAccountNo = $user->no_account;
        }

        if ($this->accountHolder != $user->account_holder) {
            $this->validate([
                'accountHolder' => 'required'
            ]);
            $newAccountHolder = $this->accountHolder;
            $account_msg = 'PROCESS';
        } else {
            $newAccountHolder = $user->account_holder;
        }

        if (Hash::check($this->password, $user->password)) {
            $user->update([
                'street_adress' => $this->streetAddress,
                'city' => $this->city,
                'state' => $this->province,
                'zipcode' => $this->zipcode,
                'country' => $this->country,
                'updated_at' => Carbon::now(),
                'kyc_verify_msg' => $kyc_msg,
                'account_verify_msg' => $account_msg,
                'no_account' => $newAccountNo,
                'account_holder' => $newAccountHolder,
                'gateways_id' => $newBank,
                'nik' => $this->nik,
                'identity_card' => $identity,
                'npwp' => $npwp,
                'no_npwp' => $this->no_npwp,
                'check_npwp' => $check_npwp
            ]);
    
            $this->emit('showAlert', ['msg' => 'User Information has been updated.']);
            return redirect()->route('user.editprofile');
        } else {
            $this->emit('showAlertError', ['msg' => 'Password does not match.']);
        }
    }

    public function showConfirmationDeleteNpwp(){
        $this->emit('showConfirmationDeleteNpwp');
    }

    public function deleteNpwp(){
        $user = User::find(auth()->user()->id);

        User::find(auth()->user()->id)->update([
            'npwp' => null,
            'no_npwp' => null,
            'check_npwp' => 0
        ]);

        if($user->kyc_verify_msg != "VALID"){
            User::find(auth()->user()->id)->update([
                'kyc_verify_msg' => "PROCESS"
            ]);
        }
        $this->emit('hideModal');
        $this->emit('showAlert', ['msg' => 'NPWP deleted successfully']);
        return redirect()->route('user.editprofile');
    }

    public function resetForm()
    {
        
    }
}

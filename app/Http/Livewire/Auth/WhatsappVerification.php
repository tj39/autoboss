<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use App\Traits\GlobalValues;
use Carbon\Carbon;
use Livewire\Component;

class WhatsappVerification extends Component
{
    use GlobalValues;

    public $otpClicked = false;
    public $otpSent = false;
    public $phone, $digit1, $digit2, $digit3, $digit4;

    public function render()
    {
        return view('livewire.auth.whatsapp-verification');
    }

    public function sendOTP()
    {
        $this->validate([
            'phone' => 'required|regex:/([+])\d{1,3}/|min:10|max:15'
        ], [
            'phone.regex' => 'Please add country code to your phone number'
        ]);

        $otpExpired = $this->getGeneralSettingValue('otp_expired');
        $generatedOTP = $this->generateRandomNumber(4);
        
        $user = User::find(auth()->user()->id);
        $user->otp_code = $generatedOTP;
        $user->otp_expired_at = Carbon::now()->addMinutes($otpExpired);
        $user->phone = $this->phone;
        $user->save();

        $whatsoApi = $this->getGeneralSettingValue('whatso_api');
        $whatsoUsername = $this->getGeneralSettingValue('whatso_username');
        $whatsoPassword = $this->getGeneralSettingValue('whatso_password');
        $fromNumber = $this->getGeneralSettingValue('tjl_phone_number');

        $data = [
            'Username' => $whatsoUsername, // Whatso username
            'Password' => $whatsoPassword, // Whatso password
            'MessageText' => "Your TP 99 OTP code: $generatedOTP. Do not share this code with others.",
            'MobileNumbers' => $this->phone, // Receiver number
            'ScheduleDate' => '',
            'FromNumber' => $fromNumber,
            'Channel' => '1'
        ];
        $json = json_encode($data);
        $url = $whatsoApi;
        $options = stream_context_create(['http' => [
                'method'  => 'POST',
                'header'  => ['Content-type: application/json', 'Accept: application/json'],
                'content' => $json
            ]
        ]);

        // Send a request
        $result = file_get_contents($url, false, $options);

        $this->otpSent = true;
        $this->emit('showAlertSuccess', ['msg' => 'OTP sent']);
    }

    public function showOTPFields()
    {
        $this->otpClicked = true;
        $this->phone = auth()->user()->phone;
    }

    public function verify()
    {
        // $this->validate([
        //     'digit1' => 'bail|required|numeric',
        //     'digit2' => 'bail|required|numeric',
        //     'digit3' => 'bail|required|numeric',
        //     'digit4' => 'bail|required|numeric',
        // ]);

        if (empty($this->digit1) || empty($this->digit2) || empty($this->digit1) || empty($this->digit1)) {
            $this->emit('showAlertError', ['msg' => 'Please enter your OTP code.']);
            return;
        }

        $user = User::find(auth()->user()->id);
        $otpInput = $this->digit1 . $this->digit2 . $this->digit3 . $this->digit4;

        if ($user->otp_expired_at <= Carbon::now()) {
            $this->emit('showAlertError', ['msg' => 'Your OTP is expired. Please resend the OTP code.']);
            return;
        }

        if ($user->otp_code != $otpInput) {
            $this->emit('showAlertError', ['msg' => 'The OTP code you entered is not correct.']);
            return;
        }

        $user->email_verified_at = Carbon::now();
        $user->otp_verified_at = Carbon::now();
        $user->save();

        $this->emit('showAlertSuccess', ['msg' => 'Your account has been verified. Redirecting you to dashboard.']);
        return redirect()->intended('dashboard');
    }

    public function back($flag)
    {
        if ($flag == 1) {
            $this->otpClicked = false;
        } elseif ($flag == 2) {
            $this->otpSent = false;
        }
    }
}

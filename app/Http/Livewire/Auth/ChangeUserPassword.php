<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class ChangeUserPassword extends Component
{
    public $oldPassword;
    public $newPassword;
    public $confirmPassword;

    public function render()
    {
        return view('livewire.auth.change-user-password');
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName, [
            'oldPassword' => 'required',
            'newPassword' => ['required', Password::min(8)
            ->letters()
            ->mixedCase()
            ->numbers()
            ->symbols()
            // ->uncompromised()
            ],
            'confirmPassword' => 'required|min:8|same:newPassword'
        ]);
    }

    public function changePassword()
    {
        $this->resetErrorBag();

        $this->validate([
            'oldPassword' => 'required',
            'newPassword' => ['required', Password::min(8)
            ->letters()
            ->mixedCase()
            ->numbers()
            ->symbols()
            // ->uncompromised()
            ],
            'confirmPassword' => 'required|min:8|same:newPassword'
        ]);

        $user = User::find(auth()->user()->id);
        if (Hash::check($this->oldPassword, $user->password)) {
            $user->update([
                'password' => Hash::make($this->newPassword),
                'updated_at' => Carbon::now()
            ]);
            $this->emit('showAlert', [
                'msg' => 'Your password changed successfully. Please re-login using your new password.'
            ]);
            if (Auth::guard('web')->attempt(['email' => $user->email, 'password' => $this->newPassword])) {
                session()->regenerate();    
                return redirect()->intended('user/change-password');
            }
        } else {
            $this->emit('showAlertError', [
                'msg' => 'Old password does not match.'
            ]);
        }

        $this->resetForm();
        // session()->regenerate();
        // return redirect()->back()->with('success','password successfully updated.');
    }

    public function resetForm()
    {
        $this->oldPassword = null;
        $this->newPassword = null;
        $this->confirmPassword = null;
    }
}

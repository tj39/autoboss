<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Password;
use Laravel\Fortify\Contracts\FailedPasswordResetLinkRequestResponse;
use Laravel\Fortify\Contracts\RequestPasswordResetLinkViewResponse;
use Laravel\Fortify\Contracts\SuccessfulPasswordResetLinkRequestResponse;
use Laravel\Fortify\Fortify;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class PasswordResetLinkController extends Controller
{
    public function create(Request $request): RequestPasswordResetLinkViewResponse
    {
        return app(RequestPasswordResetLinkViewResponse::class);
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Support\Responsable
     */
    public function store(Request $request): Responsable
    {
        $email_count = User::where('email', $request->email)->count();

        if($email_count == 0){
            return app(SuccessfulPasswordResetLinkRequestResponse::class, ['status' => 'Email not found']);
        }
        else{
            $request->validate([Fortify::email() => 'required|email']);
        $count = DB::table('password_resets')->where('email', $request->email)->count();

        if($count){
            $record = DB::table('password_resets')->where('email', $request->email)->first();
            $daterecord = Carbon::now()->timestamp - strtotime($record->created_at);
            if($daterecord>3600){
                $status = $this->broker()->sendResetLink(
                    $request->only(Fortify::email())
                );
        
                return $status == Password::RESET_LINK_SENT
                            ? app(SuccessfulPasswordResetLinkRequestResponse::class, ['status' => $status])
                            : app(FailedPasswordResetLinkRequestResponse::class, ['status' => $status]);
            }
            else{
                return app(SuccessfulPasswordResetLinkRequestResponse::class, ['status' => 'A few seconds ago an e-mail sent with further instructions, please check your email to reset password']);
            }
        }
        else{
            $status = $this->broker()->sendResetLink(
                $request->only(Fortify::email())
            );
    
            return $status == Password::RESET_LINK_SENT
                        ? app(SuccessfulPasswordResetLinkRequestResponse::class, ['status' => $status])
                        : app(FailedPasswordResetLinkRequestResponse::class, ['status' => $status]);
        }

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        }
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    protected function broker(): PasswordBroker
    {
        return Password::broker(config('fortify.passwords'));
    }
}

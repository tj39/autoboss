<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxSlip extends Model
{
    use HasFactory;

    protected $table = 'tax_slip';
}

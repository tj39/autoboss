<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClosedTicket extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'no_ticket',
        'created_date',
        'completed_date',
        'status_id',
        'category_id',
        'user_id',
        'title',
        'message',
        'url_image'
    ];
}

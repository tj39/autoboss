<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaqList extends Model
{
    use HasFactory;

    protected $table = 'faq_list';

    protected $fillable = [
        'created_at',
        'updated_at',
        'category_id',
        'question',
        'answer',
        'is_active'
    ];
}

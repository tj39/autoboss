<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeminarEvent extends Model
{
    use HasFactory;

    protected $fillable = [
        'event_name',
        'event_date',
        'event_time',
        'url',
        'meeting_id',
        'passcode',
        'status',
        'rank'
    ];

    protected $dates = ['event_date'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WithdrawData extends Model
{
    use HasFactory;

    protected $fillable = [
        'wd_code',
        'created_at',
        'user_request',
        'confirmed_at',
        'user_confirm',
        'amount',
        'status',
        'message',
        'transfer_proof'
    ];
}

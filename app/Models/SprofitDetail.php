<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SprofitDetail extends Model
{
    use HasFactory;

    protected $table = 'sprofit_detail';

    
    /**
     * Get the userRank that owns the SprofitDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userId()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Get the userRank that owns the SprofitDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function leaderId()
    {
        return $this->belongsTo(User::class, 'leader_id', 'id');
    }
}

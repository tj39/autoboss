<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RebateFile extends Model
{
    use HasFactory;

    protected $table = 'rebate_file';

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'file_name',
        'file_path',
        'is_inserted', 
        'is_calculated',
        'is_settled',
        'payroll_code'
    ];
}

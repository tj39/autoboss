<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TraderRank extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $table = 'trader_ranks';

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'code',
        'description',
        'order',
        'deposit',
        'direct_downline',
        'turnover'
    ];
}

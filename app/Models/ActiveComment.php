<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActiveComment extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $fillable = [
        'id',
        'ticket_id',
        'user_id',
        'status_id',
        'created_date',
        'message', 
        'url_image',
        'is_read_admin',
        'is_read_user'
    ];

    /**
     * Get the user that owns the ActiveComment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SprofitData extends Model
{
    use HasFactory;

    protected $table = 'sprofit_data';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActiveTicket extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'no_ticket',
        'created_date',
        'completed_date',
        'status_id',
        'category_id',
        'user_id',
        'title',
        'message',
        'url_image',
        'is_read_admin',
        'is_read_user'
    ];

    /**
     * Get the category that owns the ActiveTicket
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(TicketCategory::class, 'category_id', 'id');
    }

    /**
     * Get the status that owns the ActiveTicket
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(TicketStatus::class, 'status_id', 'id');
    }

    /**
     * Get the user_id that owns the ActiveTicket
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PortofolioFile extends Model
{
    use HasFactory;

    protected $table = 'portofolio_file';
}

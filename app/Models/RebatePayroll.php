<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RebatePayroll extends Model
{
    use HasFactory;

    protected $table = 'rebate_payroll';
}

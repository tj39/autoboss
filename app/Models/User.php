<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $table = 'user_data';

    protected $fillable = [
        'username',
        'email',
        'password',
        'firstname',
        'lastname',
        'email',
        'email_verified_at',
        'password',
        'phone',
        'is_admin',
        'role_id',
        'avatar', 
        'identity_card',
        'nik',
        'npwp',
        'no_npwp',
        'street_adress',
        'city',
        'state',
        'zipcode',
        'country',
        'kyc_verify_msg',
        'account_verify_msg',
        'gateways_id',
        'no_account',
        'account_holder',
        'commission_balance',
        'deposit_balance',
        'withdraw_balance',
        'direct_upline',
        'step',
        'client_id',
        'check_npwp',
        'updated_clientid_at',
        'request_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];
    
    /**
     * Get the user leader
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function leaderId()
    {
        return $this->belongsTo(User::class, 'direct_upline', 'id');
    }

    /**
     * Get the user leader
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seminarId()
    {
        return $this->belongsTo(User::class, 'direct_upline', 'id');
    }

    /**
     * Get the rank request
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function requestId()
    {
        return $this->belongsTo(RankRequest::class, 'request_id', 'id');
    }

    /**
     * Get the trader_rank that owns the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trader_rank()
    {
        return $this->belongsTo(TraderRank::class, 'role_id', 'id');
    }

    /**
     * Get the seminar that owns the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seminar_events()
    {
        return $this->belongsTo(SeminarEvent::class, 'seminar_id', 'id');
    }

    /**
     * Get the user that owns the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function adminRole()
    {
        return $this->belongsTo(AdminRole::class, 'role_id', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RebateSummary extends Model
{
    use HasFactory;

    protected $table = 'rebate_summary';

    public $timestamps = false;

    /**
     * Get the userRank that owns the RebateDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function leaderId()
    {
        return $this->belongsTo(User::class, 'leader_id', 'id');
    }
}

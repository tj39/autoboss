<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientidTemp extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'clientid_temp';

    protected $fillable = [
        'client_uid',
        'reg_date'
    ];
}

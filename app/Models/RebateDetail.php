<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RebateDetail extends Model
{
    use HasFactory;

    protected $table = 'rebate_detail';

    /**
     * Get the traderRanks that owns the RebateDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function leaderRank()
    {
        return $this->belongsTo(TraderRank::class, 'leader_rank', 'code');
    }

    /**
     * Get the userRank that owns the RebateDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userRank()
    {
        return $this->belongsTo(TraderRank::class, 'user_rank', 'code');
    }

    /**
     * Get the userRank that owns the RebateDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userId()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Get the userRank that owns the RebateDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function leaderId()
    {
        return $this->belongsTo(User::class, 'leader_id', 'id');
    }
}

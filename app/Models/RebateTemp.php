<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RebateTemp extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'rebate_temp';
    
    protected $fillable = [
        'reward_date',
        'client_id',
        'volume_lots',
        'reward_order',
        'investment_id',
        'file_id'
    ];
}

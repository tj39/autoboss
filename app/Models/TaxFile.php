<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxFile extends Model
{
    use HasFactory;

    protected $table = 'tax_file';

    protected $fillable = [
        'created_at',
        'folder_name',
        'category'
    ];

    public $timestamps = false;
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterRebate extends Model
{
    use HasFactory;

    protected $table = 'master_rebate';
}

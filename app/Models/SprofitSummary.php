<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SprofitSummary extends Model
{
    use HasFactory;

    protected $table = 'sprofit_summary';

    public $timestamps = false;

    protected $fillable = [
        'no_ticket',
        'created_date',
        'completed_date',
        'status_id',
        'category_id',
        'user_id',
        'title',
        'message',
        'url_image'
    ];

    /**
     * Get the user that owns the SprofitSummary
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function leaderId()
    {
        return $this->belongsTo(User::class, 'leader_id', 'id');
    }
}

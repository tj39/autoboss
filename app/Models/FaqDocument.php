<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaqDocument extends Model
{
    use HasFactory;

    protected $table = 'faq_document';

    protected $fillable = [
        'created_at',
        'updated_at',
        'name',
        'url',
    ];
}

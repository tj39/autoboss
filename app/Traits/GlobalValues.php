<?php

namespace App\Traits;

use App\Models\GeneralSetting;

trait GlobalValues
{
    /**
     * Retrieve active value of general_settings table by code.
     *
     * @param  string  $code
     * @return mixed  $value
     */
    public function getGeneralSettingValue($code)
    {
        return GeneralSetting::where('code', $code)->where('is_active', '1')->first()->value;
    }

    /**
     * Generate random number based on character length.
     *
     * @param  string  $length
     * @return string  $value
     */
    public function generateRandomNumber($length)
    {
        return substr(str_shuffle('1234567890'), 0, $length);
    }
}
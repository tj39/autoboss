<p style="text-align: center; color: darkred;">
    Ini merupakan peringatan dini untuk Anda agar mengetahui risiko yang terjadi pada saat Anda melakukan investasi dengan strategi kami. Strategi investasi ini memiliki tingkat risiko yang tinggi dan tidak cocok untuk semua investor serta berpotensi kehilangan modal baik sebagian maupun seluruhnya. Kami tidak menjaminkan apapun terhadap hasil investasi Anda. Hasil sebelumnya tidak menjamin hasil di masa depan. Keputusan berinvestasi ada di tangan Anda.
</p>
<br>
<p style="text-align: center; color: darkred;">
    <em>This is early-warning to you to know the risk at times you are doing the investment with our strategy. This strategy has a high degree of risk and is not suitable for all investors and potentially partial or total loss of capital. We do not guarantee anything about the results of your investment. Past profits do not guarantee future results. The investment decision is in your hands.</em>
</p>
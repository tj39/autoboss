<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TicketStatus;

class TicketStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TicketStatus::create([
            'code' => 'O',
            'name' => 'Open', 
            'color' => 'green'
        ]);

        TicketStatus::create([
            'code' => 'C',
            'name' => 'Closed', 
            'color' => 'red'
        ]);

        TicketStatus::create([
            'code' => 'S',
            'name' => 'Solved', 
            'color' => 'orange'
        ]);
    }
}

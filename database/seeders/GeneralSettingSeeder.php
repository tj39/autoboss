<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class GeneralSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('general_settings')->insert([
            'created_at' => Carbon::now(),
            'code' => "pagination_row",
            'description' => "Number of data displayed per page.",
            'value' => "20",
            'is_active' => "1"
        ]);

        DB::table('general_settings')->insert([
            'created_at' => Carbon::now(),
            'code' => "min_withdraw",
            'description' => "Minimum withdraw allowed (USD).",
            'value' => "10",
            'is_active' => "1"
        ]);

        DB::table('general_settings')->insert([
            'created_at' => Carbon::now(),
            'code' => "min_withdraw",
            'description' => "Maximum withdraw allowed (USD).",
            'value' => "10",
            'is_active' => "1"
        ]);

        DB::table('general_settings')->insert([
            'created_at' => Carbon::now(),
            'code' => "fixrate_payroll",
            'description' => "Fix rate for payroll.",
            'value' => "14000",
            'is_active' => "1"
        ]);

        DB::table('general_settings')->insert([
            'created_at' => Carbon::now(),
            'code' => "sch_trans_rebate",
            'description' => "Scheduler for inserting rebate data (in seconds).",
            'value' => "60",
            'is_active' => "1"
        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TicketCategory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class TicketCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TicketCategory::create([
            'code' => 'F',
            'name' => 'Finance',
        ]);

        TicketCategory::create([
            'code' => 'A',
            'name' => 'Administration',
        ]);

        TicketCategory::create([
            'code' => 'M',
            'name' => 'Management',
        ]);

        DB::table('general_settings')->insert([
            'created_at' => Carbon::now(),
            'code' => "sch_hk_ticket",
            'description' => "Scheduller ticket closed format, date, 'hours(24)'",
            'value' => "1, '00:03'",
            'is_active' => "1"
        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => "Daffa",
            'email' => "daffaammar09@gmail.com",
            'password' => Hash::make("password"),
            'firstname' => "Daffa",
            "lastname" => "fauzan",
            'email_verified_at' => Date(now()),
            'phone' => 123123,
            'is_admin' => 1,
            'role_id' => 1,
            'avatar' => 12313,
            'identity_card' => 12313,
            'npwp' => 12313,
            'street_adress' => "Kediri",
            'city' => "Kediri", 
            'state' => "East Java",
            'zipcode' => "64182",
            'country' => "Indonesia",
            'commission_balance' => 0,
            'deposit_balance' => 0,
            'withdraw_balance' => 0,
            'no_account' => 12313,
            'account_holder' => "Daffa ammar fauzan"
        ]);
    }
}

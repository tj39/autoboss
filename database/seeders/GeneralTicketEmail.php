<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeneralTicketEmail extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('general_settings')->insert([
            'created_at' => Date(now()),
            'code' => "ticket_message",
            'description' => "Notif Ticket Message",
            'value' => "You have a new message from TP99. Please login to taunyaprofit99.com and open Support Ticket menu to read the message",
            'is_active' => "1"
        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $max = 100;
        for ($i = 0; $i < $max; $i++){
            $jenis_kartu = rand(1,4);
            $kartu = rand(1,13);

            if($kartu == 11){
                $kartu = "J";
            }
            elseif($kartu == 12){
                $kartu = "Q";
            }
            elseif($kartu == 13){
                $kartu = "K";
            }

            if($jenis_kartu == 1){
                DB::table('card')->insert([
                    'club' => $kartu,
                    'spade' => "",
                    'heart' => "",
                    'diamond' => ""
                ]);
            }
            else if($jenis_kartu == 2){
                DB::table('card')->insert([
                    'club' => "",
                    'spade' => $kartu,
                    'heart' => "",
                    'diamond' => ""
                ]);
            }
            else if($jenis_kartu == 3){
                DB::table('card')->insert([
                    'club' => "",
                    'spade' => "",
                    'heart' => $kartu,
                    'diamond' => ""
                ]);
            }
            else if($jenis_kartu == 4){
                DB::table('card')->insert([
                    'club' => "",
                    'spade' => "",
                    'heart' => "",
                    'diamond' => $kartu
                ]);
            }
        }
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdraw_data', function (Blueprint $table) {
            $table->id();
            $table->string('wd_code', 191);
            $table->timestamp('dtm_create');
            $table->bigInteger('user_request');
            $table->timestamp('dtm_confirm')->nullable();
            $table->bigInteger('user_confirm')->nullable();
            $table->float('amount');
            $table->char('status', 1);
            $table->text('message')->nullable();
            $table->string('transfer_proof', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdraw_data');
    }
}

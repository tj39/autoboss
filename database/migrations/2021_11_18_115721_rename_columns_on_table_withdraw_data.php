<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnsOnTableWithdrawData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('withdraw_data', function (Blueprint $table) {
            $table->renameColumn('dtm_create', 'created_at');
            $table->renameColumn('dtm_confirm', 'confirmed_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('withdraw_data', function (Blueprint $table) {
            //
        });
    }
}

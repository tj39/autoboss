<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActiveTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_tickets', function (Blueprint $table) {
            $table->id();
            $table->string('no_ticket');
            $table->timestamp('created_date')->nullable();
            $table->timestamp('completed_date')->nullable();
            $table->foreignId('status_id');
            $table->foreignId('category_id');
            $table->foreignId('user_id');
            $table->string('title');
            $table->text('message');
            $table->text('url_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_tickets');
    }
}

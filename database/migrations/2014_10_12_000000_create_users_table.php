<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('username', 20);
            $table->string('firstname', 25);
            $table->string('lastname', 25);
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone', 13);
            $table->char('is_admin', 1);
            $table->foreignId('role_id');
            $table->text('avatar');
            $table->text('identity_card');
            $table->text('npwp');
            $table->string('street_adress');
            $table->string('city', 32);
            $table->string('state', 32);
            $table->string('zipcode', 16);
            $table->string('country', 32);
            $table->string('kyc_verify_msg');
            $table->foreignId('gateways_id');
            $table->string('no_account', 32);
            $table->string('account_holder', 50);
            $table->string('account_verify_msg');
            $table->string('commission_balance', 16);
            $table->string('deposit_balance', 16);
            $table->string('withdraw_balance', 16);
            $table->rememberToken();
            $table->foreignId('current_team_id')->nullable();
            $table->string('profile_photo_path', 2048)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

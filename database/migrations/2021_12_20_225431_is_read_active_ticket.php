<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IsReadActiveTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('active_tickets', function (Blueprint $table) {
            $table->char('is_read_admin', 1)->default(0);
            $table->char('is_read_user', 1)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('active_tickets', function (Blueprint $table) {
            //
        });
    }
}

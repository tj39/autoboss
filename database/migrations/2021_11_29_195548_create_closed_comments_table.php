<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClosedCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('closed_comments', function (Blueprint $table) {
            $table->id('closed_id');
            $table->foreignId('ticket_id');
            $table->foreignId('user_id');
            $table->timestamp('created_date')->nullable();
            $table->text('message');
            $table->text('url_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('closed_comments');
    }
}

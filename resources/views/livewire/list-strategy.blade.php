<div>
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>List Strategy</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            @if (Auth::user()->step != 'OK')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>You can access this page after all steps are completed.</h6>
                    </div>
                </div>
            @else
                <div class="card card-custom">
                    <div class="card-body">
                        <div class="row">
                            <div class="span">
                                <p>Choose one of the strategies bellow:</p>
                            </div>
                        </div>
                        <div class="row mb-3 d-flex justify-content-start">
                            <label for="strategy" class="col-sm-3 col-form-label"><strong>Strategy</strong></label>
                            <div class="col-sm-9">
                                @foreach ($strategies as $item)
                                    <button wire:click="showModalStrategyDetail({{ $item->id }})" wire:loading.attr="disabled" type="button" class="btn {{ $choosenStrategy == $item->id ? 'btn-success' : 'btn-outline-success' }}">{{ $item->net_profit . ' : ' . $item->commission }}</button>
                                @endforeach
                                <br>
                                @error('choosenStrategy')
                                    <small id="helpId" class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-custom">
                    <div class="card-body">
                        @include('livewire.strategy-detail-modal')
                        <div class="row mb-3">
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input type="text" placeholder="Search investment ID" class="form-control" wire:model="search">
                                    <div class="input-group-prepend">
                                        <button type="button" class="btn btn-success">
                                            <i class="fa fa-search search-icon"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-sm table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">No.</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Investment ID</th>
                                        <th scope="col">Initial Investment</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $key => $item)
                                    <tr>
                                        <th scope="row">{{ $data->firstItem() + $key }}</th>
                                        <td>{{ $item->date }}</td>
                                        <td>{{ $item->investment_id }}</td>
                                        <td>{{ $item->initial_investment == 0 ? '' : '$'.$item->initial_investment }}</td>
                                        @if ($item->status == 'ACTIVE')
                                            <td style="color: green">{{ $item->status }}</td>
                                        @else
                                            <td style="color: red">{{ $item->status }}</td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $data->links() }}
                        </div>
                        <div id="modal-confirmation" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
                            aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header alert-danger">
                                        <h4 class="modal-title" id="my-modal-title">Are you sure?</h4>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                        <button type="button" wire:click.prevent="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                            <div wire:loading.remove wire:target="submit">Yes</div>
                                            <div wire:loading wire:target="submit">
                                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <script>        
        document.addEventListener('livewire:load', function(e) {
            e.preventDefault()

            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });

            window.livewire.on('showModalStrategyDetail', (data) => {
                $('#modal-strategy-detail').modal('show')
            });

            window.livewire.on('hideModalStrategyDetail', (data) => {
                $('#modal-strategy-detail').modal('hide')
            });
        })
    </script>
</div>


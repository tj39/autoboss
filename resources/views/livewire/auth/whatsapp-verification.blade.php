@push('styles')
    <style>
        .otp {
            display: inline-block;
            width: 50px;
            height: 50px;
            text-align: center;
            margin: 2%;
        }
    </style>
@endpush
<div class="vertical-center">
    <div class="container" style="margin-top: -3%">
        <div class="row justify-content-center">
            <div class="col-md-5 justify-content-center">
                <div class="card" style="margin-top: 3%">
                    <div class="card-body">
                        <img src="{{ asset('assets/images/logo.png') }}" style="width: 30%; margin-left: auto; margin-right: auto;">
                        @if (!$otpClicked)
                            <x-jet-validation-errors class="mb-4" />
                            @if (session('status') == 'verification-link-sent')
                            <div class="mb-4 font-medium text-sm text-green-600">
                                {{ __('A new verification link has been sent to the email address you provided during registration.') }}
                            </div>
                            @endif
                            <div class="text-center" style="font-size: 13px">
                                <h3>SIGN IN</h3>
                                <span>Thanks for signing up! Before getting started, could you verify your email
                                    address by clicking on the link we just emailed to you? If you didn't
                                    receive
                                    the email, we will gladly send you another.</span>
                            </div>
                            <div class="mt-4 items-center justify-between">
                                <div class="row mb-3">
                                    <form method="POST" action="{{ route('verification.send') }}">
                                        @csrf
                                        <div class="d-flex justify-content-center">
                                            <button type="submit" class="btn btn-primary">Resend Verification Email</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="row mb-3 text-sm" style="text-align: center">
                                    <p>Still didn't receive the verification email? <a class="underline" href="#" wire:click="showOTPFields">Send OTP code via Whatsapp</a></p>
                                </div>
                                <div class="row mb-3">
                                    <form method="POST" action="{{ route('logout') }}">
                                        @csrf
                                        <div class="d-flex justify-content-center">
                                            <button type="submit" class="underline text-sm text-gray-600 hover:text-gray-900">
                                                {{ __('Log Out') }}
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        @else
                            <div class="mt-4 items-center">
                                @if ($otpSent)
                                    <div class="row">
                                        <div class="text-center" style="font-size: 13px">
                                            <span>Enter the OTP code we've sent to your Whatsapp number. The code will expire in 30 minutes.</span>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <form class="d-flex justify-content-center" method="get" class="digit-group" data-group-name="digits" data-autosubmit="false" autocomplete="off">
                                            <input wire:model="digit1" class="otp" type="text" oninput='digitValidate(this)' onkeyup='tabChange(1)' maxlength=1 >
                                            <input wire:model="digit2" class="otp" type="text" oninput='digitValidate(this)' onkeyup='tabChange(2)' maxlength=1 >
                                            <input wire:model="digit3" class="otp" type="text" oninput='digitValidate(this)' onkeyup='tabChange(3)' maxlength=1 >
                                            <input wire:model="digit4" class="otp" type="text" oninput='digitValidate(this)' onkeyup='tabChange(4)' maxlength=1 >
                                        </form>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-12 col-md-12 d-flex justify-content-center">
                                            <button wire:click="back(2)" class="btn btn-secondary" wire:loading.attr="disabled" style="margin-right: 1%">
                                                <div wire:loading.remove wire:target="back(2)">Back</div>
                                                <div wire:loading wire:target="back(2)">
                                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                    Loading...
                                                </div>
                                            </button>
                                            <button wire:click="verify" class="btn btn-primary" wire:loading.attr="disabled" style="margin-left: 1%">
                                                <div wire:loading.remove wire:target="verify">Verify</div>
                                                <div wire:loading wire:target="verify">
                                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                    Loading...
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="d-flex justify-content-center text-sm">
                                            <a class="link-secondary" id="resendOTPLink" onclick="updateCountdown()" wire:click="sendOTP" href="#">Resend OTP</a><span id="countdown"></span>
                                        </div>
                                    </div>
                                @else
                                    <div class="row mb-3">
                                        <div class="text-center" style="font-size: 13px">
                                            <span>Please make sure your Whatsapp number is correct before sending OTP code.</span>
                                        </div>
                                    </div>
                                    <div class="row mb-3 d-flex justify-content-center">
                                        <div class="col-sm-12 col-md-8">
                                            <input type="text" class="form-control" id="phone" name="phone" wire:model="phone" placeholder="+628123456789" onkeypress="return isNumberKey(event)">
                                            @error('phone')
                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-12 col-md-12 d-flex justify-content-center">
                                            <button wire:click="back(1)" class="btn btn-secondary" wire:loading.attr="disabled" style="margin-right: 1%">
                                                <div wire:loading.remove wire:target="back(1)">Back</div>
                                                <div wire:loading wire:target="back(1)">
                                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                    Loading...
                                                </div>
                                            </button>
                                            <button wire:click="sendOTP" class="btn btn-primary" wire:loading.attr="disabled" style="margin-left: 1%">
                                                <div wire:loading.remove wire:target="sendOTP">Send OTP</div>
                                                <div wire:loading wire:target="sendOTP">
                                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                    Loading...
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        let digitValidate = function(ele) {
            console.log(ele.value);
            ele.value = ele.value.replace(/[^0-9]/g,'');
        }

        let tabChange = function(val) {
            let ele = document.querySelectorAll('input');
            if (ele[val-1].value != '') {
            ele[val].focus()
            } else if (ele[val-1].value == '') {
                ele[val-2].focus()
            }   
        }

        let timeleft = 60;
        let downloadTimer;
        function updateCountdown() {
            downloadTimer = setInterval(function() {
                timeleft--;
                document.getElementById("countdown").textContent = "Resend OTP in " + timeleft + " seconds";
                let el = document.getElementById("resendOTPLink");
                el.style.display = 'none';
                if (timeleft <= 0) {
                    clearInterval(downloadTimer);
                    el.style.display = 'block';
                    document.getElementById("countdown").style.display = 'none';
                }
            }, 1000);
        }
    </script>
@endpush
<div>
    <x-loading-indicator />
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Edit Profile</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="conatiner-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title ml-2">User Profile</h4>
                            <form wire:submit.prevent="saveProfile" role="form">
                                <div class="form-group">
                                    <label for="" class="mb-2">Profile Picture</label><br>
                                    <label for="image-picker">
                                        @if (optional($avatar)->temporaryUrl())
                                        <img class="img-fluid" id="image-preview" src="{{ $avatar->temporaryUrl() }}"
                                            alt="your image" width="200" height="200"/>
                                        @elseif ($avatar != null)
                                        <img class="img-fluid" id="image-preview" src="{{ asset('storage/'.$avatar) }}"
                                        width="200" height="200"/>
                                        @else
                                        <img class="img-fluid" id="image-preview" src="{{ asset('assets/images/avatars/01.png') }}"
                                            alt="your image" width="200" height="200"/>
                                        @endif
                                    </label>
                                    <input id="image-picker" wire:model="avatar" type="file" accept="image/*" class="d-none" />
                                    <br>
                                    <small>Click the picture to change.</small>
                                    @error('avatar')
                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="clientId" class="form-label">Client ID</label>
                                        <input value="{{ $clientId }}" type="text" class="form-control" name="clientId" id="clientId" wire:model="clientId" {{ Auth::user()->is_clientid_valid == '1' || Auth::user()->step == '1' || Auth::user()->step == '2' ? 'disabled' : '' }} maxlength="8">
                                        @error('clientId')
                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="username" class="form-label">Username<span style="color: red"> *</span></label>
                                        <input value="{{ $username }}" type="text" class="form-control" name="username" id="username" wire:model="username" disabled>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="firstname" class="form-label">First Name<span style="color: red"> *</span></label>
                                        <input value="{{ $firstname }}" type="text" class="form-control" name="firstname" id="firstname" wire:model="firstname" placeholder="First name">
                                        @error('firstname')
                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="lastname" class="form-label">Last Name<span style="color: red"> *</span></label>
                                        <input value="{{ $lastname }}" type="text" class="form-control" name="lastname" id="lastname" wire:model="lastname" placeholder="Last name">
                                        @error('lastname')
                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12 d-flex justify-content-end">
                                        <button type="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                            <div wire:loading.remove wire:target="saveProfile">Save</div>
                                            <div wire:loading wire:target="saveProfile">
                                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                Loading...
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title ml-2">User Information</h4>
                            <form wire:submit.prevent="saveUserInformation" role="form">
                                <div class="row">
                                    @if ($user->kyc_verify_msg != 'VALID')
                                        <div class="form-group col-md-6 col-sm-12">
                                            <label for="" class="mb-2">ID Card</label><br>
                                            <label for="image-picker1">
                                                @if (optional($identity)->temporaryUrl())
                                                <img class="img-fluid" id="image-preview1" src="{{ $identity->temporaryUrl() }}"
                                                    alt="your image" width="400" height="253"/>
                                                @elseif ($identity != null)
                                                <img class="img-fluid" id="image-preview1" src="{{ asset('storage/'.$identity) }}"
                                                width="400" height="253"/>
                                                @else
                                                <img class="img-fluid" id="image-preview1" src="{{ asset('assets/images/image-error-400x253.png') }}"
                                                    alt="your image" width="400" height="253"/>
                                                @endif
                                            </label>
                                            @if ($user->kyc_verify_msg != 'VALID')
                                                <input id="image-picker1" wire:model="identity" type="file" accept="image/*" class="d-none" />
                                                <br>
                                                <small>Click the picture to change.</small>
                                            @endif
                                            @error('identity')
                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    @endif
                                    @if ($user->check_npwp != '1')
                                        @if ($user->kyc_verify_msg == 'VALID')
                                            <div class="form-group col-md-6 col-sm-12"></div>
                                        @endif
                                        <div class="form-group col-md-6 col-sm-12">
                                            <label for="" class="mb-2">NPWP</label><br>
                                            <label for="image-picker2">
                                                @if (optional($npwp)->temporaryUrl())
                                                <img class="img-fluid" id="image-preview2" src="{{ $npwp->temporaryUrl() }}"
                                                    alt="your image" width="400" height="253"/>
                                                @elseif ($npwp != null)
                                                <img class="img-fluid" id="image-preview2" src="{{ asset('storage/'.$npwp) }}"
                                                width="400" height="253"/>
                                                @else
                                                <img class="img-fluid" id="image-preview2" src="{{ asset('assets/images/image-error-400x253.png') }}"
                                                    alt="your image" width="400" height="253"/>
                                                @endif
                                            </label>
                                            @if ($user->check_npwp != '1')
                                                <input id="image-picker2" wire:model="npwp" type="file" accept="image/*" class="d-none" />
                                                <br>
                                                <small>Click the picture to change.</small>
                                            @endif
                                            @error('npwp')
                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    @endif
                                    <div class="form-group col-md-6">
                                        <label for="nik" class="form-label">No. Identity<span style="color: red"> *</span></label>
                                        <input value="{{ $nik }}" type="text" class="form-control profile" name="nik" id="nik" wire:model="nik" {{ $user->kyc_verify_msg == 'VALID' && $user->nik != '' ? 'disabled' : '' }} placeholder="1091009901000009">
                                        @error('nik')
                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="no_npwp" class="form-label">No. NPWP</label>
                                        <input value="{{ $no_npwp }}" type="text" class="form-control profile" name="no_npwp" id="no_npwp" wire:model="no_npwp" {{ $user->check_npwp == '1' ? 'disabled' : '' }} placeholder="00.125.555.0-123.000">
                                        @error('no_npwp')
                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                        @enderror
                                        <br>
                                        <button type="button" class="btn btn-primary pull-right" wire:click = "showConfirmationDeleteNpwp">Delete NPWP</button>
                                    </div>
                                </div>
                                <h5 class="text-divider"><span>Address</span></h5>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="streetAddress" class="form-label">Street Address<span style="color: red"> *</span></label>
                                        <input value="{{ $streetAddress }}" type="text" class="form-control" name="streetAddress" id="streetAddress" wire:model="streetAddress" placeholder="Street address">
                                        @error('streetAddress')
                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="city" class="form-label">City<span style="color: red"> *</span></label>
                                        <input value="{{ $city }}" type="text" class="form-control" name="city" id="city" wire:model="city" placeholder="City">
                                        @error('city')
                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="province" class="form-label">Province<span style="color: red"> *</span></label>
                                        <input value="{{ $province }}" type="text" class="form-control" name="province" id="province" wire:model="province" placeholder="Province">
                                        @error('province')
                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="zipcode" class="form-label">Zip Code<span style="color: red"> *</span></label>
                                        <input value="{{ $zipcode }}" type="text" class="form-control" name="zipcode" id="zipcode" wire:model="zipcode" placeholder="Zip code">
                                        @error('zipcode')
                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="country" class="form-label">Country<span style="color: red"> *</span></label>
                                        <div>
                                            <select class="form-select" aria-label="Country" name="country"
                                                wire:model.lazy="country" id = "country">
                                                <option value="">Select Country</option>
                                                @foreach ($countries as $country)
                                                <option value="{{ $country }}" {{ $country == $currentCountry ? 'selected' : '' }}>{{ $country }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('country')
                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <h5 class="text-divider"><span>Transfer Gateway</span></h5>
                                <div class="row">
                                    <div class="form-group col-md-12 col">
                                        <label for="selectedMethod" class="form-label">Transfer Method<span style="color: red"> *</span>
                                            <small id="helpId{{'selectedMethod'}}"
                                            class="text-danger">{{ $errors->has('selectedMethod') ? $errors->first('selectedMethod') : '' }}</small>
                                        </label>
                                        <div wire:ignore>
                                            <select class="form-select" aria-label="Default select example" name="selectedMethod"
                                                wire:model="selectedMethod" id="selectedMethod" {{ $user->account_verify_msg == 'VALID' ? 'disabled' : '' }}>
                                                <option value="BANK">Bank</option>
                                                <option value="OTHER">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                @if ($selectedMethod == 'BANK')
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="bank" class="form-label">Bank Name<span style="color: red"> *</span></label>
                                            <div wire:ignore>
                                                <select class="form-select" aria-label="Bank" name="bank" id="bank"
                                                    wire:model.lazy="bank" {{ $user->account_verify_msg == 'VALID' ? 'disabled' : '' }}>
                                                    <option value="">Select Bank</option>
                                                    @foreach ($banks as $bank)
                                                    <option value="{{ $bank->id }}" {{ $bank->id == $currentBank ? 'selected' : '' }}>{{ $bank->description }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @error('bank')
                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="accountNo" class="form-label">Account No.<span style="color: red"> *</span></label>
                                            <input value="{{ $accountNo }}" type="text" class="form-control" name="accountNo" id="accountNo" wire:model="accountNo" {{ $user->account_verify_msg == 'VALID' ? 'disabled' : '' }} placeholder="Account number">
                                            @error('accountNo')
                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label for="accountHolder" class="form-label">Account Holder<span style="color: red"> *</span></label>
                                            <input value="{{ $accountHolder }}" type="text" class="form-control" name="accountHolder" id="accountHolder" wire:model="accountHolder" {{ $user->account_verify_msg == 'VALID' ? 'disabled' : '' }} placeholder="Account holder name">
                                            @error('accountHolder')
                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </div>
                                @elseif ($selectedMethod == 'OTHER')
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="selectedBank" class="form-label">Gateway<span style="color: red"> *</span>
                                                <small id="helpId{{'selectedBank'}}"
                                                class="text-danger">{{ $errors->has('selectedBank') ? $errors->first('selectedBank') : '' }}</small>
                                            </label>
                                            <select class="form-select" aria-label="Bank" name="bank" id="bank"
                                                wire:model.lazy="bank" {{ $user->account_verify_msg == 'VALID' ? 'disabled' : '' }}>
                                                <option value="">Select Gateway</option>
                                                @foreach ($banks as $bank)
                                                <option value="{{ $bank->id }}" {{ $bank->id == $currentBank ? 'selected' : '' }}>{{ $bank->description }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="accountNo" class="form-label">Wallet Address<span style="color: red"> *</span></label>
                                            <input value="{{ $accountNo }}" type="text" class="form-control" name="accountNo" id="accountNo" 
                                                wire:model="accountNo" {{ $user->account_verify_msg == 'VALID' ? 'disabled' : '' }} placeholder="Wallet address"
                                                oninput="this.value = this.value.replace(/[^a-zA-Z0-9]/g,'');">
                                            @error('accountNo')
                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                            @enderror
                                            @if ($transferGateway != null && $transferGateway->code == 'USDT')
                                                <small><span><i class="fas fa-exclamation-circle"></i></span> Please ensure the wallet address you entered is a valid Tether(TRC20) Wallet Address.</small>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                                <h5 class="text-divider"><span>Contact</span></h5>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="email" class="form-label">Email<span style="color: red"> *</span></label>
                                        <input value="{{ $email }}" type="text" class="form-control" name="email" id="email" wire:model="email" disabled>
                                        @error('email')
                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="phone" class="form-label">Phone<span style="color: red"> *</span></label>
                                        <input value="{{ $phone }}" type="text" class="form-control" name="phone" id="phone" wire:model="phone" disabled>
                                        @error('phone')
                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-group col-md-6">
                                        <label for="password" class="form-label">Confirm Password<span style="color: red"> *</span></label>
                                        <input type="password" class="form-control" name="password" id="password" wire:model="password" placeholder="Please enter your password to save">
                                        @error('password')
                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12 d-flex justify-content-end">
                                        <button type="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                            <div wire:loading.remove wire:target="saveUserInformation">Save</div>
                                            <div wire:loading wire:target="saveUserInformation">
                                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                Loading...
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-confirm-npwp" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
        aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="alert-danger">
                    <h5 class="modal-title" id="my-modal-title">Are you sure?</h5>
                </div>
                <div class="modal-footer justify-content-center">
                    <button id='closeModal' type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-primary" wire:click="deleteNpwp">Yes</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            window.livewire.on('reloadform', component => {
                // $("#country").select2({
                //     theme: "bootstrap-5"
                // });
                // $('#country').on('change', function (e) {
                //     var item = $('#country').select2("val");
                //     @this.set('country', item);
                // });

                $("#bank").select2({
                    theme: "bootstrap-5"
                });
                $('#bank').on('change', function (e) {
                    var item = $('#bank').select2("val");
                    @this.set('bank', item);
                });
            })

            window.livewire.on('showConfirmationDeleteNpwp', (data) => {
                // console.log(data)
                $('#modal-confirm-npwp').modal('show')
            });

            window.livewire.on('hideModal', (data) => {
                // console.log(data)
                $('#modal-confirm-npwp').modal('hide')
            });

            $("#bank").select2({
                theme: "bootstrap-5"
            });
            $('#bank').on('change', function (e) {
                var item = $('#bank').select2("val");
                @this.set('bank', item);
            });

            $("#country").select2({
                theme: "bootstrap-5"
            });
            $('#country').on('change', function (e) {
                var item = $('#country').select2("val");
                @this.set('country', item);
            });

            Livewire.hook('message.processed', (el, component) => {
                $("#country").select2({
                    theme: "bootstrap-5"
                });
                $('#country').on('change', function (e) {
                    var item = $('#country').select2("val");
                    @this.set('country', item);
                });

                $("#bank").select2({
                theme: "bootstrap-5"
                });
                $('#bank').on('change', function (e) {
                    var item = $('#bank').select2("val");
                    @this.set('bank', item);
                });
            })
        });
    </script>
</div>
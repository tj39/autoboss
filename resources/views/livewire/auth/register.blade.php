<div>
    <div class="vertical-center">
        <x-loading-indicator />
        <div class="container">
            <form wire:submit.prevent="store">
                @csrf
                @if ($step == 0)
                <div class="row">
                    <div class="col d-flex justify-content-center">
                        <div class="col-md-2.5 regisphone">
                            <div class="card" style="min-height: 600px">
                                <div class="card-body">
                                    <img src="{{ asset('assets/images/logo1.png') }}" style="width: 100px; margin-left: auto; margin-right: auto;margin-bottom: 10%;">
                                    <span class="badge rounded-pill bg-primary1" style="font-size: 20px; line-height: 1.16;">1</span>
                                    <span>Reference</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">2</span>
                                    <span>Personal</span><span> Information</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">3</span>
                                    <span>Address</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">4</span>
                                    <span>Transfer Gateway</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">5</span>
                                    <span>Contact & Security</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card" style="min-height: 600px">
                                <div class="card-body">
                                    <div class="text-center" style="font-size: 13px">
                                        <h3>Reference</h3>
                                        <span>Please enter your information and proceed to</span>
                                        <br>
                                        <span>the next step so we can build your account.</span>
                                    </div>
                                    <div class="w-95 p-3 align-center" style="margin-top: -2% !important;">
                                        <label for="referenceLeader" class="form-label">Reference Leader:<span style="color: red">*</span>
                                            <small id="helpId{{'referenceLeader'}}"
                                            class="text-danger">{{ $errors->has('referenceLeader') ? $errors->first('referenceLeader') : '' }}</small>
                                        </label>
                                        <input type="text" class="form-control" id="referenceLeader" name="referenceLeader"
                                            wire:model="referenceLeader" placeholder="Referral Code">
                                    </div>
                                    <div class="flex items-center">
                                        <div class="col-md-auto">
                                            <button type="button" wire:click="checkReferral" wire:loading.attr="disabled" class="btn btn-primary ml-4">
                                                <div wire:loading.remove wire:target="checkReferral">Check</div>
                                                <div wire:loading wire:target="checkReferral">
                                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                    Loading...
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                    @if ($checkedReferal == 1)
                                    <div class="card" style="margin-top: 5%; background-color: #E91E1E;">
                                        <div class="card-body" style="background-color: #FFDFDF">
                                            <div class="text-center" style="font-size: 13px">
                                                <h3 style="color: #E91E1E">INVALID REFERENCE!</h3>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @if ($checkedReferal == 2)
                                    <div class="card" style="margin-top: 5%; background-color: #4CAF50;">
                                        <div class="card-body" style="background-color: #E4F2C7">
                                            <div class="text-center" style="font-size: 13px">
                                                <h3 style="color: #35AB97">YOUR LEADER CONTACT</h3>
                                                <h6>Name : {{ $user_referral->firstname }} {{ $user_referral->lastname }}</h6>
                                                <h6>Phone : {{ $user_referral->phone }}</h6>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="flex items-center justify-end"
                                        style="position: absolute; bottom: 0; right: 0%; margin-bottom: 3%; margin-right: 3%;">
                                        <button type="button" class="btn btn-primary btn-circle btn-md"
                                            wire:click="increaseStep"><i class="fas fa-chevron-circle-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if ($step == 1)
                <div class="row">
                    <div class="col d-flex justify-content-center">
                        <div class="col-md-2.5 regisphone">
                            <div class="card" style="min-height: 600px">
                                <div class="card-body">
                                    <img src="{{ asset('assets/images/logo1.png') }}" style="width: 100px; margin-left: auto; margin-right: auto;margin-bottom: 10%;">
                                    <span class="badge rounded-pill bg-light text-dark " style="font-size: 20px; line-height: 1.15;">1</span>
                                    <span>Reference</span>
                                    <br>
                                    <span class="badge rounded-pill bg-primary1" style="font-size: 20px; margin-top:3%; line-height: 1.15;">2</span>
                                    <span>Personal</span><span> Information</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">3</span>
                                    <span>Address</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">4</span>
                                    <span>Transfer Gateway</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">5</span>
                                    <span>Contact & Security</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card" style="min-height: 600px">
                                <div class="card-body">
                                    <div class="text-center" style="font-size: 13px">
                                        <h3>Personal Information</h3>
                                        <span>Please enter your information and proceed to</span>
                                        <br>
                                        <span>the next step so we can build your account.</span>
                                    </div>
                                    <div class="w-90 p-3 align-center" style="margin-top: -2% !important;">
                                        <label for="username" class="form-label">Username<span style="color: red">*</span>
                                             <small id="helpId{{'username'}}"
                                                class="text-danger">{{ $errors->has('username') ? $errors->first('username') : '' }}</small>
                                        </label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" name="username" wire:model="username" placeholder="Username">
                                            <button type="button" wire:click="checkUsername" wire:loading.attr="disabled" class="btn btn-primary ml-4">
                                                <div wire:loading.remove wire:target="checkUsername">Check</div>
                                                <div wire:loading wire:target="checkUsername">
                                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                    Loading...
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row w-90 p-3 align-center" style="margin-top: -10%">
                                        <div class="col {{$errors->has('firstname') ? 'has-error has-feedback' : '' }}">
                                            <label for="exampleFormControlInput1" class="form-label">Firstname<span style="color: red">*</span>
                                                <small id="helpId{{'firstname'}}"
                                                class="text-danger">{{ $errors->has('firstname') ? $errors->first('firstname') : '' }}</small>
                                            </label>
                                            <input type="text" class="form-control" name="firstname" wire:model="firstname" placeholder="Firstname">
                                        </div>
                                        <div class="col {{$errors->has('lastname') ? 'has-error has-feedback' : '' }}">
                                            <label for="exampleFormControlInput1" class="form-label">Lastname<span style="color: red">*</span>
                                                <small id="helpId{{'lastname'}}"
                                                class="text-danger">{{ $errors->has('lastname') ? $errors->first('lastname') : '' }}</small>
                                            </label>
                                            <input type="text" class="form-control" name="lastname" wire:model="lastname" placeholder="Lastname">
                                        </div>
                                    </div>
                                    <div class="row p-3 align-center" style="margin-top: -8%">
                                        <label for="avatar" class="col-form-label">Profile Picture<span style="color: red">*</span>
                                            <small id="helpId{{'avatar'}}"
                                                    class="text-danger">{{ $errors->has('avatar') ? $errors->first('avatar') : '' }}</small>
                                        </label>
                                        <div class="input-group">
                                            <input type="file" class="form-control" id="avatar" name="avatar" wire:model="avatar" style="width: 22%">
                                        </div>
                                    </div>
                                    <div class="row w-90 p-3 align-center" style="align-items: self-end; margin-top: -7%">
                                        <div class="col ">
                                            <label for="exampleFormControlInput1" class="form-label">No. NPWP</label>
                                            <input type="text" class="form-control" name="no_npwp" wire:model="no_npwp" placeholder="00.125.555.0-123.000">
                                        </div>
                                        <div class="col ">
                                            <label for="npwp" class="col-form-label">NPWP</label>
                                            <div class="input-group">
                                                <input type="file" class="form-control" id="npwp" name="npwp"
                                                    wire:model="npwp" style="width: 22%">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row w-90 p-3 align-center" style="align-items: self-end; margin-top: -7%">
                                        <div class="col ">
                                            <label for="exampleFormControlInput1" class="form-label">No. Identity<span style="color: red">*</span>
                                                <small id="helpId{{'nik'}}"
                                                    class="text-danger">{{ $errors->has('nik') ? $errors->first('nik') : '' }}</small>
                                            </label>
                                            <input type="text" class="form-control" name="nik" wire:model="nik" placeholder="1091009901000009">
                                        </div>
                                        <div class="col {{$errors->has('card_identity') ? 'has-error has-feedback' : '' }}">
                                            <label for="card_identity" class="col-form-label">ID Card/Passport<span style="color: red">*</span>
                                                <small id="helpId{{'card_identity'}}"
                                                class="text-danger">{{ $errors->has('card_identity') ? $errors->first('card_identity') : '' }}</small>
                                            </label>
                                            <div class="input-group">
                                                <input type="file" class="form-control" id="card_identity" name="card_identity"
                                                    wire:model="card_identity" style="width: 48%">
                                            </div>
                                        </div>
                                    </div>
                                    <div style="display: flex; justify-content: space-between;">
                                        <div class="flex items-center justify-end" style="bottom: 0;left: 0%;margin-bottom: 3%;margin-left: 3%;writing-mode: vertical-lr;">
                                            <button type="button" class="btn btn-primary btn-circle btn-md" wire:click="decreaseStep">
                                                <i class="fas fa-chevron-circle-left"></i>
                                            </button>
                                        </div>
                                        <div class="flex items-center justify-end" style="bottom: 0;right: 0%;margin-bottom: 3%;margin-right: 3%;">
                                            <button type="button" class="btn btn-primary btn-circle btn-md" wire:click="increaseStep">
                                                <i class="fas fa-chevron-circle-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if ($step == 2)
                <div class="row">
                    <div class="col d-flex justify-content-center">
                        <div class="col-md-2.5 regisphone">
                            <div class="card" style="min-height:600px">
                                <div class="card-body">
                                    <img src="{{ asset('assets/images/logo1.png') }}" style="width: 100px; margin-left: auto; margin-right: auto;margin-bottom: 10%;">
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; line-height: 1.15;">1</span>
                                    <span>Reference</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">2</span>
                                    <span>Personal</span><span> Information</span>
                                    <br>
                                    <span class="badge rounded-pill bg-primary1" style="font-size: 20px; margin-top:3%; line-height: 1.15;">3</span>
                                    <span>Address</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">4</span>
                                    <span>Transfer Gateway</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">5</span>
                                    <span>Contact & Security</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card" style="min-height: 600px">
                                <div class="card-body">
                                    <div class="text-center" style="font-size: 13px">
                                        <h3>Address</h3>
                                        <span>Please enter your information and proceed to</span>
                                        <br>
                                        <span>the next step so we can build your account.</span>
                                    </div>
                                    <div class="w-95 p-3 align-center" style="margin-top: -2% !important;">
                                        <label for="address" class="form-label">Street Address<span style="color: red">*</span>
                                            <small id="helpId{{'address'}}"
                                            class="text-danger">{{ $errors->has('address') ? $errors->first('address') : '' }}</small>
                                        </label>
                                        <input type="text" class="form-control" id="address" name="address"
                                            wire:model="address" placeholder="Street Address">
                                    </div>
                                    <div class="row w-90 p-3 align-center" style="margin-top: -5%;align-items: self-end;">
                                        <div class="col">
                                            <label for="exampleFormControlInput1" class="form-label">City<span style="color: red">*</span>
                                                <small id="helpId{{'city'}}"
                                                class="text-danger">{{ $errors->has('city') ? $errors->first('city') : '' }}</small>
                                            </label>
                                            <input type="text" class="form-control" name="city" wire:model="city" placeholder="City">
                                        </div>
                                        <div class="col">
                                            <label for="exampleFormControlInput1" class="form-label">Province<span style="color: red">*</span>
                                                <small id="helpId{{'province'}}"
                                                class="text-danger">{{ $errors->has('province') ? $errors->first('province') : '' }}</small>
                                            </label>
                                            <input type="text" class="form-control" name="province" wire:model="province" placeholder="Province">
                                        </div>
                                    </div>
                                    <div class="row w-90 p-3 align-center" style="margin-top: -5%">
                                        <div class="col">
                                            <label for="exampleFormControlInput1" class="form-label">Zip Code<span style="color: red">*</span>
                                                <small id="helpId{{'zip_code'}}"
                                                class="text-danger">{{ $errors->has('zip_code') ? $errors->first('zip_code') : '' }}</small>
                                            </label>
                                            <input type="text" class="form-control" name="zip_code" wire:model="zip_code" placeholder="Zip Code" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
                                        </div>
                                        <div class="col">
                                            <label for="exampleFormControlInput1" class="form-label">Country<span style="color: red">*</span>
                                                <small id="helpId{{'country'}}"
                                                class="text-danger">{{ $errors->has('country') ? $errors->first('country') : '' }}</small>
                                            </label>
                                            <div wire:ignore>
                                                <select class="form-select" aria-label="Default select example" name="country" id = "country" wire:model.lazy = "country">
                                                    <option value="">Select Country</option>
                                                    @foreach ($countries as $country)
                                                    <option value="{{ $country }}">{{ $country }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row w-90 p-3 align-center" style="margin-top: -3%">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="address_checkbox"
                                                name="address_checkbox" wire:model="address_checkbox">
                                            <label class="form-check-label" for="address_checkbox">
                                                The address data above is based on Card Identity
                                            </label>
                                        </div>
                                    </div>
                                    <div style="display: flex; justify-content: space-between;">
                                        <div class="flex items-center justify-end" style="bottom: 0;left: 0%;margin-bottom: 3%;margin-left: 3%;writing-mode: vertical-lr;">
                                            <button type="button" class="btn btn-primary btn-circle btn-md" wire:click="decreaseStep">
                                                <i class="fas fa-chevron-circle-left"></i>
                                            </button>
                                        </div>
                                        <div class="flex items-center justify-end" style="bottom: 0; right: 0%; margin-bottom: 3%; margin-right: 3%;">
                                            @if ($address_checkbox == false || $address_checkbox == null)
                                            <button type="button" class="btn btn-primary btn-circle btn-md"
                                                wire:click="increaseStep" disabled><i
                                                    class="fas fa-chevron-circle-right"></i></button>
                                            @elseif ($address_checkbox == true)
                                            <button type="button" class="btn btn-primary btn-circle btn-md"
                                                wire:click="increaseStep"><i class="fas fa-chevron-circle-right"></i></button>
                                            @endif
                                        </div>
                                    </div>                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if ($step == 3)
                <div class="row">
                    <div class="col d-flex justify-content-center">
                        <div class="col-md-2.5 regisphone">
                            <div class="card" style="min-height: 550px">
                                <div class="card-body">
                                    <img src="{{ asset('assets/images/logo1.png') }}" style="width: 100px; margin-left: auto; margin-right: auto;margin-bottom: 10%;">
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; line-height: 1.15;">1</span>
                                    <span>Reference</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">2</span>
                                    <span>Personal</span><span> Information</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">3</span>
                                    <span>Address</span>
                                    <br>
                                    <span class="badge rounded-pill bg-primary1" style="font-size: 20px; margin-top:3%; line-height: 1.15;">4</span>
                                    <span>Transfer Gateway</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">5</span>
                                    <span>Contact & Security</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card" style="min-height: 550px">
                                <div class="card-body">
                                    <div class="text-center" style="font-size: 13px">
                                        <h3>Transfer Gateway</h3>
                                        <span>Please enter your information and proceed to</span>
                                        <br>
                                        <span>the next step so we can build your account.</span>
                                    </div>
                                    <div class="w-95 p-3 align-center" style="margin-top: -2% !important;">
                                        <label for="selectedMethod" class="form-label">Transfer Method<span style="color: red"> *</span>
                                            <small id="helpId{{'selectedMethod'}}"
                                            class="text-danger">{{ $errors->has('selectedMethod') ? $errors->first('selectedMethod') : '' }}</small>
                                        </label>
                                        <div wire:ignore>
                                            <select class="form-select" aria-label="Default select example" name="selectedMethod"
                                                wire:model="selectedMethod" id="selectedMethod">
                                                <option value="">Select Method</option>
                                                <option value="BANK">Bank</option>
                                                <option value="OTHER">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    @if ($selectedMethod == 'BANK')
                                        <div class="w-95 p-3 align-center" style="margin-top: -5% !important;">
                                            <label for="exampleFormControlInput1" class="form-label">Bank<span style="color: red"> *</span>
                                                <small id="helpId{{'selectedBank'}}"
                                                class="text-danger">{{ $errors->has('selectedBank') ? $errors->first('selectedBank') : '' }}</small>
                                            </label>
                                            <div wire:ignore>
                                                <select class="form-select" aria-label="Default select example" name="selectedBank"
                                                    wire:model.lazy="selectedBank" id = "selectedBank">
                                                    <option value="">Select Bank</option>
                                                    @foreach ($listBank as $bank)
                                                    <option value="{{ $bank->id }}">{{ $bank->description }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="w-95 p-3 align-center" style="margin-top: -5%">
                                            <label for="no_account" class="form-label">No Account<span style="color: red"> *</span>
                                                <small id="helpId{{'no_account'}}"
                                                class="text-danger">{{ $errors->has('no_account') ? $errors->first('no_account') : '' }}</small>
                                            </label>
                                            <input type="text" class="form-control" id="no_account" name="no_account"
                                                wire:model="no_account" placeholder="No account bank" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
                                        </div>
                                        <div class="w-95 p-3 align-center" style="margin-top: -5%">
                                            <label for="account_holder" class="form-label">Account Holder<span style="color: red"> *</span>
                                                <small id="helpId{{'account_holder'}}"
                                                class="text-danger">{{ $errors->has('account_holder') ? $errors->first('account_holder') : '' }}</small>
                                            </label>
                                            <input type="text" class="form-control" id="account_holder" name="account_holder"
                                                wire:model="account_holder" placeholder="Account Holder">
                                        </div>
                                    @elseif ($selectedMethod == 'OTHER')
                                        <div class="w-95 p-3 align-center" style="margin-top: -5% !important;">
                                            <label for="selectedBank" class="form-label">Gateway<span style="color: red"> *</span>
                                                <small id="helpId{{'selectedBank'}}"
                                                class="text-danger">{{ $errors->has('selectedBank') ? $errors->first('selectedBank') : '' }}</small>
                                            </label>
                                            <select class="form-select" aria-label="Default select example" name="selectedBank"
                                                wire:model.lazy="selectedBank" id = "selectedBank">
                                                <option value="">Select Gateway</option>
                                                @foreach ($listBank as $bank)
                                                <option value="{{ $bank->id }}">{{ $bank->description }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="w-95 p-3 align-center" style="margin-top: -5%">
                                            <label for="no_account" class="form-label">Wallet Address<span style="color: red"> *</span>
                                                <small id="helpId{{'no_account'}}"
                                                class="text-danger">{{ $errors->has('no_account') ? $errors->first('no_account') : '' }}</small>
                                            </label>
                                            <input type="text" class="form-control" id="no_account" name="no_account"
                                                wire:model="no_account" placeholder="Wallet Address" oninput="this.value = this.value.replace(/[^a-zA-Z0-9]/g,'');">
                                            @if ($transferGateway != null && $transferGateway->code == 'USDT')
                                                <small><span><i class="fas fa-exclamation-circle"></i></span> Please ensure the wallet address you entered is a valid Tether(TRC20) Wallet Address.</small>
                                            @endif
                                        </div>
                                    @endif    
                                    <div class="row w-90 p-3 align-center" style="margin-top: -5%">
                                        <small><span><i class="fas fa-exclamation-circle"></i></span> This Transfer Gateway will be used for receiving commission.</small>
                                    </div>
                                    <div style="display: flex; justify-content: space-between;">
                                        <div class="flex items-center justify-end" style="bottom: 0;left: 0%;margin-bottom: 3%;margin-left: 3%;writing-mode: vertical-lr;">
                                            <button type="button" class="btn btn-primary btn-circle btn-md" wire:click="decreaseStep">
                                                <i class="fas fa-chevron-circle-left"></i>
                                            </button>
                                        </div>
                                        <div class="flex items-center justify-end" style="bottom: 0;right: 0%;margin-bottom: 3%;margin-right: 3%;">
                                            <button type="button" class="btn btn-primary btn-circle btn-md" wire:click="increaseStep">
                                                <i class="fas fa-chevron-circle-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if ($step == 4)
                <div class="row">
                    <div class="col d-flex justify-content-center">
                        <div class="col-md-2.5 regisphone">
                            <div class="card" style="min-height: 600px">
                                <div class="card-body">
                                    <img src="{{ asset('assets/images/logo1.png') }}" style="width: 100px; margin-left: auto; margin-right: auto;margin-bottom: 10%;">
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; line-height: 1.15;">1</span>
                                    <span>Reference</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">2</span>
                                    <span>Personal</span><span> Information</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">3</span>
                                    <span>Address</span>
                                    <br>
                                    <span class="badge rounded-pill bg-light text-dark" style="font-size: 20px; margin-top:3%; line-height: 1.15;">4</span>
                                    <span>Transfer Gateway</span>
                                    <br>
                                    <span class="badge rounded-pill bg-primary1" style="font-size: 20px; margin-top:3%; line-height: 1.15;">5</span>
                                    <span>Contact & Security</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card" style="min-height: 600px">
                                <div class="card-body">
                                    <div class="text-center" style="font-size: 13px">
                                        <h3>Contact & Security</h3>
                                        <span>Please enter your information and proceed to</span>
                                        <br>
                                        <span>the next step so we can build your account.</span>
                                    </div>
                                    <div class="w-95 p-3 align-center" style="margin-top: -2% !important;">
                                        <label for="email" class="form-label">Email<span style="color: red">*</span>
                                            <small id="helpId{{'email'}}"
                                            class="text-danger">{{ $errors->has('email') ? $errors->first('email') : '' }}</small>
                                        </label>
                                        <input type="email" class="form-control" id="email" name="email" wire:model="email" placeholder="example@gmail.com">
                                    </div>
    
                                    <div class="w-95 p-3 align-center" style="margin-top: -5%">
                                        <label for="phone" class="form-label">Phone<span style="color: red">*</span>
                                            <small id="helpId{{'phone'}}"
                                            class="text-danger">{{ $errors->has('phone') ? $errors->first('phone') : '' }}</small>
                                        </label>
                                        <input type="text" class="form-control" id="phone" name="phone" wire:model="phone" placeholder="+628123456789" onkeypress="return isNumberKey(event)">
                                    </div>
                                    <div class="row w-90 p-3 align-center" style="margin-top: -5%;align-items: self-end;">
                                        <div class="col">
                                            <label for="password" class="form-label">Password<span style="color: red">*</span></label>
                                            <input type="password" class="form-control" id="password" name="password"
                                                wire:model="password" placeholder="Password">
                                            <small id="helpId{{'password'}}"
                                                class="text-danger">{{ $errors->has('password') ? $errors->first('password') : '' }}</small>
                                        </div>
                                        <div class="col">
                                            <label for="confirm_password" class="form-label">Confirm Password<span style="color: red">*</span></label>
                                            <input type="password" class="form-control" id="confirm_password"
                                                name="confirm_password" wire:model="confirm_password" placeholder="Confirm Password">
                                            <small id="helpId{{'confirm_password'}}"
                                                class="text-danger">{{ $errors->has('confirm_password') ? $errors->first('confirm_password') : '' }}</small>
                                        </div>
                                    </div>
    
                                    <div style="display: flex; justify-content: space-between;">
                                        <div class="flex items-center justify-end" style="bottom: 0;left: 0%;margin-bottom: 3%;margin-left: 3%;writing-mode: vertical-lr;">
                                            <button type="button" class="btn btn-primary btn-circle btn-md" wire:click="decreaseStep">
                                                <i class="fas fa-chevron-circle-left"></i>
                                            </button>
                                        </div>
                                        {{-- <div class="flex items-center justify-end" style="bottom: 0; right: 0%; margin-bottom: 3%; margin-right: 3%;">
                                            <button type="button" class="btn btn-primary ml-4" wire:click="showSignupModalConfirmation">Sign Up</button>
                                        </div> --}}
                                        <div class="flex items-center justify-end" style="bottom: 0; right: 0%; margin-bottom: 3%; margin-right: 3%;">
                                            <button wire:click="showAgreementModal" type="button" class="btn btn-primary ml-4">Continue</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <div id="modal-agreement" wire:ignore.self class="modal fade" tabindex="-1" data-bs-backdrop="static" data-bs-keyboard="false"
                    aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
                    <div class="modal-dialog modal-dialog-scrollable modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5>Terms and Conditions</h5>
                                <button wire:click="hideAgreementModal" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <nav>
                                    <div class="nav nav-pills" id="nav-tab" role="tablist">
                                        <button class="nav-link active" id="nav-id-tab" data-bs-toggle="tab" data-bs-target="#nav-id" type="button" role="tab" aria-controls="nav-id" aria-selected="true">Indonesian</button>
                                        <button class="nav-link" id="nav-en-tab" data-bs-toggle="tab" data-bs-target="#nav-en" type="button" role="tab" aria-controls="nav-en" aria-selected="false">English</button>
                                    </div>
                                </nav>
                                <br>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-id" role="tabpanel" aria-labelledby="nav-id-tab">
                                        <div class="container-fluid">
                                            <zero-md src="{{ asset('assets/markdowns/terms-id.md') }}"></zero-md>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-en" role="tabpanel" aria-labelledby="nav-en-tab">
                                        <div class="container-fluid">
                                            <zero-md src="{{ asset('assets/markdowns/terms-en.md') }}"></zero-md>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <div class="form-check">
                                    <input wire:model="userAgreed" class="form-check-input" type="checkbox" value="" id="agreement">
                                    <label class="form-check-label" for="agreement">
                                        <strong>I have read and understood all of this Terms of Use and its consequences and hereby accept any rights, obligations, and conditions stipulated therein.</strong>
                                    </label>
                                </div>
                            </div>
                            <div class="modal-footer justify-content-center">
                                <div>
                                    <div class="d-flex justify-content-center">
                                        <button type="button" class="btn btn-primary" wire:click="showSignupModalConfirmation">Sign Up</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="modal-confirmation" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
                    aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header alert-danger">
                                <h4 class="modal-title" id="my-modal-title">Are you sure your data is correct?</h4>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type = "button" wire:click = "hideModal" class="btn" data-dismiss="modal" aria-label="Close"
                                    style="background-color: #616161; color : white;">No</button>
                                <button type="submit" data-sitekey="{{env('CAPTCHA_SITE_KEY')}}"
                                    data-callback='handle' data-action='submit'
                                    class="g-recaptcha btn btn-primary">
                                    Yes
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="text-center" style="color:white">
                <span>Already have an account?</span>
                <a href="{{ route('login') }}" id="show-signup" class="link" style="color: moccasin;">Sign In</a>
            </div>
        </div>
    </div>
    <div class="footer">
        This site is protected by reCAPTCHA and the Google
        <a style="color: moccasin;" href="https://policies.google.com/privacy">Privacy Policy</a> and
        <a style="color: moccasin;" href="https://policies.google.com/terms">Terms of Service</a> apply.
    </div>
</div>
@push('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            $(window).keydown(function(event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        }, false);

        function handle(e) {
            grecaptcha.ready(function () {
                grecaptcha.execute('{{env('CAPTCHA_SITE_KEY')}}', {action: 'submit'})
                    .then(function (token) {
                        @this.set('captcha', token);
                    });
            })
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        document.addEventListener('livewire:load', function (e) {
            e.preventDefault()

            window.livewire.on('reloadform', component => {
                $("#country").select2({
                    theme: "bootstrap-5"
                });
                $('#country').on('change', function (e) {
                    var item = $('#country').select2("val");
                    @this.set('country', item);
                });

                $("#selectedBank").select2({
                    theme: "bootstrap-5"
                });
                $('#selectedBank').on('change', function (e) {
                    var item = $('#selectedBank').select2("val");
                    @this.set('selectedBank', item);
                });
            })

            window.livewire.on('showSignupModalConfirmation', (data) => {
                    // console.log(data)
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModal', (data) => {
                    // console.log(data)
                $('#modal-confirmation').modal('hide')
            });

            window.livewire.on('showAgreementModal', (data) => {
                $('#modal-agreement').modal('show')
            });

            window.livewire.on('hideAgreementModal', (data) => {
                $('#modal-agreement').modal('hide')
            });

            $(function () {
                $('input[type="file"]').change(function () {
                    if ($(this).val() != "") {
                            $(this).css('color', '#333');
                    }else{
                            $(this).css('color', 'transparent');
                    }
                });
            })

            $("#country").select2({
                theme: "bootstrap-5"
            });
            $('#country').on('change', function (e) {
                var item = $('#country').select2("val");
                @this.set('country', item);
            });

            $("#selectedBank").select2({
                theme: "bootstrap-5"
            });
            $('#selectedBank').on('change', function (e) {
                var item = $('#selectedBank').select2("val");
                @this.set('selectedBank', item);
            });
        })
    </script>
@endpush

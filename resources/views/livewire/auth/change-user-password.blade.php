<div>
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Edit Password</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form wire:submit.prevent="changePassword" role="form">
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label for="oldPassword" class="form-label">Old Password<span style="color: red"> *</span></label>
                                <div class="input-group mb-3" style="flex-flow: nowrap;align-items: flex-end;">
                                    <input onkeyup="removeClass(this.id)" class="form-control @error('oldPassword') is-invalid @enderror" wire:model="oldPassword" name="oldPassword" id="oldPassword" type="password" />
                                    <div class="input-group-prepend">
                                        <button class="btn rounded-end btn-outline-success" type="button">
                                            <h6 toggle="#oldPassword" class="fa fa-eye fa-lg show-hide"></h6>
                                        </button>
                                    </div>
                                </div>
                                @error('oldPassword')
                                    <small id="helpId" class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label for="newPassword" class="form-label">New Password<span style="color: red"> *</span></label>
                                <div class="input-group mb-3" style="flex-flow: nowrap;align-items: flex-end;">
                                <input onkeyup="removeClass(this.id)" class="form-control @error('newPassword') is-invalid @enderror" wire:model="newPassword" name="newPassword" id="newPassword" type="password" />
                                    <div class="input-group-prepend">
                                        <button class="btn rounded-end btn-outline-success" type="button">
                                            <h6 toggle="#newPassword" class="fa fa-eye fa-lg show-hide"></h6>
                                        </button>
                                    </div>
                                </div>
                                @error('newPassword')
                                    <small id="helpId" class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group col-md-4">
                                <label for="confirmPassword" class="form-label">Confirm Password<span style="color: red"> *</span></label>
                                <div class="input-group mb-3" style="flex-flow: nowrap;align-items: flex-end;">
                                    <input onkeyup="removeClass(this.id)" class="form-control @error('confirmPassword') is-invalid @enderror" wire:model="confirmPassword" name="confirmPassword" id="confirmPassword" type="password" />
                                    <div class="input-group-prepend">
                                        <button class="btn rounded-end btn-outline-success" type="button">
                                            <h6 toggle="#confirmPassword" class="fa fa-eye fa-lg show-hide"></h6>
                                        </button>
                                    </div>
                                </div>
                                @error('confirmPassword')
                                    <small id="helpId" class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary pull-right" 
                                    wire:loading.attr="disabled">Save</button>
                                <div wire:loading>
                                    <img style="width: 25px;" src="{{ asset('assets/images/spinner-small.gif') }}" alt="Loading">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
<script>
    $(".show-hide").click(function () {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
    });

    document.addEventListener('livewire:load', function (e) {
        e.preventDefault()
    })

    function removeClass(id) {
        const el = document.getElementById(id);
        if (el.classList.contains('is-invalid')) {
            el.classList.remove('is-invalid');
        }
    }
</script>
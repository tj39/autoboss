<div>
    <x-loading-indicator/>
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Withholding Tax Slip</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            @if (Auth::user()->step != 'OK')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>You can access this page after all steps are completed.</h6>
                    </div>
                </div>
            @else
                <div class="card card-custom">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input type="text" placeholder="Search month/year" class="form-control" wire:model="search">
                                    <button type="button" class="btn btn-success">
                                        <i class="fa fa-search search-icon"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-sm table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 5%;">No.</th>
                                        <th scope="col" style="width: 20%;">Date</th>
                                        <th scope="col" style="width: 20%;">Category</th>
                                        <th scope="col" style="width: 20%;">NPWP</th>
                                        <th scope="col" style="width: 15%;">Gross Commission (IDR)</th>
                                        <th scope="col" style="width: 15%;">PPH (IDR)</th>
                                        <th scope="col" style="width: 5%;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $key => $item)
                                        <tr>
                                            <th scope="row">{{ $data->firstItem() + $key }}</th>
                                            <td>{{ $item->folder_name }}</td>
                                            <td>{{ $item->category }}</td>
                                            <td>{{ $item->npwp }}</td>
                                            <td>{{ number_format($item->commission_gross, 2, '.', ',') }}</td>
                                            <td>{{ number_format($item->pph, 2, '.', ',') }}</td>
                                            <td>
                                                <div class="d-flex justify-content-center">
                                                    <i class="fas fa-download" style="cursor: pointer;" wire:click="downloadFile({{ "'".$item->location."'" }})" wire:loading.attr="disabled"></i>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $data->links() }}
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <script>        
        document.addEventListener('DOMContentLoaded', function(e) {

            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });
        })
    </script>
</div>


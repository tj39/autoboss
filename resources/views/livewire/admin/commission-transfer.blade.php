<div>
    <x-loading-indicator />
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Commission Transfer</h3>
                        </div>
                        <div>
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-upload-file">Submit Transfer Proof</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            @if (Auth::user()->is_admin != '1')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>Unfortunately, you are not an admin.</h6>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <nav class="mb-4">
                            <div wire:ignore class="nav nav-pills" id="nav-tab" role="tablist">
                                <button wire:click="refreshTab" class="nav-link active" id="nav-rebate-tab" data-bs-toggle="tab" data-bs-target="#nav-rebate" type="button" role="tab" aria-controls="nav-rebate" aria-selected="false">Rebate</button>
                                <button wire:click="refreshTab" class="nav-link" id="nav-sharing-profit-tab" data-bs-toggle="tab" data-bs-target="#nav-sharing-profit" type="button" role="tab" aria-controls="nav-sharing-profit" aria-selected="true">Sharing Profit</button>
                                <div style="margin-left: auto;">
                                    @if ($isRebateEnable == 0)
                                        <button style="background-color: #006596fc !important;" class="btn btn-primary pull-right" wire:click="showModalConfirmRebate"
                                            wire:loading.attr="disabled" disabled>Generate Rebate Payroll</button>
                                    @else
                                        <button style="background-color: #006596fc !important;" class="btn btn-primary pull-right" wire:click="generateRebate"
                                            wire:loading.attr="disabled" type="button">Generate Rebate Payroll</button>
                                    @endif
                                    @if ($isSProfitEnable == 0)
                                    <button style="background-color: #006596fc !important;" class="btn btn-primary pull-right" wire:click="generateSProfit"
                                        wire:loading.attr="disabled" disabled>Generate Sharing Payroll</button>
                                    @else
                                    <button style="background-color: #006596fc !important;" class="btn btn-primary pull-right" wire:click="generateSProfit"
                                        wire:loading.attr="disabled" type="button">Generate Sharing Payroll</button>
                                    @endif
                                </div>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            {{-- Tab rebate --}}
                            <div wire:ignore.self class="tab-pane fade show active" id="nav-rebate" role="tabpanel" aria-labelledby="nav-rebate-tab">
                                <div class="container-fluid">
                                    <div class="row mb-3">
                                        <div class="table-responsive">
                                            <table class="table table-sm table-striped">
                                                <thead>
                                                    <th style="width: 5%;" scope="col">No.</th>
                                                    <th style="width: 22.5%;" scope="col">Start Date</th>
                                                    <th style="width: 22.5%;" scope="col">End Date</th>
                                                    <th style="width: 20%;" scope="col">Lot Gross</th>
                                                    <th style="width: 20%;" scope="col">Code</th>
                                                    <th style="width: 5%;" scope="col">Payroll File</th>
                                                    <th style="width: 5%;" scope="col">Report File</th>
                                                </thead>
                                                <tbody>
                                                    @foreach ($rebatePayrollList as $key => $item)
                                                        <tr>
                                                            <th scope="row">{{ $rebatePayrollList->firstItem() + $key }}</th>
                                                            <td>{{ $item->start_date }}</td>
                                                            <td>{{ $item->end_date }}</td>
                                                            <td>{{ $item->lots_all }}</td>
                                                            <td>{{ $item->code }}</td>
                                                            <td>
                                                                <div class="d-flex justify-content-center">
                                                                    <i class="fas fa-file-download" style="cursor: pointer;" wire:click="downloadFile({{ $item->id }}, '1', 'payroll')" ></i>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                @if ($item->is_final == '1')
                                                                    <div class="d-flex justify-content-center">
                                                                        <i class="fas fa-file-download" style="cursor: pointer;" wire:click="downloadFile({{ $item->id }}, '1', 'report')" ></i>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            {{ $rebatePayrollList->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- Tab sharing profit --}}
                            <div wire:ignore.self class="tab-pane fade" id="nav-sharing-profit" role="tabpanel" aria-labelledby="nav-sharing-profit-tab">
                                <div class="container-fluid">
                                    <div class="row mb-3">
                                        <div class="table-responsive">
                                            <table class="table table-sm table-striped">
                                                <thead>
                                                    <th style="width: 5%;" scope="col">No.</th>
                                                    <th style="width: 25%;" scope="col">Month</th>
                                                    <th style="width: 35%;" scope="col">Code</th>
                                                    <th style="width: 10%;" scope="col">Payroll File</th>
                                                    <th style="width: 10%;" scope="col">Report File</th>
                                                </thead>
                                                <tbody>
                                                    @foreach ($sprofitPayrollList as $key => $item)
                                                        <tr>
                                                            <th scope="row">{{ $sprofitPayrollList->firstItem() + $key }}</th>
                                                            <td>{{ $item->date }}</td>
                                                            <td>{{ $item->code }}</td>
                                                            <td>
                                                                <div class="d-flex justify-content-center">
                                                                    <i class="fas fa-file-download" style="cursor: pointer;" wire:click="downloadFile({{ $item->id }}, '2', 'payroll')" ></i>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                @if ($item->is_final == '1')
                                                                    <div class="d-flex justify-content-center">
                                                                        <i class="fas fa-file-download" style="cursor: pointer;" wire:click="downloadFile({{ $item->id }}, '2', 'report')" ></i>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            {{ $sprofitPayrollList->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- File upload modal --}}
                            <div id="modal-upload-file" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Submit Proof Number</h5>
                                            <button wire:click="resetFields" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                                <form wire.submit.prevent="submit" enctype="multipart/form-data">
                                                    <div class="row mb-3">
                                                        <label for="commissionType" class="col-sm-4 col-form-label">File<span style="color: red"> *</span></label>
                                                        <div class="col-sm-8">
                                                            <select wire:model="commissionType" class="form-select" name="commissionType" id="commissionType" aria-label="Column">
                                                                <option value="">Choose Type</option>
                                                                <option value="1">Rebate</option>
                                                                <option value="2">Sharing Profit</option>
                                                            </select>
                                                            @error('commissionType')
                                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="row mb-3">
                                                        <label for="period" class="col-sm-4 col-form-label">Period<span style="color: red"> *</span></label>
                                                        <div class="col-sm-8">
                                                            @if ($commissionType == '1')
                                                                <div class="bd-example">
                                                                    <div class="form-group d-flex flex-row">
                                                                        <input wire:model.lazy="startDate" type="text" id="startDate" class="form-control" placeholder="Start Date">
                                                                        <span class="flex-grow-0">
                                                                            <span class="btn">To</span>
                                                                        </span>
                                                                        <input wire:model.lazy="endDate" type="text" id="endDate" class="form-control" placeholder="End Date">
                                                                    </div>
                                                                    @error('startDate')
                                                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                                                    @enderror
                                                                    @error('endDate')
                                                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                                                    @enderror
                                                                </div>
                                                            @elseif ($commissionType == '2')
                                                                <div class="bd-example">
                                                                    <div class="form-group d-flex flex-row">
                                                                        <input wire:model.lazy="month" type="month" name="month" class="form-control" placeholder="Month">
                                                                    </div>
                                                                    @error('month')
                                                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                                                    @enderror
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="row mb-3">
                                                        <label for="code" class="col-sm-4 col-form-label">Code<span style="color: red"> *</span></label>
                                                        <div class="col-sm-8">
                                                            <select wire:model="code" class="form-select" name="code" id="code" aria-label="Column">
                                                                <option value="">Choose Code</option>
                                                                @foreach ($codes as $code)
                                                                    <option value="{{ $code->code }}">{{ $code->code }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('code')
                                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="row mb-3">
                                                        <label for="proofNumber" class="col-sm-4 col-form-label">Transfer Proof Number<span style="color: red"> *</span></label>
                                                        <div class="col-sm-8">
                                                            <input wire:model="proofNumber" class="form-control" id="proofNumber" type="text">
                                                            @error('proofNumber')
                                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="row mb-3">
                                                        <label for="transferType" class="col-sm-4 col-form-label">Transfer Type<span style="color: red"> *</span></label>
                                                        <div class="col-sm-8">
                                                            <select wire:model="transferType" class="form-select" name="transferType" id="transferType" aria-label="Column">
                                                                <option value="">Choose Transfer Type</option>
                                                                <option value="BCA">BCA</option>
                                                                <option value="LLG">LLG</option>
                                                                <option value="RTG">RTG</option>
                                                                <option value="USDT">USDT</option>
                                                            </select>
                                                            @error('transferType')
                                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" wire:click="showModalConfirmation" wire:loading.attr="disabled" class="btn btn-primary">
                                                <div wire:loading.remove wire:target="showModalConfirmation, commissionType, code, proofNumber">Submit</div>
                                                <div wire:loading wire:target="showModalConfirmation, commissionType, code, proofNumber">
                                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                    Loading...
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="modal-confirmation" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
                                aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header alert-danger">
                                            <h4 class="modal-title" id="my-modal-title">Are you sure?</h4>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                            <button type="button" wire:click.prevent="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                                <div wire:loading.remove wire:target="submit">Yes</div>
                                                <div wire:loading wire:target="submit">
                                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @push('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            let startDatePicker = null;
            let endDatePicker = null; 
            Livewire.hook('message.processed', (el, component) => {
                if (@this.commissionType === '1') {
                    if (startDatePicker === null) {
                        startDatePicker = new Pikaday({ field: document.getElementById('startDate') });
                    }
                    if (endDatePicker === null) {
                        endDatePicker = new Pikaday({ field: document.getElementById('endDate') });
                    }
                } else if (@this.commissionType === '2') {
                    startDatePicker = null;
                    endDatePicker = null;
                }
            })

            window.livewire.on('resetPikaday', (data) => {
                startDatePicker = null;
                endDatePicker = null;
            });
        });

        document.addEventListener('livewire:load', function(e) {
            e.preventDefault()

            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });

            window.livewire.on('showModalUploadFile', (data) => {
                $('#modal-upload-file').modal('show')
            });

            window.livewire.on('hideModalUploadFile', (data) => {
                $('#modal-upload-file').modal('hide')
            });
        })
    </script>
    @endpush
</div>
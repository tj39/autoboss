<div>
    <x-loading-indicator />
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>User Data</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            @if (Auth::user()->is_admin != '1')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>Unfortunately, you are not an admin.</h6>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-md-3 col-sm-12">
                                <label class="form-label" for="search1">Client ID / Username / Name</label>
                                <input wire:model.defer="search1" class="form-control" type="text" name="search1" id="search1">
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <label class="form-label" for="search2">Email / Phone / NIK</label>
                                <input wire:model.defer="search2" class="form-control" type="text" name="search2" id="search2">
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <button type="button" wire:click="searchUserData" wire:loading.attr="disabled" class="btn btn-primary" style="margin-top: 2rem;">
                                    Search
                                </button>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="table-responsive" style="font-size: smaller;">
                                <table class="table table-sm table-striped">
                                    <thead>
                                        <th style="width: 5%;" scope="col">No.</th>                                        
                                        <th style="width: 10%;" scope="col">Client ID</th>
                                        <th style="width: 25%;" scope="col">Name</th>
                                        <th style="width: 15%;" scope="col">Email</th>
                                        <th style="width: 15%;" scope="col">Phone</th>
                                        <th style="width: 25%;" scope="col">KYC</th>
                                        <th style="width: 5%;" scope="col"></th>
                                    </thead>
                                    <tbody>
                                        @if ($users != null)
                                            @foreach ($users as $key => $item)
                                                <tr>
                                                    <th scope="row">{{ $users->firstItem() + $key }}</th>
                                                    <td>{{ $item->client_id }}</td>
                                                    <td>{{ $item->username . ' - ' . $item->firstname . ' ' . $item->lastname }}</td>
                                                    <td>{{ $item->email }}</td>
                                                    <td>{{ $item->phone }}</td>
                                                    <td style="white-space: pre-wrap;">{{ $item->kyc_verify_msg }}</td>
                                                    <td>
                                                        <div class="d-flex justify-content-center">
                                                            <i class="fas fa-edit" wire:click="showEditModal({{ $item->id }})" style="cursor: pointer;"></i>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                @if ($users != null)
                                    {{ $users->links() }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @if ($editClicked)
                    <div id="modal-edit" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">{{ $user->client_id . ' - ' . $user->firstname . ' ' . $user->lastname }}</h5>
                                    <button wire:click="resetFields" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        <form wire.submit.prevent="submit">
                                            <div class="row mb-3">
                                                <label for="email" class="col-sm-4 col-form-label">Phone<span style="color: red"> *</span></label>
                                                <div class="col-sm-8">
                                                    <input wire:model="phone" type="text" class="form-control" id="phone" placeholder="+6285111222333">
                                                    @error('phone')
                                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label for="email" class="col-sm-4 col-form-label">Email<span style="color: red"> *</span></label>
                                                <div class="col-sm-8">
                                                    <input wire:model="email" type="email" class="form-control" id="email">
                                                    @error('email')
                                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label for="selectedMethod" class="col-sm-4 col-form-label">Transfer Method<span style="color: red"> *</span>
                                                    <small id="helpId{{'selectedMethod'}}"
                                                    class="text-danger">{{ $errors->has('selectedMethod') ? $errors->first('selectedMethod') : '' }}</small>
                                                </label>
                                                <div wire:ignore class="col-sm-8">
                                                    <select class="form-select" aria-label="Default select example" name="selectedMethod"
                                                        wire:model="selectedMethod" id="selectedMethod">
                                                        <option value="BANK">Bank</option>
                                                        <option value="OTHER">Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                            @if ($selectedMethod == 'BANK')
                                                <div class="row mb-3">
                                                    <label for="code" class="col-sm-4 col-form-label">Bank Name<span style="color: red"> *</span></label>
                                                    <div class="col-sm-8">
                                                        <select wire:model.lazy="selectedBank" class="form-select" id="selectedBank" aria-label="Column">
                                                            <option value="">Choose Bank</option>
                                                            @foreach ($banks as $bank)
                                                                <option value="{{ $bank->id }}">{{ $bank->description }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('selectedBank')
                                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <label for="accountNo" class="col-sm-4 col-form-label">Account Number<span style="color: red"> *</span></label>
                                                    <div class="col-sm-8">
                                                        <input wire:model="accountNo" class="form-control" id="accountNo" type="text">
                                                        @error('accountNo')
                                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <label for="accountHolder" class="col-sm-4 col-form-label">Account Holder<span style="color: red"> *</span></label>
                                                    <div class="col-sm-8">
                                                        <input wire:model="accountHolder" class="form-control" id="accountHolder" type="text">
                                                        @error('accountHolder')
                                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            @elseif ($selectedMethod == 'OTHER')
                                                <div class="row mb-3">
                                                    <label for="selectedBank" class="col-sm-4 col-form-label">Gateway<span style="color: red"> *</span>
                                                        <small id="helpId{{'selectedBank'}}"
                                                        class="text-danger">{{ $errors->has('selectedBank') ? $errors->first('selectedBank') : '' }}</small>
                                                    </label>
                                                    <div class="col-sm-8">
                                                        <select class="form-select" aria-label="Selected Bank" name="selectedBank" id="selectedBank"
                                                            wire:model.lazy="selectedBank">
                                                            <option value="">Select Gateway</option>
                                                            @foreach ($banks as $bank)
                                                            <option value="{{ $bank->id }}">{{ $bank->description }}</option>
                                                            @endforeach
                                                        </select>
                                                        @error('selectedBank')
                                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label for="accountNo" class="col-sm-4 col-form-label">Wallet Address<span style="color: red"> *</span></label>
                                                    <div class="col-sm-8">
                                                        <input value="{{ $accountNo }}" type="text" class="form-control" name="accountNo" id="accountNo" 
                                                            wire:model="accountNo" placeholder="Wallet address"
                                                            oninput="this.value = this.value.replace(/[^a-zA-Z0-9]/g,'');">
                                                        @error('accountNo')
                                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                        @if ($transferGateway != null && $transferGateway->code == 'USDT')
                                                            <small><span><i class="fas fa-exclamation-circle"></i></span> Please ensure the wallet address you entered is a valid Tether(TRC20) Wallet Address.</small>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endif
                                        </form>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" wire:click="showModalConfirmation" wire:loading.attr="disabled" class="btn btn-primary">
                                        <div wire:loading.remove wire:target="showModalConfirmation">Save</div>
                                        <div wire:loading wire:target="showModalConfirmation">
                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                            Loading...
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div id="modal-confirmation" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
                    aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header alert-danger">
                                <h4 class="modal-title" id="my-modal-title">Are you sure?</h4>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                <button type="button" wire:click.prevent="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                    <div wire:loading.remove wire:target="submit">Yes</div>
                                    <div wire:loading wire:target="submit">
                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });

            window.livewire.on('showModalEdit', (data) => {
                $('#modal-edit').modal('show')
            });

            window.livewire.on('hideModalEdit', (data) => {
                $('#modal-edit').modal('hide')
            });

            Livewire.hook('message.processed', (el, component) => {
                $("#selectedBank").select2({
                    theme: "bootstrap-5",
                    dropdownParent: $("#modal-edit")
                });
                $('#selectedBank').on('change', function (e) {
                    var item = $('#selectedBank').select2("val");
                    @this.set('selectedBank', item);
                });
            })
        });
    </script>
</div>

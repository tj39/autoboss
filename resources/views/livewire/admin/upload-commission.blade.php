<div>
    <x-loading-indicator />
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Upload Commission</h3>
                        </div>
                        @if ($isSprofitUploadDisabled && $isRebateUploadDisabled)
                            <div>
                                <button type="button" class="btn btn-secondary" disabled>Upload CSV</button>
                            </div>
                        @else
                            <div>
                                <button wire:click="showModalUploadFile" type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-upload-file">Upload CSV</button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            @if (Auth::user()->is_admin != '1')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>Unfortunately, you are not an admin.</h6>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <nav class="mb-4">
                            <div wire:ignore class="nav nav-pills" id="nav-tab" role="tablist">
                                <button wire:click="refreshTab" class="nav-link active" id="nav-rebate-tab" data-bs-toggle="tab" data-bs-target="#nav-rebate" type="button" role="tab" aria-controls="nav-rebate" aria-selected="true">Rebate</button>
                                <button wire:click="refreshTab" class="nav-link" id="nav-sharing-profit-tab" data-bs-toggle="tab" data-bs-target="#nav-sharing-profit" type="button" role="tab" aria-controls="nav-sharing-profit" aria-selected="false">Sharing Profit</button>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            {{-- Tab rebate --}}
                            <div wire:ignore.self class="tab-pane fade show active" id="nav-rebate" role="tabpanel" aria-labelledby="nav-rebate-tab">
                                <div class="container-fluid">
                                    <div class="row mb-3">
                                        <div class="table-responsive">
                                            <table class="table table-sm table-striped">
                                                <thead>
                                                    <th style="width: 5%;" scope="col">No.</th>
                                                    <th style="width: 15%;" scope="col">Date</th>
                                                    <th style="width: 40%;" scope="col">Name</th>
                                                    <th style="width: 5%;" scope="col">File</th>
                                                    <th style="width: 10%;" scope="col">Inserted</th>
                                                    <th style="width: 10%;" scope="col">Calculated</th>
                                                    <th style="width: 10%;" scope="col">Settled</th>
                                                    <th style="width: 20%;" scope="col">Payroll Code</th>
                                                </thead>
                                                <tbody>
                                                    @foreach ($rebateFileList as $key => $item)
                                                        <tr>
                                                            <th scope="row">{{ $rebateFileList->firstItem() + $key }}</th>
                                                            <td>{{ $item->date }}</td>
                                                            <td>{{ $item->file_name }}</td>
                                                            <td>
                                                                <div class="d-flex justify-content-center">
                                                                    <i class="fas fa-file-download" style="cursor: pointer;" wire:click="downloadCsv({{ $item->id }}, '1')" wire:loading.attr="disabled"></i>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                @if ($item->is_inserted == '1')
                                                                    <div class="d-flex justify-content-center bg-success">
                                                                        <span style="color: #fff;"><i class="fas fa-check"></i></span>
                                                                    </div>
                                                                @elseif ($item->is_inserted == '2')
                                                                    <div class="d-flex justify-content-center bg-warning">
                                                                        <span style="color: #fff;"><i class="far fa-clock"></i></span>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($item->is_calculated == '1')
                                                                    <div class="d-flex justify-content-center bg-success">
                                                                        <span style="color: #fff;"><i class="fas fa-check"></i></span>
                                                                    </div>
                                                                @elseif ($item->is_calculated == '2')
                                                                    <div class="d-flex justify-content-center bg-warning">
                                                                        <span style="color: #fff;"><i class="far fa-clock"></i></span>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($item->is_settled == '1')
                                                                    <div class="d-flex justify-content-center bg-success">
                                                                        <span style="color: #fff;"><i class="fas fa-check"></i></span>
                                                                    </div>
                                                                @elseif ($item->is_settled == '2')
                                                                    <div class="d-flex justify-content-center bg-warning">
                                                                        <span style="color: #fff;"><i class="far fa-clock"></i></span>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td>{{ $item->payroll_code }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            {{ $rebateFileList->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- Tab sharing profit --}}
                            <div wire:ignore.self class="tab-pane fade" id="nav-sharing-profit" role="tabpanel" aria-labelledby="nav-sharing-profit-tab">
                                <div class="container-fluid">
                                    <div class="row mb-3">
                                        <div class="table-responsive">
                                            <table class="table table-sm table-striped">
                                                <thead>
                                                    <th style="width: 5%;" scope="col">No.</th>
                                                    <th style="width: 15%;" scope="col">Date</th>
                                                    <th style="width: 40%;" scope="col">Name</th>
                                                    <th style="width: 5%;" scope="col">File</th>
                                                    <th style="width: 10%;" scope="col">Inserted</th>
                                                    <th style="width: 10%;" scope="col">Calculated</th>
                                                    <th style="width: 10%;" scope="col">Settled</th>
                                                    <th style="width: 20%;" scope="col">Payroll Code</th>
                                                </thead>
                                                <tbody>
                                                    @foreach ($sprofitFileList as $key => $item)
                                                        <tr>
                                                            <th scope="row">{{ $sprofitFileList->firstItem() + $key }}</th>
                                                            <td>{{ $item->date }}</td>
                                                            <td>{{ $item->file_name }}</td>
                                                            <td>
                                                                <div class="d-flex justify-content-center">
                                                                    <i class="fas fa-file-download" style="cursor: pointer;" wire:click="downloadCsv({{ $item->id }}, '2')" wire:loading.attr="disabled"></i>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                @if ($item->is_inserted == '1')
                                                                    <div class="d-flex justify-content-center bg-success">
                                                                        <span style="color: #fff;"><i class="fas fa-check"></i></span>
                                                                    </div>
                                                                @elseif ($item->is_inserted == '2')
                                                                    <div class="d-flex justify-content-center bg-warning">
                                                                        <span style="color: #fff;"><i class="far fa-clock"></i></span>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($item->is_calculated == 'H')
                                                                    <div class="d-flex justify-content-center bg-light">
                                                                        <span style="color: #212529;"><i class="fas fa-hourglass-half"></i></span>
                                                                    </div>
                                                                @elseif ($item->is_calculated == '1')
                                                                    <div class="d-flex justify-content-center bg-success">
                                                                        <span style="color: #fff;"><i class="fas fa-check"></i></span>
                                                                    </div>
                                                                @elseif ($item->is_calculated == '2')
                                                                    <div class="d-flex justify-content-center bg-warning">
                                                                        <span style="color: #fff;"><i class="far fa-clock"></i></span>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($item->is_settled == 'H')
                                                                    <div class="d-flex justify-content-center bg-light">
                                                                        <span style="color: #212529;"><i class="fas fa-hourglass-half"></i></span>
                                                                    </div>
                                                                @elseif ($item->is_settled == '1')
                                                                    <div class="d-flex justify-content-center bg-success">
                                                                        <span style="color: #fff;"><i class="fas fa-check"></i></span>
                                                                    </div>
                                                                @elseif ($item->is_settled == '2')
                                                                    <div class="d-flex justify-content-center bg-warning">
                                                                        <span style="color: #fff;"><i class="far fa-clock"></i></span>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td>{{ $item->payroll_code }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            {{ $sprofitFileList->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- File upload modal --}}
                            <div id="modal-upload-file" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Upload CSV</h5>
                                            <button wire:click="resetFields" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                                <form wire.submit.prevent="submit" enctype="multipart/form-data">
                                                    <div class="row mb-3">
                                                        <label for="commissionType" class="col-sm-3 col-form-label">File<span style="color: red"> *</span></label>
                                                        <div class="col-sm-9">
                                                            <select wire:model="commissionType" class="form-select form-select-sm" name="commissionType" id="commissionType" aria-label="Column">
                                                                <option value="">Choose Type</option>
                                                                @if (!$isRebateUploadDisabled)
                                                                    <option value="1">Rebate</option>
                                                                @endif                                                               
                                                                @if (!$isSprofitUploadDisabled)
                                                                    <option value="2">Sharing Profit</option>
                                                                @endif                                                                
                                                            </select>
                                                        </div>
                                                        @error('commissionType')
                                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                    <div class="row mb-3">
                                                        <label for="excelFile" class="col-sm-3 col-form-label">File (.csv)<span style="color: red"> *</span></label>
                                                        <div class="col-sm-9">
                                                            <input wire:model="excelFile" class="form-control form-control-sm" id="excelFile" type="file" accept=".csv">
                                                        </div>
                                                        @error('excelFile')
                                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                    @if ($commissionType == '2')
                                                        <div class="row mb-3">
                                                            <label for="isPaid" class="col-sm-3 col-form-label">Is Paid</label>
                                                            <div class="col-sm-9">
                                                                <input wire:model="isPaid" class="form-check-input" type="checkbox" value="" id="isPaid">
                                                            </div>
                                                        </div>
                                                    @endif
                                                </form>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            @if ($isSprofitUploadDisabled && $isRebateUploadDisabled)
                                                <button type="button" class="btn btn-secondary">Submit</button>
                                            @else
                                                <button type="button" wire:click="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                                    <div wire:loading.remove wire:target="submit, commissionType, excelFile, isPaid">Submit</div>
                                                    <div wire:loading wire:target="submit, commissionType, excelFile, isPaid">
                                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                        Loading...
                                                    </div>
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <script>
        document.addEventListener('livewire:load', function(e) {
            e.preventDefault()

            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });

            window.livewire.on('showModalUploadFile', (data) => {
                $('#modal-upload-file').modal('show')
            });

            window.livewire.on('hideModalUploadFile', (data) => {
                $('#modal-upload-file').modal('hide')
            });
        })

        // function showIsPaid() {
        //     var commissionTypeEl = document.getElementById('commissionType');
        //     var isPaidDropdownEl = document.getElementById('isPaidDropdown');
        //     if (commissionTypeEl.value == '1') {
        //         isPaidDropdownEl.style.display = 'block';
        //     } else {
        //         isPaidDropdownEl.style.display = 'none';
        //     }
        // }
    </script>
</div>
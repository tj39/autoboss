<div>
    @push('styles')
        <style>
            .circle {
                display: inline-block;
                border-radius: 50%;
                min-width: 20px;
                min-height: 20px;
                padding: 5px;
                background: red;
                color: white;
                text-align: center;
                line-height: 1;
                box-sizing: content-box;
                white-space: nowrap;
                }
                .circle:before {
                content: "";
                display: inline-block;
                vertical-align: middle;
                padding-top: 100%;
                height: 0;
                }
                .circle span {
                display: inline-block;
                vertical-align: middle;
            }
        </style>
    @endpush
    <x-loading-indicator />
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Support Ticket</h3>
                        </div>
                        @if ($ticketclosed  != 0)
                            <button class="btn btn-primary pull-right" wire:click="archiveTicket" wire:model="archiveTicket"
                                        wire:loading.attr="disabled" >Archive Ticket</button>
                        @else
                            <button disabled class="btn btn-primary pull-right" wire:click="archiveTicket"
                                        wire:loading.attr="disabled" >Archive Ticket</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="{{ asset('/assets/images/dashboard/top-header.jpg') }}" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div wire:ignore class="row">
            <div class="col-md-12 col-lg-12">
                <div class="row row-cols-1">
                    <div class="d-slider1 overflow-hidden ">
                        <ul class="swiper-wrapper list-inline m-0 p-0 mb-2">
                            <li wire:click = "queryTicket('open')" class="swiper-slide card card-slide" data-aos="fade-up" data-aos-delay="700">
                                <div class="card-body">
                                    <div class="progress-widget">
                                        <i class="fas fa-envelope-open" style="font-size: xx-large;padding-left: 10%;"></i>
                                        <div class="progress-detail">
                                            <p class="mb-2">Ticket Open</p>
                                            <h4 class="counter">{{ $ticketopencount }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li wire:click = "queryTicket('solve')" class="swiper-slide card card-slide" data-aos="fade-up" data-aos-delay="700">
                                <div class="card-body">
                                    <div class="progress-widget">
                                        <i class="fas fa-envelope" style="font-size: xx-large;padding-left: 10%;"></i>
                                        <div class="progress-detail">
                                            <p class="mb-2">Ticket Solved</p>
                                            <h4 class="counter">{{ $ticketsolvecount }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li wire:click = "queryTicket('close')" class="swiper-slide card card-slide" data-aos="fade-up" data-aos-delay="700">
                                <div class="card-body">
                                    <div class="progress-widget">
                                        <i class="fas fa-mail-bulk" style="font-size: xx-large;padding-left: 10%;"></i>
                                        <div class="progress-detail">
                                            <p class="mb-2">Ticket Closed</p>
                                            <h4 class="counter">{{ $ticketclosecount }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="swiper-slide card card-slide" data-aos="fade-up" data-aos-delay="700">
                                <a wire:click = "queryTicket('unread')" style="text-decoration: none; cursor: pointer;">
                                    <div class="card-body">
                                        <div class="progress-widget">
                                            <i class="fas fa-mail-bulk" style="font-size: xx-large;padding-left: 10%;"></i>
                                            <div class="progress-detail">
                                                <p class="mb-2">Ticket Unread</p>
                                                <h4 class="counter">{{ $ticketunreadcount }}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <div class="swiper-button swiper-button-next"></div>
                        <div class="swiper-button swiper-button-prev"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-body" style="font-size: smaller;">
                    <div class="row mb-3">
                        <div class="col-md-3 col-sm-12">
                            <label class="form-label" for="searchName">Name</label>
                            <input wire:model="searchName" class="form-control" type="text" name="searchName" id="searchName" placeholder="Name">
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <label class="form-label" for="searchTitle">Title</label>
                            <input wire:model="searchTitle" class="form-control" type="text" name="searchTitle" id="searchTitle" placeholder="Title">
                        </div>
                    </div>
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <!-- <th scope="col">No. Ticket</th> -->
                                <th scope="col">Date</th>
                                <th scope="col">Name</th>
                                <th scope="col">Title</th>
                                <th scope="col">Category</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($tickets as $key => $ticket)
                            @if ($ticket->is_read_admin == 0)
                            <tr>
                            @else
                            <tr class="table-light">
                            @endif
                                <td>{{ $tickets->firstItem() + $key }}</td>
                                <!-- <td>{{ $ticket->no_ticket }}</td> -->
                                <td>{{ date_format(date_create($ticket->created_date), 'd M Y H:i')}}</td>
                                <td>{{ $ticket->user->firstname }} {{ $ticket->user->lastname }}</td>
                                <td>{{ $ticket->title }}</td>
                                <td>{{ $ticket->category->name }}</td>
                                <td style="color : {{ $ticket->status->color }}">{{ $ticket->status->name }}</td>
                                <td><a style="color :black" href = "{{ route('admin.detail.ticket', ['id' => $ticket->id, 'page' => $tickets->currentPage(), 'table' => $table ]) }}" style="font-weight: bold;"><i class="fas fa-comments" style="cursor: pointer;"></i></a>
                                    </td>
                                @if ($this->getCommentRead($ticket->id) != 0)
                                <td><div class="circle"><span>{{ $this->getCommentRead($ticket->id) }}</span></div></td>
                                @else
                                <td></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $tickets->links() }}
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            
        });
    </script>
</div>


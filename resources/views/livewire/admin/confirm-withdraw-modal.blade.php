@if ($wdId)
<div wire:ignore.self class="modal fade" id="confirm-withdraw-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Withdraw - {{ $wdCode }}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    {{-- <input type="hidden" wire:model="wdId"> --}}
                    <div class="row">
                        <label for="amount" class="col-sm-3 col-form-label"><strong>Amount (USD)</strong></label>
                        <div class="col-sm-9">
                            <input id="amount" style="border: 0; background-color: #fff;" type="text" class="form-control" value="${{ $amount }}" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label for="account" class="col-sm-3 col-form-label"><strong>Account</strong></label>
                        <div class="col-sm-9">
                            <input id="account" style="border: 0; background-color: #fff;" type="text" class="form-control" value="{{ $account }}" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click="hideModal" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" wire:click="showModalConfirmation" wire:loading.attr="disabled" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
<div id="modal-confirmation" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert-danger">
                <h4 class="modal-title" id="my-modal-title">Are you sure?</h4>
            </div>
            <div class="modal-footer justify-content-between">
                <button type = "button" wire:click="hideModalConfirmation" class="btn" data-dismiss="modal" aria-label="Close"
                    style="background-color: #616161; color : white;">No</button>
                <button type="button" wire:click.prevent="confirm" wire:loading.attr="disabled" class="btn btn-primary">Yes</button>
            </div>
        </div>
    </div>
</div>
@endif

<div>
    <x-loading-indicator />
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>FAQ List</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            @if (Auth::user()->is_admin != '1')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>Unfortunately, you are not an admin.</h6>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <nav class="mb-4">
                            <div wire:ignore.self class="nav nav-pills" id="nav-tab" role="tablist">
                                <button wire:click="refreshTab('category')" class="nav-link {{ $selectedTab == 'category' ? 'active' : ''}}" id="nav-category-tab" data-bs-toggle="tab" data-bs-target="#nav-category" type="button" role="tab" aria-controls="nav-category" aria-selected="false">Category</button>
                                <button wire:click="refreshTab('document')" class="nav-link {{ $selectedTab == 'document' ? 'active' : ''}}" id="nav-document-tab" data-bs-toggle="tab" data-bs-target="#nav-document" type="button" role="tab" aria-controls="nav-document" aria-selected="true">Document</button>
                                <div style="margin-left: auto;">
                                    @if ($selectedTab == 'category')
                                        <button type="button" class="btn btn-primary" wire:click="showEditModal('add', 'category', 0)">Add Category</button>
                                    @else
                                        <button type="button" class="btn btn-primary" wire:click="showEditModal('add', 'document', 0)">Add Document</button>
                                    @endif
                                </div>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            {{-- Tab rebate --}}
                            <div wire:ignore.self class="tab-pane fade {{ $selectedTab == 'category' ? 'show active' : ''}}" id="nav-category" role="tabpanel" aria-labelledby="nav-category-tab">
                                <div class="container-fluid">
                                    <div class="row mb-3">
                                        <div class="table-responsive">
                                            <table class="table table-sm table-striped">
                                                <thead>
                                                    <th style="width: 5%;" scope="col">No.</th>
                                                    <th style="width: 60%;" scope="col">Category Name</th>
                                                    <th style="width: 15%;" scope="col">Is Active</th>
                                                    <th style="width: 10%;" scope="col"></th>
                                                    <th style="width: 10%;" scope="col"></th>
                                                </thead>
                                                <tbody>
                                                    @foreach ($categories as $key => $item)
                                                        <tr>
                                                            <th scope="row">{{ $categories->firstItem() + $key }}</th>
                                                            <td>{{ $item->name }}</td>
                                                            <td>
                                                                @if ($item->is_active == '1')
                                                                    <div class="d-flex" style="color: green; padding-left: 2%">
                                                                        <i class="fas fa-check"></i>
                                                                    </div>
                                                                @else
                                                                    <div class="d-flex" style="color: red; padding-left: 3%">
                                                                        <i class="fas fa-times"></i>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <div class="d-flex justify-content-center">
                                                                    <i class="fas fa-edit" style="cursor: pointer;" wire:click="showEditModal('edit', 'category', {{ $item->id }})" ></i>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="d-flex justify-content-center">
                                                                    <i class="fas fa-list-alt" style="cursor: pointer;" wire:click="viewQuestionList({{ $item->id }}, {{ $categories->currentPage() }})" ></i>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            {{ $categories->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- Tab sharing profit --}}
                            <div wire:ignore.self class="tab-pane fade {{ $selectedTab == 'document' ? 'show active' : ''}}" id="nav-document" role="tabpanel" aria-labelledby="nav-document-tab">
                                <div class="container-fluid">
                                    <div class="row mb-3">
                                        <div class="table-responsive">
                                            <table class="table table-sm table-striped">
                                                <thead>
                                                    <th style="width: 5%;" scope="col">No.</th>
                                                    <th style="width: 35%;" scope="col">Name</th>
                                                    <th style="width: 50%;" scope="col">URL</th>
                                                    <th style="width: 10%;" scope="col"></th>
                                                </thead>
                                                <tbody>
                                                    @foreach ($documents as $key => $item)
                                                        <tr>
                                                            <th scope="row">{{ $documents->firstItem() + $key }}</th>
                                                            <td>{{ $item->name }}</td>
                                                            <td><a target="_blank" href="{{ $item->url }}">{{ $item->url }}</a></td>
                                                            <td>
                                                                <div class="d-flex justify-content-center">
                                                                    <i class="fas fa-edit" style="cursor: pointer;" wire:click="showEditModal('edit', 'document', {{ $item->id }})" ></i>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            {{ $documents->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- File upload modal --}}
                            @if ($editClicked)
                                <div id="modal-edit" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                @if ($mode == 'add')
                                                    @if ($type == 'category')
                                                        <h5 class="modal-title">Add Category</h5>
                                                    @else
                                                        <h5 class="modal-title">Add Document</h5>
                                                    @endif
                                                @else
                                                    @if ($type == 'category')
                                                        <h5 class="modal-title">Edit Category</h5>
                                                    @else
                                                        <h5 class="modal-title">Edit Document</h5>
                                                    @endif
                                                @endif
                                                <button wire:click="resetFields" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="container-fluid">
                                                    @if ($type == 'category')
                                                        <div class="row mb-3">
                                                            <label for="categoryName" class="col-sm-4 col-form-label">Category Name<span style="color: red"> *</span></label>
                                                            <div class="col-sm-8">
                                                                <input wire:model="categoryName" class="form-control" id="categoryName" type="text">
                                                                @error('categoryName')
                                                                    <small id="helpId" class="text-danger">{{ $message }}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="row mb-3">
                                                            <label class="form-check-label col-sm-4 col-form-label" for="isActive">Is Active</label>
                                                            <div class="col-sm-8">
                                                                <input wire:model="isActive" class="form-check-input" type="checkbox" value="" id="isActive" style="margin-top: 2%">
                                                                @error('isActive')
                                                                    <small id="helpId" class="text-danger">{{ $message }}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="row mb-3">
                                                            <label for="documentName" class="col-sm-4 col-form-label">Document Name<span style="color: red"> *</span></label>
                                                            <div class="col-sm-8">
                                                                <input wire:model="documentName" class="form-control" id="documentName" type="text">
                                                                @error('documentName')
                                                                    <small id="helpId" class="text-danger">{{ $message }}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="row mb-3">
                                                            <label for="pdfFile" class="col-sm-4 col-form-label">File (.pdf)<span style="color: red"> *</span></label>
                                                            <div class="col-sm-8">
                                                                <input wire:model="pdfFile" class="form-control" id="pdfFile" type="file" accept=".pdf">
                                                                @error('pdfFile')
                                                                    <small id="helpId" class="text-danger">{{ $message }}</small>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" wire:click="showModalConfirmation" wire:loading.attr="disabled" class="btn btn-primary">
                                                    <div wire:loading.remove wire:target="showModalConfirmation, categoryName, isActive, documentName, pdfFile">Submit</div>
                                                    <div wire:loading wire:target="showModalConfirmation, categoryName, isActive, documentName, pdfFile">
                                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                        Loading...
                                                    </div>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            
                            <div id="modal-confirmation" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
                                aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header alert-danger">
                                            <h4 class="modal-title" id="my-modal-title">Are you sure?</h4>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                            <button type="button" wire:click.prevent="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                                <div wire:loading.remove wire:target="submit">Yes</div>
                                                <div wire:loading wire:target="submit">
                                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @push('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });

            window.livewire.on('showModalEdit', (data) => {
                $('#modal-edit').modal('show')
            });

            window.livewire.on('hideModalEdit', (data) => {
                $('#modal-edit').modal('hide')
            });
        });
    </script>
    @endpush
</div>
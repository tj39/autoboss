<div>
    <x-loading-indicator />
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Seminar List</h3>
                        </div>
                        <div>
                            <button type="button" class="btn btn-primary" wire:click="showEditModal(null, 'add')">Add</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            @if (Auth::user()->is_admin != '1')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>Unfortunately, you are not an admin.</h6>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="table-responsive" style="font-size: smaller;">
                                <table class="table table-sm table-striped">
                                    <thead>
                                        <th style="width: 5%;" scope="col">No.</th>                                        
                                        <th style="width: 45%;" scope="col">Event Name</th>
                                        <th style="width: 25%;" scope="col">Event Date</th>
                                        <th style="width: 15%;" scope="col">Rank</th>
                                        <th style="width: 5%;" scope="col"></th>
                                        <th style="width: 5%;" scope="col"></th>
                                        <th style="width: 5%;" scope="col"></th>
                                    </thead>
                                    <tbody>
                                        @if ($events != null)
                                            @foreach ($events as $key => $item)
                                                <tr>
                                                    <th scope="row">{{ $events->firstItem() + $key }}</th>
                                                    <td>{{ $item->event_name }}</td>
                                                    <td>{{ date_format(date_create($item->event_date), 'D, d M Y') . ' (' . $item->event_time . ')'}}</td>
                                                    <td>{{ $item->rank }}</td>
                                                    <td>
                                                        <div class="d-flex justify-content-center">
                                                            <i class="fas fa-edit" wire:click="showEditModal({{ $item->id }}, 'edit')" style="cursor: pointer;"></i>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        @if ($item->user_count > 0)
                                                            <div class="d-flex justify-content-center">
                                                                <i class="fas fa-users" wire:click="showUserModal({{ $item->id }})" style="cursor: pointer;"></i>
                                                            </div>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <i class="fas fa-envelope" style="cursor: pointer;" title="Send Email Reminder" wire:click = "sendEmailReminder({{ $item->id }})"></i>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                @if ($events != null)
                                    {{ $events->links() }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @if ($editClicked)
                    <div id="modal-edit" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    @if ($mode == 'add')
                                        <h5 class="modal-title">Add Seminar Data</h5>
                                    @else
                                        <h5 class="modal-title">Edit - {{ $seminarEvent->event_name }}</h5>
                                    @endif
                                    <button wire:click="resetFields" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        <form wire.submit.prevent="submit">
                                            <div class="row mb-3">
                                                <label for="eventName" class="col-sm-4 col-form-label">Event Name<span style="color: red"> *</span></label>
                                                <div class="col-sm-8">
                                                    <input wire:model="eventName" type="text" class="form-control" id="eventName">
                                                    @error('eventName')
                                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label for="eventDate" class="col-sm-4 col-form-label">Event Date<span style="color: red"> *</span></label>
                                                <div class="col-sm-8">
                                                    <div class="bd-example">
                                                        <div class="form-group d-flex flex-row">
                                                            <input wire:model.lazy="eventDate" type="text" id="eventDate" class="form-control">
                                                            @error('eventDate')
                                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label for="eventTime" class="col-sm-4 col-form-label">Event Time<span style="color: red"> *</span></label>
                                                <div class="col-sm-8">
                                                    <input wire:model="eventTime" type="text" class="form-control" id="eventTime">
                                                    @error('eventTime')
                                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label for="url" class="col-sm-4 col-form-label">URL<span style="color: red"> *</span></label>
                                                <div class="col-sm-8">
                                                    <input wire:model="url" type="text" class="form-control" id="url">
                                                    @error('url')
                                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label for="meetingID" class="col-sm-4 col-form-label">Meeting ID<span style="color: red"> *</span></label>
                                                <div class="col-sm-8">
                                                    <input wire:model="meetingID" type="text" class="form-control" id="meetingID">
                                                    @error('meetingID')
                                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label for="passcode" class="col-sm-4 col-form-label">Passcode<span style="color: red"> *</span></label>
                                                <div class="col-sm-8">
                                                    <input wire:model="passcode" type="text" class="form-control" id="passcode">
                                                    @error('passcode')
                                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label for="rank" class="col-sm-4 col-form-label">Rank<span style="color: red"> *</span></label>
                                                <div class="col-sm-8">
                                                    <select wire:model="rank" class="form-select" id="rank" aria-label="Column">
                                                        <option value="">Select Rank</option>
                                                        @foreach ($ranks as $rank)
                                                            <option value="{{ $rank->description }}">{{ $rank->description }}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('rank')
                                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                            @if ($mode == 'edit')
                                                <div class="row mb-3">
                                                    <label for="status" class="col-sm-4 col-form-label">Status<span style="color: red"> *</span></label>
                                                    <div class="col-sm-8">
                                                        <select wire:model="status" class="form-select" id="status" aria-label="Column">
                                                            <option value="NEW">New</option>
                                                            <option value="ENDED">Ended</option>
                                                        </select>
                                                        @error('status')
                                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            @endif
                                        </form>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" wire:click="showModalConfirmation" wire:loading.attr="disabled" class="btn btn-primary">
                                        <div wire:loading.remove wire:target="showModalConfirmation">Save</div>
                                        <div wire:loading wire:target="showModalConfirmation">
                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                            Loading...
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if ($userModalClicked)
                    <div id="modal-users" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Trader List</h5>
                                    <button wire:click="resetFields" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        <div class="row mb-3">
                                            <div class="col-md-3 col-sm-12">
                                                <input wire:model="userSearch" class="form-control" type="text" name="userSearch" id="userSearch" placeholder="Client ID / Name">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="table-responsive" style="font-size: smaller;">
                                                <table class="table table-sm table-striped">
                                                    <thead>
                                                        <th style="width: 5%;" scope="col">No.</th>                                        
                                                        <th style="width: 20%;" scope="col">Client ID</th>
                                                        <th style="width: 45%;" scope="col">Name</th>
                                                        <th style="width: 30%;" scope="col">Email</th>
                                                    </thead>
                                                    <tbody>
                                                        @if ($users != null)
                                                            @foreach ($users as $key => $item)
                                                                <tr>
                                                                    <th scope="row">{{ $users->firstItem() + $key }}</th>
                                                                    <td>{{ $item->client_id }}</td>
                                                                    <td>{{ $item->firstname . ' ' . $item->lastname}}</td>
                                                                    <td>{{ $item->email }}</td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                                @if ($users != null)
                                                    {{ $users->links() }}
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div id="modal-confirmation" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
                    aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header alert-danger">
                                <h4 class="modal-title" id="my-modal-title">Are you sure?</h4>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                <button type="button" wire:click.prevent="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                    <div wire:loading.remove wire:target="submit">Yes</div>
                                    <div wire:loading wire:target="submit">
                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @push('scripts')
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            let eventDate = null;

            window.livewire.on('resetPikaday', (data) => {
                eventDate = null;
            });

            window.livewire.on('destroyBackdrop', (data) => {
                let elems = document.querySelectorAll('.modal-backdrop');
                elems.forEach(el => {
                    el.remove();
                });
            });

            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });

            window.livewire.on('showModalEdit', (data) => {
                $('#modal-edit').modal('show')
                if (eventDate === null) {
                    eventDate = new Pikaday({ field: document.getElementById('eventDate') });
                }
            });

            window.livewire.on('hideModalEdit', (data) => {
                $('#modal-edit').modal('hide')
            });

            window.livewire.on('showUserModal', (data) => {
                $('#modal-users').modal('show')
            });

            window.livewire.on('hideUserModal', (data) => {
                $('#modal-users').modal('hide')
            });
        });
    </script>
    @endpush
</div>

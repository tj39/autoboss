<div>
    <x-loading-indicator />
    <div class="iq-navbar-header" style="height: 8.125rem; margin-bottom: 0.75rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Question And Answer</h3>
                        </div>
                        <div>
                            <a href="/admin/faq-list?page={{ $page }}" type="button" class="btn btn-secondary">Back</a>
                            <button wire:click="showEditModal('add', 0)" type="button" class="btn btn-primary">Add Q&A</button>
                        </div>
                    </div>
                    <div>
                        <h6>Category: {{ $categoryName }}</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 12.25rem;">
            <img src="{{ asset('/assets/images/dashboard/top-header.jpg') }}" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            @if (Auth::user()->is_admin != '1')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>Unfortunately, you are not an admin.</h6>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <div class="container-fluid">
                            <div class="row mb-3">
                                <div class="col-md-3 col-sm-12">
                                    <div class="input-group">
                                        <input wire:model="search" type="text" class="form-control" placeholder="Keyword">
                                        <button type="button" class="btn btn-success">
                                            <div wire:loading.remove wire:target="search">
                                                <span><i class="fa fa-search search-icon"></i></span>
                                            </div>
                                            <div wire:loading wire:target="search">
                                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion" id="accordionExample">
                                @foreach ($questions as $item)
                                    <div class="{{ $item->is_active == '0' ? 'bg-light' : '' }}" style="border-bottom: 1px solid rgba(0,0,0,0.125) !important">
                                        <div class="accordion-header" id="heading{{$item->id}}">
                                            <div class="row">
                                                <div class="col-md-11 d-flex justify-content-between align-items-center accordion-heading" style="cursor: pointer; padding-right: 2%" data-bs-toggle="collapse" href="#collapse{{$item->id}}" role="button" aria-expanded="false" aria-controls="collapse{{$item->id}}">
                                                    <a class="btn btn-link text-dark" style="text-align: left;">
                                                        <strong>{{ $item->question }}</strong>
                                                    </a>
                                                    <span class="fas fa-chevron-down question-header"></span>
                                                </div>
                                                <div class="col-md-1 d-flex justify-content-between align-items-center" style="padding-right: 3%">
                                                    <i wire:click="showEditModal('edit', {{ $item->id }})" class="fas fa-edit text-primary" style="cursor: pointer"></i>
                                                    <i wire:click="showModalConfirmationDelete({{ $item->id }})" class="fas fa-trash text-danger" style="cursor: pointer"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapse{{$item->id}}" class="accordion-collapse collapse" aria-labelledby="heading{{$item->id}}" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                                {!! $item->answer !!}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @if ($editClicked)
                            <div id="modal-edit" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog modal-xl">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            @if ($mode == 'add')
                                                <h5 class="modal-title">Add Q&A</h5>
                                            @else
                                                <h5 class="modal-title">Edit Q&A</h5>
                                            @endif
                                            <button wire:click="resetFields" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                                <div class="row mb-3">
                                                    <label for="question" class="col-sm-3 col-form-label">Question<span style="color: red"> *</span></label>
                                                    <div class="col-sm-9">
                                                        <input wire:model.defer="question" class="form-control" id="question" type="text">
                                                        @error('question')
                                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <label class="form-check-label col-sm-3 col-form-label" for="isActive">Is Active</label>
                                                    <div class="col-sm-9">
                                                        <input wire:model.defer="isActive" class="form-check-input" type="checkbox" value="" id="isActive" style="margin-top: 2%">
                                                        @error('isActive')
                                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="row form-group form-inline" wire:ignore>
                                                    <label for="answer" class="col-form-label" style="margin-right: 115px">Answer <span style="color: red"> *</span></label>
                                                    <textarea class="form-control input-full" id="answer" name="answer" wire:model.defer="answer"></textarea>
                                                    @error('answer')
                                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" wire:click="showModalConfirmation" wire:loading.attr="disabled" class="btn btn-primary">
                                                <div wire:loading.remove wire:target="showModalConfirmation, categoryName, isActive, documentName, docFile">Submit</div>
                                                <div wire:loading wire:target="showModalConfirmation, categoryName, isActive, documentName, docFile">
                                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                    Loading...
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div id="modal-confirmation" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
                            aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header alert-danger">
                                        <h4 class="modal-title" id="my-modal-title">Are you sure?</h4>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                        <button type="button" wire:click.prevent="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                            <div wire:loading.remove wire:target="submit">Yes</div>
                                            <div wire:loading wire:target="submit">
                                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @push('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            let editor = undefined;
            Livewire.hook('message.processed', (el, component) => {
                if (editor) {
                    editor.on('change', function(event) {
                        @this.set('answer', event.editor.getData());
                    })
                }
            })

            window.livewire.on('initEditor', (data) => {
                editor = CKEDITOR.replace('answer', {
                    height: 300,
                });
                editor.setData(@this.answer)
            })

            window.livewire.on('destroyEditor', (data) => {
                if (editor) {
                    editor.destroy()
                    editor = undefined
                }
            })

            window.livewire.on('destroyBackdrop', (data) => {
                let elems = document.querySelectorAll('.modal-backdrop');
                elems.forEach(el => {
                    el.remove();
                });
            });

            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });

            window.livewire.on('showModalEdit', (data) => {
                $('#modal-edit').modal('show')
            });

            window.livewire.on('hideModalEdit', (data) => {
                $('#modal-edit').modal('hide')
            });
        });
    </script>
    @endpush
</div>
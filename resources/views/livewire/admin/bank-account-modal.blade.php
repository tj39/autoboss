@if ($userId)
<div wire:ignore.self class="modal fade" id="bank-account-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Verify Transfer Account - {{ $user->firstname }} {{ $user->lastname }}</h5>
                <button wire:click="hideBankAccountModal" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <input type="hidden" wire:model="id">
                    <div class="row">
                        <label for="transferAccount" class="col-sm-3 col-form-label"><strong>Transfer Account</strong></label>
                        <div class="col-sm-9">
                            <input id="transferAccount" style="border: 0; background-color: #fff;" type="text" class="form-control" value="{{ $bankAccount }}" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label for="accountHolder" class="col-sm-3 col-form-label"><strong>Account Holder</strong></label>
                        <div class="col-sm-9">
                            <input id="accountHolder" style="border: 0; background-color: #fff;" type="text" class="form-control" value="{{ $user->account_holder }}" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label for="note" class="col-sm-3 col-form-label"><strong>Note<span style="color: red"> *</span></strong></label>
                        <div class="col-sm-9">
                            <select class="form-select" aria-label="Note" name="note" wire:model="note">
                                <option value="">Please select</option>
                                <option value="VALID">Valid</option>
                                <option value="Please write your bank account name in field Account Holder">Please write your bank account name in field Account Holder</option>
                                <option value="The account number at the selected bank is invalid or not found">The account number at the selected bank is invalid or not found</option>
                                <option value="The name of the account holder does not match">The name of the account holder does not match</option>
                                <option value="Please change bank account">Please change bank account</option>
                                <option value="USDT wallet address is invalid">Your USDT wallet address is invalid</option>
                            </select>
                            @error('note')
                                <small id="helpId" class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>                       
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="align-self: center;">
                <button type="button" wire:click.prevent="verify('account')" wire:loading.attr="disabled" class="btn btn-primary">
                    <div wire:loading.remove wire:click.prevent="verify('account')">Save</div>
                    <div wire:loading wire:click.prevent="verify('account')">
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </div>  
                </button>
            </div>
        </div>
    </div>
</div>
@endif

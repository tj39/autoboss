<div>
    <x-loading-indicator />
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Upload Tax Slip</h3>
                        </div>
                        <div>
                            <button wire:click="showUploadModal(0, 'add')" type="button" class="btn btn-primary">Upload CSV</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            @if (Auth::user()->is_admin != '1')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>Unfortunately, you are not an admin.</h6>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <div class="container-fluid">
                            <div class="row mb-3">
                                <div class="table-responsive">
                                    <table class="table table-sm table-striped">
                                        <thead>
                                            <th style="width: 5%;" scope="col">No.</th>
                                            <th style="width: 25%;" scope="col">Date</th>
                                            <th style="width: 40%;" scope="col">Folder Name</th>
                                            <th style="width: 20%;" scope="col">Category</th>
                                            <th style="width: 10%;" scope="col"></th>
                                        </thead>
                                        <tbody>
                                            @foreach ($taxFileList as $key => $item)
                                                <tr>
                                                    <th scope="row">{{ $taxFileList->firstItem() + $key }}</th>
                                                    <td>{{ $item->created_at }}</td>
                                                    <td>{{ $item->folder_name }}</td>
                                                    <td>{{ $item->category }}</td>
                                                    <td>
                                                        <div class="d-flex justify-content-center">
                                                            <i class="fas fa-edit" style="cursor: pointer;" wire:click="showUploadModal({{ $item->id }}, 'edit')" wire:loading.attr="disabled"></i>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{ $taxFileList->links() }}
                                </div>
                            </div>
                        </div>
                        {{-- File upload modal --}}
                        <div id="modal-upload-file" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Upload Tax Slip</h5>
                                        <button wire:click="resetFields" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row mb-3">
                                            <label for="folderName" class="col-sm-3 col-form-label">Folder Name<span style="color: red"> *</span></label>
                                            <div class="col-sm-9">
                                                <input wire:model="folderName" class="form-control" id="folderName" type="text">
                                                @error('folderName')
                                                    <small id="helpId" class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <label for="category" class="col-sm-3 col-form-label">Category<span style="color: red"> *</span></label>
                                            <div class="col-sm-9">
                                                <select wire:model="category" class="form-select" name="category" id="category" aria-label="Column">
                                                    <option value="">All</option>
                                                    <option value="REBATE">Rebate</option>
                                                    <option value="SHARING PROFIT">Sharing Profit</option>
                                                </select>
                                                @error('category')
                                                    <small id="helpId" class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        @if ($mode == 'add')
                                            <div class="row mb-3">
                                                <label for="csvFile" class="col-sm-3 col-form-label">File (.csv)<span style="color: red"> *</span></label>
                                                <div class="col-sm-9">
                                                    <input wire:model="csvFile" class="form-control" id="csvFile" type="file" accept=".csv">
                                                    @error('csvFile')
                                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" wire:click="showModalConfirmation" wire:loading.attr="disabled" class="btn btn-primary">
                                            <div wire:loading.remove wire:target="showModalConfirmation">Submit</div>
                                            <div wire:loading wire:target="showModalConfirmation">
                                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                Loading...
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="modal-confirmation" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
                            aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header alert-danger">
                                        <h4 class="modal-title" id="my-modal-title">Are you sure?</h4>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                        <button type="button" wire:click.prevent="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                            <div wire:loading.remove wire:target="submit">Yes</div>
                                            <div wire:loading wire:target="submit">
                                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });

            window.livewire.on('showModalUploadFile', (data) => {
                $('#modal-upload-file').modal('show')
            });

            window.livewire.on('hideModalUploadFile', (data) => {
                $('#modal-upload-file').modal('hide')
            });
        });
    </script>
</div>
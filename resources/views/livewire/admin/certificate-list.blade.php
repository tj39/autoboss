<div>
    <x-loading-indicator />
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Certificate Data</h3>
                        </div>
                        <div>
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-upload-file">Upload CSV</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            @if (Auth::user()->is_admin != '1')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>Unfortunately, you are not an admin.</h6>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-md-3 col-sm-12">
                                <input wire:model="searchCode" class="form-control" type="text" name="search1" id="search1" placeholder="Code/Name">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="table-responsive">
                                <table class="table table-sm table-striped">
                                    <thead>
                                        <th style="width: 5%;" scope="col">No.</th>                                        
                                        <th wscope="col">Type</th>
                                        <th scope="col">Code</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Achievment</th>
                                        <th scope="col">Date</th>
                                        <th scope="col"></th>
                                    </thead>
                                    <tbody>
                                        @foreach ($certificate as $key => $item)
                                            <tr>
                                                <td>{{ $certificate->firstItem() + $key }}</td>
                                                <td>{{ $item->type }}</td>
                                                <td>{{ $item->code }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->title }}</td>
                                                <td>{{ $item->achievement_date }}</td>
                                                <td><a class="btn btn-primary" href="{{ $item->url_certificate }}" target="_blank"><i class="fas fa-file-pdf"></i></a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $certificate->links() }}
                            </div>
                        </div>
                    </div>
                </div>

                <div id="modal-upload-file" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Upload CSV</h5>
                                <button wire:click="resetFields" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <form wire.submit.prevent="submit" enctype="multipart/form-data">
                                        <div class="row mb-3">
                                            <label for="excelFile" class="col-sm-3 col-form-label">File (.csv)<span style="color: red"> *</span></label>
                                            <div class="col-sm-9">
                                                <input wire:model="excelFile" class="form-control form-control-sm" id="excelFile" type="file" accept=".csv">
                                            </div>
                                            @error('excelFile')
                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" wire:click="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                    <div wire:loading.remove wire:target="submit, excelFile">Submit</div>
                                    <div wire:loading wire:target="submit, excelFile">
                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                        Loading...
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <script>
        document.addEventListener('livewire:load', function(e) {
            e.preventDefault()

            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });

            window.livewire.on('showModalUploadFile', (data) => {
                $('#modal-upload-file').modal('show')
            });

            window.livewire.on('hideModalUploadFile', (data) => {
                $('#modal-upload-file').modal('hide')
            });
        })
    </script>
</div>
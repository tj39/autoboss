@if ($userId)
<div wire:ignore.self class="modal fade" id="kyc-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Verify KYC - {{ $user->firstname }} {{ $user->lastname }}</h5>
                <button wire:click="hideKYCmodal" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <input type="hidden" wire:model="id">
                    <div class="row mb-3">
                        <div class="col-sm-6">
                            <img class="img-fluid img-thumbnail" src="{{ asset('storage/'.$user->avatar) }}" alt="User photo"
                                width="200" height="200"
                                onerror="this.onerror=null; this.src='{{ asset('assets/images/avatars/01.png') }}'">
                        </div>
                        <div class="col-sm-6">
                            <img class="img-fluid img-thumbnail" src="{{ asset('storage/'.$user->identity_card) }}" alt="User photo"
                                width="316" height="200"
                                onerror="this.onerror=null; this.src='{{ asset('assets/images/image-error-400x253.png') }}'">
                        </div>
                    </div>
                    <div class="row">
                        <label for="nik" class="col-sm-3 col-form-label"><strong>No. Identity</strong></label>
                        <div class="col-sm-9">
                            <input id="nik" style="border: 0; background-color: #fff;" type="text" class="form-control" value="{{ $user->nik }}" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label for="no_npwp" class="col-sm-3 col-form-label"><strong>No. NPWP</strong></label>
                        <div class="col-sm-9">
                            <input id="no_npwp" style="border: 0; background-color: #fff;" type="text" class="form-control" value="{{ $user->no_npwp }}" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label for="npwp" class="col-sm-3 col-form-label"><strong>NPWP</strong></label>
                        <div class="col-sm-9">
                            <button onclick="viewNpwp()" type="button" class="btn btn-primary"><span><i class="fas fa-eye"></i></span> View</button>
                            <img class="img-fluid img-thumbnail" id="npwpImage" style="display: none;" src="{{ asset('storage/'.$user->npwp) }}" alt="NPWP photo"
                                width="316" height="200"
                                onerror="this.onerror=null; this.src='{{ asset('assets/images/image-error-400x253.png') }}'">
                        </div>
                    </div>
                    <div class="row">
                        <label for="streetAddress" class="col-sm-3 col-form-label"><strong>Street Address</strong></label>
                        <div class="col-sm-9">
                            <input id="streetAddress" style="border: 0; background-color: #fff;" type="text" class="form-control" value="{{ $user->street_adress }}" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label for="city" class="col-sm-3 col-form-label"><strong>City</strong></label>
                        <div class="col-sm-9">
                            <input id="city" style="border: 0; background-color: #fff;" type="text" class="form-control" value="{{ $user->city }}" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label for="province" class="col-sm-3 col-form-label"><strong>Province</strong></label>
                        <div class="col-sm-9">
                            <input id="province" style="border: 0; background-color: #fff;" type="text" class="form-control" value="{{ $user->state }}" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label for="zipcode" class="col-sm-3 col-form-label"><strong>Zip Code</strong></label>
                        <div class="col-sm-9">
                            <input id="zipcode" style="border: 0; background-color: #fff;" type="text" class="form-control" value="{{ $user->zipcode }}" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label for="country" class="col-sm-3 col-form-label"><strong>Country</strong></label>
                        <div class="col-sm-9">
                            <input id="country" style="border: 0; background-color: #fff;" type="text" class="form-control" value="{{ $user->country }}" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <label for="note" class="col-sm-3 col-form-label"><strong>Note<span style="color: red"> *</span></strong></label>
                        <div class="col-sm-9">
                            <select class="form-select" aria-label="Note" name="note" wire:model="note">
                                <option value="">Please select</option>
                                <option value="VALID">Valid</option>
                                <option value="Duplicate identity">Duplicate identity</option>
                                <option value="Please use KTP for identity card">Please use KTP photo for identity card</option>
                                <option value="Your name not match with identity card">Your name not match with identity card</option>
                                <option value="Identity card is not clear / please use other identity card">Photo Identity card is not clear / please use other identity card</option>
                                <option value="No. Identity not match">No. Identity not match with identity card photo</option>
                                <option value="Please insert NPWP photo">Please insert NPWP photo</option>
                                <option value="NPWP is not valid">NPWP is not valid</option>
                                <option value="No. NPWP not match">No. NPWP not match with NPWP photo</option>
                                <option value="Please fill field No. Identity and or No. NPWP">Please fill field No. Identity and or No. NPWP</option>
                            </select>
                            @error('note')
                                <small id="helpId" class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>                       
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="align-self: center;">
                <button type="button" wire:click.prevent="verify('kyc')" wire:loading.attr="disabled" class="btn btn-primary">
                    <div wire:loading.remove wire:click.prevent="verify('kyc')">Save</div>
                    <div wire:loading wire:click.prevent="verify('kyc')">
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </div>  
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    function viewNpwp() {
        var el = document.getElementById('npwpImage');
        if (el.style.display === "none") {
            el.style.display = "block";
        } else {
            el.style.display = "none";
        }
    }
</script>
@endif

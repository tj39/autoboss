<div>
    <x-loading-indicator />
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Client Validation</h3>
                        </div>
                        <div>
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-upload-file">Upload CSV</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
    <div wire:ignore class="row">
            <div class="col-md-12 col-lg-12">
                <div class="row row-cols-1">
                    <div class="d-slider1 overflow-hidden ">
                        <ul class="swiper-wrapper list-inline m-0 p-0 mb-2">
                            <li wire:click = "queryClient('process')" class="swiper-slide card card-slide" data-aos="fade-up" data-aos-delay="700">
                                <div class="card-body">
                                    <div class="progress-widget">
                                        <i class="far fa-pause-circle" style="font-size: xx-large;padding-left: 10%;"></i>
                                        <div class="progress-detail">
                                            <p class="mb-2">On Process</p>
                                            <h4 class="counter">{{ $clientprocesscount }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li wire:click = "queryClient('invalid')" class="swiper-slide card card-slide" data-aos="fade-up" data-aos-delay="700">
                                <div class="card-body">
                                    <div class="progress-widget">
                                        <i class="far fa-times-circle" style="font-size: xx-large;padding-left: 10%;"></i>
                                        <div class="progress-detail">
                                            <p class="mb-2">Invalid</p>
                                            <h4 class="counter">{{ $clientinvalidcount }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="swiper-button swiper-button-next"></div>
                        <div class="swiper-button swiper-button-prev"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            @if (Auth::user()->is_admin != '1')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>Unfortunately, you are not an admin.</h6>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="table-responsive" style="font-size: smaller;">
                                <table class="table table-sm table-striped">
                                    <thead>
                                        <th style="width: 5%;" scope="col">No.</th>                                        
                                        <th style="width: 25%;" scope="col">Date</th>
                                        <th style="width: 25%;" scope="col">Name</th>
                                        <th style="width: 10%;" scope="col">Client ID</th>
                                        <th style="width: 12.5%;" scope="col">Phone</th>
                                        <th style="width: 25%;" scope="col">Direct Upline</th>
                                        <th style="width: 12.5%;" scope="col">Direct Upline Phone</th>
                                        <th style="width: 10%;" scope="col">Status</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($users as $key => $item)
                                            <tr>
                                                <th scope="row">{{ $users->firstItem() + $key }}</th>
                                                <td>{{ $item->updated_clientid_at }}</td>
                                                <td>{{ $item->firstname . ' ' . $item->lastname }}</td>
                                                <td>{{ $item->client_id }}</td>
                                                <td>{{ $item->phone }}</td>
                                                <td>{{ $item->leaderId->firstname . ' ' . $item->leaderId->lastname }}</td>
                                                <td>{{ $item->leaderId->phone }}</td>
                                                @if ($item->is_clientid_valid == '0')
                                                    <td>
                                                        On Process
                                                    </td>
                                                @elseif ($item->is_clientid_valid == '2')
                                                    <td style="color: red">
                                                        Invalid
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $users->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                {{-- File upload modal --}}
                <div id="modal-upload-file" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Upload CSV</h5>
                                <button wire:click="resetFields" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <form wire.submit.prevent="submit" enctype="multipart/form-data">
                                        <div class="row mb-3">
                                            <label for="excelFile" class="col-sm-3 col-form-label">File (.csv)<span style="color: red"> *</span></label>
                                            <div class="col-sm-9">
                                                <input wire:model="excelFile" class="form-control form-control-sm" id="excelFile" type="file" accept=".csv">
                                            </div>
                                            @error('excelFile')
                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" wire:click="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                    <div wire:loading.remove wire:target="submit, excelFile">Submit</div>
                                    <div wire:loading wire:target="submit, excelFile">
                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                        Loading...
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <script>
        document.addEventListener('livewire:load', function(e) {
            e.preventDefault()

            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });

            window.livewire.on('showModalUploadFile', (data) => {
                $('#modal-upload-file').modal('show')
            });

            window.livewire.on('hideModalUploadFile', (data) => {
                $('#modal-upload-file').modal('hide')
            });
        })
    </script>
</div>
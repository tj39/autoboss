<div>
    <x-loading-indicator />
    @include('livewire.admin.kyc-modal')
    @include('livewire.admin.bank-account-modal')
    @include('livewire.admin.npwp-modal')
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Trader Validation</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="text" placeholder="Search name" class="form-control"
                                wire:model="search">
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-success">
                                        <i class="fa fa-search search-icon"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bd-example table-responsive">
                        <table class="table table-sm table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">KYC</th>
                                    <th scope="col">Transfer Gateway</th>
                                    <th scope="col">NPWP</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $key => $item)
                                <tr>
                                    <th scope="row">{{ $data->firstItem() + $key }}</th>
                                    <td>{{ $item->date }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        @if ($item->kyc_verify_msg != 'VALID' && $item->kyc_verify_msg != 'PROCESS')
                                            Not Verified
                                        @elseif($item->kyc_verify_msg == 'VALID')
                                            Verified
                                        @else
                                            <a href="#" wire:click.prevent="showKycModal({{ $item->id }})" style="cursor: pointer;" data-bs-toggle="tooltip" data-bs-placement="top" title="Verify KYC">
                                                <i style="color: green;" class="fas fa-solid fa-id-card fa-2x"></i>
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($item->account_verify_msg != 'VALID' && $item->account_verify_msg != 'PROCESS')
                                            Not Verified
                                        @elseif($item->account_verify_msg == 'VALID')
                                            Verified
                                        @else
                                            <a href="#" wire:click.prevent="showBankAccountModal({{ $item->id }})" style="cursor: pointer;" data-bs-toggle="tooltip" data-bs-placement="top" title="Verify Transfer Account">
                                                <i style="color: green;" class="fas fa-solid fa-credit-card fa-2x"></i>
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($item->check_npwp == 2 && $item->kyc_verify_msg == 'VALID')
                                            <a href="#" wire:click.prevent="showNpwpModal({{ $item->id }})" style="cursor: pointer;" data-bs-toggle="tooltip" data-bs-placement="top" title="Verify NPWP">
                                                <i style="color: green;" class="fas fa-solid fa-id-card-alt fa-2x"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $data->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>        
        document.addEventListener('livewire:load', function(e) {
            e.preventDefault()

            window.livewire.on('getKycData', (data) => {
                $('#kyc-modal').modal('show')
            });

            window.livewire.on('getBankAccountData', (data) => {
                $('#bank-account-modal').modal('show')
            });

            window.livewire.on('getNpwpData', (data) => {
                $('#npwp-modal').modal('show')
            });

            window.livewire.on('hideModal', (data) => {
                $('#kyc-modal').modal('hide')
                $('#bank-account-modal').modal('hide')
                $('#npwp-modal').modal('hide')
                var cols = document.getElementsByClassName('modal-backdrop');
                for(i = 0; i < cols.length; i++) {
                    cols[i].style.display = 'none';
                }
                document.body.style.overflow = "auto";
            });
        })

        function viewNpwp() {
            var el = document.getElementById('npwpImage');
            if (el.style.display === "none") {
                el.style.display = "block";
            } else {
                el.style.display = "none";
            }
        }
    </script>
</div>

<div>
    @include('livewire.admin.confirm-withdraw-modal')
    <div class="iq-navbar-header" style="height: 150px;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h1>Withdraw Confirmation</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 200px;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-3">
                        {{-- <div class="col-md-8"></div> --}}
                        <div class="col-md-4">
                            <div class="input-group">
                                <input type="text" placeholder="Search code" class="form-control"
                                wire:model="searchWdCode">
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-success">
                                        <i class="fa fa-search search-icon"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bd-example table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Withdraw Code</th>
                                    <th scope="col">Transfer Amount (USD)</th>
                                    <th scope="col">Status</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $key => $item)
                                <tr>
                                    <th scope="row">{{ $data->firstItem() + $key }}</th>
                                    <td>{{ $item->date }}</td>
                                    <td>{{ $item->wd_code }}</td>
                                    <td>${{ $item->amount }}</td>
                                    <td>
                                        @if ($item->status == 'P')
                                            <span style="color: #f39c12;">Process</span>
                                        @elseif ($item->status == 'S')
                                            <span style="color: #2ecc71;">Completed</span>
                                        @else
                                            <span style="color: #e74c3c">Failed</span>
                                        @endif
                                    </td>
                                    <td>
                                        <i class="fas fa-solid {{ $item->status == 'P' ? 'fa-check' : '' }} fa-2x" style="cursor: pointer;" wire:click="showModal({{ $item->id }})"
                                            wire:loading.attr="disabled" wire:target="showModal({{ $item->id }})"
                                            data-bs-toggle="tooltip" data-bs-placement="top" title="{{ $item->status == 'P' ? 'Confirm' : '' }}"></i>
                                        <div wire:loading wire:target="showModal({{ $item->id }})">
                                            <img style="width: 25px;" src="{{ asset('assets/images/spinner-small.gif') }}" alt="Loading">
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $data->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>        
        document.addEventListener('livewire:load', function(e) {
            e.preventDefault()

            tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
            tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                return new bootstrap.Tooltip(tooltipTriggerEl)
            })

            window.livewire.on('showModal', (data) => {
                $('#confirm-withdraw-modal').modal('show')
            });

            window.livewire.on('hideModal', (data) => {
                $('#confirm-withdraw-modal').modal('hide')
                var cols = document.getElementsByClassName('modal-backdrop');
                for(i = 0; i < cols.length; i++) {
                    cols[i].style.display = 'none';
                }
            });

            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });
        })
    </script>
</div>

@if ($userId)
<div wire:ignore.self class="modal fade" id="npwp-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Verify NPWP - {{ $user->firstname }} {{ $user->lastname }}</h5>
                <button wire:click="hideNPWPmodal" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <input type="hidden" wire:model="id">
                    <div class="row mb-3">
                        <div class="col-sm-6">
                            <img class="img-fluid img-thumbnail" src="{{ asset('storage/'.$user->identity_card) }}" alt="Identity Card"
                                width="316" height="200"
                                onerror="this.onerror=null; this.src='{{ asset('assets/images/image-error-400x253.png') }}'">
                        </div>
                        <div class="col-sm-6">
                            <img class="img-fluid img-thumbnail" src="{{ asset('storage/'.$user->npwp) }}" alt="NPWP"
                                width="316" height="200"
                                onerror="this.onerror=null; this.src='{{ asset('assets/images/image-error-400x253.png') }}'">
                        </div>
                    </div>
                    <div class="row">
                        <label for="no_npwp" class="col-sm-3 col-form-label"><strong>No. NPWP</strong></label>
                        <div class="col-sm-9">
                            <input id="no_npwp" style="border: 0; background-color: #fff;" type="text" class="form-control" value="{{ $user->no_npwp }}" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="align-self: center;">
                <button type="button" wire:click.prevent="verifynpwp('3')" wire:loading.attr="disabled" class="btn btn-danger">
                    <div wire:loading.remove wire:click.prevent="verifynpwp('3')">Invalid</div>
                    <div wire:loading wire:click.prevent="verifynpwp('3')">
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </div>  
                </button>
                <button type="button" wire:click.prevent="verifynpwp('1')" wire:loading.attr="disabled" class="btn btn-primary">
                    <div wire:loading.remove wire:click.prevent="verifynpwp('1')">Valid</div>
                    <div wire:loading wire:click.prevent="verifynpwp('1')">
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </div>  
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    function viewNpwp() {
        var el = document.getElementById('npwpImage');
        if (el.style.display === "none") {
            el.style.display = "block";
        } else {
            el.style.display = "none";
        }
    }
</script>
@endif

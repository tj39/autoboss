<div>
    <x-loading-indicator />
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Training Invoice</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            @if (Auth::user()->is_admin != '1')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>Unfortunately, you are not an admin.</h6>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-md-3 col-sm-12">
                                <label class="form-label" for="search1">Client ID / Name</label>
                                <input wire:model="search1" class="form-control" type="text" name="search1" id="search1">
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <label class="form-label" for="search2">Rank</label>
                                <select wire:model="search2" class="form-select" name="search2" id="search2" aria-label="Column">
                                    <option value="">Select One</option>
                                    @foreach ($ranks as $rank)
                                        <option value="{{ $rank->code }}">{{ $rank->description }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="table-responsive" style="font-size: smaller;">
                                <table class="table table-sm table-striped">
                                    <thead>
                                        <th style="width: 5%;" scope="col">No.</th>                                        
                                        <th style="width: 20%;" scope="col">Name</th>
                                        <th style="width: 10%;" scope="col">Client ID</th>
                                        <th style="width: 10%;" scope="col">NIK</th>
                                        <th style="width: 10%;" scope="col">NPWP</th>
                                        <th style="width: 40%;" scope="col">Address</th>
                                        <th style="width: 5%;" scope="col"></th>
                                        <th style="width: 5%;" scope="col"></th>
                                    </thead>
                                    <tbody>
                                        @if ($invoices != null)
                                            @foreach ($invoices as $key => $item)
                                                <tr>
                                                    <th scope="row">{{ $invoices->firstItem() + $key }}</th>
                                                    <td>{{ $item->firstname . ' ' . $item->lastname }}</td>
                                                    <td>{{ $item->client_id }}</td>
                                                    <td>{{ $item->nik }}</td>
                                                    <td>{{ $item->no_npwp }}</td>
                                                    <td>{{ $item->address }}</td>
                                                    <td>
                                                        <div class="d-flex justify-content-center">
                                                            <i class="fas fa-eye" wire:click="showViewModal({{ $item->id }})" style="cursor: pointer;"></i>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex justify-content-center">
                                                            <i class="fas fa-receipt" wire:click="showUploadModal({{ $item->id }})" style="cursor: pointer;"></i>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                @if ($invoices != null)
                                    {{ $invoices->links() }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @if ($viewClicked)
                    <div id="modal-view" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">View Transfer Proof - {{ $user->firstname . ' ' . $user->lastname }}</h5>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <label for="invoiceNumber" class="col-sm-3 col-form-label">Invoice Number</label>
                                            <div class="col-sm-9">
                                                <input class="form-control-plaintext" id="invoiceNumber" type="text" value="{{ $user->requestId->no_invoice }}" readonly style="color: #04475F">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <img class="img-fluid" id="image-preview" src="{{ asset('storage/'.$transferProofPath) }}" alt="Transfer Proof">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="col">
                                        <div class="d-flex justify-content-center">
                                            <button wire:click="resetViewModal" type="button" data-bs-dismiss="modal" class="btn btn-primary">OK</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if ($uploadClicked)
                    <div id="modal-upload-file" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Upload Invoice - {{ $user->firstname . ' ' . $user->lastname }}</h5>
                                    <button wire:click="resetFields" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        <div class="row mb-3">
                                            <label for="pdfFile" class="col-sm-3 col-form-label">File (.pdf)<span style="color: red"> *</span></label>
                                            <div class="col-sm-9">
                                                <input wire:model="pdfFile" class="form-control" id="pdfFile" type="file" accept="application/pdf">
                                                @error('pdfFile')
                                                    <small id="helpId" class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" wire:click="showModalConfirmation" wire:loading.attr="disabled" class="btn btn-primary">
                                        <div wire:loading.remove wire:target="showModalConfirmation">Submit</div>
                                        <div wire:loading wire:target="showModalConfirmation">
                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                            Loading...
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div id="modal-confirmation" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
                    aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header alert-danger">
                                <h4 class="modal-title" id="my-modal-title">Are you sure?</h4>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                <button type="button" wire:click.prevent="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                    <div wire:loading.remove wire:target="submit">Yes</div>
                                    <div wire:loading wire:target="submit">
                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    @push('scripts')
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });

            window.livewire.on('showViewModal', (data) => {
                $('#modal-view').modal('show')
            });

            window.livewire.on('hideViewModal', (data) => {
                $('#modal-view').modal('hide')
            });

            window.livewire.on('showUploadModal', (data) => {
                $('#modal-upload-file').modal('show')
            });

            window.livewire.on('hideUploadModal', (data) => {
                $('#modal-upload-file').modal('hide')
            });
        });
    </script>
    @endpush
</div>

<div>
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Rank Approval</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            @if (Auth::user()->is_admin != '1')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>Unfortunately, you are not an admin.</h6>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-sm-3 col-md-3">
                                <div class="input-group">
                                    <input type="text" placeholder="Client ID / Name" class="form-control" wire:model="search">
                                    <button type="button" class="btn btn-success">
                                        <div wire:loading.remove wire:target="search">
                                            <span><i class="fa fa-search search-icon"></i></span>
                                        </div>
                                        <div wire:loading wire:target="search">
                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="table-responsive">
                                <table class="table table-sm table-striped">
                                    <thead>
                                        <th style="width: 5%;" scope="col">No.</th>
                                        <th style="width: 10%;" scope="col">Date</th>
                                        <th style="width: 15%;" scope="col">Client ID</th>
                                        <th style="width: 30%;" scope="col">Name</th>
                                        <th style="width: 15%;" scope="col">From Rank</th>
                                        <th style="width: 15%;" scope="col">To Rank</th>
                                        <th style="width: 5%;" scope="col">Portofolio</th>
                                        <th style="width: 5%;" scope="col"></th>
                                    </thead>
                                    <tbody>
                                        @foreach ($rankApprovals as $key => $item)
                                            <tr>
                                                <th scope="row">{{ $rankApprovals->firstItem() + $key }}</th>
                                                <td>{{ $item->updated_at }}</td>
                                                <td>{{ $item->client_id }}</td>
                                                <td>{{ $item->firstname . ' ' . $item->lastname }}</td>
                                                <td>{{ $item->from_rank }}</td>
                                                <td>{{ $item->to_rank }}</td>
                                                <td><i class="fas fa-copy" style="cursor: pointer;" wire:click="showPortofolioModal({{ $item->user_id }}, {{ $item->id }})"></i></td>
                                                <td><i class="fas fa-arrow-circle-up" style="cursor: pointer;" wire:click="showRankUpgradeModal({{ $item->user_id }}, {{ $item->id }})"></i></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $rankApprovals->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                {{-- Portofolio modal --}}
                @if ($portofolioModalClicked)
                    <div id="portofolio-modal" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Portofolio - {{ $name }}</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        <div class="table-responsive">
                                            <table class="table table-sm table-striped">
                                                <thead>
                                                    <th style="width: 5%" scope="col">No.</th>
                                                    <th style="width: 55%" scope="col">Name</th>
                                                    <th style="width: 20%" scope="col"></th>
                                                    <th style="width: 20%" scope="col">File</th>
                                                </thead>
                                                <tbody>
                                                    @foreach ($portofolios as $key => $portofolio)
                                                        <tr>
                                                            <th scope="row">{{ $portofolios->firstItem() + $key }}</th>
                                                            <td>{{ $portofolio->file_name }}</td>
                                                            <td>
                                                                @if ($portofolio->is_accept == '1')
                                                                    <div class="d-flex justify-content-center bg-success">
                                                                        <span style="color: #fff;"><i class="fas fa-check"></i></span>
                                                                    </div>
                                                                @elseif ($portofolio->is_accept == '0')
                                                                    <div class="d-flex justify-content-center bg-danger">
                                                                        <span style="color: #fff;"><i class="fas fa-times"></i></span>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <i class="fas fa-file-alt" style="cursor: pointer;" wire:click="showPdfModal({{ $portofolio->id }})"></i>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            {{ $portofolios->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                {{-- View PDF modal --}}
                @if ($viewPortofolio)
                    <div id="view-portofolio-modal" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">View Portofolio</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        <embed src="{{ asset('storage/' . $filePath) }}" type="application/pdf" width="100%" height="600px" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                {{-- Rank Upgrade modal --}}
                @if ($rankUpgradeModalClicked)
                    <div id="rank-upgrade-modal" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Rank Upgrade - {{ $name }}</h5>
                                    <button wire:click="resetFields" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        <div class="row mb-3">
                                            <label for="file" class="col-sm-4 col-form-label">File<span style="color: red"> *</span></label>
                                            {{-- <div class="col-sm-8">
                                                <select wire:model="file" class="form-select form-select-sm" multiple="multiple" name="file" id="file" aria-label="Column">
                                                    <option value="">Choose File</option>
                                                    @foreach ($files as $file)
                                                        <option value="{{ $file->id }}">{{ $file->file_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @error('file')
                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                            @enderror --}}
                                            <div class="col-sm-8">
                                                <div class="multiselect">
                                                    <div class="selectBox" onclick="showCheckboxes()">
                                                        <select class="form-control">
                                                            <option>Choose File</option>
                                                        </select>
                                                        <div class="overSelect"></div>
                                                    </div>
                                                    <div id="checkboxes" style="display: none; max-height: 100px; overflow: scroll;" class="form-control" wire:ignore.self>
                                                        @foreach ($files ?? [] as $key => $item)
                                                            <input type="checkbox" class="form-check-input" wire:model="file" id="{{ $item->id }}"  
                                                                name="file" value="{{ $item->id }}" />
                                                            <label class="inline-flex items-center form-check-label" for="{{ $item->id }}">
                                                                {{ $item->file_name }}
                                                            </label>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <label for="message" class="col-sm-4 col-form-label">Message</label>
                                            <div class="col-sm-8">
                                                <textarea wire:model="message" class="form-control" id="message" rows="2"></textarea>
                                                @error('message')
                                                    <small id="helpId" class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer d-flex justify-content-center">
                                    <button type="button" wire:click="showModalConfirmation('0')" wire:loading.attr="disabled" class="btn btn-danger">
                                        <div wire:loading.remove wire:target="showModalConfirmation('0')">Reject</div>
                                        <div wire:loading wire:target="showModalConfirmation('0')">
                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                            Loading...
                                        </div>
                                    </button>
                                    <button type="button" wire:click="showModalConfirmation('1')" wire:loading.attr="disabled" class="btn btn-primary">
                                        <div wire:loading.remove wire:target="showModalConfirmation('1')">Accept</div>
                                        <div wire:loading wire:target="showModalConfirmation('1')">
                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                            Loading...
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div id="modal-confirmation" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
                    aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header alert-danger">
                                <h4 class="modal-title" id="my-modal-title">Are you sure?</h4>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                <button type="button" wire:click.prevent="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                    <div wire:loading.remove wire:target="submit">Yes</div>
                                    <div wire:loading wire:target="submit">
                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <x-loading-indicator />
    <script>
        function showCheckboxes() {
            var checkboxes = document.getElementById("checkboxes");
            if (checkboxes.style.display === "none") {
                checkboxes.style.display = "block";
            } else {
                checkboxes.style.display = "none";
            }
        }

        document.addEventListener('livewire:load', function(e) {
            e.preventDefault()

            window.livewire.on('showPortofolioModal', (data) => {
                $('#portofolio-modal').modal('show')
            });

            window.livewire.on('hidePortofolioModal', (data) => {
                $('#portofolio-modal').modal('hide')
            });

            window.livewire.on('showPdfModal', (data) => {
                $('#view-portofolio-modal').modal('show')
            });

            window.livewire.on('hidePdfModal', (data) => {
                $('#view-portofolio-modal').modal('hide')
            });

            window.livewire.on('showRankUpgradeModal', (data) => {
                $('#rank-upgrade-modal').modal('show')
            });

            window.livewire.on('hideRankUpgradeModal', (data) => {
                $('#rank-upgrade-modal').modal('hide')
            });

            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });

            window.livewire.on('resetFields', (data) => {
                var checkboxes = document.getElementById("checkboxes");
                checkboxes.style.display = "none";
            });
        })
    </script>
</div>
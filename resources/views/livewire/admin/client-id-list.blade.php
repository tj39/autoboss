<div>
    <x-loading-indicator />
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Client ID List</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            @if (Auth::user()->is_admin != '1')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>Unfortunately, you are not an admin.</h6>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-md-3 col-sm-12">
                                <label class="form-label" for="search1">User</label>
                                <input wire:model="search1" class="form-control" type="text" name="search1" id="search1" placeholder="Client ID / Name..">
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <label class="form-label" for="search2">Direct Upline</label>
                                <input wire:model="search2" class="form-control" type="text" name="search2" id="search2" placeholder="Client ID / Name..">
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <label class="form-label" for="search3">Root Upline</label>
                                <input wire:model="search3" class="form-control" type="text" name="search3" id="search3" placeholder="Name..">
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <label class="form-label" for="searchRank">Rank</label>
                                <select name="searchRank" id="searchRank" class="form-select" wire:model = "searchRank">
                                    <option value="">Select Rank</option>
                                    @foreach ($ranks as $key => $rank)
                                    <option value="{{ $rank->id }}">{{ $rank->description }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="table-responsive" style="font-size: smaller;">
                                <table class="table table-sm table-striped">
                                    <thead>
                                        <th style="width: 5%;" scope="col">No.</th>                                        
                                        <th style="width: 15%;" scope="col">Register Date</th>
                                        <th style="width: 22%;" scope="col">Name</th>
                                        <th style="width: 10%;" scope="col">Client ID</th>
                                        <th style="width: 22%;" scope="col">Direct Upline</th>
                                        <th style="width: 22%;" scope="col">Root Upline</th>
                                        <th scope="col">Rank</th>
                                        <th style="width: 4%;" scope="col">Step</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($users as $key => $item)
                                            <tr>
                                                <th scope="row">{{ $users->firstItem() + $key }}</th>
                                                <td>{{ $item->created_at }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->client_id }}</td>
                                                <td>{{ $item->upline }}</td>
                                                <td>{{ $item->root_upline }}</td>
                                                <td>{{ $item->description }}</td>
                                                <td>{{ $item->step }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $users->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
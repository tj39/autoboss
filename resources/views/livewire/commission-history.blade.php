<div>
    <x-loading-indicator />
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Commission History</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header"
                class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div wire:ignore class="row">
            <div class="col-md-12 col-lg-12">
                <div class="row row-cols-1">
                    <div class="d-slider1 overflow-hidden ">
                        <ul class="swiper-wrapper list-inline m-0 p-0 mb-2">
                            <li class="swiper-slide card card-slide" data-aos="fade-up" data-aos-delay="700">
                                <div class="card-body">
                                    <div class="progress-widget">
                                        <i class="fas fa-hand-holding-usd" style="font-size: xxx-large;padding-left: 10%;"></i>
                                        <div class="progress-detail">
                                            <p class="mb-2">Sharing Profit</p>
                                            <h4 class="counter">{{ $sharing_profit }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="swiper-slide card card-slide" data-aos="fade-up" data-aos-delay="700">
                                <div class="card-body">
                                    <div class="progress-widget">
                                        <i class="fas fa-coins" style="font-size: xxx-large;padding-left: 10%;"></i>
                                        <div class="progress-detail">
                                            <p class="mb-2">Rebate</p>
                                            <h4 class="counter">{{ $rebate_profit }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="swiper-button swiper-button-next"></div>
                        <div class="swiper-button swiper-button-prev"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card card-custom">
                <div class="card-body">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link {{ $table == "profit" ? 'active' : '' }}" style="cursor: pointer"
                                wire:click="changeTable('profit')">Sharing Profit</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ $table == "rebate" ? 'active' : '' }}" style="cursor: pointer"
                                wire:click="changeTable('rebate')">Rebate</a>
                        </li>
                    </ul>
                    <hr>
                    @if ($table == "profit")
                    <table class="table table-striped table-sm" style="font-size: smaller;">
                        <thead>
                            <tr style="vertical-align: middle;">
                                <th scope="col">No.</th>
                                <th scope="col">Date</th>
                                <th scope="col">Code</th>
                                <th scope="col" style="white-space: pre-wrap;">USD Commissison</th>
                                <th scope="col" style="white-space: pre-wrap;">Gross Commission (IDR)</th>
                                <th scope="col">PPH (IDR)</th>
                                <th scope="col" style="white-space: pre-wrap;">Net Commission (IDR)</th>
                                <th scope="col">Admin Fee (IDR)</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sprofit_summary as $key => $sprofit)
                            <tr>
                                <td>{{ $sprofit_summary->firstItem() + $key }}</td>
                                @if ($sprofit->updated_at == null)
                                    <td>{{ date_format(date_create($sprofit->created_at), 'F Y') }}</td>
                                @else
                                    <td>{{ date_format(date_create($sprofit->updated_at), 'F Y') }}</td>
                                @endif
                                <td>{{ $sprofit->code }}</td>
                                <td>{{ $sprofit->commission_usd }}</td>
                                <td>{{ number_format($sprofit->commission_gross,0,'.',',') }}</td>
                                <td>{{ number_format($sprofit->ppn,0,'.',',') }}</td>
                                <td>{{ number_format($sprofit->commission_net,0,'.',',') }}</td>
                                @if ($sprofit->is_exported == 1 || $sprofit->is_exported == 3)
                                <td>{{ number_format($sprofit->admin_fee,0,'.',',') }}</td>
                                @else
                                <td></td>
                                @endif
                                @if ($sprofit->transfer_proof == null || $sprofit->transfer_proof == '')
                                <td style="color: orange">PENDING</td>
                                @else
                                <td style="color: green">SETTLED</td>
                                @endif
                                <td><i class="fas fa-file-invoice" style="cursor: pointer"
                                        wire:click="showModalDetail('{{ $sprofit->id }}')" data-bs-toggle="tooltip"
                                        data-bs-placement="top" title="View Detail"></i></td>
                                @if ($sprofit->transfer_proof == null || $sprofit->transfer_proof == '')
                                    <td><i hidden class="fas fa-file-pdf" style="cursor: pointer" title="Download PDF"
                                        wire:click.prevent="exportPdf('{{ $sprofit->id }}')" data-bs-toggle="tooltip"
                                        data-bs-placement="top" title="Export PDF"></i></td>
                                @else
                                    <td><i class="fas fa-file-pdf" style="cursor: pointer" title="Download PDF"
                                        wire:click.prevent="exportPdf('{{ $sprofit->id }}')" data-bs-toggle="tooltip"
                                        data-bs-placement="top" title="Export PDF"></i></td>
                                @endif
                                
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $sprofit_summary->links() }}
                    @elseif ($table == "rebate")
                    <table class="table table-striped table-sm" style="font-size: smaller;">
                        <thead>
                            <tr style="vertical-align: middle;">
                                <th scope="col">No.</th>
                                <th scope="col">Date</th>
                                <th scope="col">Code</th>
                                <th scope="col" style="white-space: pre-wrap;">USD Commissison</th>
                                <th scope="col" style="white-space: pre-wrap;">Gross Commission (IDR)</th>
                                <th scope="col">PPH (IDR)</th>
                                <th scope="col" style="white-space: pre-wrap;">Net Commission (IDR)</th>
                                <th scope="col">Admin Fee (IDR)</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($rebate_summary as $key => $rebate)
                            <tr>
                                <td>{{ $rebate_summary->firstItem() + $key }}</td>
                                @if ($rebate->updated_at == null)
                                    <td>{{ date_format(date_create($rebate->created_at), 'd F Y') }}</td>
                                @else
                                    <td>{{ date_format(date_create($rebate->updated_at), 'd F Y') }}</td>
                                @endif
                                <td>{{ $rebate->code }}</td>
                                <td>{{ $rebate->commission_usd }}</td>
                                <td>{{ number_format($rebate->commission_gross,0,'.',',') }}</td>
                                <td>{{ number_format($rebate->ppn,0,'.',',') }}</td>
                                <td>{{ number_format($rebate->commission_net,0,'.',',') }}</td>
                                @if ($rebate->is_exported == 1 || $rebate->is_exported == 3)
                                <td>{{ number_format($rebate->admin_fee,0,'.',',') }}</td>
                                @else
                                <td></td>
                                @endif
                                @if ($rebate->transfer_proof == null || $rebate->transfer_proof == '')
                                <td style="color: orange">PENDING</td>
                                @else
                                <td style="color: green">SETTLED</td>
                                @endif
                                <td><i class="fas fa-file-invoice" style="cursor: pointer"
                                        wire:click="showModalDetail('{{ $rebate->id }}')"></i></td>
                                @if ($rebate->transfer_proof == null || $rebate->transfer_proof == '')
                                    <td><i hidden class="fas fa-file-pdf" style="cursor: pointer"
                                        wire:click.prevent="exportPdf('{{ $rebate->id }}')"></i></td>
                                @else
                                    <td><i class="fas fa-file-pdf" style="cursor: pointer"
                                        wire:click.prevent="exportPdf('{{ $rebate->id }}')"></i></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $rebate_summary->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>

    @if ($detail_click)
    <div id="modal-detail" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
        aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    @if ($table == "profit")
                        <h5>Sharing Profit Detail - {{ $summary_code }}</h5>
                    @else
                        <h5>Rebate Detail - {{ $summary_code }}</h5>
                    @endif
                </div>
                <div class="modal-body">
                    <div style="overflow: auto">
                        @if ($table == "profit")
                        <table class="table table-striped table-sm">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Downline ID</th>
                                    <th scope="col">Investmen ID</th>
                                    <th scope="col">Generation</th>
                                    <th scope="col">Percentage</th>
                                    <th scope="col">Real Commission</th>
                                    <th scope="col">Dividend Commission</th>
                                    <th scope="col">Generation Commission</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($details as $key => $profit)
                                <tr>
                                    <td>{{ $details->firstItem() + $key }}</td>
                                    <td>{{ $profit->userId->firstname .' '.  $profit->userId->lastname . ' - ' . $profit->user_clientid }}</td>
                                    <td>{{ $profit->investment_id }}</td>
                                    <td>{{ $profit->leader_generation }}</td>
                                    <td>{{ $profit->percentage }}</td>
                                    <td>{{ $profit->real_commission }}</td>
                                    <td>{{ $profit->div_commission }}</td>
                                    <td>{{ $profit->commission_usd }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $details ->links() }}
                        @elseif ($table == "rebate")
                        <table class="table table-striped table-sm">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Reward Date</th>
                                    <th scope="col">Downline ID</th>
                                    <th scope="col">Your Rank</th>
                                    <th scope="col">Downline Rank</th>
                                    <th scope="col">Max Commission</th>
                                    <th scope="col">Volume (lots)</th>
                                    <th scope="col">Rebate Commission</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($details as $key => $rebate)
                                <tr>
                                    <td>{{ $details->firstItem() + $key }}</td>
                                    <td>{{ $rebate->reward_date}}</td>
                                    <td>{{ $rebate->userId->firstname .' '. $rebate->userId->lastname . ' - '. $rebate->user_clientid }}</td>
                                    <td>{{ $rebate->leaderRank->description }}</td>
                                    @if ($rebate->user_rank == "-")
                                        <td>{{ $rebate->user_rank }}</td>
                                    @else
                                        <td>{{ $rebate->userRank->description }}</td>
                                    @endif
                                    <td>{{ $rebate->max_commission }}</td>
                                    <td>{{ $rebate->volume_lots }}</td>
                                    <td>{{ $rebate->commission_usd }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>                        
                        {{ $details ->links() }}
                        @endif
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                </div>
            </div>
        </div>
    </div>
    @endif
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            window.livewire.on('showModalDetail', (data) => {
                // console.log(data)
                $('#modal-detail').modal('show')
            });
        });

    </script>
</div>

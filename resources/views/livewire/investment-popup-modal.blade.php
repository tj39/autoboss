
@if ($investmentPopupClicked == true)
    <div id="modal-investment-popup" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Investment - {{ $investmentPopupName }}</h5>
                    <button wire:click="hideInvestmentPopup" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            Total Investment: ${{ $investmentPopupTotal }}
                        </div>
                        <div class="table-responsive">
                            <table class="table table-sm table-striped">
                                <thead>
                                    <th style="width: 5%;" scope="col">No.</th>
                                    <th style="width: 25%;" scope="col">Investment ID</th>
                                    <th style="width: 50%;" scope="col">Initial Investment</th>
                                    <th style="width: 20%;" scope="col">Status</th>
                                </thead>
                                <tbody>
                                    @foreach ($investmentList as $key => $item)
                                        <tr>
                                            <th scope="row">{{ $investmentList->firstItem() + $key }}</th>
                                            <td>{{ $item->investment_id }}</td>
                                            <td>${{ $item->initial_investment }}</td>
                                            <td>{{ $item->status }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $investmentList->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

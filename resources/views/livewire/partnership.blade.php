<div>
    <x-loading-indicator />
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Partnership</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            @if (Auth::user()->step != 'OK')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>You can access this page after all steps are completed.</h6>
                    </div>
                </div>
            @elseif (Auth::user()->is_clientid_valid != '1')
                <div class="card card-custom">
                    <div class="card-body">
                        <h6>Your Client ID is not valid. You can access this page after you submit valid Client ID.</h6>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="card-body">
                        <nav class="mb-4">
                            <div wire:ignore class="nav nav-pills" id="nav-tab" role="tablist">
                                <button onclick="showPortofolioTable(id)" wire:click="refreshTab" class="nav-link active" id="nav-dashboard-tab" data-bs-toggle="tab" data-bs-target="#nav-dashboard" type="button" role="tab" aria-controls="nav-dashboard" aria-selected="true">Dashboard</button>
                                <button onclick="showPortofolioTable(id)" wire:click="refreshTab" class="nav-link" id="nav-affiliate-tab" data-bs-toggle="tab" data-bs-target="#nav-affiliate" type="button" role="tab" aria-controls="nav-affiliate" aria-selected="false">Affiliate</button>
                                @if ($currentRankCode == 'P' || $currentRankCode == 'D')
                                    <button onclick="showPortofolioTable(id)" wire:click="refreshTab" class="nav-link" id="nav-portofolio-tab" data-bs-toggle="tab" data-bs-target="#nav-portofolio" type="button" role="tab" aria-controls="nav-portofolio" aria-selected="false">Portofolio File</button>
                                @endif
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            {{-- Tab dashboard --}}
                            <div wire:ignore.self class="tab-pane fade show active" id="nav-dashboard" role="tabpanel" aria-labelledby="nav-dashboard-tab">
                                <div class="container-fluid">
                                    <label for="referenceUrl" class="form-label">Reference Leader URL</label>
                                    <div class="input-group mb-4">
                                        <input id="referenceUrl" type="text" class="form-control" value="{{ request()->getHost() . '/ref=' . $referenceUrl }}" disabled>
                                        <button onclick="copyText('referenceUrl')" class="btn btn-outline-success" type="button">Copy</button>
                                    </div>
                                    <div class="row">
                                        @if ($currentRankCode == 'D')
                                            <h5>You have reached {{ $nextRankDesc }} Rank. Yeay!</h5>
                                        @else
                                            <h5>Task List for upgrading to {{ $nextRankDesc }} Rank</h5>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="table-responsive-sm">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td style="width: 50%;">
                                                            Investment
                                                        </td>
                                                        <td style="width: 10%;">
                                                            <div>
                                                                @if ($depositTaskStatus == 1)
                                                                    <button class="btn btn-success btn-sm">Completed</button>
                                                                @else
                                                                    <button class="btn btn-secondary btn-sm">In Progress</button>
                                                                @endif
                                                            </div>
                                                        </td>
                                                        <td style="width: 40%;">
                                                            @if ($percentDeposit == 0)
                                                                <div class="progress bg-soft-secondary shadow-none w-100">
                                                                    <div class="progress-bar bg-secondary" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentDeposit }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentDeposit }}%; transition: width 2s ease 0s;">{{ $percentDeposit }}%</div>
                                                                </div>
                                                            @elseif ($percentDeposit < 51)
                                                                <div class="progress bg-soft-danger shadow-none w-100">
                                                                    <div class="progress-bar bg-danger" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentDeposit }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentDeposit }}%; transition: width 2s ease 0s;">{{ $percentDeposit }}%</div>
                                                                </div>
                                                            @elseif ($percentDeposit >= 51 && $percentDeposit < 100)
                                                                <div class="progress bg-soft-warning shadow-none w-100">
                                                                    <div class="progress-bar bg-warning" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentDeposit }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentDeposit }}%; transition: width 2s ease 0s;">{{ $percentDeposit }}%</div>
                                                                </div>
                                                            @elseif ($percentDeposit >= 100)
                                                                <div class="progress bg-soft-success shadow-none w-100">
                                                                    <div class="progress-bar bg-success" data-toggle="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%; transition: width 2s ease 0s;">100%</div>
                                                                </div>
                                                            @endif
                                                            <span style="font-size: .8rem">{{ number_format($totalDeposited, 2, '.', ',') . '/' . number_format($targetDeposit, 2, '.', ',') }} USD</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%;">
                                                            Downline
                                                        </td>
                                                        <td style="width: 10%;">
                                                            <div>
                                                                @if ($downlineTaskStatus == 1)
                                                                    <button class="btn btn-success btn-sm">Completed</button>
                                                                @else
                                                                    <button class="btn btn-secondary btn-sm">In Progress</button>
                                                                @endif
                                                            </div>
                                                        </td>
                                                        <td style="width: 40%;">
                                                            @if ($percentDownline == 0)
                                                                <div class="progress bg-soft-secondary shadow-none w-100">
                                                                    <div class="progress-bar bg-secondary" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentDownline }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentDownline }}%; transition: width 2s ease 0s;">{{ $percentDownline }}%</div>
                                                                </div>
                                                            @elseif ($percentDownline < 51)
                                                                <div class="progress bg-soft-danger shadow-none w-100">
                                                                    <div class="progress-bar bg-danger" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentDownline }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentDownline }}%; transition: width 2s ease 0s;">{{ $percentDownline }}%</div>
                                                                </div>
                                                            @elseif ($percentDownline >= 51 && $percentDownline < 100)
                                                                <div class="progress bg-soft-warning shadow-none w-100">
                                                                    <div class="progress-bar bg-warning" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentDownline }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentDownline }}%; transition: width 2s ease 0s;">{{ $percentDownline }}%</div>
                                                                </div>
                                                            @elseif ($percentDownline >= 100)
                                                                <div class="progress bg-soft-success shadow-none w-100">
                                                                    <div class="progress-bar bg-success" data-toggle="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%; transition: width 2s ease 0s;">100%</div>
                                                                </div>
                                                            @endif
                                                            <span style="font-size: .8rem">{{ $downline . '/' . $targetDownline }} people</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%;">
                                                            Turnover
                                                        </td>
                                                        <td style="width: 10%;">
                                                            <div>
                                                                @if ($turnoverTaskStatus == 1)
                                                                    <button class="btn btn-success btn-sm">Completed</button>
                                                                @else
                                                                    <button class="btn btn-secondary btn-sm">In Progress</button>
                                                                @endif
                                                            </div>
                                                        </td>
                                                        <td style="width: 40%;">
                                                            @if ($percentTurnover == 0)
                                                                <div class="progress bg-soft-secondary shadow-none w-100">
                                                                    <div class="progress-bar bg-secondary" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentTurnover }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentTurnover }}%; transition: width 2s ease 0s;">{{ $percentTurnover }}%</div>
                                                                </div>
                                                            @elseif ($percentTurnover < 51)
                                                                <div class="progress bg-soft-danger shadow-none w-100">
                                                                    <div class="progress-bar bg-danger" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentTurnover }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentTurnover }}%; transition: width 2s ease 0s;">{{ $percentTurnover }}%</div>
                                                                </div>
                                                            @elseif ($percentTurnover >= 51 && $percentTurnover < 100)
                                                                <div class="progress bg-soft-warning shadow-none w-100">
                                                                    <div class="progress-bar bg-warning" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentTurnover }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentTurnover }}%; transition: width 2s ease 0s;">{{ $percentTurnover }}%</div>
                                                                </div>
                                                            @elseif ($percentTurnover >= 100)
                                                                <div class="progress bg-soft-success shadow-none w-100">
                                                                    <div class="progress-bar bg-success" data-toggle="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%; transition: width 2s ease 0s;">100%</div>
                                                                </div>
                                                            @endif
                                                            <span style="font-size: .8rem">{{ number_format($turnover, 2, '.', ',') . '/' . number_format($targetTurnover, 2, '.', ',') }} USD</span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row d-flex justify-content-end">
                                        <div class="col-sm-3 col-md-3 d-flex justify-content-end">
                                            @if (!$canRequestUpgrade || $currentRankCode == 'D')
                                                @if ((Auth::user()->seminar_id != null && Auth::user()->seminar_status == 'ATTENDING' && Auth::user()->requestId->to_rank == 'S'))
                                                    <button type="button" wire:click="requestRankUpgrade(1)" wire:loading.attr="disabled" class="btn btn-primary">
                                                        <div wire:loading.remove wire:target="requestRankUpgrade">Upload Certificate</div>
                                                        <div wire:loading wire:target="requestRankUpgrade">
                                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                            Loading...
                                                        </div>
                                                    </button>
                                                @elseif (Auth::user()->requestId != null && (Auth::user()->requestId->status == 'REJECTED' || Auth::user()->requestId->status == 'DOWNGRADE'))
                                                    <button type="button" class="btn btn-secondary" disabled>Upload Certificate</button>
                                                @elseif (Auth::user()->requestId != null && Auth::user()->requestId->status == 'INVOICE')
                                                    <button type="button" wire:click="downloadInvoice" wire:loading.attr="disabled" class="btn btn-primary">
                                                        <div wire:loading.remove wire:target="downloadInvoice">Download Invoice</div>
                                                        <div wire:loading wire:target="downloadInvoice">
                                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                            Loading...
                                                        </div>
                                                    </button>
                                                @elseif (Auth::user()->requestId != null && ((Auth::user()->requestId->to_rank == 'G' || Auth::user()->requestId->to_rank == 'P') && Auth::user()->requestId->status == 'ACCEPTED'))
                                                    <button type="button" wire:click="requestRankUpgrade(1)" wire:loading.attr="disabled" class="btn btn-primary">
                                                        <div wire:loading.remove wire:target="requestRankUpgrade">Upload Certificate</div>
                                                        <div wire:loading wire:target="requestRankUpgrade">
                                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                            Loading...
                                                        </div>
                                                    </button>
                                                @else
                                                    <button type="button" class="btn btn-secondary" disabled>Upgrade Request</button>
                                                @endif
                                            @elseif (Auth::user()->requestId != null && ((Auth::user()->requestId->status == 'REJECTED' || Auth::user()->requestId->status == 'DOWNGRADE')
                                                || ((Auth::user()->requestId->to_rank == 'G' || Auth::user()->requestId->to_rank == 'P') && Auth::user()->requestId->status == 'ACCEPTED')))
                                                <button type="button" wire:click="requestRankUpgrade(1)" wire:loading.attr="disabled" class="btn btn-primary">
                                                    <div wire:loading.remove wire:target="requestRankUpgrade">Upload Certificate</div>
                                                    <div wire:loading wire:target="requestRankUpgrade">
                                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                        Loading...
                                                    </div>
                                                </button>
                                            @elseif (Auth::user()->requestId != null && Auth::user()->requestId->status == 'PAID')
                                                <button type="button" class="btn btn-secondary" disabled>Download Invoice</button>
                                            @else
                                                <button type="button" wire:click="requestRankUpgrade(2)" wire:loading.attr="disabled" class="btn btn-primary">
                                                    <div wire:loading.remove wire:target="requestRankUpgrade">Upgrade Request</div>
                                                    <div wire:loading wire:target="requestRankUpgrade">
                                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                        Loading...
                                                    </div>
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- Tab affiliate --}}
                            <div wire:ignore.self class="tab-pane fade" id="nav-affiliate" role="tabpanel" aria-labelledby="nav-affiliate-tab">
                                <div class="container-fluid">
                                    <div class="row mb-3">
                                        <h5>Your Direct Upline</h5>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="table-responsive">
                                            <table class="table table-sm table-striped">
                                                <thead>
                                                    <th style="width: 5%;" scope="col">No.</th>
                                                    <th style="width: 55%;" scope="col">Name</th>
                                                    <th style="width: 40%;" scope="col">Phone</th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">1</th>
                                                        <td>{{ $directUplineName }}</td>
                                                        <td>{{ $directUplinePhone }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row mb-3 d-flex justify-content-between">
                                        <div class="col"><h5>Your Downline</h5></div>
                                        <div class="col d-flex justify-content-end"><span>Total Record(s): {{ $totalRecordDownline }}</span></div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-3 col-md-3">
                                            <div class="input-group">
                                                <input type="text" placeholder="Client ID / Name" class="form-control" wire:model="search">
                                                <div class="input-group-prepend">
                                                    <button type="button" class="btn btn-success">
                                                        <div wire:loading.remove wire:target="search, filterColumn, filterValue">
                                                            <span><i class="fa fa-search search-icon"></i></span>
                                                        </div>
                                                        <div wire:loading wire:target="search, filterColumn, filterValue">
                                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                        </div>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <select onchange="showFilterValue()" wire:model="filterColumn" class="form-select" name="filterColumn" id="filterColumn" aria-label="Column">
                                                <option value="">Filter by</option>
                                                <option value="rank">Rank</option>
                                                <option value="generation">Generation</option>
                                                <option value="status">Status</option>
                                            </select>
                                        </div>
                                        <div wire:ignore.self id="filterValues" style="display: none;" class="col-sm-3 col-md-3">
                                            <select wire:model="filterValue" class="form-select" name="filterValue" id="filterValue" aria-label="Column">
                                                <option value="">Select One</option>
                                                @foreach ($filterValues as $value)
                                                    <option value="{{ $value }}">{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="table-responsive">
                                            <table class="table table-sm table-striped">
                                                <thead>
                                                    <th style="width: 5%;" scope="col">No.</th>
                                                    <th style="width: 15%;" scope="col">Client ID</th>
                                                    <th style="width: 30%;" scope="col">Name</th>
                                                    <th style="width: 15%;" scope="col">Phone</th>
                                                    <th style="width: 10%;" scope="col">Rank</th>
                                                    <th style="width: 10%;" scope="col">Generation</th>
                                                    <th style="width: 10%;" scope="col">Status</th>
                                                    <th style="width: 5%;" scope="col">Upline</th>
                                                </thead>
                                                <tbody>
                                                    @foreach ($downlineList as $key => $item)
                                                        <tr>
                                                            <th scope="row">{{ ($currentPage-1) * $rowPerPage + $key + 1 }}</th>
                                                            <td>{{ $item->client_id }}</td>
                                                            <td><a class="link-primary" href wire:click.prevent="showInvestmentPopup('{{ $item->client_id }}', '{{ $item->name }}')">{{ $item->name }}</a></td>
                                                            <td>{{ $item->phone }}</td>
                                                            <td>{{ $item->rank }}</td>
                                                            <td>{{ $item->generation }}</td>
                                                            <td>@if ($item->status == '0')
                                                                    On Process
                                                                @elseif ($item->status == '1')
                                                                    Valid
                                                                @elseif ($item->status == '2')
                                                                    Invalid
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if ($item->upline != Auth::user()->id)
                                                                    <div class="d-flex justify-content-center">
                                                                        <i class="fas fa-user" style="cursor: pointer;" wire:click="showDirectUplinePopup({{ $item->upline }})" wire:loading.attr="disabled"></i>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            @if ($hasPages)
                                                <nav aria-label="Page navigation example">
                                                    <ul class="pagination">
                                                        @if ($currentPage == 1)
                                                            <li class="page-item disabled">
                                                                <a class="page-link" aria-label="Previous">
                                                                    <span aria-hidden="true">‹</span>
                                                                </a>
                                                            </li>
                                                        @else
                                                            <li class="page-item">
                                                                <a wire:click="goToPreviousPage" wire:key="custom-pagination-page-previous" class="page-link" aria-label="Previous" style="cursor: pointer;">
                                                                    <span aria-hidden="true">‹</span>
                                                                </a>
                                                            </li>
                                                        @endif
                                                        @foreach ($customPages as $customPage)
                                                            @if ($currentPage > 3 && $customPage == 2)
                                                                <li wire:key="custom-pagination-page-1" class="page-item">
                                                                    <a wire:click="goToSpecificPage(1)" class="page-link" style="cursor: pointer;">1</a>
                                                                </li>
                                                                <li class="page-item disabled" aria-disabled="true">
                                                                    <span class="page-link">...</span>
                                                                </li>
                                                            @endif
                                                            @if ($customPage == $currentPage)
                                                                <li wire:key="custom-pagination-page-{{ $customPage }}" class="page-item active">
                                                                    <span class="page-link">{{ $customPage }}</span>
                                                                </li>
                                                            @elseif ($customPage == $currentPage + 1 || $customPage == $currentPage + 2 || $customPage == $currentPage - 1 || $customPage == $currentPage - 2)
                                                                <li wire:key="custom-pagination-page-{{ $customPage }}" class="page-item">
                                                                    <a wire:click="goToSpecificPage({{ $customPage }})" class="page-link" style="cursor: pointer;">{{ $customPage }}</a>
                                                                </li>
                                                            @endif
                                                            @if ($currentPage < $totalPages - 2 && $customPage == $totalPages - 1)
                                                                <li class="page-item disabled" aria-disabled="true">
                                                                    <span class="page-link">...</span>
                                                                </li>
                                                                <li wire:key="custom-pagination-page-{{ $totalPages }}" class="page-item">
                                                                    <a wire:click="goToSpecificPage({{ $totalPages }})" class="page-link" style="cursor: pointer;">{{ $totalPages }}</a>
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                        @if ($currentPage == $totalPages)
                                                            <li class="page-item disabled">
                                                                <a class="page-link" href aria-label="Next">
                                                                    <span aria-hidden="true">›</span>
                                                                </a>
                                                            </li>
                                                        @else
                                                            <li class="page-item">
                                                                <a wire:click="goToNextPage" wire:key="custom-pagination-page-next" class="page-link" aria-label="Next" style="cursor: pointer;">
                                                                    <span aria-hidden="true">›</span>
                                                                </a>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </nav>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- Tab portofolio file --}}
                            @if ($currentRankCode == 'P' || $currentRankCode == 'D')
                                <div wire:ignore.self class="tab-pane fade" id="nav-portofolio" role="tabpanel" aria-labelledby="nav-portofolio-tab">
                                    <div class="container-fluid">
                                        <div class="row mb-3">
                                            <h5>Upload Portofolio File</h5>
                                        </div>
                                        <div class="row mb-3">
                                            <label for="uploadPdf" class="col-sm-3 col-form-label">File (.pdf)<span style="color: red"> *</span></label>
                                            <div class="col-sm-9">
                                                <input wire:model="certificatePdf" class="form-control" id="uploadPdf" type="file" accept="application/pdf">
                                            </div>
                                            @error('certificatePdf')
                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                        <div class="row mb-3">
                                            <label for="fileName" class="col-sm-3 col-form-label">File Name<span style="color: red"> *</span></label>
                                            <div class="col-sm-9">
                                                <input wire:model="fileName" class="form-control" id="fileName" type="text">
                                            </div>
                                            @error('fileName')
                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                        <div class="row d-flex justify-content-end">
                                            <div class="col-sm-3 col-md-3 d-flex justify-content-end">
                                                @if (!$canRequestUpgrade || $currentRankCode == 'D')
                                                    <button type="button" class="btn btn-secondary" disabled>Submit</button>
                                                @else
                                                    <button type="button" wire:click="uploadPdfFile" wire:loading.attr="disabled" class="btn btn-primary">
                                                        <div wire:loading.remove wire:target="uploadPdfFile">Submit</div>
                                                        <div wire:loading wire:target="uploadPdfFile">
                                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                            Loading...
                                                        </div>
                                                    </button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            {{-- File upload modal --}}
                            <div id="modal-upload-file" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Rank Upgrade Request</h5>
                                            <button wire:click="resetFields" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                                <div class="row mb-3">
                                                    <div class="col">
                                                        <div class="form-check col-sm-12">
                                                            <input wire:model="seminarCheck" class="form-check-input" type="checkbox" value="" id="seminarCheck">
                                                            <label class="form-check-label" for="seminarCheck">
                                                                <strong>I have attended a webinar/seminar before.</strong>
                                                            </label>
                                                        </div>
                                                        @if (!$seminarCheck)
                                                            <small>
                                                                If you have already attended a seminar before, please check this checkbox to upload the certificate. If you haven't, you can attend the webinar listed below.
                                                            </small>
                                                        @endif
                                                    </div>
                                                </div>
                                                <hr style="margin-top: 1rem; border: 0; border-top: 1px solid rgba(0, 0, 0, 0.1);">
                                                <div class="row d-flex justify-content-center">
                                                    <div wire:loading wire:target="seminarCheck" class="col-sm-4">
                                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                        Loading...
                                                    </div>
                                                </div>
                                                @if ($seminarCheck)
                                                    <div class="row mb-3">
                                                        <div class="col">
                                                            Please upload your certificate to proceed.
                                                        </div>
                                                    </div>
                                                    <div class="row mb-3">
                                                        <label for="uploadPdf" class="col-sm-3 col-form-label">File (.pdf)<span style="color: red"> *</span></label>
                                                        <div class="col-sm-9">
                                                            <input wire:model="certificatePdf" class="form-control" id="uploadPdf" type="file" accept="application/pdf">
                                                        </div>
                                                        @error('certificatePdf')
                                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                    <div class="row mb-3">
                                                        <label for="fileName" class="col-sm-3 col-form-label">File Name<span style="color: red"> *</span></label>
                                                        <div class="col-sm-9">
                                                            <input wire:model="fileName" class="form-control" id="fileName" type="text">
                                                        </div>
                                                        @error('fileName')
                                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                @else
                                                    @if ($seminarEvent)
                                                        <div class="row">
                                                            <strong>Upcoming webinar:</strong>
                                                        </div>
                                                        <div class="row">
                                                            <label for="eventName" class="col-sm-3 col-form-label">Name</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" readonly class="form-control-plaintext" id="eventName" value="{{ $seminarEvent->event_name }}" style="color: #04475F">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label for="eventDate" class="col-sm-3 col-form-label">Date</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" readonly class="form-control-plaintext" id="eventDate" value="{{ date_format(date_create($seminarEvent->event_date), 'D, d M Y') }}" style="color: #04475F">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <label for="eventTime" class="col-sm-3 col-form-label">Time</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" readonly class="form-control-plaintext" id="eventTime" value="{{ $seminarEvent->event_time }}" style="color: #04475F">
                                                            </div>
                                                        </div>
                                                        <div class="row mb-1">
                                                            <label for="eventUrl" class="col-sm-3 col-form-label">URL</label>
                                                            <div class="col-sm-9">
                                                                <div class="input-group">
                                                                    <input id="eventUrl" type="text" class="form-control" value="{{ $seminarEvent->url }}" disabled>
                                                                    <button onclick="copyText('eventUrl')" class="btn btn-outline-success" type="button">Copy</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row mb-3">
                                                            <label for="eventPasscode" class="col-sm-3 col-form-label">Passcode</label>
                                                            <div class="col-sm-9">
                                                                <div class="input-group">
                                                                    <input id="eventPasscode" type="text" class="form-control" value="{{ $seminarEvent->passcode }}" disabled>
                                                                    <button onclick="copyText('eventPasscode')" class="btn btn-outline-success" type="button">Copy</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @if ($nextRankCode == 'G' || $nextRankCode == 'P')
                                                            <hr style="margin-top: 1rem; border: 0; border-top: 1px solid rgba(0, 0, 0, 0.1);">
                                                            <div class="row">
                                                                <div class="col">
                                                                    <strong>Please upload your transfer proof.</strong>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <label for="invoiceNumber" class="col-sm-3 col-form-label">Invoice Number</label>
                                                                <div class="col-sm-9">
                                                                    <input class="form-control-plaintext" id="invoiceNumber" type="text" value="{{ $invoiceNumber }}" readonly style="color: #04475F">
                                                                    <small><span><i class="fas fa-exclamation-circle"></i></span> Please add this number to your transfer report.</small>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <label for="amountToBePaid" class="col-sm-3 col-form-label">Amount to be Paid</label>
                                                                <div class="col-sm-9">
                                                                    <input class="form-control-plaintext" id="amountToBePaid" type="text" value="IDR {{ number_format($amountToBePaid, 2, '.', ',') }}" readonly style="color: #04475F">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <label for="accountNumber" class="col-sm-3 col-form-label">Account Number</label>
                                                                <div class="col-sm-9">
                                                                    <input class="form-control-plaintext" id="accountNumber" type="text" value="{{ $accountNumber }}" readonly style="color: #04475F">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <label for="destinationAccount" class="col-sm-3 col-form-label">Destination Account</label>
                                                                <div class="col-sm-9">
                                                                    <input class="form-control-plaintext" id="destinationAccount" type="text" value="{{ $destinationAccount }}" readonly style="color: #04475F">
                                                                </div>
                                                            </div>
                                                            <div class="row mb-3">
                                                                <label for="transferProofImg" class="col-sm-3 col-form-label">Transfer Proof (.png, .jpg, .jpeg) Max 1MB<span style="color: red"> *</span></label>
                                                                <div class="col-sm-9">
                                                                    <input wire:model="transferProofImg" class="form-control" id="transferProofImg" type="file" accept="image/jpg, image/jpeg, image/png">
                                                                    @error('transferProofImg')
                                                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <input wire:model="agreeToAttend" class="form-check-input" type="checkbox" value="" id="agreeToAttend">
                                                            <label class="form-check-label" for="agreeToAttend">
                                                                <strong>I agree to attend the webinar listed above.</strong>
                                                            </label>
                                                            @error('agreeToAttend')
                                                                <small id="helpId" class="text-danger">{{ $message }}</small>
                                                            @enderror
                                                        @else
                                                            <div class="row mb-3">
                                                                <div class="col">
                                                                    <div class="form-check">
                                                                        <input wire:model="agreeToAttend" class="form-check-input" type="checkbox" value="" id="agreeToAttend">
                                                                        <label class="form-check-label" for="agreeToAttend">
                                                                            <strong>I agree to attend the webinar listed above.</strong>
                                                                        </label>
                                                                        @error('agreeToAttend')
                                                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    @else
                                                        <div class="row mb-3">
                                                            <div class="col">
                                                                There is no upcoming webinar at the moment. Please stay tune!
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" wire:click="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                                <div wire:loading.remove wire:target="submit">Submit</div>
                                                <div wire:loading wire:target="submit">
                                                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                    Loading...
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if ($modalUpload2)
                                <div id="modal-upload-file2" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                @if (Auth::user()->seminar_id != null && Auth::user()->seminar_status == 'EXPIRED')
                                                    <h5 class="modal-title">Rank Upgrade Request</h5>
                                                @else
                                                    <h5 class="modal-title">Upload Certificate</h5>
                                                @endif
                                                <button wire:click="resetFields" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="container-fluid">
                                                    <div class="row mb-3">
                                                        <div class="col">
                                                            Please upload your certificate to proceed.
                                                        </div>
                                                    </div>
                                                    <div class="row mb-3">
                                                        <label for="uploadPdf" class="col-sm-3 col-form-label">File (.pdf)<span style="color: red"> *</span></label>
                                                        <div class="col-sm-9">
                                                            <input wire:model="certificatePdf" class="form-control form-control-sm" id="uploadPdf" type="file" accept="application/pdf">
                                                        </div>
                                                        @error('certificatePdf')
                                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                    <div class="row mb-3">
                                                        <label for="fileName" class="col-sm-3 col-form-label">File Name<span style="color: red"> *</span></label>
                                                        <div class="col-sm-9">
                                                            <input wire:model="fileName" class="form-control form-control-sm" id="fileName" type="text">
                                                        </div>
                                                        @error('fileName')
                                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" wire:click="submit" wire:loading.attr="disabled" class="btn btn-primary">
                                                    <div wire:loading.remove wire:target="submit">Submit</div>
                                                    <div wire:loading wire:target="submit">
                                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                                        Loading...
                                                    </div>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div wire:ignore.self class="card card-custom" id="portofolio-table" style="display: none;">
                    @if ($currentRankCode == 'P' || $currentRankCode == 'D')
                        <div class="card-body p-1-custom">
                            <div class="row mb-3">
                                <h5>Portofolio List</h5>
                            </div>
                            <div class="row">
                                <div class="table-responsive">
                                    <table class="table table-sm table-striped">
                                        <thead>
                                            <th style="width: 5%;" scope="col">No.</th>
                                            <th style="width: 25%;" scope="col">Date</th>
                                            <th style="width: 50%;" scope="col">Name</th>
                                            <th style="width: 10%;" scope="col">Status</th>
                                            <th style="width: 5%;" scope="col">File</th>
                                            <th style="width: 5%;" scope="col"></th>
                                        </thead>
                                        <tbody>
                                            @foreach ($portofolioList as $key => $item)
                                                <tr>
                                                    <th scope="row">{{ $portofolioList->firstItem() + $key }}</th>
                                                    <td>{{ $item->date }}</td>
                                                    <td>{{ $item->file_name }}</td>
                                                    @if ($item->is_accept == '0')
                                                        <td>
                                                            <div class="d-flex justify-content-center bg-danger">
                                                                <span style="color: #fff;"><i class="fas fa-times"></i></span>
                                                            </div>
                                                        </td>
                                                    @elseif ($item->is_accept == '1')
                                                        <td>
                                                            <div class="d-flex justify-content-center bg-success">
                                                                <span style="color: #fff;"><i class="fas fa-check"></i></span>
                                                            </div>
                                                        </td>
                                                    @else
                                                        <td></td>
                                                    @endif
                                                    <td>
                                                        <div class="d-flex justify-content-center">
                                                            <i class="fas fa-file-download" style="cursor: pointer;" wire:click="downloadPortofolio({{ $item->id }})" wire:loading.attr="disabled"></i>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex justify-content-center">
                                                            @if (!$canRequestUpgrade)
                                                                <i hidden class="fas fa-trash-alt" style="cursor: pointer;" wire:click="showModalConfirmation({{ $item->id }})" wire:loading.attr="disabled"></i>
                                                            @else
                                                                <i class="fas fa-trash-alt" style="cursor: pointer;" wire:click="showModalConfirmation({{ $item->id }})" wire:loading.attr="disabled"></i>
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{ $portofolioList->links() }}
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div id="modal-confirmation" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
                    aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header alert-danger">
                                <h4 class="modal-title" id="my-modal-title">Are you sure?</h4>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                                <button type="button" wire:click.prevent="deletePortofolio({{ $selectedIdToDelete }})" wire:loading.attr="disabled" class="btn btn-primary">
                                    <div wire:loading.remove wire:target="deletePortofolio({{ $selectedIdToDelete }})">Yes</div>
                                    <div wire:loading wire:target="deletePortofolio({{ $selectedIdToDelete }})">
                                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                @include('livewire.investment-popup-modal')
                @include('livewire.direct-upline-modal')
            @endif
        </div>
    </div>
    <script>
        document.addEventListener('livewire:load', function(e) {
            e.preventDefault()

            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });

            window.livewire.on('showModalUploadFile', (data) => {
                $('#modal-upload-file').modal('show')
            });

            window.livewire.on('hideModalUploadFile', (data) => {
                $('#modal-upload-file').modal('hide')
            });

            window.livewire.on('showModalUploadFile2', (data) => {
                $('#modal-upload-file2').modal('show')
            });

            window.livewire.on('hideModalUploadFile2', (data) => {
                $('#modal-upload-file2').modal('hide')
            });

            window.livewire.on('showInvestmentPopup', (data) => {
                $('#modal-investment-popup').modal('show')
            });

            window.livewire.on('hideInvestmentPopup', (data) => {
                $('#modal-investment-popup').modal('hide')
            });

            window.livewire.on('showDirectUplinePopup', (data) => {
                $('#modal-direct-upline').modal('show')
            });

            window.livewire.on('hideDirectUplinePopup', (data) => {
                $('#modal-direct-upline').modal('hide')
            });
        })

        function showPortofolioTable(id) {
            var el = document.getElementById('portofolio-table');
            if (id == 'nav-portofolio-tab') {
                el.style.display = 'block';
            } else {
                el.style.display = 'none';
            }
        }

        function showFilterValue() {
            var columnEl = document.getElementById('filterColumn');
            var filterValuesEl = document.getElementById('filterValues');
            if (columnEl.value != '') {
                filterValuesEl.style.display = 'block';
            } else {
                filterValuesEl.style.display = 'none';
            }
        }
    </script>
</div>



@if ($uplineId)
    <div id="modal-direct-upline" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Direct Upline - {{ $userUpline->firstname . ' ' . $userUpline->lastname }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="table-responsive">
                            <table class="table table-sm table-striped">
                                <tbody>
                                    <tr>
                                        <td><strong>Client ID</strong></td>
                                        <td>{{ $userUpline->client_id }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Name</strong></td>
                                        <td>{{ $userUpline->firstname . ' ' . $userUpline->lastname }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Phone</strong></td>
                                        <td>{{ $userUpline->phone }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Rank</strong></td>
                                        <td>{{ $uplineRank }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
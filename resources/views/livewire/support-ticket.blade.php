<div>
    @push('styles')
        <style>
            .circle {
                display: inline-block;
                border-radius: 50%;
                min-width: 20px;
                min-height: 20px;
                padding: 5px;
                background: red;
                color: white;
                text-align: center;
                line-height: 1;
                box-sizing: content-box;
                white-space: nowrap;
                }
                .circle:before {
                content: "";
                display: inline-block;
                vertical-align: middle;
                padding-top: 100%;
                height: 0;
                }
                .circle span {
                display: inline-block;
                vertical-align: middle;
            }
        </style>
    @endpush
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Support Ticket</h3>
                        </div>
                        <a href="{{ route('user.create.ticket') }}" type="button" class="btn btn-primary" wire:loading.attr="disabled">Create Ticket</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">No. Ticket</th>
                                <th scope="col">Date</th>
                                <th scope="col">Title</th>
                                <th scope="col">Category</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($tickets as $key => $ticket)
                            @if ($ticket->is_read_user == 0)
                            <tr>
                            @else
                            <tr class="table-light">
                            @endif
                                <td>{{ $tickets->firstItem() + $key }}</td>
                                <td>{{ $ticket->no_ticket }}</td>
                                <td>{{ $ticket->created_date }}</td>
                                <td>{{ $ticket->title }}</td>
                                <td>{{ $ticket->category->name }}</td>
                                <td style="color : {{ $ticket->status->color }}">{{ $ticket->status->name }}</td>
                                <td><a style="color :black" href = "{{ route('user.detail.ticket', ['id' => $ticket->id, 'page' => $tickets->currentPage()]) }}" style="font-weight: bold;"><i class="fas fa-comments" style="cursor: pointer;"></i></a></td>
                                @if ($this->getCommentRead($ticket->id) != 0)
                                <td><div class="circle"><span>{{ $this->getCommentRead($ticket->id) }}</span></div></td>
                                @else
                                <td></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $tickets->links() }}
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            
        });
    </script>
</div>


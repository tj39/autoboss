<div>
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Detail Ticket</h3>
                        </div>
                        @if ($ticket->status_id == 2 || $ticket->status_id == 3)
                        <button class="btn btn-primary pull-right" wire:click="showModalConfirmSolved"
                            wire:loading.attr="disabled" disabled>Solved</button>
                        @else
                        <button class="btn btn-primary pull-right" wire:click="showModalConfirmSolved"
                            wire:loading.attr="disabled" type="button">Solved</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="{{ asset('/assets/images/dashboard/top-header.jpg') }}" alt="header"
                class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <ul class="list-group">
                        <li class="list-group-item border-0 border-bottom-1 d-flex">
                            <span class="col-sm-6 col-md-3">No. Ticket</span>
                            <span class="col-sm-6 col-md-3">: {{ $ticket->no_ticket }}</span>
                        </li>
                        <li class="list-group-item border-0 border-bottom-1 d-flex">
                            <span class="col-sm-6 col-md-3">Name</span>
                            <span class="col-sm-6 col-md-3">: {{ $ticket->user->username }} - {{ $ticket->user->firstname }} {{ $ticket->user->lastname }}</span>
                        </li>
                        <li class="list-group-item border-0 border-bottom-1 d-flex">
                            <span class="col-sm-6 col-md-3">Client ID</span>
                            <span class="col-sm-6 col-md-3">: {{ $ticket->user->client_id }}</span>
                        </li>
                        <li class="list-group-item border-0 border-bottom-1 d-flex">
                            <span class="col-sm-6 col-md-3">Title</span>
                            <span class="col-sm-6 col-md-3">: {{ $ticket->title }}</span>
                        </li>
                        <li class="list-group-item border-0 border-bottom-1 d-flex">
                            <span class="col-sm-6 col-md-3">Category</span>
                            <span class="col-sm-6 col-md-3">: {{ $ticket->category->name }}</span>
                        </li>
                        <li class="list-group-item border-0 border-bottom-1 d-flex">
                            <span class="col-sm-6 col-md-3">Message</span>
                            <span class="col-sm-6 col-md-3">: {{ $ticket->message }}</span>
                        </li>
                        <li class="list-group-item border-0 border-bottom-1 d-flex">
                            <span class="col-sm-6 col-md-3">Url Image</span>
                            <span class="col-sm-6 col-md-3">: <a href="{{ $ticket->url_image }}"
                                    target="_blank">{{ $ticket->url_image }}</a></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        @foreach ($comment as $item)
        <div class = "row">
            <div class="col-md-12">
                <div class="card col-md-6" style="{{$item->user->is_admin == 1 ? 'float: left' : 'float: right'}}">
                    <div class="card-body">
                        <ul class="list-group">
                            @if ($item->user->is_admin == 1)
                            <li class="list-group-item border-0 border-bottom-1 d-flex justify-content-between align-items-center">
                                <span class="pull-right"><b>{{ strtok($item->user->firstname, " ") }} {{ strtok($item->user->lastname, " ") }}</b></span>
                                <span style="text-align: right;"><i>{{ $item->created_date }}</i></span>
                            </li>
                            @else
                            @if (Auth::user()->is_admin == 1)
                            <li class="list-group-item border-0 border-bottom-1 d-flex justify-content-between align-items-center">
                                <b>{{ strtok($item->user->username, " ") }}</b>
                                <span class="pull-right"><i>{{ $item->created_date }}</i></span>
                            </li>
                            @else
                            <li class="list-group-item border-0 border-bottom-1 d-flex justify-content-between align-items-center">
                                <span class="pull-right"><i>{{ $item->created_date }}</i></span>
                                <b>You</b>
                            </li>
                            @endif
                            @endif
                            <li
                                class="list-group-item border-0">
                                {!! $item->message !!}
                            </li>
                            <li
                                class="list-group-item border-0">
                                <a href="{{ $item->url_image }}" target="_blank">{{ $item->url_image }}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @if ($ticket->status_id == 2 || $ticket->status_id == 3)
                    <div class="form-group form-inline {{$errors->has('message') ? 'has-error has-feedback' : '' }}" wire:ignore hidden>
                        <label for="message" class="col-form-label" style="margin-right: 115px">Message</label>
                        <textarea class="form-control input-full" id="message" name="message"
                        wire:model.defer="message" disabled></textarea>
                        <small id="helpId{{'message'}}"
                            class="text-danger">{{ $errors->has('message') ? $errors->first('message') : '' }}</small>
                    </div>
                    <div class="input-group {{$errors->has('imageurl') ? 'has-error has-feedback' : '' }}">
                        <div class="input-group-prepend">
                            <a class="btn btn-primary" type="button" href="https://imglink.io/" target="_blank">Upload Image Here</a>
                        </div>
                        <input id="imageurl" value="" name="imageurl" wire:model="imageurl"
                            type="text" class="form-control col-md-3" placeholder="Url Image" disabled>
                    </div>
                    <br>
                    <div class="form-group">
                        <button class="btn btn-primary pull-right" wire:click="showModalKonfirmasi"
                            wire:loading.attr="disabled" style="float: right;" disabled>Send</button>
                        <a class="btn btn-secondary"
                            wire:loading.attr="disabled" style="float: right; margin-right: 2%" href="{{ route('user.support.ticket') }}">Back</a>
                    </div>
                    @else
                    <div class="form-group form-inline {{$errors->has('message') ? 'has-error has-feedback' : '' }}" wire:ignore>
                        <label for="message" class="col-form-label" style="margin-right: 115px">Message</label>
                        <textarea class="form-control input-full" id="message" name="message"
                        wire:model.defer="message" wire:key = "message">{!! $message !!}</textarea>
                        <small id="helpId{{'message'}}"
                            class="text-danger">{{ $errors->has('message') ? $errors->first('message') : '' }}</small>
                    </div>
                    <div class="input-group {{$errors->has('imageurl') ? 'has-error has-feedback' : '' }}">
                        <div class="input-group-prepend">
                            <a class="btn btn-primary" type="button" href="https://imglink.io/" target="_blank">Upload Image Here</a>
                        </div>
                        <input id="imageurl" value="" name="imageurl" wire:model="imageurl"
                            type="text" class="form-control col-md-3" placeholder="Url Image">
                    </div>
                    <br>
                    <div class="form-group">
                        <button class="btn btn-primary pull-right" wire:click="showModalConfirmSend"
                            wire:loading.attr="disabled" style="float: right;">
                            <div wire:loading.remove wire:target="showModalConfirmSend">Send</div>
                            <div wire:loading wire:target="showModalConfirmSend">
                                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                Loading...
                            </div>        
                        </button>
                        <a class="btn btn-secondary"
                            wire:loading.attr="disabled" style="float: right; margin-right: 2%" href="/user/support-ticket?page={{ $page }}">Back</a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div id="modal-confirm-solved" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
        aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="alert-danger">
                    <h5 class="modal-title" id="my-modal-title">Are you sure?</h5>
                </div>
                <div class="modal-footer justify-content-center">
                    <button id='closeModal' type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-primary" wire:click="storeStatus">Yes</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-confirm-send" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
        aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="alert-danger">
                    <h5 class="modal-title" id="my-modal-title">Are you sure?</h5>
                </div>
                <div class="modal-footer justify-content-center">
                    <button id='closeModal' type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-primary" wire:click="sendMessage">Yes</button>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
    <script>
        const editor = CKEDITOR.replace('message',{
        height: 500,
        });
        editor.on('change', function(event){
            console.log(event.editor.getData())
            @this.set('message', event.editor.getData());
        })
    </script>
    @endpush
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            window.livewire.on('showModalConfirmSolved', (data) => {
            // console.log(data)
                $('#modal-confirm-solved').modal('show')
            });

            window.livewire.on('showModalConfirmSend', (data) => {
            // console.log(data)
                $('#modal-confirm-send').modal('show')
            });

            window.livewire.on('hideModal', (data) => {
            // console.log(data)
                $('#modal-confirm-send').modal('hide')
                $('#modal-confirm-solved').modal('hide')
            });
        });

    </script>
</div>

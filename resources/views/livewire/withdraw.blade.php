<div>
    <div class="iq-navbar-header" style="height: 150px;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h1>Withdraw</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 200px;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <nav>
                        <div class="nav nav-pills" id="nav-tab" role="tablist">
                            <button class="nav-link active" id="nav-request-tab" data-bs-toggle="tab" data-bs-target="#nav-request" type="button" role="tab" aria-controls="nav-request" aria-selected="true">Request</button>
                            <button class="nav-link" id="nav-history-tab" data-bs-toggle="tab" data-bs-target="#nav-history" type="button" role="tab" aria-controls="nav-history" aria-selected="false">History</button>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-request" role="tabpanel" aria-labelledby="nav-request-tab">
                            <div class="container-fluid">
                                <div class="row mb-3">
                                    <label for="balance" class="col-sm-3 col-form-label"><strong>Balance</strong></label>
                                    <div class="col-sm-9">
                                        <input wire:model="balance" id="balance" style="border: 0; background-color: #fff;" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="gateway" class="col-sm-3 col-form-label"><strong>Withdraw Gateway</strong></label>
                                    <div class="col-sm-9">
                                        <select class="form-select" aria-label="Gateway" id="gateway" wire:model="gateway">
                                            <option value="">Select method</option>
                                            @foreach ($gateways as $gateway)
                                            <option value="{{ $gateway->id }}">{{ $gateway->description }}</option>
                                            @endforeach
                                        </select>
                                        @error('gateway')
                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="amount" class="col-sm-3 col-form-label"><strong>Amount (USD)</strong></label>
                                    <div class="col-sm-9">
                                        <input wire:model="amount" id="amount" type="number" class="form-control" placeholder="Minimum withdraw is ${{ $minWithdraw }}">
                                        @error('amount')
                                            <small id="helpId" class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="source" class="col-sm-3 col-form-label"><strong>Source</strong></label>
                                    <div class="col-sm-9">
                                        <select class="form-select" aria-label="Source" id="source" wire:model="source">
                                            <option value="1">Wallet</option>
                                            <option value="2">MT4 Account</option>
                                        </select>
                                    </div>
                                </div>
                                @if ($source == 2)
                                <div class="row mb-3">
                                    <label for="account" class="col-sm-3 col-form-label"><strong>Account</strong></label>
                                    <div class="col-sm-9">
                                        <select class="form-select" aria-label="Source" id="account" wire:model="account">
                                            <option value="{{ $user->id }}">{{ $user->username }}</option>
                                        </select>
                                    </div>
                                </div>
                                @endif
                                <div class="row mb-3">
                                    <div class="form-check" style="margin-left: 19px;">
                                        <input wire:model="agreement" class="form-check-input" type="checkbox" value="" id="agreement">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            I Agree with the Terms and Conditions.
                                        </label>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-3">
                                        <input wire:model="password" class="form-control" type="password" id="password" placeholder="Enter your password">
                                    </div>
                                    <div class="col-md-2 float-end">
                                        <button wire:click="request()" type="button" class="btn btn-primary" 
                                            wire:loading.attr="disabled">Submit</button>
                                        <div wire:loading>
                                            <img style="width: 25px;" src="{{ asset('assets/images/spinner-small.gif') }}" alt="Loading">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-history" role="tabpanel" aria-labelledby="nav-history-tab">
                            <div class="bd-example table-responsive">
                                <div class="d-flex justify-content-end mb-3" wire:loading.class="is-loading" wire:target="exportPdf">
                                    <i class="fas fa-solid fa-file-pdf fa-2x" style="cursor: pointer;" wire:click="exportPdf"
                                        wire:loading.attr="disabled" wire:target="exportPdf"
                                        data-bs-toggle="tooltip" data-bs-placement="top" title="Download current page as PDF"></i>
                                        <div wire:loading wire:target="exportPdf">
                                            <img style="width: 25px;" src="{{ asset('assets/images/spinner-small.gif') }}" alt="Loading">
                                        </div>
                                </div>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="col">No.</th>
                                            <th scope="col">Date</th>
                                            <th scope="col">Withdraw Code</th>
                                            <th scope="col">Amount (USD)</th>
                                            <th scope="col">Rate</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Message</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if ($historyData->total() != 0)
                                            @foreach ($historyData as $key => $item)
                                            <tr>
                                                <th scope="row">{{ $historyData->firstItem() + $key }}</th>
                                                <td>{{ $item->date }}</td>
                                                <td>{{ $item->wd_code }}</td>
                                                <td>${{ $item->amount }}</td>
                                                <td>Rp. {{ $rate }}</td>
                                                <td>
                                                    @if ($item->status == 'P')
                                                        <span style="color: #f39c12;">Process</span>
                                                    @elseif ($item->status == 'S')
                                                        <span style="color: #2ecc71;">Completed</span>
                                                    @else
                                                        <span style="color: #e74c3c">Failed</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($item->status == 'S')
                                                        Success - {{ $item->confirmed_at }}
                                                    @elseif ($item->status == 'F')
                                                        {{ $item->message }}
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach    
                                        @else
                                            <tr>
                                                <td colspan="7">No record to be shown.</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                                {{ $historyData->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            
        });
    </script>
</div>


@if ($strategy)
    <div id="modal-strategy-detail" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Trading Strategy - {{ $strategy->net_profit . ' : ' . $strategy->commission }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="table-responsive">
                            <table class="table table-sm table-striped">
                                <tbody>
                                    <tr>
                                        <td><strong>Commission</strong></td>
                                        <td>{{ $strategy->commission }}%</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Leverage</strong></td>
                                        <td>{{ $strategy->leverage }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Min. Investment</strong></td>
                                        <td>${{ $strategy->min_investment }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Max. Investment</strong></td>
                                        <td>${{ $strategy->max_investment }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        {{-- <div class="row">
                            <label for="commission" class="col-sm-4 col-form-label"><strong>Commission</strong></label>
                            <div class="col-sm-8">
                                <input id="commission" readonly type="text" class="form-control-plaintext" value="{{ $strategy->commission }}">
                            </div>
                        </div>
                        <div class="row">
                            <label for="leverage" class="col-sm-4 col-form-label"><strong>Leverage</strong></label>
                            <div class="col-sm-8">
                                <input id="leverage" readonly type="text" class="form-control-plaintext" value="{{ $strategy->leverage }}">
                            </div>
                        </div>
                        <div class="row">
                            <label for="minInvestment" class="col-sm-4 col-form-label"><strong>Min. Investment</strong></label>
                            <div class="col-sm-8">
                                <input id="minInvestment" readonly type="text" class="form-control-plaintext" value="{{ $strategy->min_investment }}">
                            </div>
                        </div>
                        <div class="row">
                            <label for="maxInvestment" class="col-sm-4 col-form-label"><strong>Max. Investment</strong></label>
                            <div class="col-sm-8">
                                <input id="maxInvestment" readonly type="text" class="form-control-plaintext" value="{{ $strategy->max_investment }}">
                            </div>
                        </div> --}}
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <a href="{{ $this->strategy->url_strategy }}" target="_blank" type="button" class="btn btn-primary" wire:loading.attr="disabled" wire:click="selectStrategy({{ $strategy->id }})">Choose</a>
                    
                </div>
            </div>
        </div>
    </div>
@endif
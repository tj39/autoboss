<div>
    <x-loading-indicator />
    @if ($certificateCount == 0)
    <div class="col d-flex justify-content-center mt-3">
        <div class="col-md-5">
            <div class="card">
                <div class="card-body">
                    <h6 class="text-center">Not Found</h6>
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="col d-flex justify-content-center mt-3">
        <div class="col-md-5">
            <div class="card">
                <div class="card-body">
                    <h6 class="text-center"><i class="fas fa-check-circle" style="color: green"></i>Verified Certificate</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="col d-flex justify-content-center">
        <div class="col-md-5">
            <div class="card">
                <div class="card-body">
                    <ul class="list-unstyled">
                        <li>Code</li>
                        <li class="fw-bold" style="color: black">{{ $certificate->code }}</li>
                        <li class="mt-3">Rank Certificate</li>
                        <li class="fw-bold" style="color: black">{{ $certificate->type }}</li>
                        <li class="mt-3">Title</li>
                        <li class="fw-bold" style="color: black">{{ $certificate->title }}</li>
                        <li class="mt-3">Name</li>
                        <li class="fw-bold" style="color: black">{{ $certificate->name }}</li>
                        <li class="mt-3">Achievement Date</li>
                        <li class="fw-bold" style="color: black">{{ date_format(date_create($certificate->achievement_date), 'D, d M Y') }}</li>
                     </ul>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>


<div>
    <div class="iq-navbar-header" style="height: 12.5rem;">
        <div class="container-fluid iq-container">
            <div class="row row-custom">
                <div class="col-md-12 col-sm-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Hello, {{ Auth::user()->firstname . ' ' . Auth::user()->lastname }}!</h3>
                            @if (Auth::user()->is_admin != 1)
                                @if (Auth::user()->step == 'OK')
                                    <p>We help you to get stable profit from every trade made by TP99.</p>
                                @else
                                    <p>Complete the steps to enjoy your automated trading.</p>
                                @endif
                            @endif
                        </div>
                        <!-- <div>
                            @if (Auth::user()->is_admin != 1)
                                @if (Auth::user()->step == 'OK')
                                <a href="#" class="btn btn-link btn-success">
                                    <i class="icon fas fa-solid fa-check"></i>
                                    Completed
                                </a>
                                @else
                                <a href="#" class="btn btn-link btn-danger">
                                    @if (Auth::user()->step == '1')
                                        <i class="icon fas fa-solid fa-id-card"></i>
                                        KYC Verification
                                    @elseif (Auth::user()->step == '2')
                                        <i class="fas fa-user"></i>
                                        Create Exness Account
                                    @elseif (Auth::user()->step == '3')
                                        <i class="fas fa-user"></i>
                                        Copy Strategy
                                    @endif
                                </a>
                                @endif
                            @endif
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="row">
                <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                    <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
                    </symbol>
                    <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
                        <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z" />
                    </symbol>
                    <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                    </symbol>
                </svg>
                @if (Auth::user()->is_admin != 1)
                    <div class="col-sm-12 col-md-12">
                        @if (Auth::user()->is_clientid_valid != 1 && (Auth::user()->client_id != NULL || Auth::user()->client_id != ''))
                            <div class="alert alert-custom alert-bottom alert-danger d-flex align-items-center text-wrap" role="alert">
                                <svg class="bi flex-shrink-0 me-2" width="24" height="24">
                                    <use xlink:href="#exclamation-triangle-fill" />
                                </svg>
                                <div style="font-size: .8rem">
                                    @if (Auth::user()->is_clientid_valid == 0)
                                        Your Client ID is being verified (1x24).
                                    @elseif (Auth::user()->is_clientid_valid == 2)
                                        Your Client ID is invalid. Contact your leader to make sure your Client ID is under 9307325.
                                    @endif
                                </div>
                            </div>
                        @elseif (Auth::user()->check_npwp == 3 )
                            <div class="alert alert-custom alert-bottom alert-danger d-flex align-items-center text-wrap" role="alert">
                                <svg class="bi flex-shrink-0 me-2" width="24" height="24">
                                    <use xlink:href="#exclamation-triangle-fill" />
                                </svg>
                                <div style="font-size: .8rem">
                                    Your NPWP invalid. Please change in Edit Profile.
                                </div>
                            </div>
                        @else
                            @if ($rankRequest != null)
                                @if ($rankRequest->status == 'PENDING')
                                    <div class="alert alert-custom alert-bottom alert-primary d-flex align-items-center text-wrap" role="alert">
                                        <svg class="bi flex-shrink-0 me-2" width="24" height="24">
                                            <use xlink:href="#info-fill" />
                                        </svg>
                                        <div style="font-size: .8rem">
                                            {{ $rankRequest->message }}
                                        </div>
                                    </div>
                                @elseif (($rankRequest->status == 'ACCEPTED' || $rankRequest->status == 'DOWNGRADE'))
                                    @if ($rankRequest->is_dismissed != '1')
                                        <div class="alert alert-custom alert-bottom alert-primary d-flex align-items-center alert-dismissible fade show text-wrap" role="alert">
                                            <svg class="bi flex-shrink-0 me-2" width="24" height="24">
                                                <use xlink:href="#info-fill" />
                                            </svg>
                                            <div style="font-size: .8rem">
                                                {!! $rankRequest->message !!}
                                            </div>
                                            <button wire:click="dismissMessage({{ $rankRequest->id }})" type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close" style="margin-top: -0.6rem;"></button>
                                        </div>
                                    @elseif ($rankRequest->status == 'DOWNGRADE' && $rankRequest->is_dismissed == '1' && Auth::user()->request_id != null && Auth::user()->seminar_id != null && Auth::user()->seminar_events->status != 'EXPIRED')
                                        <div class="alert alert-custom alert-bottom alert-primary d-flex align-items-center text-wrap" role="alert">
                                            <svg class="bi flex-shrink-0 me-2" width="24" height="24">
                                                <use xlink:href="#info-fill" />
                                            </svg>
                                            <div style="font-size: .8rem">
                                                {{ 'Webinar reminder: ' . date_format(date_create(Auth::user()->seminar_events->event_date), 'D, d M Y') . ', ' . Auth::user()->seminar_events->event_time}}. URL: <a href="' . Auth::user()->seminar_events->url .'"><strong> {{Auth::user()->seminar_events->url}}</strong>. Passcode: {{Auth::user()->seminar_events->passcode}} </strong></a>
                                            </div>
                                        </div>
                                    @endif
                                @elseif ($rankRequest->status == 'REJECTED')
                                    <div class="alert alert-custom alert-bottom alert-danger d-flex align-items-center text-wrap" role="alert">
                                        <svg class="bi flex-shrink-0 me-2" width="24" height="24">
                                            <use xlink:href="#exclamation-triangle-fill" />
                                        </svg>
                                        <div style="font-size: .8rem">
                                            {{ $rankRequest->message }}
                                        </div>
                                    </div>
                                @endif
                            @elseif ($canRequestUpgrade)
                                <div class="alert alert-custom alert-bottom alert-primary d-flex align-items-center text-wrap" role="alert">
                                    <svg class="bi flex-shrink-0 me-2" width="24" height="24">
                                        <use xlink:href="#info-fill" />
                                    </svg>
                                    <div style="font-size: .8rem">
                                        All tasks completed. You can request rank upgrade on the <strong>Partnership</strong> menu.
                                    </div>
                                </div>
                            @endif
                        @endif     
                    </div>
                @endif
            </div>
        </div>
        <div class="iq-header-img" style="height: 16.5rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="conatiner-fluid content-inner content-inner-custom mt-n5 py-0">
        <div wire:ignore class="row">
            <div class="col-md-12 col-lg-12">
                <div class="row row-cols-1">
                    <div class="d-slider1 overflow-hidden ">
                        <ul class="swiper-wrapper list-inline m-0 p-0 mb-2">
                            @if (Auth::user()->is_admin == 1)
                                @if ($role == 'ADM')
                                    <li class="swiper-slide card card-slide" data-aos="fade-up" data-aos-delay="700">
                                        <div class="card-body">
                                            <div role="button" onclick="gotoTraderValidationPage()" class="progress-widget">
                                                <svg width="60" height="60" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="users" class="svg-inline--fa fa-users" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M319.9 320c57.41 0 103.1-46.56 103.1-104c0-57.44-46.54-104-103.1-104c-57.41 0-103.1 46.56-103.1 104C215.9 273.4 262.5 320 319.9 320zM369.9 352H270.1C191.6 352 128 411.7 128 485.3C128 500.1 140.7 512 156.4 512h327.2C499.3 512 512 500.1 512 485.3C512 411.7 448.4 352 369.9 352zM512 160c44.18 0 80-35.82 80-80S556.2 0 512 0c-44.18 0-80 35.82-80 80S467.8 160 512 160zM183.9 216c0-5.449 .9824-10.63 1.609-15.91C174.6 194.1 162.6 192 149.9 192H88.08C39.44 192 0 233.8 0 285.3C0 295.6 7.887 304 17.62 304h199.5C196.7 280.2 183.9 249.7 183.9 216zM128 160c44.18 0 80-35.82 80-80S172.2 0 128 0C83.82 0 48 35.82 48 80S83.82 160 128 160zM551.9 192h-61.84c-12.8 0-24.88 3.037-35.86 8.24C454.8 205.5 455.8 210.6 455.8 216c0 33.71-12.78 64.21-33.16 88h199.7C632.1 304 640 295.6 640 285.3C640 233.8 600.6 192 551.9 192z"></path></svg>
                                                <div class="progress-detail">
                                                    <p class="mb-2">Not Verified</p>
                                                    <h4 class="counter">{{ $unverifiedTraderCount }}</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endif
                            @else
                                <li class="swiper-slide card card-slide" data-aos="fade-up" data-aos-delay="700">
                                    <div class="card-body">
                                        <div class="progress-widget">
                                            <i class="fas fa-wallet" style="font-size: xxx-large;padding-left: 10%;"></i>
                                            <div class="progress-detail">
                                                <p class="mb-2">Commission</p>
                                                <h4 class="counter">${{ Auth::user()->commission_balance == '' ? 0 : Auth::user()->commission_balance }}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="swiper-slide card card-slide" data-aos="fade-up" data-aos-delay="700">
                                    <div class="card-body">
                                        <div class="progress-widget">
                                            <i class="fas fa-dollar-sign" style="font-size: xxx-large;padding-left: 10%;"></i>
                                            <div class="progress-detail">
                                                <p class="mb-2">Investment</p>
                                                <h4 class="counter">${{ (Auth::user()->deposit_balance == '' ? 0 : Auth::user()->deposit_balance) - (Auth::user()->withdraw_balance == '' ? 0 : Auth::user()->withdraw_balance) }}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="swiper-slide card card-slide" data-aos="fade-up" data-aos-delay="900">
                                    <div class="card-body">
                                        <div class="progress-widget">
                                            <i class="fas fa-exchange-alt" style="font-size: xxx-large;padding-left: 10%;"></i>
                                            <div class="progress-detail">
                                                <p class="mb-2">Settled</p>
                                                <h4 class="counter">${{ Auth::user()->settled_balance == '' ? 0 : Auth::user()->settled_balance }}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        </ul>
                        <div class="swiper-button swiper-button-next"></div>
                        <div class="swiper-button swiper-button-prev"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12">
            @if (Auth::user()->is_admin != 1)
                @if (Auth::user()->step == '1')
                    <div class="card card-custom">
                        <div class="card-body">
                            <div class="row mb-3">
                                <h4>Step 1 - KYC Verification</h4>
                            </div>
                            @if (Auth::user()->kyc_verify_msg != 'PROCESS' && Auth::user()->kyc_verify_msg != 'VALID')
                                <div class="alert alert-bottom alert-warning w-100 fade show d-flex align-items-center" role="alert">
                                    <svg class="bi flex-shrink-0 me-2" width="24" height="24">
                                        <use xlink:href="#exclamation-triangle-fill" />
                                    </svg>
                                    <div>
                                        {{ Auth::user()->kyc_verify_msg }}
                                    </div>
                                </div>
                            @endif
                            @if (Auth::user()->account_verify_msg != 'PROCESS' && Auth::user()->account_verify_msg != 'VALID')
                                <div class="alert alert-bottom alert-warning w-100 fade show d-flex align-items-center" role="alert">
                                    <svg class="bi flex-shrink-0 me-2" width="24" height="24">
                                        <use xlink:href="#exclamation-triangle-fill" />
                                    </svg>
                                    <div>
                                        {{ Auth::user()->account_verify_msg }}
                                    </div>
                                </div>
                            @endif
                            @if ((Auth::user()->kyc_verify_msg == 'PROCESS' || Auth::user()->kyc_verify_msg == 'VALID') && (Auth::user()->account_verify_msg == 'PROCESS' || Auth::user()->account_verify_msg == 'VALID'))
                                <p>Your KYC identity is being checked.</p>
                            @endif
                        </div>
                    </div>
                @endif
                @if (Auth::user()->step == '2')
                    <div class="card card-custom">
                        <div class="card-body">
                            <div class="row mb-3">
                                <h4>Step 2 - Create Exness Account</h4>
                            </div>
                            <div class="row mb-3">
                                <label for="exnessClientId" class="col-sm-3 col-form-label"><strong>Client ID</strong><span style="color: red"> *</span></label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                    <a href="{{ $ref_exness }}"style="color: #fff700d1;" target="_blank" class="btn btn btn-dark" type="button" id="button-addon1">Create Account</a>
                                        <input type="text" wire:model="clientId" class="form-control" id="exnessClientId" maxlength="8" placeholder="Enter your Client ID">
                                    </div>
                                    @error('clientId')
                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                        <br>
                                    @enderror
                                    <small>Please create Exness account first by clicking the button above to get the client ID.</small>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="downloadApp" class="col-sm-3 col-form-label"><strong>Download Social Trading</strong></label>
                                <div class="col-sm-4">
                                    <a href="https://apps.apple.com/id/app/exness-social-trading/id1392465628" target="_blank">
                                        <img src="{{ asset('assets/images/badges/app-store-badge.svg') }}" alt="App Store">
                                    </a>
                                    <a href="https://play.google.com/store/apps/details?id=com.exness.investments&hl=in&gl=US" target="_blank">
                                        <img src="{{ asset('assets/images/badges/google-play-badge.svg') }}" alt="Google Play">
                                    </a>
                                </div>
                                <div class="col-sm-5"></div>
                            </div>
                            <div class="row mb-3 d-flex justify-content-end">
                                <div class="col-md-2">
                                    <button type="button" wire:click="showModalConfirmation({{ $step }})" wire:loading.attr="disabled" class="btn btn-primary">
                                        <div wire:loading.remove wire:target="showModalConfirmation({{ $step }})">Submit</div>
                                        <div wire:loading wire:target="showModalConfirmation({{ $step }})">
                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                            Loading...
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if (Auth::user()->step == '3')
                    <div class="card card-custom">
                        <div class="card-body">
                            <div class="row mb-3">
                                <h4>Step 3 - Copy Trading Strategy</h4>
                            </div>
                            <p>Please choose one of the strategies bellow. Make sure you have already made deposit to your Social Trading wallet.</p>
                            <div class="row mb-3 d-flex justify-content-start">
                                <label for="strategy" class="col-sm-3 col-form-label"><strong>Strategy</strong></label>
                                <div class="col">
                                    @foreach ($strategies as $item)
                                        <button wire:click="showModalStrategyDetail({{ $item->id }})" wire:loading.attr="disabled" type="button" class="btn {{ $choosenStrategy == $item->id ? 'btn-success' : 'btn-outline-success' }}">{{ $item->net_profit . ' : ' . $item->commission }}</button>
                                    @endforeach
                                    <br>
                                    @error('choosenStrategy')
                                        <small id="helpId" class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                            <p><strong>Note: </strong>You can see or add more strategies at the <strong>List Strategy</strong> menu.</p>
                            <br>
                            <div class="row mb-3 d-flex justify-content-end">
                                <div class="col-md-2">
                                    <button type="button" wire:click="showModalConfirmation({{ $step }})" wire:loading.attr="disabled" class="btn btn-primary">
                                        <div wire:loading.remove wire:target="showModalConfirmation({{ $step }})">Complete</div>
                                        <div wire:loading wire:target="showModalConfirmation({{ $step }})">
                                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                            Loading...
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if (Auth::user()->step == 'OK')
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card card-custom">
                                <div class="card-header bg-light">
                                    <h4>Commission History</h4>
                                </div>
                                <div class="card-body">
                                    {{-- TODO --}}
                                    <div class="table-wrapper-scroll-y my-custom-scrollbar table-responsive">
                                        <table class="table table-sm table-striped">
                                            <thead>
                                                <th style="width: 5%" scope="col">No.</th>
                                                <th style="width: 30%" scope="col">Date</th>
                                                <th style="width: 30%" scope="col">Category</th>
                                                <th style="width: 20%" scope="col">Commission (USD)</th>
                                                <th style="width: 15%" scope="col">Status</th>
                                            </thead>
                                            <tbody>
                                                @if (count($benefitHistory) != 0)
                                                    @foreach ($benefitHistory as $key => $history)
                                                        <tr>
                                                            <th scope="row">{{ $key+1 }}</th>
                                                            <td>{{ $history->created_at }}</td>
                                                            <td>{{ $history->category }}</td>
                                                            <td>${{ $history->amount }}</td>
                                                            @if ($history->status == 'PENDING')
                                                                <td style="color: orange">{{ $history->status }}</td>
                                                            @else
                                                                <td style="color: green">{{ $history->status }}</td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="5">No history found.</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-custom">
                                <div class="card-header bg-light">
                                    <h4>Rank Tasks</h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="mb-2">
                                                            Investment
                                                            </div>
                                                        @if ($percentDeposit == 0)
                                                            <div class="progress bg-soft-secondary shadow-none w-100">
                                                                <div class="progress-bar bg-secondary" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentDeposit }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentDeposit }}%; transition: width 2s ease 0s;">{{ $percentDeposit }}%</div>
                                                            </div>
                                                        @elseif ($percentDeposit < 51)
                                                            <div class="progress bg-soft-danger shadow-none w-100">
                                                                <div class="progress-bar bg-danger" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentDeposit }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentDeposit }}%; transition: width 2s ease 0s;">{{ $percentDeposit }}%</div>
                                                            </div>
                                                        @elseif ($percentDeposit >= 51 && $percentDeposit < 100)
                                                            <div class="progress bg-soft-warning shadow-none w-100">
                                                                <div class="progress-bar bg-warning" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentDeposit }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentDeposit }}%; transition: width 2s ease 0s;">{{ $percentDeposit }}%</div>
                                                            </div>
                                                        @elseif ($percentDeposit >= 100)
                                                            <div class="progress bg-soft-success shadow-none w-100">
                                                                <div class="progress-bar bg-success" data-toggle="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%; transition: width 2s ease 0s;">100%</div>
                                                            </div>
                                                        @endif
                                                        <span style="font-size: .8rem">{{ $totalDeposited . '/' . $targetDeposit }} USD</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="mb-2">
                                                            Downline
                                                        </div>
                                                        @if ($percentDownline == 0)
                                                            <div class="progress bg-soft-secondary shadow-none w-100">
                                                                <div class="progress-bar bg-secondary" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentDownline }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentDownline }}%; transition: width 2s ease 0s;">{{ $percentDownline }}%</div>
                                                            </div>
                                                        @elseif ($percentDownline < 51)
                                                            <div class="progress bg-soft-danger shadow-none w-100">
                                                                <div class="progress-bar bg-danger" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentDownline }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentDownline }}%; transition: width 2s ease 0s;">{{ $percentDownline }}%</div>
                                                            </div>
                                                        @elseif ($percentDownline >= 51 && $percentDownline < 100)
                                                            <div class="progress bg-soft-warning shadow-none w-100">
                                                                <div class="progress-bar bg-warning" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentDownline }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentDownline }}%; transition: width 2s ease 0s;">{{ $percentDownline }}%</div>
                                                            </div>
                                                        @elseif ($percentDownline >= 100)
                                                            <div class="progress bg-soft-success shadow-none w-100">
                                                                <div class="progress-bar bg-success" data-toggle="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%; transition: width 2s ease 0s;">100%</div>
                                                            </div>
                                                        @endif
                                                        <span style="font-size: .8rem">{{ $downline . '/' . $targetDownline }} people</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="mb-2">
                                                            Turnover
                                                        </div>
                                                        @if ($percentTurnover == 0)
                                                            <div class="progress bg-soft-secondary shadow-none w-100">
                                                                <div class="progress-bar bg-secondary" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentTurnover }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentTurnover }}%; transition: width 2s ease 0s;">{{ $percentTurnover }}%</div>
                                                            </div>
                                                        @elseif ($percentTurnover < 51)
                                                            <div class="progress bg-soft-danger shadow-none w-100">
                                                                <div class="progress-bar bg-danger" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentTurnover }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentTurnover }}%; transition: width 2s ease 0s;">{{ $percentTurnover }}%</div>
                                                            </div>
                                                        @elseif ($percentTurnover >= 51 && $percentTurnover < 100)
                                                            <div class="progress bg-soft-warning shadow-none w-100">
                                                                <div class="progress-bar bg-warning" data-toggle="progress-bar" role="progressbar" aria-valuenow="{{ $percentTurnover }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $percentTurnover }}%; transition: width 2s ease 0s;">{{ $percentTurnover }}%</div>
                                                            </div>
                                                        @elseif ($percentTurnover >= 100)
                                                            <div class="progress bg-soft-success shadow-none w-100">
                                                                <div class="progress-bar bg-success" data-toggle="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%; transition: width 2s ease 0s;">100%</div>
                                                            </div>
                                                        @endif
                                                        <span style="font-size: .8rem">{{ $turnover . '/' . $targetTurnover }} USD</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        </div>
    </div>
    @include('livewire.strategy-detail-modal')
    <div id="modal-confirmation" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
        aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header alert-danger">
                    <h4 class="modal-title" id="my-modal-title">Are you sure?</h4>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button>
                    <button type="button" wire:click.prevent="submit({{ $step }})" wire:loading.attr="disabled" class="btn btn-primary">
                        <div wire:loading.remove wire:target="submit({{ $step }})">Yes</div>
                        <div wire:loading wire:target="submit({{ $step }})">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        </div>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script>        
        document.addEventListener('livewire:load', function(e) {
            e.preventDefault()

            window.livewire.on('showModalConfirmation', (data) => {
                $('#modal-confirmation').modal('show')
            });

            window.livewire.on('hideModalConfirmation', (data) => {
                $('#modal-confirmation').modal('hide')
            });

            window.livewire.on('showModalStrategyDetail', (data) => {
                $('#modal-strategy-detail').modal('show')
            });

            window.livewire.on('hideModalStrategyDetail', (data) => {
                $('#modal-strategy-detail').modal('hide')
            });
        })
    </script>
</div>

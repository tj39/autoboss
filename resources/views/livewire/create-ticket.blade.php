<div>
    <div class="iq-navbar-header" style="height: 8.125rem;">
        <div class="container-fluid iq-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <h3>Create Ticket</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-header-img" style="height: 11.25rem;">
            <img src="../assets/images/dashboard/top-header.jpg" alt="header" class="img-fluid w-100 h-100 animated-scaleX">
        </div>
    </div>
    <div class="container-fluid content-inner mt-n5 py-0">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group form-inline {{$errors->has('title') ? 'has-error has-feedback' : '' }}">
                        <label for="title" class="col-form-label" style = "margin-right: 138px">Title</label>                       
                        <input type="text" class="form-control col-md-3" id="title" name = "title" wire:model = "title">
                        <small id="helpId{{'title'}}" class="text-danger">{{ $errors->has('title') ? $errors->first('title') : '' }}</small>
                    </div>
                    <div class="form-group form-inline {{$errors->has('category') ? 'has-error has-feedback' : '' }}">
                        <label for="category" class="placeholder" style = "margin-right: 115px">Category</label>                    
                        <select name="category" wire:model="category" class="form-control col-md-3 " id ="category" >
                            <option value = "">Select Category</option>
                            @foreach ($ticket_category as $items)
                            <option value = "{{ $items->id }}">{{ $items->name }}</option>
                            @endforeach
                        </select>
                        <small id="helpId" class="text-danger">{{ $errors->has('category') ? $errors->first('category') : '' }}</small>
                    </div>
                    <div class="form-group form-inline {{$errors->has('message') ? 'has-error has-feedback' : '' }}">
                        <label for="message" class="col-form-label" style = "margin-right: 115px">Message&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>    
                        <textarea class="form-control" id="message" name = "message" wire:model = "message" rows="10" cols="100">
                        </textarea>
                        <small id="helpId{{'message'}}" class="text-danger">{{ $errors->has('message') ? $errors->first('message') : '' }}</small>
                    </div>
                    <label for="image_url" class="placeholder" style = "margin-right: 115px">Url Image</label>
                    <div class="input-group {{$errors->has('image_url') ? 'has-error has-feedback' : '' }}">
                        <div class="input-group-prepend">
                            <a class="btn btn-primary" type="button" href="https://imglink.io/"
                                target="_blank">Upload Image Here</a>
                        </div>
                        <input id="image_url" value="" name="image_url" wire:model="image_url"
                            type="text" class="form-control col-md-3" placeholder="Url Image">
                    </div>
                    <br>
                    <div class="form-group">
                    <button class="btn btn-primary" style="float: right" wire:click="showModalConfirmation"
                            wire:loading.attr="disabled">Send</button>
                        <a class="btn btn-secondary"
                        wire:loading.attr="disabled" style="float: right; margin-right: 2%" href="{{ route('user.support.ticket') }}">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-confirm" wire:ignore.self class="modal fade" tabindex="-1" role="dialog"
        aria-labelledby="my-modal-title" aria-hidden="true" class="justify-content-center">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="alert-danger">
                    <h5 class="modal-title" id="my-modal-title">Are you sure?</h5>
                </div>
                <div class="modal-footer justify-content-center">
                    <button id='closeModal' type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-primary" wire:click="store">Yes</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            window.livewire.on('showModalConfirmation', (data) => {
            // console.log(data)
                $('#modal-confirm').modal('show')
            });
        });
    </script>
</div>


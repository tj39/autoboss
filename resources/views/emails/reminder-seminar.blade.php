<!DOCTYPE html>
<html>
<head>
    <title>TP99</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
</head>
<body>
    <p>Hi TPP 99 Member!</p>
    <p>We invite you to attend our zoom training.  The {{ $details['event_name'] }} training will be held on:</p>
    <p>Date : {{ date_format(date_create($details['event_date']), 'D, d M Y') }}</p>
    <p>Time : {{ $details['event_time'] }}</p>
    <p>Url : {{ $details['url'] }}</p>
    <p>Passcode : {{ $details['passcode'] }}</p>
    <p>Save the date and don't miss it!</p>
    <p>Let's success with TP99!</p>
    <p><i class='fas fa-exclamation-circle' style='font-size:48px;color:red'></i>NOTE:** Unfortunately, if you cannot attend the training, your rank will be downgraded automatically a few days after the training day.</p>
    <p>Regards,</p>
    <p>TP 99 Management</p>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>TP99</title>
</head>
<body>
    <p>Hello, {{ $data['fullname'] }}.</p>
    <br>
    <p>{{ $data['message'] }}</p>
    <br>
    <p>Regards,</p>
    <p style="color: #1aa053;"><strong>TP99</strong></p>
</body>
</html>
<!DOCTYPE html>
<html>

<head>
    <title>TP99</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
        integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
</head>

<body>
    <p>Halo management admin!</p>
    <p>You have to ensure the seminar "{{ $details['event_name'] }}" schedule,</p>
    <p>because it will be held on {{ date_format(date_create($details['event_time']), 'D, d M Y') }}.</p>
    <p>Please reschedule before the reminder message for traders on {{ date_format(date_create($details['event_date']), 'D, d M Y') }} if you want to cancel.</p>
</body>

</html>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'TP 99') }}</title>

    <!-- Fonts -->
    {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap"> --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">

    <!-- Styles -->
    {{-- <link rel="stylesheet" href="{{ mix('css/app.css') }}"> --}}
    <link rel="stylesheet" href="{{asset('assets/css/login.css')}}">
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}" />

    <!-- Library / Plugin Css Build -->
    <link rel="stylesheet" href="{{asset('assets/css/core/libs.min.css')}}" />

    <!-- Hope Ui Design System Css -->
    <link rel="stylesheet" href="{{asset('assets/css/hope-ui.min.css?v=1.1.0')}}" />

    <!-- Custom Css -->
    <link rel="stylesheet" href="{{asset('assets/css/custom.min.css?v=1.1.0')}}" />

       <!-- Dark Css -->
       <link rel="stylesheet" href="{{ asset('assets/css/dark.min.css') }}"/>

    <!-- Bootstrap Select -->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/1.1.1/css/bootstrap-multiselect.css" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css">

    <!-- RTL Css -->
    {{-- <link rel="stylesheet" href="{{asset('assets/css/rtl.min.css')}}" /> --}}
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> --}}

    <style>
        :root {
            font-size: smaller;
        }
        /* START overriding CSS for collapsible nav-item */
        .sidebar-default .navbar-nav .nav-item .nav-link:not(.disabled):hover:not(.active):not([aria-expanded=true]) {
            background-color: rgba(26, 160, 83,0.1);
            color: #1aa053;
        }

        .sidebar-default .navbar-nav .nav-item .nav-link:not(.disabled).active, .sidebar-default .navbar-nav .nav-item .nav-link:not(.disabled)[aria-expanded=true] {
            background-color: #1aa053;
            color: #fff;
            -webkit-box-shadow: 0 10px 20px -10px rgb(17 38 146 / 40%);
            box-shadow: 0 10px 20px -10px rgb(17 38 146 / 40%);
        }
        /* END overriding CSS for collapsible nav-item */

        /* START Form divider */
        .text-divider {
            margin: 2em 0; line-height: 0; text-align: center;
        }
        .text-divider span {
            background-color: #fff;
            padding: 1em;
        }
        .text-divider:before { 
            content: " "; 
            display: block; 
            border-top: 2px solid #e3e3e3; 
            border-bottom: 2px solid #f7f7f7;
        }
        /* END Form divider */

        /* START Custom pagination color */
        .page-item.active .page-link {
            background-color: #1aa053;
            border-color: #1aa053;
        }

        .page-link {
            color: #1aa053;
        }
        /* END Custom pagination color */

        .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
            color: #fff;
            background-color: #1aa053;
        }

        .nav-link {
            color: #1aa053;
        }

        .form-check-input {
            border-color: #1aa053;
        }

        .form-check-input:checked {
            border-color: #1aa053;
            color: #1aa053;
        }

        .table-sm>:not(caption)>*>* {
            padding: 0.25rem 1.5rem;
        }

        .sidebar+.main-content .iq-navbar-header .iq-container {
            padding: -webkit-calc(var(--bs-gutter-x, .75rem) * 2);
            padding: calc(var(--bs-gutter-x, .75rem) * 2);
        }

        .content-inner {
            padding: -webkit-calc(var(--bs-gutter-x, .75rem) * 2);
            padding: calc(var(--bs-gutter-x, .75rem) * 2);
        }

        .card-custom {
            margin-top: -1rem;
        }

        .card-slide {
            min-width: 253.2px !important;
        }

        .row {
            --bs-gutter-x: 1rem;
        }

        .alert-custom {
            padding-left: 0.3rem;
            padding-right: 2.2rem;
            padding-top: 0.3rem;
            padding-bottom: 0.3rem;
            max-width: 100%;
            height: min-content;
            height: -moz-min-content;
            height: -webkit-min-content;
        }

        @media (max-width: 767.98px) {
            .conatiner-fluid.content-inner-custom.mt-n5 {
                margin-top: 0.8rem !important; 
            }
            .row-custom {
                margin-bottom: 0.5rem !important;
            }
        }

        .p-1-custom {
            padding-left: 2.5rem !important;
            padding-right: 2.5rem !important;
        }

        .card-body {
            overflow: auto;
        }

        .footer {
            position: relative; 
        }

        @media only screen and (max-width: 800px) {
            .d-md-block {
                display: block !important;
            }
        }

        @media only screen and (max-width: 280px) {
            .d-md-block {
                font-family: auto;
            }
        }

        .selectBox {
            position: relative;
        }

        .selectBox select {
            width: 100%;
            font-weight: bold;
        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }

        #checkboxes {
            display: none;
            border: 1px #dadada solid;
        }

        #checkboxes label {
            display: block;
            margin-left: 10%;
            margin-top: -9%
        }

        .profile::placeholder {
            color: #8a92a64d;
        }
        
        .accordion-heading[aria-expanded='true'] > .question-header {
            -webkit-transition: -webkit-transform .5s ease-in-out;
            -ms-transition: -ms-transform .5s ease-in-out;
            transition: transform .5s ease-in-out;
            transform: rotate(-180deg);
        }

        .accordion-heading[aria-expanded='false'] > .question-header {
            -webkit-transition: -webkit-transform .5s ease-in-out;
            -ms-transition: -ms-transform .5s ease-in-out;
            transition: transform .5s ease-in-out;
            transform: rotate(0deg);
        }
    </style>

    @livewireStyles
    @stack('styles')
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
</head>

<body>
    <!-- loader Start -->
    <div id="loading">
        <div class="loader simple-loader">
            <div class="loader-body"></div>
        </div>
    </div>
    <!-- loader END -->
    <aside class="sidebar sidebar-default navs-rounded-all ">
        <div class="sidebar-header d-flex align-items-center justify-content-start">
            @php
                use App\Providers\RouteServiceProvider;
                if (Auth::user()->is_admin == 1) {
                    $route = RouteServiceProvider::ADMIN_HOME;
                } else {
                    $route = RouteServiceProvider::HOME;
                }
            @endphp
            <a href="{{ $route }}" class="navbar-brand">
                <!--Logo start-->
                <img width="30" src="{{ asset('assets/images/logo-small.png') }}">
                <!--logo End-->
                <h4 class="logo-title">TP 99</h4>
            </a>
            <div class="sidebar-toggle bg-success" data-toggle="sidebar" data-active="true">
                <i class="icon">
                    <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M4.25 12.2744L19.25 12.2744" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                        <path d="M10.2998 18.2988L4.2498 12.2748L10.2998 6.24976" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
                    </svg>
                </i>
            </div>
        </div>
        <div class="sidebar-body pt-0 data-scrollbar">
            <div class="sidebar-list">
                <!-- Sidebar Menu Start -->
                <ul class="navbar-nav iq-main-menu" id="sidebar-menu">
                    <li class="nav-item static-item">
                        <a class="nav-link static-item disabled" href="#" tabindex="-1">
                            <span class="default-icon">Home</span>
                            <span class="mini-icon">-</span>
                        </a>
                    </li>
                    @if (Auth::user()->is_admin == 1)
                        @php
                            $role = App\Models\AdminRole::where('id', Auth::user()->role_id)->first();
                        @endphp
                        @if ($role->code == 'ADM')
                        <li class="nav-item">
                            <a class="nav-link {{ request()->routeIs('admin.dashboard') ? 'active bg-success' : '' }}" aria-current="page" href="{{ route('admin.dashboard') }}">
                                <i class="icon">
                                    <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.4"
                                            d="M16.0756 2H19.4616C20.8639 2 22.0001 3.14585 22.0001 4.55996V7.97452C22.0001 9.38864 20.8639 10.5345 19.4616 10.5345H16.0756C14.6734 10.5345 13.5371 9.38864 13.5371 7.97452V4.55996C13.5371 3.14585 14.6734 2 16.0756 2Z"
                                            fill="currentColor"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M4.53852 2H7.92449C9.32676 2 10.463 3.14585 10.463 4.55996V7.97452C10.463 9.38864 9.32676 10.5345 7.92449 10.5345H4.53852C3.13626 10.5345 2 9.38864 2 7.97452V4.55996C2 3.14585 3.13626 2 4.53852 2ZM4.53852 13.4655H7.92449C9.32676 13.4655 10.463 14.6114 10.463 16.0255V19.44C10.463 20.8532 9.32676 22 7.92449 22H4.53852C3.13626 22 2 20.8532 2 19.44V16.0255C2 14.6114 3.13626 13.4655 4.53852 13.4655ZM19.4615 13.4655H16.0755C14.6732 13.4655 13.537 14.6114 13.537 16.0255V19.44C13.537 20.8532 14.6732 22 16.0755 22H19.4615C20.8637 22 22 20.8532 22 19.44V16.0255C22 14.6114 20.8637 13.4655 19.4615 13.4655Z"
                                            fill="currentColor"></path>
                                    </svg>
                                </i>
                                <span class="item-name">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->routeIs('admin.tradervalidation') ? ' active bg-success' : '' }}" aria-current="page" href="{{ route('admin.tradervalidation') }}">
                                <i class="icon">
                                    <svg width="20" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="address-card" class="svg-inline--fa fa-address-card" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M512 32H64C28.65 32 0 60.65 0 96v320c0 35.35 28.65 64 64 64h448c35.35 0 64-28.65 64-64V96C576 60.65 547.3 32 512 32zM176 128c35.35 0 64 28.65 64 64s-28.65 64-64 64s-64-28.65-64-64S140.7 128 176 128zM272 384h-192C71.16 384 64 376.8 64 368C64 323.8 99.82 288 144 288h64c44.18 0 80 35.82 80 80C288 376.8 280.8 384 272 384zM496 320h-128C359.2 320 352 312.8 352 304S359.2 288 368 288h128C504.8 288 512 295.2 512 304S504.8 320 496 320zM496 256h-128C359.2 256 352 248.8 352 240S359.2 224 368 224h128C504.8 224 512 231.2 512 240S504.8 256 496 256zM496 192h-128C359.2 192 352 184.8 352 176S359.2 160 368 160h128C504.8 160 512 167.2 512 176S504.8 192 496 192z"></path></svg>
                                </i>
                                <span class="item-name">Trader Validation</span>
                            </a>
                        </li>
                        @endif
                        @if ($role->code == 'FIN')
                        <!-- <li class="nav-item">
                            <a class="nav-link align-items-center" data-bs-toggle="collapse" href="#horizontal-menu" role="button" aria-expanded="false" aria-controls="horizontal-menu">
                                <i class="icon fas fa-solid fa-money-bill"></i>
                                <span class="item-name">Transactions</span>
                                <i class="right-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
                                    </svg>
                                </i>
                            </a>
                            <ul class="sub-nav collapse" id="horizontal-menu" data-bs-parent="#sidebar-menu">
                                <li class="nav-item">
                                    <a class="nav-link {{ request()->routeIs('admin.confirmwithdraw') ? 'active bg-success' : '' }}" href="{{ route('admin.confirmwithdraw') }}">
                                        <i class="icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="10" viewBox="0 0 24 24" fill="currentColor">
                                                <g>
                                                <circle cx="12" cy="12" r="8" fill="currentColor"></circle>
                                                </g>
                                            </svg>
                                        </i>
                                      <i class="sidenav-mini-icon"> W </i>
                                      <span class="item-name"> Withdraw Confirmation </span>
                                    </a>
                                </li>
                            </ul>
                        </li> -->
                        <li class="nav-item align-items-center">
                            <a class="nav-link align-items-center {{ request()->routeIs('admin.uploadcommission') ? ' active bg-success' : '' }}" aria-current="page" href="{{ route('admin.uploadcommission') }}">
                                <i class="icon fas fa-file-invoice-dollar" style="margin-left: 4px; margin-right: 4px;"></i>
                                <span class="item-name">Upload Commission</span>
                            </a>
                        </li>
                        <li class="nav-item align-items-center">
                            <a class="nav-link align-items-center {{ request()->routeIs('admin.commissiontransfer') ? ' active bg-success' : '' }}" aria-current="page" href="{{ route('admin.commissiontransfer') }}">
                                <i class="icon fas fa-file-invoice-dollar" style="margin-left: 4px; margin-right: 4px;"></i>
                                <span class="item-name">Commission Transfer</span>
                            </a>
                        </li>
                        <li class="nav-item align-items-center">
                            <a class="nav-link align-items-center {{ request()->routeIs('admin.seminarinvoice') ? ' active bg-success' : '' }}" aria-current="page" href="{{ route('admin.seminarinvoice') }}">
                                <i class="icon fas fa-receipt" style="margin-left: 4px; margin-right: 4px;"></i>
                                <span class="item-name">Traning Invoice</span>
                            </a>
                        </li>
                        <li class="nav-item align-items-center">
                            <a class="nav-link align-items-center {{ request()->routeIs('admin.uploadtaxslip') ? ' active bg-success' : '' }}" aria-current="page" href="{{ route('admin.uploadtaxslip') }}">
                                <i class="icon fas fa-receipt" style="margin-left: 4px; margin-right: 4px;"></i>
                                <span class="item-name">Upload Tax Slip</span>
                            </a>
                        </li>	
                        @endif
                        @if ($role->code == 'CS')
                        @php
                            $comment_admin_count = App\Models\ActiveComment::where('is_read_admin', 0)->whereNotIn('status_id', [2, 3])->count();
                            $ticket_admin_count = App\Models\ActiveTicket::where('active_tickets.is_read_admin', 0)
                                ->whereNotIn('active_tickets.status_id', [2, 3])
                                ->where('id', 'not in', function ($query) {
                                    $query->selectRaw('ticket_id')->from('active_comments');
                                })
                                ->count();
                        @endphp
                        <li class="nav-item">
                            <a class="nav-link align-items-center {{ request()->routeIs('admin.support.ticket') || request()->routeIs('admin.detail.ticket') ? 'active bg-success' : '' }}" aria-current="page" href="{{ route('admin.support.ticket', ['table' => 'all']) }}">
                                <i class="fas fa-ticket-alt"></i>
                                <span class="item-name">Support Ticket</span>
                                @if ($comment_admin_count + $ticket_admin_count > 0)
                                <span style="padding: 3%;" class="badge rounded-pill bg-danger">{{ $comment_admin_count + $ticket_admin_count }}</span>
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link align-items-center {{ request()->routeIs('admin.clientidlist') ? ' active bg-success' : '' }}" aria-current="page" href="{{ route('admin.clientidlist') }}">
                                <i class="icon far fa-list-alt" style="margin-right: 2px;"></i>
                                <span class="item-name">Client ID List</span>
                            </a>
                        </li>
                        @if (Auth::user()->username == 'adminayu')
                                <li class="nav-item">
                                    <a class="nav-link align-items-center {{ request()->routeIs('admin.edituserdata') ? ' active bg-success' : '' }}" aria-current="page" href="{{ route('admin.edituserdata') }}">
                                        <i class="icon fas fa-user-edit"></i>
                                        <span class="item-name">User Data</span>
                                    </a>
                                </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link align-items-center {{ request()->routeIs('admin.faqlist') ? ' active bg-success' : '' }}" aria-current="page" href="{{ route('admin.faqlist') }}">
                                <i class="icon fas fa-question-circle" style="margin-right: 2px;"></i>
                                <span class="item-name">FAQ List</span>
                            </a>
                        </li>
                        @endif
                        @if ($role->code == 'MGT')
                        <li class="nav-item">
                            <a class="nav-link align-items-center {{ request()->routeIs('admin.clientvalidation') ? 'active bg-success' : '' }}" aria-current="page" href="{{ route('admin.clientvalidation') }}">
                                <i class="icon fas fa-user-check" style="margin-right: -1.5px;"></i>
                                <span class="item-name">Client Validation</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link align-items-center {{ request()->routeIs('admin.rankapproval') ? 'active bg-success' : '' }}" aria-current="page" href="{{ route('admin.rankapproval') }}">
                                <i class="icon fas fa-trophy"></i>
                                <span class="item-name">Rank Approval</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link align-items-center {{ request()->routeIs('admin.clientidlist') ? ' active bg-success' : '' }}" aria-current="page" href="{{ route('admin.clientidlist') }}">
                                <i class="icon fas fa-list-alt" style="margin-right: 2px;"></i>
                                <span class="item-name">Client ID List</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link align-items-center {{ request()->routeIs('admin.seminarlist') ? ' active bg-success' : '' }}" aria-current="page" href="{{ route('admin.seminarlist') }}">
                                <i class="icon fas fa-calendar-week" style="margin-left: 0.5px; margin-right: 3px;"></i>
                                <span class="item-name">Seminar List</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link align-items-center {{ request()->routeIs('admin.certificatedata') ? ' active bg-success' : '' }}" aria-current="page" href="{{ route('admin.certificatedata') }}">
                                <i class="icon fas fa-certificate" style="margin-right: 2.5px;"></i>
                                <span class="item-name">Certificate Data</span>
                            </a>
                        </li>
                        @endif
                    @else
                        <li class="nav-item">
                            <a class="nav-link {{ request()->routeIs('dashboard') ? 'active bg-success' : '' }}" aria-current="page" href="{{ route('dashboard') }}">
                                <i class="icon">
                                    <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.4"
                                            d="M16.0756 2H19.4616C20.8639 2 22.0001 3.14585 22.0001 4.55996V7.97452C22.0001 9.38864 20.8639 10.5345 19.4616 10.5345H16.0756C14.6734 10.5345 13.5371 9.38864 13.5371 7.97452V4.55996C13.5371 3.14585 14.6734 2 16.0756 2Z"
                                            fill="currentColor"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M4.53852 2H7.92449C9.32676 2 10.463 3.14585 10.463 4.55996V7.97452C10.463 9.38864 9.32676 10.5345 7.92449 10.5345H4.53852C3.13626 10.5345 2 9.38864 2 7.97452V4.55996C2 3.14585 3.13626 2 4.53852 2ZM4.53852 13.4655H7.92449C9.32676 13.4655 10.463 14.6114 10.463 16.0255V19.44C10.463 20.8532 9.32676 22 7.92449 22H4.53852C3.13626 22 2 20.8532 2 19.44V16.0255C2 14.6114 3.13626 13.4655 4.53852 13.4655ZM19.4615 13.4655H16.0755C14.6732 13.4655 13.537 14.6114 13.537 16.0255V19.44C13.537 20.8532 14.6732 22 16.0755 22H19.4615C20.8637 22 22 20.8532 22 19.44V16.0255C22 14.6114 20.8637 13.4655 19.4615 13.4655Z"
                                            fill="currentColor"></path>
                                    </svg>
                                </i>
                                <span class="item-name">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-bs-toggle="collapse" href="#horizontal-menu" role="button" aria-expanded="false" aria-controls="horizontal-menu">
                                <i class="icon">
                                    <svg width="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.4" d="M10.0833 15.958H3.50777C2.67555 15.958 2 16.6217 2 17.4393C2 18.2559 2.67555 18.9207 3.50777 18.9207H10.0833C10.9155 18.9207 11.5911 18.2559 11.5911 17.4393C11.5911 16.6217 10.9155 15.958 10.0833 15.958Z" fill="currentColor"></path>
                                        <path opacity="0.4" d="M22.0001 6.37867C22.0001 5.56214 21.3246 4.89844 20.4934 4.89844H13.9179C13.0857 4.89844 12.4102 5.56214 12.4102 6.37867C12.4102 7.1963 13.0857 7.86 13.9179 7.86H20.4934C21.3246 7.86 22.0001 7.1963 22.0001 6.37867Z" fill="currentColor"></path>
                                        <path d="M8.87774 6.37856C8.87774 8.24523 7.33886 9.75821 5.43887 9.75821C3.53999 9.75821 2 8.24523 2 6.37856C2 4.51298 3.53999 3 5.43887 3C7.33886 3 8.87774 4.51298 8.87774 6.37856Z" fill="currentColor"></path>
                                        <path d="M21.9998 17.3992C21.9998 19.2648 20.4609 20.7777 18.5609 20.7777C16.6621 20.7777 15.1221 19.2648 15.1221 17.3992C15.1221 15.5325 16.6621 14.0195 18.5609 14.0195C20.4609 14.0195 21.9998 15.5325 21.9998 17.3992Z" fill="currentColor"></path>
                                    </svg>
                                </i>
                                <span class="item-name">User Menu</span>
                                <i class="right-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
                                    </svg>
                                </i>
                            </a>
                            <ul class="sub-nav collapse" id="horizontal-menu" data-bs-parent="#sidebar-menu">
                                <li class="nav-item">
                                    <a class="nav-link {{ request()->routeIs('user.editprofile') ? 'active bg-success' : '' }}" href="{{ route('user.editprofile') }}">
                                      <i class="icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="10" viewBox="0 0 24 24" fill="currentColor">
                                                <g>
                                                <circle cx="12" cy="12" r="8" fill="currentColor"></circle>
                                                </g>
                                            </svg>
                                        </i>
                                      <i class="sidenav-mini-icon"> C </i>
                                      <span class="item-name"> Edit Profile </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ request()->routeIs('user.changepassword') ? 'active bg-success' : '' }}" href="{{ route('user.changepassword') }}">
                                      <i class="icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="10" viewBox="0 0 24 24" fill="currentColor">
                                                <g>
                                                <circle cx="12" cy="12" r="8" fill="currentColor"></circle>
                                                </g>
                                            </svg>
                                        </i>
                                      <i class="sidenav-mini-icon"> C </i>
                                      <span class="item-name"> Edit Password </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link align-items-center" data-bs-toggle="collapse" href="#horizontal-menu-1" role="button" aria-expanded="false" aria-controls="horizontal-menu">
                                <i class="icon fas fa-solid fa-money-bill"></i>
                                <span class="item-name">Transactions</span>
                                <i class="right-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
                                    </svg>
                                </i>
                            </a>
                            <ul class="sub-nav collapse" id="horizontal-menu-1" data-bs-parent="#sidebar-menu">
                                <li class="nav-item">
                                    <a class="nav-link {{ request()->routeIs('user.withdraw') ? 'active bg-success' : '' }}" href="{{ route('user.withdraw') }}">
                                        <i class="icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="10" viewBox="0 0 24 24" fill="currentColor">
                                                <g>
                                                <circle cx="12" cy="12" r="8" fill="currentColor"></circle>
                                                </g>
                                            </svg>
                                        </i>
                                        <i class="sidenav-mini-icon"> W </i>
                                        <span class="item-name"> Withdraw </span>
                                    </a>
                                </li>
                            </ul>
                        </li> -->
                        <li class="nav-item">
                            <a class="nav-link align-items-center {{ request()->routeIs('user.liststrategy') ? 'active bg-success' : '' }}" aria-current="page" href="{{ route('user.liststrategy') }}">
                                <i class="icon fas fa-chart-line" style="margin-left: 2.5px; margin-right: 4px;"></i>
                                <span class="item-name">List Strategy</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link align-items-center {{ request()->routeIs('user.commission.history') ? 'active bg-success' : '' }}" aria-current="page" href="{{ route('user.commission.history') }}">
                                <i class="fas fa-history" style="margin-left: 2.5px; margin-right: 4px;"></i>
                                <span class="item-name">Commission History</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link align-items-center {{ request()->routeIs('user.partnership') ? 'active bg-success' : '' }}" aria-current="page" href="{{ route('user.partnership') }}">
                                <i class="icon fas fa-handshake" style="margin-left: 0.5px; margin-right: 3.5px;"></i>
                                <span class="item-name">Partnership</span>
                            </a>
                        </li>
                        <li class="nav-item align-items-center">
                            <a class="nav-link align-items-center {{ request()->routeIs('user.withholdingtaxslip') ? ' active bg-success' : '' }}" aria-current="page" href="{{ route('user.withholdingtaxslip') }}">
                                <i class="icon fas fa-receipt" style="margin-left: 4px; margin-right: 4px;"></i>
                                <span class="item-name">Withholding Tax Slip</span>
                            </a>
                        </li>
                        @php
                            $comment_user_count = App\Models\ActiveTicket::join('active_comments', 'active_comments.ticket_id', '=', 'active_tickets.id')
                                                ->where('active_tickets.user_id', Auth::user()->id)
                                                ->where('active_comments.is_read_user', 0)
                                                ->count();
                        @endphp
                        <li class="nav-item">
                            <a class="nav-link align-items-center {{ request()->routeIs('user.support.ticket') || request()->routeIs('user.create.ticket') || request()->routeIs('user.detail.ticket') ? 'active bg-success' : '' }}" aria-current="page" href="{{ route('user.support.ticket') }}">
                                <i class="fas fa-ticket-alt" style="margin-left: 1.5px; margin-right: 3.5px;"></i>
                                <span class="item-name">Support Ticket</span>
                                @if ($comment_user_count > 0)
                                <span style="padding: 3%;" class="badge rounded-pill bg-danger">{{ $comment_user_count }}</span>
                                @endif
                            </a>
                        </li>
                    @endif
                </ul>
                <!-- Sidebar Menu End -->
            </div>
        </div>
        <div class="sidebar-footer"></div>
    </aside>
    <!-- Page Content -->
    <main class="main-content">
        <div class="position-relative">
            <!--Nav Start-->
            <nav class="nav navbar navbar-expand-lg navbar-light iq-navbar">
                <div class="container-fluid navbar-inner">
                    <a href="{{ route('dashboard') }}" class="navbar-brand">
                        <!--Logo start-->
                        <img width="30" src="{{ asset('assets/images/logo-small.png') }}">
                        <!--logo End-->
                        <h4 class="logo-title">TP 99</h4>
                    </a>
                    <div class="sidebar-toggle bg-success" data-toggle="sidebar" data-active="true">
                        <i class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
                            </svg>
                        </i>
                    </div>
                    <ul class="navbar-nav ms-auto align-items-center navbar-list mb-2 mb-lg-0">                               
                        <li class="nav-item dropdown" style="margin-top: 0.3rem;">
                            <a class="nav-link py-0 d-flex align-items-center" href="#" id="navbarDropdown"
                                role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <img src="{{ asset('storage/'.Auth::user()->avatar) }}" alt="User-Profile"
                                    class="img-fluid avatar avatar-50 avatar-rounded"
                                    onerror="this.onerror=null; this.src='{{ asset('assets/images/avatars/01.png') }}'">
                                <div class="caption ms-3 d-none d-md-block ">
                                    <h6 class="mb-0 caption-title">{{ Auth::user()->firstname }}</h6>
                                    @php
                                        if (Auth::user()->is_admin == 1) {
                                            $role = App\Models\AdminRole::where('id', Auth::user()->role_id)->first();
                                        } else {
                                            $role = App\Models\TraderRank::where('id', Auth::user()->role_id)->first();
                                        }
                                    @endphp
                                    <p class="mb-0 caption-sub-title">{{ $role->description }}</p>
                                </div>                              
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                <li><form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <a class="dropdown-item" id="notifDropdown" title="Keluar" href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); this.closest('form').submit();">
                                        Logout
                                    </a>
                                </form></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav> <!-- Nav Header Component Start -->
            {{ $slot }}
            @if (Auth::user()->is_admin != '1')
                <div id="modal-disclaimer" wire:ignore.self class="modal fade" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Disclaimer</h5>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <div class="row mb-3">
                                        <div class="container-fluid">
                                            <zero-md src="{{ asset('assets/markdowns/disclaimer-en.md') }}"></zero-md>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div>
                                    <form method="POST" action="{{ route('logout') }}">
                                        @csrf
                                        <a class="btn btn-danger" href="{{ route('logout') }}"
                                            onclick="event.preventDefault(); sessionStorage.setItem('disclaimer', '0'); this.closest('form').submit();">
                                            Cancel
                                        </a>
                                    </form>
                                </div>
                                <button type="button" onclick="closeDisclaimer()" class="btn btn-primary">
                                    Agree
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <!-- Footer Section Start -->
        <footer class="footer">
            <div class="footer-body">
                <ul class="left-panel list-inline mb-0 p-0">
                    <li class="list-inline-item"><a href="#">Privacy Policy</a></li>
                    <li class="list-inline-item"><a href="#">Terms of Use</a></li>
                </ul>
                <div class="right-panel text-secondary">
                    ©<script>document.write(new Date().getFullYear())</script> TP 99, crafted with
                    <span class="text-gray">
                        <svg width="15" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M15.85 2.50065C16.481 2.50065 17.111 2.58965 17.71 2.79065C21.401 3.99065 22.731 8.04065 21.62 11.5806C20.99 13.3896 19.96 15.0406 18.611 16.3896C16.68 18.2596 14.561 19.9196 12.28 21.3496L12.03 21.5006L11.77 21.3396C9.48102 19.9196 7.35002 18.2596 5.40102 16.3796C4.06102 15.0306 3.03002 13.3896 2.39002 11.5806C1.26002 8.04065 2.59002 3.99065 6.32102 2.76965C6.61102 2.66965 6.91002 2.59965 7.21002 2.56065H7.33002C7.61102 2.51965 7.89002 2.50065 8.17002 2.50065H8.28002C8.91002 2.51965 9.52002 2.62965 10.111 2.83065H10.17C10.21 2.84965 10.24 2.87065 10.26 2.88965C10.481 2.96065 10.69 3.04065 10.89 3.15065L11.27 3.32065C11.3618 3.36962 11.4649 3.44445 11.554 3.50912C11.6104 3.55009 11.6612 3.58699 11.7 3.61065C11.7163 3.62028 11.7329 3.62996 11.7496 3.63972C11.8354 3.68977 11.9247 3.74191 12 3.79965C13.111 2.95065 14.46 2.49065 15.85 2.50065ZM18.51 9.70065C18.92 9.68965 19.27 9.36065 19.3 8.93965V8.82065C19.33 7.41965 18.481 6.15065 17.19 5.66065C16.78 5.51965 16.33 5.74065 16.18 6.16065C16.04 6.58065 16.26 7.04065 16.68 7.18965C17.321 7.42965 17.75 8.06065 17.75 8.75965V8.79065C17.731 9.01965 17.8 9.24065 17.94 9.41065C18.08 9.58065 18.29 9.67965 18.51 9.70065Z" fill="currentColor"></path>
                        </svg>
                    </span> by <a href="https://koprener.com">TJL</a>.
                </div>
            </div>
        </footer>
        <!-- Footer Section End -->
    </main>

    <!-- Library Bundle Script -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="{{asset('assets/js/core/libs.min.js')}}"></script>
    <script src="{{asset('assets/js/core/external.min.js')}}"></script>
    <script src="{{asset('assets/js/charts/dashboard.js')}}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="{{asset('assets/js/hope-ui.js')}}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/1.1.1/js/bootstrap-multiselect.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js"></script>
    <script src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
    <script src="https://cdn.jsdelivr.net/npm/pikaday/pikaday.js"></script>
    <script type="module" src="https://cdn.jsdelivr.net/gh/zerodevx/zero-md@2/dist/zero-md.min.js"></script>

    {{-- <script src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script> --}}
    @stack('scripts')
    <script>
        $( document ).ready(function() {
            // Tooltip initialization
            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                return new bootstrap.Tooltip(tooltipTriggerEl)
            })
            // End tooltip initialization

            $(".modal").on("hidden.bs.modal", function () {
                tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            })

            $(".modal").on("shown.bs.modal", function () {
                tooltipList.forEach(element => {
                    element.hide()
                })
            })

            if ((sessionStorage && !sessionStorage.getItem('disclaimer')) || (sessionStorage && sessionStorage.getItem('disclaimer') === '0')) {
                $('#modal-disclaimer').modal('show');
            }
        });

        function closeDisclaimer() {
            if (sessionStorage) {
                sessionStorage.setItem('disclaimer', '1');
            }
            $('#modal-disclaimer').modal('hide');
        }

        document.addEventListener('livewire:load', function (e) {
            window.livewire.on('showAlert', (data) => {
                Swal.fire({
                    icon: 'success',
                    title: 'Success',
                    text: data.msg,
                    timer: 2000,
                    showCancelButton: false,
                    showConfirmButton: false
                })
            });

            window.livewire.on('showAlertError', (data) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: data.msg,
                    timer: 2000,
                    showCancelButton: false,
                    showConfirmButton: false
                })
            });

            window.livewire.on('showAlertInfo', (data) => {
                Swal.fire({
                    icon: 'info',
                    html: data.msg,
                    showCancelButton: false,
                    confirmButtonColor: '#1aa053',
                    confirmButtonText: 'OK'
                })
            });

            window.livewire.on('showAlertWarning', (data) => {
                Swal.fire({
                    icon: 'warning',
                    text: data.msg,
                    showCancelButton: false,
                    confirmButtonText: 'OK'
                })
            });

            window.livewire.on('showAlertSuccess', (data) => {
                Swal.fire({
                    icon: 'success',
                    title: 'Success',
                    html: data.msg,
                    showCancelButton: false,
                    confirmButtonColor: '#1aa053',
                    confirmButtonText: 'OK'
                })
            });

            window.livewire.on('showAlertWarningPartnership', (data) => {
                Swal.fire({
                    icon: 'warning',
                    title: 'Warning',
                    html: data.msg,
                    showCancelButton: false,
                    confirmButtonColor: '#f16a1b',
                    confirmButtonText: 'OK'
                })
            });
        })

        function gotoTraderValidationPage() {
            window.location.href = "{{ route('admin.tradervalidation') }}"
        }

        function copyText(id) {
            const cb = navigator.clipboard;
            const inputEl = document.getElementById(id);
            cb.writeText(inputEl.value).then(() => 
                Swal.fire({
                    text: "Copied!",
                    timer: 1000,
                    showCancelButton: false,
                    showConfirmButton: false
                })
            );
        }
    </script>
    @livewireScripts
</body>

</html>

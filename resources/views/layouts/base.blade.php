<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'TP 99') }}</title>

    <!-- Fonts -->
    {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap"> --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
        integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">

    <!-- Styles -->
    {{-- <link rel="stylesheet" href="{{ mix('css/app.css') }}"> --}}
    <link rel="stylesheet" href="{{asset('assets/css/login.css')}}">
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}" />

    <!-- Library / Plugin Css Build -->
    <link rel="stylesheet" href="{{asset('assets/css/core/libs.min.css')}}" />

    <!-- Hope Ui Design System Css -->
    <link rel="stylesheet" href="{{asset('assets/css/hope-ui.min.css?v=1.1.0')}}" />

    <!-- Custom Css -->
    <link rel="stylesheet" href="{{asset('assets/css/custom.min.css?v=1.1.0')}}" />

    <!-- Dark Css -->
    <link rel="stylesheet" href="{{ asset('assets/css/dark.min.css') }}" />

    <!-- Bootstrap Select -->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/1.1.1/css/bootstrap-multiselect.css" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css">

    <!-- RTL Css -->
    {{-- <link rel="stylesheet" href="{{asset('assets/css/rtl.min.css')}}" /> --}}
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> --}}

    <style>
        /* START overriding CSS for collapsible nav-item */
        .sidebar-default .navbar-nav .nav-item .nav-link:not(.disabled):hover:not(.active):not([aria-expanded=true]) {
            background-color: rgba(26, 160, 83, 0.1);
            color: #1aa053;
        }

        .sidebar-default .navbar-nav .nav-item .nav-link:not(.disabled).active,
        .sidebar-default .navbar-nav .nav-item .nav-link:not(.disabled)[aria-expanded=true] {
            background-color: #1aa053;
            color: #fff;
            -webkit-box-shadow: 0 10px 20px -10px rgb(17 38 146 / 40%);
            box-shadow: 0 10px 20px -10px rgb(17 38 146 / 40%);
        }

        /* END overriding CSS for collapsible nav-item */

        /* START Form divider */
        .text-divider {
            margin: 2em 0;
            line-height: 0;
            text-align: center;
        }

        .text-divider span {
            background-color: #fff;
            padding: 1em;
        }

        .text-divider:before {
            content: " ";
            display: block;
            border-top: 2px solid #e3e3e3;
            border-bottom: 2px solid #f7f7f7;
        }

        /* END Form divider */

        /* START Custom pagination color */
        .page-item.active .page-link {
            background-color: #1aa053;
            border-color: #1aa053;
        }

        .page-link {
            color: #1aa053;
        }

        /* END Custom pagination color */

        .nav-pills .nav-link.active,
        .nav-pills .show>.nav-link {
            color: #fff;
            background-color: #1aa053;
        }

        .nav-link {
            color: #1aa053;
        }

        .form-check-input {
            border-color: #1aa053;
        }

        .form-check-input:checked {
            border-color: #1aa053;
            color: #1aa053;
        }

        .table-sm>:not(caption)>*>* {
            padding: 0.25rem 1.5rem;
        }

        .sidebar+.main-content .iq-navbar-header .iq-container {
            padding: -webkit-calc(var(--bs-gutter-x, .75rem) * 2);
            padding: calc(var(--bs-gutter-x, .75rem) * 2);
        }

        .content-inner {
            padding: -webkit-calc(var(--bs-gutter-x, .75rem) * 2);
            padding: calc(var(--bs-gutter-x, .75rem) * 2);
        }

        .card-custom {
            margin-top: -1rem;
        }

        .card-slide {
            min-width: 253.2px !important;
        }

        .row {
            --bs-gutter-x: 1rem;
        }

        .alert-custom {
            padding-left: 0.3rem;
            padding-right: 2.2rem;
            padding-top: 0.3rem;
            padding-bottom: 0.3rem;
            max-width: 100%;
            height: min-content;
            height: -moz-min-content;
            height: -webkit-min-content;
        }

        @media (max-width: 767.98px) {
            .conatiner-fluid.content-inner-custom.mt-n5 {
                margin-top: 0.8rem !important;
            }

            .row-custom {
                margin-bottom: 0.5rem !important;
            }
        }

        .p-1-custom {
            padding-left: 2.5rem !important;
            padding-right: 2.5rem !important;
        }

        .card-body {
            overflow: auto;
        }

        .footer {
            position: relative;
        }

        @media only screen and (max-width: 800px) {
            .d-md-block {
                display: block !important;
            }
        }

        @media only screen and (max-width: 280px) {
            .d-md-block {
                font-family: auto;
            }
        }

        .selectBox {
            position: relative;
        }

        .selectBox select {
            width: 100%;
            font-weight: bold;
        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }

        #checkboxes {
            display: none;
            border: 1px #dadada solid;
        }

        #checkboxes label {
            display: block;
            margin-left: 10%;
            margin-top: -9%
        }

        .profile::placeholder {
            color: #8a92a64d;
        }

    </style>

    @livewireStyles
    @stack('styles')
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
</head>

<body>
    <!-- loader Start -->
    <div id="loading">
        <div class="loader simple-loader">
            <div class="loader-body"></div>
        </div>
    </div>
    <!-- loader END -->

    <div class="align-items-center">
        {{ $slot }}
    </div>
    <!-- Footer Section Start -->
    </main>

    <!-- Library Bundle Script -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="{{asset('assets/js/core/libs.min.js')}}"></script>
    <script src="{{asset('assets/js/core/external.min.js')}}"></script>
    <script src="{{asset('assets/js/charts/dashboard.js')}}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="{{asset('assets/js/hope-ui.js')}}" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/1.1.1/js/bootstrap-multiselect.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js"></script>
    <script src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
    <script src="https://cdn.jsdelivr.net/npm/pikaday/pikaday.js"></script>

    @stack('scripts')
    <script>
        $(document).ready(function () {
            // Tooltip initialization
            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                return new bootstrap.Tooltip(tooltipTriggerEl)
            })
            // End tooltip initialization

            $(".modal").on("hidden.bs.modal", function () {
                tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                    return new bootstrap.Tooltip(tooltipTriggerEl)
                })
            })

            $(".modal").on("shown.bs.modal", function () {
                tooltipList.forEach(element => {
                    element.hide()
                })
            })
        });

        document.addEventListener('livewire:load', function (e) {
            window.livewire.on('showAlert', (data) => {
                Swal.fire({
                    icon: 'success',
                    title: 'Success',
                    text: data.msg,
                    timer: 2000,
                    showCancelButton: false,
                    showConfirmButton: false
                })
            });

            window.livewire.on('showAlertError', (data) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: data.msg,
                    timer: 2000,
                    showCancelButton: false,
                    showConfirmButton: false
                })
            });

            window.livewire.on('showAlertInfo', (data) => {
                Swal.fire({
                    icon: 'info',
                    html: data.msg,
                    showCancelButton: false,
                    confirmButtonColor: '#1aa053',
                    confirmButtonText: 'OK'
                })
            });

            window.livewire.on('showAlertWarning', (data) => {
                Swal.fire({
                    icon: 'warning',
                    text: data.msg,
                    showCancelButton: false,
                    confirmButtonText: 'OK'
                })
            });

            window.livewire.on('showAlertSuccess', (data) => {
                Swal.fire({
                    icon: 'success',
                    title: 'Success',
                    html: data.msg,
                    showCancelButton: false,
                    confirmButtonColor: '#1aa053',
                    confirmButtonText: 'OK'
                })
            });
        })

        function gotoTraderValidationPage() {
            window.location.href = "{{ route('admin.tradervalidation') }}"
        }

        function copyText(id) {
            const cb = navigator.clipboard;
            const inputEl = document.getElementById(id);
            cb.writeText(inputEl.value).then(() =>
                Swal.fire({
                    text: "Copied!",
                    timer: 1000,
                    showCancelButton: false,
                    showConfirmButton: false
                })
            );
        }

    </script>
    @livewireScripts
</body>

</html>

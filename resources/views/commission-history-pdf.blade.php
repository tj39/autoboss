<!DOCTYPE html>
<html>
<head>
    <title>Commission History</title>
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous"> --}}
    <style>
        * {
            font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        }

        .row {
            margin-right: -15px;
            margin-left: -15px;
        }

        .col,
        .col-1,
        .col-10,
        .col-11,
        .col-12,
        .col-2,
        .col-3,
        .col-4,
        .col-5,
        .col-6,
        .col-7,
        .col-8,
        .col-9,
        .col-auto,
        .col-lg,
        .col-lg-1,
        .col-lg-10,
        .col-lg-11,
        .col-lg-12,
        .col-lg-2,
        .col-lg-3,
        .col-lg-4,
        .col-lg-5,
        .col-lg-6,
        .col-lg-7,
        .col-lg-8,
        .col-lg-9,
        .col-lg-auto,
        .col-md,
        .col-md-1,
        .col-md-10,
        .col-md-11,
        .col-md-12,
        .col-md-2,
        .col-md-3,
        .col-md-4,
        .col-md-5,
        .col-md-6,
        .col-md-7,
        .col-md-8,
        .col-md-9,
        .col-md-auto,
        .col-sm,
        .col-sm-1,
        .col-sm-10,
        .col-sm-11,
        .col-sm-12,
        .col-sm-2,
        .col-sm-3,
        .col-sm-4,
        .col-sm-5,
        .col-sm-6,
        .col-sm-7,
        .col-sm-8,
        .col-sm-9,
        .col-sm-auto,
        .col-xl,
        .col-xl-1,
        .col-xl-10,
        .col-xl-11,
        .col-xl-12,
        .col-xl-2,
        .col-xl-3,
        .col-xl-4,
        .col-xl-5,
        .col-xl-6,
        .col-xl-7,
        .col-xl-8,
        .col-xl-9,
        .col-xl-auto {
            position: relative;
            width: 100%;
            padding-right: 15px;
            padding-left: 15px
        }

        .col {
            -ms-flex-preferred-size: 0;
            flex-basis: 0;
            -ms-flex-positive: 1;
            flex-grow: 1;
            max-width: 100%
        }

        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 1rem;
        }

        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #eceeef;
        }

        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #eceeef;
        }

        .table tbody+tbody {
            border-top: 2px solid #eceeef;
        }

        .table .table {
            background-color: #fff;
        }

        .table-sm th,
        .table-sm td {
            padding: 0.3rem;
        }

        .table-bordered {
            border: 1px solid #eceeef;
        }

        .table-bordered th,
        .table-bordered td {
            border: 1px solid #eceeef;
        }

        .table-bordered thead th,
        .table-bordered thead td {
            border-bottom-width: 2px;
        }

        .table-striped tbody tr:nth-of-type(odd) {
            background-color: rgba(0, 0, 0, 0.05);
        }

        .table-hover tbody tr:hover {
            background-color: rgba(0, 0, 0, 0.075);
        }

        .table-active,
        .table-active>th,
        .table-active>td {
            background-color: rgba(0, 0, 0, 0.075);
        }

        .table-hover .table-active:hover {
            background-color: rgba(0, 0, 0, 0.075);
        }

        .table-hover .table-active:hover>td,
        .table-hover .table-active:hover>th {
            background-color: rgba(0, 0, 0, 0.075);
        }

        .table-success,
        .table-success>th,
        .table-success>td {
            background-color: #dff0d8;
        }

        .table-hover .table-success:hover {
            background-color: #d0e9c6;
        }

        .table-hover .table-success:hover>td,
        .table-hover .table-success:hover>th {
            background-color: #d0e9c6;
        }

        .table-info,
        .table-info>th,
        .table-info>td {
            background-color: #d9edf7;
        }

        .table-hover .table-info:hover {
            background-color: #c4e3f3;
        }

        .table-hover .table-info:hover>td,
        .table-hover .table-info:hover>th {
            background-color: #c4e3f3;
        }

        .table-warning,
        .table-warning>th,
        .table-warning>td {
            background-color: #fcf8e3;
        }

        .table-hover .table-warning:hover {
            background-color: #faf2cc;
        }

        .table-hover .table-warning:hover>td,
        .table-hover .table-warning:hover>th {
            background-color: #faf2cc;
        }

        .table-danger,
        .table-danger>th,
        .table-danger>td {
            background-color: #f2dede;
        }

        .table-hover .table-danger:hover {
            background-color: #ebcccc;
        }

        .table-hover .table-danger:hover>td,
        .table-hover .table-danger:hover>th {
            background-color: #ebcccc;
        }

        .thead-inverse th {
            color: #fff;
            background-color: #292b2c;
        }

        .thead-default th {
            color: #464a4c;
            background-color: #eceeef;
        }

        .table-inverse {
            color: #fff;
            background-color: #292b2c;
        }

        .table-inverse th,
        .table-inverse td,
        .table-inverse thead th {
            border-color: #fff;
        }

        .table-inverse.table-bordered {
            border: 0;
        }

        .table-responsive {
            display: block;
            width: 100%;
            overflow-x: auto;
            -ms-overflow-style: -ms-autohiding-scrollbar;
        }

        .table-responsive.table-bordered {
            border: 0;
        }
        .page_break {
            page-break-before: always;
        }

        .left {
            float: left;
            width: 90%;
        }
        .right {
            float: right;
            width: 1%;
        }
    </style>
</head>
<body>
    <div class="container container-fluid">
        <div class="row">
            <div class="col-md-12">
                <center>
                    @if ($table == "profit")
                    <h5>SHARING PROFIT SUMMARY</h5>
                    @else
                    <h5>REBATE SUMMARY</h5>
                    @endif
                </center>
                <br>
                <br>
                <ul class="list-group" style="list-style-type:none;">
                    <li
                        class="list-group-item border-0 border-bottom-1 d-flex justify-content-between align-items-center">
                        Transfer Proof : <span style="float:right; margin-right: 15% !important;">{{ $summary->transfer_proof }}</span>
                    </li>
                    <li
                        class="list-group-item border-0 border-bottom-1 d-flex justify-content-between align-items-center">
                        Code : <span style="float:right; margin-right: -12.5% !important;">{{ $summary->code }}</span>
                    </li>
                    <li
                        class="list-group-item border-0 border-bottom-1 d-flex justify-content-between align-items-center">
                        Commission (USD) <span style="float:right; margin-right: 15% !important;">{{ $summary->commission_usd }}</span>
                    </li>
                    <hr style="width: 90%; float: left;">
                    <br>
                    <li
                        class="list-group-item border-0 border-bottom-1 d-flex justify-content-between align-items-center">
                        Commission (IDR) <span style="float:right; margin-right: 15% !important;">{{ number_format($summary->commission_gross,0,'.',',') }}</span>
                    </li>
                    <li
                        class="list-group-item border-0 border-bottom-1 d-flex justify-content-between align-items-center">
                        PPH (IDR) <span style="float:right; margin-right: -5.5% !important;">{{ number_format($summary->ppn,0,'.',',') }}</span>
                    </li>
                    <hr class="left">
                    <hr class="right">
                    <br>
                    <li
                        class="list-group-item border-0 border-bottom-1 d-flex justify-content-between align-items-center">
                        Commission Netto (IDR) <span style="float:right; margin-right: 15% !important;">{{ number_format($summary->commission_net,0,'.',',') }}</span>
                    </li>
                </ul>
                <div class="page_break"></div>
                <center>
                    @if ($table == "profit")
                    <h5>SHARING PROFIT DETAIL</h5>
                    @else
                    <h5>REBATE DETAIL</h5>
                    @endif
                </center>
                <div class="bd-example table-responsive">
                    @if ($table == "profit")
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr class="table-active">
                                <th scope="col">Client ID</th>
                                <th scope="col">Investment ID</th>
                                <th scope="col">Generation</th>
                                <th scope="col">Percentage</th>
                                <th scope="col">Real Commission</th>
                                <th scope="col">Dividend Commission</th>
                                <th scope="col">Generation Commission</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($details as $key => $sprofit)
                            <tr>
                                <td>{{ $sprofit->userId->firstname .' '.  $sprofit->userId->lastname . ' - ' . $sprofit->user_clientid }}</td>
                                <td>{{ $sprofit->investment_id }}</td>
                                <td>{{ $sprofit->leader_generation }}</td>
                                <td>{{ $sprofit->percentage }}</td>
                                <td>{{ $sprofit->real_commission }}</td>
                                <td>{{ $sprofit->div_commission }}</td>
                                <td>{{ $sprofit->commission_usd }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @elseif ($table == "rebate")
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr class="table-active">
                                <th scope="col">Reward Date</th>
                                <th scope="col">Client ID</th>
                                <th scope="col">Your Rank</th>
                                <th scope="col">Downline Rank</th>
                                <th scope="col">Max Commission</th>
                                <th scope="col">Volume (lots)</th>
                                <th scope="col">Commission (USD)</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($details as $key => $rebate)
                            <tr>
                                <td>{{ $rebate->reward_date }}</td>
                                <td>{{ $rebate->userId->firstname .' '. $rebate->userId->lastname . ' - '. $rebate->user_clientid }}</td>
                                <td>{{ $rebate->leaderRank->description }}</td>
                                @if ($rebate->user_rank == "-")
                                    <td>{{ $rebate->user_rank }}</td>
                                @else
                                    <td>{{ $rebate->userRank->description }}</td>
                                @endif
                                <td>{{ $rebate->max_commission }}</td>
                                <td>{{ $rebate->volume_lots }}</td>
                                <td>{{ $rebate->commission_usd }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</body>
</html>
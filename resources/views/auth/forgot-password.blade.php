<x-guest-layout>
    <div class="vertical-center">
        <!-- 
        ^--- Added class  -->
        <div class="container" style="margin-top: -3%">
            <div class="row justify-content-center">
                <div class="col-md-5 justify-content-center">
                    
                    <div class="card" style="margin-top: 3%">
                        <div class="card-body">
                            <img src="{{ asset('assets/images/logo.png') }}" style="width: 30%; margin-left: auto; margin-right: auto;">
                            <form method="POST" action="{{ route('password.email') }}">
                                @csrf
                                <div class="text-center" style="font-size: 13px">
                                    <h3>Forgot Password</h3>
                                    <span>Please enter your email for us to send a</span>
                                    <br>
                                    <span>password reset message.</span>
                                </div>
                                <div class="block">
                                    <x-jet-label for="email" value="{{ __('Email') }}" />
                                    <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
                                    @if (session('status'))
                                    @if (session('status') == "Email not found" || session('status') == "A few seconds ago an e-mail sent with further instructions, please check your email to reset password")
                                    <div class="mb-4 font-medium text-sm" style="color: red">
                                        {{ session('status') }}
                                    </div>
                                    @elseif (session('status') == "We have emailed your password reset link!")
                                    <div class="mb-4 font-medium text-sm" style="color: green">
                                        {{ session('status') }}
                                    </div>
                                    @endif
                                @endif
                                </div>
                    
                                <div class="container" style="margin-top: 2%;text-align-last: center;">
                                    <div class="row justify-content-center">
                                        <div class="col-md-auto">
                                            <button type="submit" class="btn btn-primary ml-4">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="text-center" style="color:white">
                        <span>Already have an account?</span>
                        <a href="{{ route('login') }}" id="show-signup" class="link" style="color: moccasin;">Sign In</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        This site is protected by reCAPTCHA and the Google
        <a style="color: moccasin;" href="https://policies.google.com/privacy">Privacy Policy</a> and
        <a style="color: moccasin;" href="https://policies.google.com/terms">Terms of Service</a> apply.
    </div>
</x-guest-layout>
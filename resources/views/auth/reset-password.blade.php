<x-guest-layout>
    <div class="vertical-center">
        <!-- 
        ^--- Added class  -->
        <div class="container" style="margin-top: -3%">
            <div class="row justify-content-center">
                <div class="col-md-5 justify-content-center">                    
                    <div class="card" style="margin-top: 3%">
                        <div class="card-body">
                            <img src="{{ asset('assets/images/logo.png') }}" style="width: 30%; margin-left: auto; margin-right: auto;">
                            <form method="POST" action="{{ route('password.update') }}">
                                @csrf
                                <input type="hidden" name="token" value="{{ $request->route('token') }}">
                                <input type="hidden" name = "email" value="{{ $request->email }}">

                                <div class="mt-4">
                                    <x-jet-label for="password" value="{{ __('Password') }}" />
                                    <div class="input-group mb-3" style="flex-flow: nowrap;align-items: flex-end;">
                                        <x-jet-input class="block mt-1 w-full" type="password" name="password" id="password" required autocomplete="new-password" />
                                        <div class="input-group-prepend">
                                            <button class="btn rounded-end btn-outline-success" type="button">
                                                <h6 toggle="#password" class="fa fa-eye fa-lg show-hide"></h6>
                                            </button>
                                        </div>
                                    </div>
                                    <x-jet-validation-errors class="mb-4" />
                                </div>

                                <div class="mt-4">
                                    <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                                    <div class="input-group mb-3" style="flex-flow: nowrap;align-items: flex-end;">
                                        <x-jet-input class="block mt-1 w-full" type="password" name="password_confirmation" id="password_confirmation" required autocomplete="new-password" />
                                        <div class="input-group-prepend">
                                            <button class="btn rounded-end btn-outline-success" type="button">
                                                <h6 toggle="#password_confirmation" class="fa fa-eye fa-lg show-hide"></h6>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                    
                                <div class="container" style="margin-top: 2%;text-align-last: center;">
                                    <div class="row justify-content-center">
                                        <div class="col-md-auto">
                                            <button type="submit" class="btn btn-primary ml-4">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
        <script>
            $(".show-hide").click(function () {
    
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });
        </script>
    </div>
    <div class="footer">
        This site is protected by reCAPTCHA and the Google
        <a style="color: moccasin;" href="https://policies.google.com/privacy">Privacy Policy</a> and
        <a style="color: moccasin;" href="https://policies.google.com/terms">Terms of Service</a> apply.
    </div>
</x-guest-layout>

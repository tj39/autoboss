<x-guest-layout>
    <div class="vertical-center">
        <!-- 
        ^--- Added class  -->
        <div class="container" style="margin-top: -3%">
            <div class="row justify-content-center">
                <div class="col-md-5 justify-content-center">
                   
                    <div class="card" style="margin-top: 3%;">
                        <div class="card-body">
                             <img src="{{ asset('assets/images/logo2.png') }}" style="width: 30%; margin-left: auto; margin-right: auto; margin-bottom: 2%;">
                            @if (session('status'))
                            <div class="mb-4 font-medium text-sm" style="color: green">
                                {{ session('status') }}
                            </div>
                            @endif
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="text-center" style="font-size: 13px">
                                    <!--<h3>SIGN IN</h3>-->
                                    <span>Please enter your email/username</span>
                                    <br>
                                    <span>and password to log in your account.</span>
                                </div>
                                <div class="w-75 p-3 align-center" style="margin-left: auto; margin-right: auto; margin-top:4%; padding: 0.3rem !important;">
                                    <x-jet-label value="{{ __('Email/Username') }}" />
                                    @if (session('identity'))
                                    <x-jet-input class="block mt-1 w-full" type="text" name="identity"
                                        :value="old('email')" required autofocus value="{{ session('identity') }}"/>
                                    @else
                                    <x-jet-input class="block mt-1 w-full" type="text" name="identity"
                                        :value="old('email')" required autofocus />
                                    @endif
                                    @if (session('email'))
                                    <div class="mb-4 font-medium text-sm" style="color: red">
                                        {{ session('email') }}
                                    </div>
                                    @endif
                                </div>

                                <div class="w-75 p-3 align-center" style="margin-left: auto; margin-right: auto; padding: 0.3rem !important;">
                                    <x-jet-label value="{{ __('Password') }}" />
                                    <div class="input-group mb-3" style="flex-flow: nowrap;align-items: flex-end;">
                                        <x-jet-input class="block mt-1 w-full" id="password-field" type="password" name="password" required autocomplete="current-password" />
                                        <button class="btn rounded-end btn-outline-success" type="button">
                                            <h6 toggle="#password-field" class="fa fa-eye fa-lg show-hide"></h6>
                                        </button>
                                    </div>
                                     @if (session('password'))
                                    <div class="mb-4 font-medium text-sm" style="color: red">
                                        {{ session('password') }}
                                    </div>
                                    @endif
                                    <div class="text-right" style="margin-top: -0.8rem">
                                        <a class="underline text-sm text-gray-600 hover:text-gray-900"
                                            href="{{ route('password.request') }}">
                                            {{ __('Forgot your password?') }}
                                        </a>
                                    </div>
                                </div>
                                <div class="container" style="margin-top: 1rem">
                                    <div class="d-flex justify-content-center">
                                        <div class="col-md-auto">
                                            <button type="submit" class="btn btn-primary">Sign In</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="text-center" style="color:white">
                        <span>Don't have an account?</span>
                        <a href="{{ route('register') }}" id="show-signup" class="link" style="color: moccasin;">Sign Up</a>
                    </div>
                </div>
            </div>
        </div>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
        <script>
            $(".show-hide").click(function () {
    
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });
        </script>
    </div>
    <div class="footer">
        This site is protected by reCAPTCHA and the Google
        <a style="color: moccasin;" href="https://policies.google.com/privacy">Privacy Policy</a> and
        <a style="color: moccasin;" href="https://policies.google.com/terms">Terms of Service</a> apply.
    </div>
</x-guest-layout>
